[status,SHARE]=system('echo $HOME');
% Strip the trailing CR and append /matlab
SHARE=[SHARE(1:end-1),'/matlab'];
path(path,SHARE)
startup_local
rmpath(SHARE);
clear status SHARE

% Set the default paper type to A4
set(0, 'DefaultFigurePaperType','A4');
%set(0, 'DefaultFigurePaperUnits','cm'); % Causes an error
set(0, 'DefaultTextFontName','Times ');
