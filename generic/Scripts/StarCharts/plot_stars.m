load stars
%load HaleBopp_ephem


stars1=find(mag_stars<-1);
stars2=find((mag_stars>=-1)&(mag_stars<0));
stars3=find((mag_stars>=0)&(mag_stars<1));
stars4=find((mag_stars>=1)&(mag_stars<2));
stars5=find((mag_stars>=2)&(mag_stars<3));
stars6=find((mag_stars>=3)&(mag_stars<4));
stars7=find((mag_stars>=4)&(mag_stars<5));
stars8=find((mag_stars>=5)&(mag_stars<6));
stars9=find((mag_stars>=6));

h1=plot(ra_stars(stars1),dec_stars(stars1),'.');
set(h1,'MarkerSize',21);
hold
h2=plot(ra_stars(stars2),dec_stars(stars2),'.');
set(h2,'MarkerSize',19);
h3=plot(ra_stars(stars3),dec_stars(stars3),'.');
set(h3,'MarkerSize',17);
h4=plot(ra_stars(stars4),dec_stars(stars4),'.');
set(h4,'MarkerSize',15);
h5=plot(ra_stars(stars5),dec_stars(stars5),'.');
set(h5,'MarkerSize',13);
h6=plot(ra_stars(stars6),dec_stars(stars6),'.');
set(h6,'MarkerSize',11);
h7=plot(ra_stars(stars7),dec_stars(stars7),'.');
set(h7,'MarkerSize',9);
h8=plot(ra_stars(stars8),dec_stars(stars8),'.');
set(h8,'MarkerSize',7);
h9=plot(ra_stars(stars9),dec_stars(stars9),'.');
set(h9,'MarkerSize',5);

comet6=find(mags>=6);
comet5=find((mags>=5)&(mags<6));
comet4=find((mags>=4)&(mags<5));
comet3=find((mags>=3)&(mags<4));
comet2=find((mags>=2)&(mags<3));
comet1=find((mags>=1)&(mags<2));
comet0=find((mags>=0)&(mags<1));
comet00=find(mags<0);

c1=plot(ra_hms(comet00),dec(comet00),'*');
set(c1,'MarkerSize',15);
c2=plot(ra_hms(comet0),dec(comet0),'*');
set(c2,'MarkerSize',13);
c3=plot(ra_hms(comet1),dec(comet1),'*');
set(c3,'MarkerSize',11);
c4=plot(ra_hms(comet2),dec(comet2),'*');
set(c4,'MarkerSize',9);
c5=plot(ra_hms(comet3),dec(comet3),'*');
set(c5,'MarkerSize',7);
c6=plot(ra_hms(comet4),dec(comet4),'*');
set(c6,'MarkerSize',5);
c7=plot(ra_hms(comet5),dec(comet5),'*');
set(c7,'MarkerSize',3);
c8=plot(ra_hms(comet6),dec(comet6),'*');
set(c8,'MarkerSize',2);

set(gca,'XDir','reverse')
