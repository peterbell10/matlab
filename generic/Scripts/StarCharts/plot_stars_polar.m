load comet.mat
load ephem.html
mags=ephem(:,16);
mask_dec_stars=dec_stars>50;
mask_dec=dec>50;


stars1=find(mask_dec_stars&(mag_stars<-1));
stars2=find(mask_dec_stars&(mag_stars>=-1)&(mag_stars<0));
stars3=find(mask_dec_stars&(mag_stars>=0)&(mag_stars<1));
stars4=find(mask_dec_stars&(mag_stars>=1)&(mag_stars<2));
stars5=find(mask_dec_stars&(mag_stars>=2)&(mag_stars<3));
stars6=find(mask_dec_stars&(mag_stars>=3)&(mag_stars<4));
stars7=find(mask_dec_stars&(mag_stars>=4)&(mag_stars<5));
stars8=find(mask_dec_stars&(mag_stars>=5)&(mag_stars<6));
stars9=find(mask_dec_stars&(mag_stars>=6));

h9=polar(2*pi/24*(24-ra_stars(stars9)),90-dec_stars(stars9),'.');
set(h9,'MarkerSize',5);
hold
h1=polar(2*pi/24*(24-ra_stars(stars1)),90-dec_stars(stars1),'.');
set(h1,'MarkerSize',21);
h2=polar(2*pi/24*(24-ra_stars(stars2)),90-dec_stars(stars2),'.');
set(h2,'MarkerSize',19);
h3=polar(2*pi/24*(24-ra_stars(stars3)),90-dec_stars(stars3),'.');
set(h3,'MarkerSize',17);
h4=polar(2*pi/24*(24-ra_stars(stars4)),90-dec_stars(stars4),'.');
set(h4,'MarkerSize',15);
h5=polar(2*pi/24*(24-ra_stars(stars5)),90-dec_stars(stars5),'.');
set(h5,'MarkerSize',13);
h6=polar(2*pi/24*(24-ra_stars(stars6)),90-dec_stars(stars6),'.');
set(h6,'MarkerSize',11);
h7=polar(2*pi/24*(24-ra_stars(stars7)),90-dec_stars(stars7),'.');
set(h7,'MarkerSize',9);
h8=polar(2*pi/24*(24-ra_stars(stars8)),90-dec_stars(stars8),'.');
set(h8,'MarkerSize',7);

comet6=find(mask_dec&(mags>=6));
comet5=find(mask_dec&(mags>=5)&(mags<6));
comet4=find(mask_dec&(mags>=4)&(mags<5));
comet3=find(mask_dec&(mags>=3)&(mags<4));
comet2=find(mask_dec&(mags>=2)&(mags<3));
comet1=find(mask_dec&(mags>=1)&(mags<2));
comet0=find(mask_dec&(mags>=0)&(mags<1));
comet00=find(mask_dec&(mags<0));

c1=polar(2*pi/24*(24-ra_hms(comet00)),90-dec(comet00),'*');
set(c1,'MarkerSize',15);
c2=polar(2*pi/24*(24-ra_hms(comet0)),90-dec(comet0),'*');
set(c2,'MarkerSize',13);
c3=polar(2*pi/24*(24-ra_hms(comet1)),90-dec(comet1),'*');
set(c3,'MarkerSize',11);
c4=polar(2*pi/24*(24-ra_hms(comet2)),90-dec(comet2),'*');
set(c4,'MarkerSize',9);
c5=polar(2*pi/24*(24-ra_hms(comet3)),90-dec(comet3),'*');
set(c5,'MarkerSize',7);
c6=polar(2*pi/24*(24-ra_hms(comet4)),90-dec(comet4),'*');
set(c6,'MarkerSize',5);
c7=polar(2*pi/24*(24-ra_hms(comet5)),90-dec(comet5),'*');
set(c7,'MarkerSize',3);
c8=polar(2*pi/24*(24-ra_hms(comet6)),90-dec(comet6),'*');
set(c8,'MarkerSize',2);
