function [x,y,width,height]=getbox(a)
% getbox: Interactively get the coordinates of a box on a figure.
%
% [x,y,width,height]=getbox(a)
%
% ARGUMETNS
%  a	A dummy variable to allow running as a function.
%
% RETURNS
%  x		The lower x coordinate
%  y		The lower y coordinate
%  width	The width of the box (x-dir)
%  height	The height of the box (y-dir)
%
% SEE ALSO
%  rbbox, waitforbuttonpress

% AUTHOR: Eric Tittley
%
% HISTORY
%  011017 Mature version
%
% COMPATIBILITY: Matlab & Octave

k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');    % button down detected
finalRect = rbbox;                   % return figure units
point2 = get(gca,'CurrentPoint');    % button up detected
point1 = point1(1,1:2);              % extract x and y
point2 = point2(1,1:2);
p1 = min(point1,point2);             % calculate locations
offset = abs(point1-point2);         % and dimensions
x=p1(1);
y=p1(2);
width=offset(1);
height=offset(2);
