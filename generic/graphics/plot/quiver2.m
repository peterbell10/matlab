% quiver2	Quiver plot using irregular data points.
%
% handle=quiver2(x,y,dx,dy,scale)
%
% ARGUMENTS
%  x,y   Vectors (length N) of coordinates of the positions to plot the quivers
%  dx,dy Vectors (length N) of lengths of the X and Y components of the quivers
%  scale [OPTIONAL] A factor to scale the lengths of the quivers [default=1]
%
% RETURNS
%  handle to the figure
%
% SEE ALSO
%  quiver (works on an regular grid)

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  080812 First version
%  110307 Added comments.

function hand=quiver2(x,y,dx,dy,scale)

% Set default scale
if nargin==4, scale=1; end

n=length(x);

handle=plot(x,y,'o');

% Can't we vectorize this?
for i=1:n
 line([x(i),x(i)+scale*dx(i)],[y(i),y(i)+scale*dy(i)])
end
