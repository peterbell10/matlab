function h=ellipse(a,b,x0,y0,theta,C,Nb)
% h=ellipse(a,b,x0,y0,theta,[C,Nb]);
% ELLIPSE adds ELLIPSEs to the current plot
%
% ELLIPSE(a,b,x0,y0,theta) adds an ELLIPSE with semi-major axis a
% and semi-minor axis b centered at point x0,y0. 
% If a and b are vectors of length L and x0,y0 scalars, L ELLIPSEs
% are added at point x0,y0.
% If a and b are scalara and x0,y0 vectors of length M, M ELLIPSEs are added at
% the points x0,y0.
% If a, b, x0, and y0 are vectors of the same length L=M, M ELLIPSEs are added.
% If a and b are vectors of length L and x0,y0 vectors of
% length M~=L, L*M ELLIPSEs are added, at each point x0,y0, L ELLIPSEs of
% radius r.
%
% ELLIPSE(a,b,x0,y0,theta,C)
% adds ELLIPSEs of color C. C may be a string ('r','b',...) or the RGB value. 
% If no color is specified, it makes automatic use of the colors specified by 
% the axes ColorOrder property. For several ELLIPSEs C may be a vector.
%
% ELLIPSE(a,b,x0,y0,theta,C,Nb), Nb specifies the number of points used to draw
% the  ELLIPSE. The default value is 300. Nb may be used for each ELLIPSE
% individually.
%
% h=ELLIPSE(...) returns the handles to the ELLIPSEs.
%
% theta is in units of degrees.
%
% See: CIRCLE

% Written by Eric Tittley (00 02 17) UMBC
% Based on circle.m, written by Peter Blattner, Institute of Microtechnology
% University of Neuchatel, Switzerland, blattner@imt.unine.ch

% HISTORY
%  01-08-23 Fixed bug that had the ellipse rotating about the centre of
%	the figure, not the centre of the ellipse.
%	Fixed bug in syntax comments: routine presumes semi-major axes but
%	the comments said major axes.

% Check the number of input arguments 
if nargin<5,
  error('h=ellipse(a,b,x0,y0,theta,C,Nb)');
end;

if nargin<6,
  C=[];
end

if nargin<7,
  Nb=[];
end

% set up the default values
if isempty(a),a=1;end;
if isempty(b),b=1;end;
if isempty(x0),x0=0;end;
if isempty(y0),y0=0;end;
if isempty(theta),theta=0;end;
if isempty(C),C=get(gca,'colororder');end;
if isempty(Nb),Nb=300;end;

% work on the variable sizes
% Make the variables columnar
x0=x0(:);
y0=y0(:);
a=a(:);
b=b(:);
theta(:);
Nb=Nb(:);

if isstr(C),C=C(:);end;

if length(x0)~=length(y0),
  error('length(x0)~=length(y0)');
end;

if length(a)~=length(b),
  error('length(a)~=length(b)');
end;

if( length(theta)~=length(a) | length(theta)~=length(x0) )
 error('the number of thetas must = number of radii or positions');
end

L=length(a);
M=length(x0);
T=length(theta);

% there are 8 cases but only 4 possible number of ellipses to draw
% 1 position,  1 theta,  1 radius  => Case 1 => 1 ellipse
% L positions, 1 theta,  1 radius  => Case 2 => L ellipses
% L positions, L thetas, 1 radius  => Case 3 => L ellipses
% L positions, L thetas, L radii   => Case 4 => L ellipses   Degenerate
% 1 position,  1 theta,  M radii   => Case 5 => M ellipses
% 1 position,  M theta,  M radii   => Case 6 => M ellipses
% L positions, L thetas, M radii   => Case 7 => L*M ellipses
% L positions, M thetas, M radii   => Case 8 => L*M ellipses

if    (L==1 & T==1 & M==1), state=1;
elseif(L>1  & T==1 & M==1), state=2;
elseif(L>1  & T==L & M==1), state=3;
elseif(L==1 & T==1 & M>1 ), state=4;
elseif(L==1 & T==M & M>1 ), state=5;
elseif(L>1  & T==L & M==L), state=6;
elseif(L>1  & T==L & M>1 & M~=L), state=7;
elseif(L>1  & T==M & M>1 & M~=L), state=8;
else, error('How did I get to this state?')
end

% how many rings are plotted
switch state
 case 1
  maxk=1;
 case 2
  maxk=L;
 case 3
  maxk=L;
 case 4
  maxk=L;
 case 5
  maxk=M;
 case 6
  maxk=M;
 case 7
  maxk=L*M;
 case 8
  maxk=L*M;
 otherwise
  error('How did I get to this state?')
end

% drawing loop

for k=1:maxk

 if(state==1 | state==2 | state==3)
  a_rad=a;
  b_rad=b;
 elseif(state==4 |state==5 | state==6)
  a_rad=a(k);
  b_rad=b(k);
 else
  indx=fix((k-1)/L)+1;
  a_rad=a(indx);
  b_rad=b(indx);
 end

 if(state==1 | state==2 | state==4)
  theta_=theta;
 elseif(state==3 | state==5 | state==6)
  theta_=theta(k);
 elseif(state==7)
  theta_=theta(rem(k-1,L)+1);
 else
  theta_=theta(fix((k-1)/L)+1);
 end

 if(state==1 | state==5 | state==6)
  xpos=x0;
  ypos=y0;
 elseif(state==2 | state==3 | state==4)
  xpos=x0(k);
  ypos=y0(k);
 else
  indx=rem(k-1,L)+1;
  xpos=x0(indx);
  ypos=y0(indx);
 end

 the=linspace(0,2*pi,Nb(rem(k-1,size(Nb,1))+1,:)+1);
 h(k)=line(a_rad*cos(the)+xpos,b_rad*sin(the)+ypos);
 rotate(h(k),[0 0 1],theta_,[xpos,ypos,0]);
 set(h(k),'color',C(rem(k-1,size(C,1))+1,:));

end
