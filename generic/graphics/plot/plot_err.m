function handle=plot_err(x,y,dx,dy,colour,symbol,logflag)
% Plot symbols with error bars in the x and y-dir.
%
% handle=plot_err(x,y,dx,dy,colour,symbol,logflag)
%
% x, y		Vectors of the data points.
% dy		Vector of errors for y.
% dx		Vector of errors for x.
% colour	Character for the colour.
% symbol	The symbol to use for the data point.
% logflag	(optional) If the x-axis is going to be log scaled, then
%		set this flag to 1 to produce appropriate wings.
%		Note that the user still has to set the y-scale to log if
%		so desired.
%
% SEE ALSO
%  errorbar

% AUTHOR: Eric Tittley
%
% HISTORY
%  Renamed from ploterr to plot_err since the nnet toolbox has
%  an (obsolete) function called ploterr.
%
%  01 01 19: Made notification that this duplicates the MATLAB
%	function errorbar.
%  02 03 29 Added functionality to plot in log scale.
%  03 01 28 Added error bars to x as well.
%	Removed warning message about MATLAB errorbar function.
%  04 08 30 Minor modification to help message.


if(nargin==6), logflag=0; end


N=length(x);
dy_lo=y-(max(y)-min(y))/(8*N);
dy_hi=y+(max(y)-min(y))/(8*N);
if(logflag==0)
 dx_lo=x-(max(x)-min(x))/(8*N);
 dx_hi=x+(max(x)-min(x))/(8*N);
else
 dx_t=(max(log(x))-min(log(x)))/(8*N); % In log scale
 dx_lo=exp(log(x)-dx_t);
 dx_hi=exp(log(x)+dx_t);
% dy_t=(max(log(y))-min(log(y)))/(8*N); % In log scale
% dy_lo=exp(log(y)-dy_t);
% dy_hi=exp(log(y)+dy_t);
end

handle=plot(x,y,[colour,symbol]);

if(size(dx,2)==2)
 dxl=dx(:,1);
 dxu=dx(:,2);
else
 dxl=-dx;
 dxu=dx;
end
if(size(dy,2)==2)
 dyl=dy(:,1);
 dyu=dy(:,2);
else
 dyl=-dy;
 dyu=dy;
end

for i=1:N
 if(dx(i)~=0)
  % X-axis error bars.
  l=line([x(i)+dxl(i) x(i)+dxu(i)],[y(i) y(i)]);
  set(l,'color',colour)
  l=line([x(i)+dxu(i) x(i)+dxu(i)],[dy_lo(i) dy_hi(i)]);
  set(l,'color',colour)
  l=line([x(i)+dxl(i) x(i)+dxl(i)],[dy_lo(i) dy_hi(i)]);
  set(l,'color',colour)
 end
 % Y-axis error bars.
 l=line([x(i) x(i)],[y(i)+dyl(i) y(i)+dyu(i)]);
 set(l,'color',colour)
 l=line([dx_lo(i) dx_hi(i)],[y(i)+dyu(i) y(i)+dyu(i)]);
 set(l,'color',colour)
 l=line([dx_lo(i) dx_hi(i)],[y(i)+dyl(i) y(i)+dyl(i)]);
 set(l,'color',colour)
end

if(logflag==1)
 set(gca,'xscale','log')
end
