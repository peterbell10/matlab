%  graphics/plot Plotting routines and tools
% 
%  circle	Adds a circle to a plot.
%
%  ellipse	Adds ellipses to the current plot.
%
%  PlotColourCoded  Plot x,y,z as a two dimensional plot with a color
%                   determined by z
%
%  PlotWithDensity  plot() which changes the point colour where points overlap
%
%  plotXYZ	Plots the points, (X,Y), with color scaled by Z.
% 
%  plot_err	Plots data points with error bars.
%
%  plotarrow	Plot an arrow on the current figure.
%
%  quiver2	Creates a quiver plot at using irregular data points.
