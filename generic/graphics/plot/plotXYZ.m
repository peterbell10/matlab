function pl=plotXYZ(X,Y,Z,string)
% syntax pl=plotXYZ(X,Y,Z,string)
%
% Plots the points, (X,Y), with color scaled by Z.
% string, is an optional argument that would be sent to plot(x,y,'.',string)
% command.
%
% pl is a vector of axis handles

if(nargin==3), string='''visible'',''on'''; end

map=colormap;
[m_map,n_map]=size(map);

N_Z=length(Z);

max_Z=max(Z);
min_Z=min(Z);
scale_Z=max_Z-min_Z;
colours=uint8((Z-min_Z)/scale_Z*(m_map-1)+1);

hold on
if(N_Z>m_map)
 pl=zeros(m_map,1);
 for i=1:m_map
  cmd=['pl(i)=plot(X(buffer),Y(buffer),''.'',''color'',map(i,:),',string,');'];
  buffer=find(colours==i);
  if(~isempty(buffer))
   eval(cmd)
  end
 end
else
 pl=zeros(N_Z,1);
 cmd=['pl(i)=plot(X(i),Y(i),''.'',''color'',map(colours(i),:),',string,');'];
 for i=1:N_Z
  eval(cmd)
 end
end
hold off
