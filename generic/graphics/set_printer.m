% set_printer: Configure the figure's postscript position and paper size.

Ver=sscanf(version,'%i.%i.%i');

if(~isoctave() | (isoctave() & Ver(1)>3 & Ver(2)>1) )
 set(gcf,'PaperUnits','centimeters')
 set(gcf,'PaperType','a4')
 set(gcf,'PaperPosition',[0.5 0.5 15 10])
else
end
