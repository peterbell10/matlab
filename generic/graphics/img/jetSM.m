function map=jetSM(nLevels)
% jetSM: a jet-like colourmap similar to what SM (SuperMongo) provides
%
% map=jetSM(nLevels)
%
% ARGUMENTS
%  nLevels   Number of colourmap levels
%
% RETURNS
%  map       A colourmap [nLevels,3]
%
% SEE ALSO
%  colourmap

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab and OCtave


nJetLevels = round(56/64 * nLevels);
nLevelsPad = (nLevels-nJetLevels);

map=jet(nJetLevels);

mapTop = zeros(nLevelsPad,3);
mapBottom = mapTop;

mapTop(:,3)=[0:nLevelsPad-1]/nLevelsPad*map(1,3);

map(end-nLevelsPad+1:end,1)=1;
map(end-nLevelsPad+1:end,2)=[1:nLevelsPad]/nLevelsPad;
map(end-nLevelsPad+1:end,3)=[1:nLevelsPad]/nLevelsPad;

map=[mapTop;map];
