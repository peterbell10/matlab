function Map=ColourVsIntensityMap(Map_in)

NIntensities=256;

% Colour Map needs to be 3xN
[N,M]=size(Map_in);
if( N ~= 3 )
 Map_in = Map_in';
end
M=size(Map_in,2);

% Normalise the map
%Norm=sum(Map_in);
% The sum is the logical choice, but it produces a map that looks wrong.
Norm=sqrt(sum(Map_in.^2));
Norm(Norm<=0) = Norm(Norm<=0) +1;
[NormArr,White] = meshgrid(Norm,[1 1 1]);
Map_in = Map_in./NormArr;

% Initialise
Map = zeros(NIntensities,M,3);

% Fill the first third
FirstThird=floor(NIntensities/3);
for i=1:FirstThird
 Scale = (i-1)/(FirstThird-1);
 ColourMap = Scale*Map_in;
 Map(i,:,:) = permute(ColourMap,[3,2,1]);
end

% Fill in the de-saturated map
Slope=(White-Map_in)/(NIntensities-FirstThird);
for i=FirstThird+1:NIntensities
 ColourMap = Map_in + (i-FirstThird)*Slope;
 Map(i,:,:) = permute(ColourMap,[3,2,1]);
end
