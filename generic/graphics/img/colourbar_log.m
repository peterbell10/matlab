% colourbar_log: Colourbar in log units
%
% cb=colourbar_log(lim);
%
% ARGUMENTS
%  lim          2-element vector [Zlo Zhigh]
%
% RETURNS
%  cb           Handle to the colourbar

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function cb=colourbar_log(lim)

logRangeList=[log10(lim(1)):1:log10(lim(2))];
NTicks=length(logRangeList);

for i=1:NTicks
 TL_new{i}=['10^{',int2str(logRangeList(i)),'}'];
end

CLim=get(gca,'CLim'); % The current colourbar will span CLim(1) to CLim(2)

XTick = [CLim(1):(CLim(2)-CLim(1))/(NTicks-1):CLim(2)];

cb=colorbar('location','eastoutside', ...
            'XTickLabel',TL_new', ...
	    'XTick',XTick);
