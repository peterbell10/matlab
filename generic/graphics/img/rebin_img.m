function img_out=rebin_img(img,factor)
% img_out=rebin_img(img,factor);
%
% Given an image, img, and a binning factor, factor,
% returns an image that is factor times smaller in each dimension
% and each pixel corresponds to factorxfactor pixels in the
% original.
%
% factor must be a common denominator of each dimension of the input
% image.

% HISTORY
%  00 11 20: First version

% Check to see that the arguments are compatible.
[m,n]=size(img);
if( rem(m,factor)~=0 | rem(n,factor)~=0 ) 
 error('factor must be a denominator of each dimension of the input image')
end

% Initiate the array for the new image
m_new=m/factor;
n_new=n/factor;
img_out=zeros(m_new,n_new);

% We will construct the new matrix row-wise
for i=1:m_new
 %collapse the factor rows into a single row
 row_sum=sum(img( (i-1)*factor+1:i*factor , :),1);
 % Now sum up the columns in groups of factor
 for j=1:factor
  img_out(i,:)=img_out(i,:)+row_sum(j:factor:end-factor+j);
 end
end
