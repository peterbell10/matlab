function map=colourmap(num,cmykflag);
if(nargin==1)
 cmykflag=0;
else
 if(cmykflag~=0)
  cmykflag=1;
 end
end

% Dark Matter colour map (black violet white blue black)
%C(1,:)=[ 0 0.25 0.5 1 1 0 0 ]; 
%C(2,:)=[ 0 0.0  0   0 1 0 0 ];
%C(3,:)=[ 0 0.25 0.5 1 1 1 0 ];

% white yellow red orange grey green blue violet black
if(cmykflag==0)
 C(1,:)=[1  1  1    1  .9  0.5  0  0  1  0 ];
 C(2,:)=[1  1  0.5  0  .5  0.5  1  0  0  0 ];
 C(3,:)=[1  0  0    0  .0  0.5  0  1  1  0 ];
else
 C(1,:)=[1  1  1   1  0.9  0.5    0   0    0    1   0 ];
 C(2,:)=[1  1  .5  0  0.5  0.5    1   1    0    0   0 ];
 C(3,:)=[1  0  0   0  0    0.5    0   1    1    1   0 ];
end

% Just Cyan
%C=zeros(3,3);
%C(1,:)=[0 1 1];
%C(2,:)=[0 0 1];
%C(3,:)=[0 1 1];

% white blue green orrange red violet black
%C(1,:)=[1 0 0   1.0 1 0.5 0 ];
%C(2,:)=[1 0 0.5 0.7 0 0   0 ];
%C(3,:)=[1 1 0   0.0 0 0.5 0 ];

% black, red (spectrum) violet black
%C(1,:)=[0 1 1  0  0    1 0];   %Red
%C(2,:)=[0 0 .7 .7 0.35 0 0]; %Green
%C(3,:)=[0 0 0  0  1    1 0];   %Blue

% blue to grey to red
%C(1,:)=[0 0    0.5 0.75 1 ];   %Red
%C(2,:)=[0 0    0.5 0    0 ];   %Green
%C(3,:)=[1 0.75 0.5 0    0 ];   %Blue

[n,m]=size(C);
num_breaks=m-1; %Number of colour entries minus one.
brk(1)=1;
for i=1:num_breaks,
 brk(i+1)=round(i*num/num_breaks);
end
brk(num_breaks+1)=num;

map=zeros(num,3);

map(1,:)=C(:,1)';
for i=1:num_breaks;
 slope=(C(:,i+1)-C(:,i))/(brk(i+1)-brk(i));
 map(brk(i):brk(i+1),:)=(1&[brk(i):brk(i+1)]')*map(brk(i),:)+([brk(i):brk(i+1)]-brk(i))'*slope';
end
map=abs(map);
