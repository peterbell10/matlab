% CreateRGBI: Combine two images to create an RGBI (aka RGBL) image
%
% Img = CreateRGBI(IntensityImage,ColourImage,ColourMap)
%
% ARGUMENTS
%  IntensityImage  The MxN image that will set the intensity (brightness)
%  ColourImage     The MxN image that will set the colour
%  ColourMap       The colourmap to use
%
% RETURNS
%  Img             And RGBI image (MxNx3)
%
% SEE ALSO
%  A_scale

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab
%
% HISTORY
%  11 11 25 First version

function Img = CreateRGBI(IntensityImage,ColourImage,ColourMap)

% We want a Mx3 colourmap
[M,N]=size(ColourMap);
if(M==3) then
 ColourMap=ColourMap';
end
NColours=size(ColourMap,1);

% Check the sizes of the input images
[N,M]=size(IntensityImage);
[Nc,Mc]=size(ColourImage);
if( N~=Nc || M~=Mc )
 disp(['The size of IntensityImage must be the same as ColourImage'])
 return
end

if(0)
 % Method:
 %  Turn the ColourImage into and RGB image
 %  Convert the ColourImage into and HSL image
 %  Scale the IntensityImage to 0 to 1, hence creating an L channel
 %  Create and RGB image using the H,S from the ColourImage and L from the
 %   IntensityImage

 %  Turn the ColourImage into and RGB image
 [X, Map] = gray2ind(ColourImage,NColours);
 ColourImageRGB = ind2rgb(X,ColourMap);
 HSV=rgb2hsv(ColourImageRGB);

 %  Scale the IntensityImage to 0 to 1, hence creating an L channel
 MinI=min(IntensityImage(:));
 MaxI=max(IntensityImage(:));
 IntensityImage=(IntensityImage-MinI)/(MaxI-MinI);

 %  Create and RGB image using the H,S from the ColourImage and L from the
 %   IntensityImage
 HSV(:,:,3)=IntensityImage;

 Img=hsv2rgb(HSV);

else % Method 2
 Map=ColourVsIntensityMap(ColourMap);
 % That makes it: PixelColour=Map(IntensityIndex,ColourIndex,:)

 % IntensityIndex
 MinI=min(IntensityImage(:));
 MaxI=max(IntensityImage(:));
 IntensityIndex=floor(255*(IntensityImage-MinI)/(MaxI-MinI))+1;

 % ColourIndex
 MinI=min(ColourImage(:));
 MaxI=max(ColourImage(:));
 ColourIndex=floor(255*(ColourImage-MinI)/(MaxI-MinI))+1;
 
 Img = zeros(N,M,3);
 
 % The following needs to me vectorised
 for j=1:M
  for i=1:N
   Img(i,j,:)=Map(IntensityIndex(i,j),ColourIndex(i,j),:);
  end
 end
 
end % End of method
