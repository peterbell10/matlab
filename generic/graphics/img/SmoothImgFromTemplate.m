function img=SmoothFromTemplate(img_in,radii_img)
%
% img=SmoothFromTemplate(img_in,radii_img);
%
% Given and image, img_in, and an array of the same size, radii_img,
% which contains the radii over which to smooth, smooth the image
% by the radii

[M,N]    =size(img_in);
[radii_M,radii_N]=size(radii_img);

if( (M~=radii_M) | (N~=radii_N) )
 error('img_in and radii_img must be the same size');
end

mask=0*img_in;
for i=1:M
 for j=1:N
  radius=radii_img(i,j);
  radius2=radius^2;
  total=0;
  ncells=0;
  k_low=floor(i-radius);
  if(k_low<1), k_low=1; end
  k_hi =ceil(i+radius);
  if(k_hi>M), k_hi=M; end
  for k=k_low:k_hi;
   dk2=(k-i)^2;
   l_low=floor(j-radius);
   if(l_low<1), l_low=1; end
   l_hi =ceil(j+radius);
   if(l_hi>N), l_hi=N; end
   % The next is an inner loop of l over l_low to l_hi that has been vectorised.
   l=[l_low:l_hi];
   dist2 = dk2 + (l-j).^2 ;
   span=l(find(dist2<=radius2));
   total=total+sum(img_in(k,span));
   ncells=ncells+length(span);
   % end of inner loop
  end % loop over k
  img(i,j)=total/ncells;
 end % loop over j
end % loop over i
 
