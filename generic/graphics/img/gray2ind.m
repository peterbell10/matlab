% gray2ind: Convert a grayscale image (intensity) into an indexed image
%
% [X, map] = gray2ind(I,n)
%
% ARGUMENTS
%  I  The grayscale (intensity) image to convert (float)
%  n  The number of colours in the colourmap     (integer)
%
% RETURNS
%  X    The indexed image (uint8)
%  map  The grayscale map into which X maps.
%
% REQUIRES
%  A_scale
%
% SEE ALSO
%  A_scale ind2rgb rgb2ind

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  111125 First version

% I have no idea why this function is in the Image Processing Toolbox and not
% the main.

function [X, map] = gray2ind(I,n)

minI=min(I(:));
maxI=max(I(:));
X = A_scale(I,minI,maxI,n);
map=gray(n);
