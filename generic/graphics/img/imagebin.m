function A=imagebin(x,y,Res,Limits)
% Bin (x,y) positions to create an image
%
% A=imagebin(x,y,Res,Limits);
%
% INPUT
%  x,y	The x and y coordinates.
%  Res	The resolution of the image to create.
%        Res scalar: A will have size Res x Res
%        Res vector: A will have size Res(1) x Res(2)
%  Limits	The range for the x and y coordinates of the image.
%		Limits = [ x_lo x_hi y_lo y_hi ]
%
% OUTPUT
%  A	The image.  The first index will be into y and the
%	second into x.
%
% SEE ALSO
%  numdensity rebin_img PlotWithDensity

% AUTHOR: Eric Tittley
%
% History:
%  00 05 17 Created
%  02 04 02 Added comments
%  13 12 24 Cope with variable resolution in the X & Y direction

if( length(Res) == 1 )
 Res=[Res Res];
end
X=floor((x-Limits(1))/(Limits(2)-Limits(1))*(Res(2)-1))+1;
Y=floor((y-Limits(3))/(Limits(4)-Limits(3))*(Res(1)-1))+1;

Good=find(X>=1 & X<=Res(2) & Y>=1 & Y<=Res(1) );

if(length(Good)<length(X))
 % Then we need to trim
 X=X(Good);
 Y=Y(Good);
end
clear Good

A=zeros(Res(1),Res(2));

% Note, the first loop is about 500 times slower than the second
% (Test this more carefully on an unloaded system)
%if( (500*Res(2)^2)<length(X))
if( 0 )
disp('Using Method 1')
 for i=1:Res(2)
  mask_x = (X==i);
  if(~isempty(mask_x)) 
   for j=1:Res(1)
    A(j,i)=sum( mask_x & Y==j );
   end
  end
 end
else
disp('Using Method 2')
 for i=1:length(X)
  A(Y(i),X(i))=A(Y(i),X(i))+1;
 end
end
