% generic/graphics/movies: Tools to generate movies.
%
%  img2gifmov	Takes the series of images, scales them to 1:128, writes them
%		to gif frames, and then creates a gif movie out of them.
%
%  movie2gifmov	Takes the series of movie frames, M (Nframes), created using
%		GETFRAME and writes them to gif frames and then creates a gif
%		movie out of them.
