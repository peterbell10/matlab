function img2gifmov(A,filename,loopflag,delay)
% img2gifmov: Generates an animated GIF from a series of arrays.
%
% img2gifmov(A,filename,loopflag,delay);
%
% ARGUMENTS
%  A		A series of MxN arrays (MxNxNframes)
%  filename	The GIF file to create.
%  loopflag	=1, to make a looping gif movie, 0 otherwise
%  delay	The delay, in seconds, between frames (1/100th minimum fraction)
%
% RETURNS
%  nothing
%
% NOTES
%  Takes the series of images, A (MxNxNframes),
%  scales them to 1:128
%  writes them to gif frames
%  and then creates a gif movie out of them
%
%  The present colourmap is the default
%
% REQUIRES
%  shell funtions: tifftogif, gifmerge
%
% SEE ALSO
%  movie2gifmov

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  040227 Mature version
%  080616 Regularised comments.  Changed paths to tifftogif


ColourDepth=128;
map=colormap;

[M,N,Nframes]=size(A);

minA=min(min(min(A)));
maxA=max(max(max(A)));
spanA=maxA-minA;
A=uint8( (A-minA)/spanA*(ColourDepth-1)+1 );

for i=1:Nframes
 file=['/tmp/temp_',int2str_zeropad(i,3),'.tif'];
 disp(file)
 imwrite(A(:,:,i),map,file,'tif')
 file=['/tmp/temp_',int2str_zeropad(i,3)];
 cmd=['!tifftogif ',file];
 eval(cmd)
end

delay=round(delay*100);
if(loopflag==1)
 cmd=['!gifmerge -l0 -',int2str(delay),' /tmp/temp_*.gif > ',filename];
else, cmd=['!gifmerge -',int2str(delay),' /tmp/temp_*.gif > ',filename];
end
 
eval(cmd)
eval('!rm /tmp/temp_*.gif /tmp/temp_*.tif')
