% Finding roots (zeros) of functions of 1 or more variable.
%
%  Roots of functions of 1 variable
%   Bisection	Bisection method
%   Muller	Muller's method
%   Netwon	Newton's method
%   Secant 	Secant method
