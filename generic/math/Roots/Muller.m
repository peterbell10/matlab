function [x_r,x]=Muller(f,x1,x2,x3)
% Muller's Method for finding roots of non-linear equations.
%
% x_r=Muller(f,x1,x2,x3)	Returns the root satisfying f(x_r)=0
% [x_r,x]=Muller(f,x1,x2,x3)	Returns the root, plus intermediate steps, x.
%
% f	The function
% x1	The 1st guess point.
% x2	The 2nd guess point.
% x3	The 3rd guess point.
%
% It is better, but not necessary, that xl and xu bracket the root.
%
% SEE ALSO
%  Newton, Bisection, Secant

% AUTHOR: Eric Tittley
%
% HISTORY
%  03 01 21 First Version
%
% COMPATIBILITY: Matlab, Octave

% Save the intermediate values if required.
if(nargout==2)
 niters=0;
end

y1=f(x1);
y2=f(x2);
y3=f(x3);
p=(y2-y1)/(x2-x1);

Tol=2*eps;

% The main loop
while(abs(y3)>2*Tol)
 q = ( y3 - y2 )/(x3 - x2);
 r = ( q - p )/(x3 - x1);
 s = q + r*( x3 - x2 );
 x_r = x3 - 2*y3/( s + sign(s)*sqrt( s^2 - 4*y3*r) );
 y_r = f(x_r);

 % Update variables for next iteration.
 x1 = x2;
 x2 = x3;
 x3 = x_r;
 y1 = y2;
 y2 = y3;
 y3 = y_r;
 p = q;

 % Save the intermediate values if requested.
 if(nargout==2)
  niters=niters+1;
  x(niters)=x_r;
 end

end

if(nargout==2)
 x=x(:); % Force to be a column vector
end
