function [x_r,x]=Secant(f,a,b)
% Secant method for finding roots of non-linear equations.
%
% x_r=Secant(f,a,b)	Returns the root satisfying f(x_r)=0
% [x_r,x]=Secant(f,a,b)	Returns the root, plus intermediate steps, x.
%
% f	The function
% a	The 1st guess point.
% b	The 2nd guess point.
%
% SEE ALSO
%  Newton, Bisection, Muller

% AUTHOR: Eric Tittley
%
% HISTORY
%  03 01 21 First version.
%  03 01 22 Cleaned up.
%
% COMPATIBILITY: Matlab, Octave

% Save the intermediate values if required.
if(nargout==2)
 x(1)=b;
 niters=1;
end

% Pre-calculate the values at the ends.
fa=f(a);
fb=f(b);
Tol=4*eps;
while( abs(fb)>Tol )
 % Determine the next point
 x_r=b-fb*(b-a)/(fb-fa);

 % Update for next iteration
 a=b;
 b=x_r;
 fa=fb;
 fb=f(x_r);

 % Save the intermediate values if requested.
 if(nargout==2)
  niters=niters+1;
  x(niters)=x_r;
 end

end

if(nargout==2)
 x=x(:); % Force to be a column vector
end
