function c=clrNaN(c,value)
% clrNaN: Clear NaN
%
% Clears all NaN's from an array and replaces them with value.
%
% c=clrNaN(c,value)
%
% ARGUMENTS
%  c		The array to modify (scalar or array)
%  value	The value to replace the NaN's with.
%
% RETURNS
%  The array, c, with all the NaN's turned to value
%
% REQUIRES
%
% SEE ALSO
%  NaN, isnan

% AUTHOR: Eric Tittley
%
% HISTORY
%  951031 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

c(find(isnan(c)))=value*(1&[1:length(find(isnan(c)))]);
