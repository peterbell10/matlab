% RichardsonExtrapolation: Extrapolate estimates to infinite sample resolution.

function A = RichardsonExtrapolation(As)

N=length(As);

R=zeros(N);
R(:,1)=As;
for j=2:N % The columns
 for i=j:N % The rows
  R=Richardson(R,i,j);
 end
end

A = R(N,N);

% End of RichardsonExtrapolation

% Fill a Richardson Table element.
function R=Richardson(R,i,j)
R(i,j)=( 2^j*R(i,j-1) - R(i-1,j-1) )/(2^j-1);


