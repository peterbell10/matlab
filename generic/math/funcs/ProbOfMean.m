function p=ProbOfMean(count,Means)
% ProbOfMean: The probability of the given mean value presuming Poisson stats.
%
% p=ProbOfMean(count,Means)
%
% Returns the probability, p(i), of the mean value being Means(i)
% if the measured amount is count and Poisson statistics is presumed.
%
% ARGUMENTS
%  count	The number of samples taken
%  Means	The mean values (scalar or vector)
%
% RETURNS
%  p	The probabilities.
%
% REQUIRES
%  Poisson
% 
% SEE ALSO
%  Poisson

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave
%
% Isn't this a rather trivial routine?  What is the point?  What is it used for?

for i=1:length(Means)
 p(i)=Poisson(Means(i),count);
end
