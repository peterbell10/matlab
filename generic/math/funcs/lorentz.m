function lorentz_out=lorentz(wave,wave0,amplitude,fwhm)
% lorentz: The Lorentz Profile
%
% lorentz_out=lorentz(wave,wave0,amplitude,fwhm)
%
% This function returns a Lorentzian profile vector with the
% parameters given by the input.
%
% ARGUMENTS
%  wave		Positions (wavelengths, frequencies, etc.) (scalar or vector)
%  wave0	The centre position of the Gaussian. (scalar)
%  amplitude	The 'amplitude' of the Gaussian. (scalar)
%  fwhm		The FWHM of the Gaussian. (scalar)
%
% RETURNS
%  lorentz_out	The profile. A vector of the same dimension as wave.
%
% REQUIRES
%
% SEE ALSO
%  gauss, voigt

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

lorentz_out=amplitude* (fwhm/(2*pi))*((wave-wave0).^2+(fwhm/2)^2).^(-1);
