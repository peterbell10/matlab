% voigt: Voigt profile
%
% The Voigt profile is the convolution of a Gaussian with a Lorentzian.
%
% profile=voigt(GaussParams,LorentzParams,nu)
%
% ARGUMENTS
%  GaussParams	The parameters of the Gaussian.  See P in gauss().
%		GaussParams = [ampl sigma]
%  LorentzParams	The parameters of the Lorentz.
%		LorentzParams=[amplitude fwhm]
%  nu		The frequencies for which to calculate the profile.
%		[vector of even length, preferably of a power of two]
%
% RETURNS
%  profile	The profile at the frequencies nu.
%
% NOTES
%  Since this is a convolution, wave0 in the calls to gauss() and lorentz()
%  are both 0.
%  Units are up to the user.  So long as GaussParams, LorentzParams, and nu
%  are compatible, it should work.  Indeed, wavelength or any other suitable
%  dimension can be used for 'nu'.
%
% REQUIRES
%  gauss, lorentz, shift
%
% SEE ALSO
%  gauss, lorentz

% AUTHOR: Eric Tittley
%
% HISTORY
%  071029 First version based an a much older script.
%  080416 Bug fix: If nu wasn't symmetric [-N:N], profile was shifted from 0.
%
% COMPATIBILITY: Matlab, Octave
%
% TODO
%  Check what is the shortest vector that works.

function profile=voigt(GaussParams,LorentzParams,nu)

% Shift nu so that nu=0 is at the left edge
dnu = nu(2)-nu(1);
num = nu(1)/dnu;
nu=shift(nu,num,2);

gaus=gauss(nu,[GaussParams,0]);
lorr=lorentz(nu,0,LorentzParams(1),LorentzParams(2));
profile=real(ifft(fft(gaus).*fft(lorr)));
profile=shift(profile,-num,2);
