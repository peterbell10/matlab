function p=Poisson(Mean,counts)
% Poisson: The probability distribution of for Poisson statistics.
%
% p=Poisson(Mean,counts)
%
% Returns the probability distribution, p(i), of measuring counts(i)
% presuming that the true mean value is Mean and presuming Poisson
% statistics.
%
% ARGUMENTS
%  Mean		The expected mean (scalar)
%  counts	The actual counts (scalar or vector)
%
% RETURNS
%  p		The probability of the measured counts
%
% REQUIRES
%
% SEE ALSO
%  ProbOfMean

% AUTHOR: Eric Tittley
%
% HISTORY:
%  00 10 22 Modified to not fail when counts gets large
%  07 10 29 Modified comments
%
% COMPATIBILITY: Matlab, Octave

%r_fact=counts*0;
% The next would be much faster if we could assume that
% counts = [0:count_max]
% Then we could use r_fact=[1,cumprod(counts(2:end))];
%for i=1:length(counts)
% r_fact(i)=prod(1:counts(i));
%end
%p=exp(-Mean)*((Mean.^counts)./r_fact);

% The next does the same as the above, but does not fail when
% counts gets large (>150 or so)
for i=1:length(counts)
 p(i)=exp(-Mean)*prod(Mean./[1:counts(i)]);
end


