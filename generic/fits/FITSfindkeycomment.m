function str=FITSfindkeycomment(Header,KeyWord)
  
% FITSfindkeycomment  -  Returns keys comment in header.
%
%
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
%
% Comment is the text which is placed after / sign in  key.
% KeyWord could also be numeric and tell about what number of key to access.
%
%
  
  if ischar(KeyWord),
    num=double(FITSfindkey(Header,KeyWord));
  else
    num=double(KeyWord);
  end
  str='';
  if num>length(Header),
    return;
  elseif num<1
    return;
  end
  
  s=Header(num,10:end);
  
  isq=0;
  %search for start (a) for comment, the / sign.
  for a=1:length(s),
    if s(a)==char(39),
      isq=~isq;
    end
    if s(a)==47 & isq==0,
      str=deblank(s(a+1:end));
      return;
    end
  end
