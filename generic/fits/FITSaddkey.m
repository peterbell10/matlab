function newheader=FITSaddkey(header,key,value,pos,comment)
% FITSaddkey -  Adds keyvalues to header.
%
% function newheader=FITSaddkey(header,key,value, pos,[comment])
%
%    header  Existing header character matrix to add line to.
%
%    key     Name of FITS header key to add.
%
%    value   If is char array the put in as string,
%            else convert number to ascii and put in.
%            Note, if value is 'T' or 'F', it is assumed to be a
%            bolean, which is stored unquoted.
%
%    pos     Line no to put key in. Existing key and all
%            downwards will moved down one step. Value 0
%            or a line no that not already exists will
%            add key at end.
%
%    comment Text to append after the value which is simply a comment line.
%            This is optional.
%
%  If key is either COMMENT or HISTORY, then the value is ignored (just put
%  an empty matrix, []) and the comment line is used to fill the line. If
%  The comment line is left out, the text is left blank.
%
% returns:
%
% newheader  A new header char matrix with new key insterted.
%

% HISTORY
% Based on:
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
% 00 10 19
%  Added extra comments.
%  Fixed to conform to the standard without assumptions on the input.
%  Added support for FITS comments.
%
% 00 11 02
%  Added support for Bolean values.

% The FITS Specification breaks the header field into 3 parts:
% Keyword		(bytes 1-8)
% Value indicator	(bytes 9-10)
% Value/Comment		(bytes 11-80)
%
% The Value indicator is trivial, it is simply '= ', unless the line is
% only comments, in which case the comments may span bytes 9-80
%
% It seems to be customary to start the comment at byte 31, but this is not
% required.

% Check to see that the input header is valid
if( ~ isempty(header) )
 [N,M]=size(header);
 if(M~=80)
  error('Input header invalid size.  It must be 80 characters wide.')
 end
else
 N=0;
end % if header is not empty

if( length(pos)==0 | pos>N | pos<1 )
 pos=N+1;
end

% Create the new line
% First set up the default comment line.
CommentFlag=0;
CommentStr=char(0*(9:80)+32); % start with a blank line of the maximum
                              % allowable length
if(nargin==5) % if we have been passed a comment
 CommentFlag=1;
 if( length(comment)>length(CommentStr) )
  error('Comment String too long.  Must be less than 72 characters.')
 end
 CommentStr(1:length(comment))=comment;
end

% The key label may be only 8 characters long
if(length(key)>8)
 key=key(1:8);
end

if( strcmp(key,'COMMENT') | strcmp(key,'HISTORY') ) % A pure comment line
 newheader=sprintf('%-8s%s',key,CommentStr);
else % the more likely scenario
 if(ischar(value)==0) % Numeric value
  if(CommentFlag)
   newheader=sprintf('%-8s=%21G / %s',key,value,CommentStr);
  else
   newheader=sprintf('%-8s=%21G   %s',key,value,CommentStr);
  end
 elseif( length(value)==1 & ( value=='T' | value=='F' ) ) % Bolean
  if(CommentFlag)
   newheader=sprintf('%-8s= %20s / %s',key,value,CommentStr);
  else
   newheader=sprintf('%-8s= %20s   %s',key,value,CommentStr);
  end
 else % String value
  if(length(value)<20) % String short, so pad out so that comment starts at
                       % byte 31
   if(CommentFlag)
    newheader=sprintf('%-8s= %-20s / %s',key,[char(39),value,char(39)],CommentStr);
   else
    newheader=sprintf('%-8s= %-20s   %s',key,[char(39),value,char(39)],CommentStr);
   end
  else % String long, don't pad
   if(CommentFlag)
    newheader=sprintf('%-8s= %s / %s',key,[char(39),value,char(39)],CommentStr);
   else
    newheader=sprintf('%-8s= %s   %s',key,[char(39),value,char(39)],CommentStr);
   end
  end
 end % if numeric, else
end % if comment, else

%Trim the line to 80 characters
if(length(newheader)>80)
 newheader=newheader(1:80);
end

% Split the previous header into two parts, that which will proceed
% the new line, and that which will follow.
% These may be empty.
header1=header(1:pos-1,:);
header2=header(pos:end,:);

% Stick the line in the header
newheader=strvcat(header1,newheader);
newheader=strvcat(newheader,header2);
