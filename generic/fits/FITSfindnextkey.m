function NextNr=FITSfindnextkey(Header,Current)
  
% NextNr=FITSfindnextkey(Header,Current) - finds next key with same name.
%
%
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
%
% Finds next possition that have same key name as Current "points" to.
% Current is a number of a key in the header. The returned value Next
% is the possition of next key name which has same key name as
% Current "points" to.
% If theire isn't any more key whith the same name then 0 will be
% returned tas NextNr.
  
  NextNr=strmatch(deblank(Header(Current+1,1:8)),Header(:,1:8),'exact');
  
  NextNr=NextNr(find(NextNr==Current)+1:end);
  
  if length(NextNr)==0,
    NextNr=0;
  else
    NextNr=NextNr(1);
  end
  

