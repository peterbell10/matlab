function nr=FITSfindkeyvalue(Header,KeyWord)
  
% FITSFINDKEYVALUE -  Returns the numeric value that is assigned to KeyWord.
%
% nr=FITSfindkeyvalue(Header,KeyWord);
%
% If it's not a numeric key or the key does't exist then [] is returned.
% KeyWord could also be numeric and tell about what number of key to access.
%
% SEE ALSO
%  FITSfindkeystring FITSfindkey FITSfindkeycomment FITSfindnextkey

% HISTORY
%  Version 1.0 Beta 99-09-25 Peter Rydes�ter, GNU General Public License.
%
%   00 07 21: (E. Tittley) Added to the top comments the syntax.
%
%   00 03 01: (E. Tittley) Strip off trailing comments started by '/'
%
%   03 06 07 Added SEE ALSO comments to hearder.

nr=[];
if ischar(KeyWord),
  a=FITSfindkey(Header,KeyWord);
else
  a=KeyWord;
end
if a>length(Header),
  return;
elseif a<1,
  return;
end
% Isolate only those value of the string that are numeric (0-9,+,-,.)
extr=Header(a,10:80); 
tmp=uint8(extr);
% But first, strip off the comment on each line (after the '/' (char 47))
cmt_pos=find(tmp==47);
if(~isempty(cmt_pos))
 tmp=tmp(1:cmt_pos(1)-1);
end
% valid numerical symbols: 0123456789.-+EeDd
mask=find( (tmp>=48 & tmp<=57) | tmp==46 | tmp==45 ...
          | tmp==43 | tmp==69 | tmp==101 | tmp==68 | tmp==100 );
nr=str2num(extr(mask));
