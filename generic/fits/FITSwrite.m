function status = FITSwrite(header,data,name)
% FITSwrite: Write FITS files.
%
% status = FITSwrite(header,data,name)
%
% ARGUMENTS
%   header    A char matrix holding all FITS file header
%             "keywords". All chars after position 80
%             are truncated. Header char matrix is easily
%             created with FITSaddkey.
%             If you don't have any keywords to put in to
%             your header, then just put in '' or [] as an
%             argument.
%
%             Image will be stored in datatype specified by
%             BITPIX keyword. Remember to set that keyword
%             to wanted datatype.
%             Valid values are:
%              8 Character or unsigned binary integer
%             16 16-bit twos-complement binary integer
%             32 32-bit twos-complement binary integer
%            -32 IEEE single precision floating point
%            -64 IEEE double precision floating point
%
%             The minimum header depends on the form of the data.
%
%   data      Data.  Currently supported: Image data and table
%             data.
%
%   name      Path, name and extention for file to store in.
%
% RETURNS
%  status
%
% Note: 1) The full set of possibilities described by the FITS
%          standard (http://archive.stsci.edu/FITS/FITS_standard/)
%          is yet to to be implemented.
%       2) Values for BSCALE and BZERO will be applied to the data
%          before writing.
%
% SEE ALSO
%  FITSread, FITSaddkey, FITSremovekey

% AUTHORS: Bjorn Gustavsson, Peter Rydesaeter, Eric Tittley
%
% HISTORY
%  98-05-25 (as wFITS.m) Original version Bjorn Gustavsson
%  99-09-25 Peter Rydes�ter
%	FITSwrite version
%	GNU General Public License.

% COMPATIBILITY: Matlab, Octave

% We are going to either either create the file if it does not exist or append
% to the end.
if(exist(name))
 rec=1;
else
 rec=0;
end

%In FITS format, the primary data array shall consist of a single data array of
% 0-999 dimensions.  Here, we call the array, 'image'.
% Needless to say, it is not an extension.
DataType=FITSfindkeystring(header,'XTENSION');
% We can only accept absent 'XTENSION' keywords if it is record 0
if(length(DataType)==0)
 if( rec==0 )
  if(length(data)==0)
   DataType='EMPTY';
  elseif(length(data)>1)
   DataType='IMAGE';
  else
   % Presume that if size(data)=[1,1], then it is a structure, not an array.
   % This is potentially false and constitutes a bug.  But we need a way to
   % test if something is a structure.
   error('Record 0 must be empty or a single array')
  end
 else % Not record 0
  if(length(data)==0)
   DataType='EMPTY';
  elseif(length(data)>1)
   DataType='IMAGE';
  else
   % Presume that if size(data)=[1,1], then it is a structure, not an array.
   % This is potentially false and constitutes a bug.  But we need a way to
   % test if something is a structure.
   error('Keyword XTENSION must be set if not an empty record or image record')
  end
 end
elseif(rec==0)
 error('Record 0 cannot be an XTENSION')
end

% If the array is empty, then overide the wishes of the user. 
if(isempty(data)), DataType='EMPTY'; end

% Open/Create the file as necessary
status=0;
if(exist(name))
 fid = fopen(name,'a','ieee-be');
 if(fid == -1)
  status=-1;
  error('Can not find/open FITS file.');
 end
else
 fid = fopen(name,'w','ieee-be');
 if(fid == -1)
  status=-1;
  error('Can not create FITS file.');
 end
end

switch sscanf(DataType,'%s')
 case 'EMPTY'

  % Remove old keywords
  header=FITSremovekey(header,'SIMPLE');
  header=FITSremovekey(header,'BITPIX');
  header=FITSremovekey(header,'NAXIS');
  header=FITSremovekey(header,'NAXIS1');
  header=FITSremovekey(header,'NAXIS2');
  header=FITSremovekey(header,'EXTEND');
  header=FITSremovekey(header,'END');

  %Add new keywords.
  header=FITSaddkey(header,'SIMPLE','T',1);
  header=FITSaddkey(header,'BITPIX',8  ,2);
  header=FITSaddkey(header,'NAXIS' ,0  ,3);
  header=FITSaddkey(header,'EXTEND','T',4);

  % Write header
  status=local_write_header(fid,header,rec);
  if(status~=0)
   error('unable to write header')
  end

 case 'IMAGE',
  bitpix=FITSfindkeyvalue(header,'BITPIX');
  if(isempty(bitpix)), bitpix=0; end
  switch bitpix
   case 0,
    disp('Warning: BITPIX not set. Assuming double')
    image=double(data);
    bitpix=-64;
   case 8,
    image=uint8(data);
   case 16,
    image=int16(data);
   case 32,
    image=int32(data);
   case -32,
    image=single(data);
   case -64,
    image=double(data);
   otherwise,
    error('I do not know what to do with the given value of BITPIX')
  end
  datatypesize=abs(bitpix)/8;

  si=size(data);

  % Remove old keywords
  header=FITSremovekey(header,'SIMPLE');
  header=FITSremovekey(header,'BITPIX');
  header=FITSremovekey(header,'NAXIS');
  header=FITSremovekey(header,'NAXIS1');
  header=FITSremovekey(header,'NAXIS2');
  header=FITSremovekey(header,'END');

  %Add new keywords.
  header=FITSaddkey(header,'SIMPLE','T',1);
  header=FITSaddkey(header,'BITPIX',bitpix,2);
  header=FITSaddkey(header,'NAXIS' ,length(si),     3);
  header=FITSaddkey(header,'NAXIS1',si(2), 4);
  header=FITSaddkey(header,'NAXIS2',si(1), 5);
  if(length(si)>2), header=FITSaddkey(header,'NAXIS3',si(3), 6); end
  if(length(si)>3), header=FITSaddkey(header,'NAXIS4',si(4), 7); end

  status=local_write_header(fid,header,rec);
  if(status~=0)
   error('unable to write header')
  end

  % This is a bit of a contentious section.  It is convenient to have the
  % FITS writer scale the data independently of the user, but then the
  % user must know not to do it first
  if( FITSfindkey(header,'BZERO')>0 & FITSfindkey(header,'BSCALE')>0 )
   data=   data / FITSfindkeyvalue(header,'BSCALE')  ...
         - FITSfindkeyvalue(header,'BZERO');
  end
  % End of scaling

  % Transpose images
  if(length(si)==2), data=data'; end

  % Write data.
  switch bitpix,
   case 8,   fwrite(fid,data,'uint8');
   case 16,  fwrite(fid,data,'int16');
   case 32,  fwrite(fid,data,'int32');
   case -32, fwrite(fid,data,'float32');
   case -64, fwrite(fid,data,'float64');
   otherwise,
    error('Impossible state.  This should have been caught.')
  end

 case 'BINTABLE',
  % A row of a binary table is a collection of data fields of (potentially)
  % different lengths For example, 100 integers, followed by 50 floats, followed
  % by 25 characters.  The data is laid out one row at a time.

  % ARRAY DESCRIPTORS ARE NOT YET SUPPORTED

  % We will presume that the table data is in a structure.
  % Each field of the structure will have Nrows rows, which will be the
  % same for each field.
  % Each field can have Ncols columns, and this can be different for each
  % field.
  % However, if none of the fields is an array, then we can forget them, and
  % just use the singleton dimension.

  % A crude check to see if data is of a size other than 1x1, which is what
  % is reported by size for structures.
  [dummyx,dummyy]=size(data);
  if(dummyx~=1 | dummyy~=1)
   error('data must be a structure if to be written to a BINTABLE')
  end
  % I wish there were a way to determine if a variable represents a structure.
  % The next line can cause an unclean error if data is an array of size 1x1.
  fields=fieldnames(data);
  [ncols,dummy]=size(fields);
  [nrows,dummy]=size(getfield(data,fields{1}));
  
  % Sanity check
  % Here we will find the master_index.  If it is 1, then the rows of the tables
  % will correspond to rows of the fields.  If it is 2, then table rows will
  % correspond to columns of the fields.
  for i=1:ncols
   [field_rows(i),field_cols(i)]=size(getfield(data,fields{i}));
  end
  if(ncols>1) % if more than one field
   if(sum(field_rows==1)==ncols) % the 1st index is singleton for every field
    nrows=field_cols(1);
    master_index=2;
    % All the other field 2nd index lengths must be the same, otherwise there
    % can only be one row
    if(sum(field_cols(2:end)==nrows)~=(ncols-1))
     nrows=1;
     master_index=1;
    end
   elseif(sum(field_cols==1)==ncols) % the 2nd index is singleton for every field
    nrows=field_rows(1);
    master_index=1;
    % All the other field 1st index lengths must be the same, otherwise there
    % can only be one row
    if(sum(field_rows(2:end)==nrows)~=(ncols-1))
     nrows=1;
     master_index=2;
    end
   else % Neither index is singleton
    % If neither is singleton, then either the rows or the columns must be the
    % same length for each field, and this will become the master_index
    nrows=field_rows(1);
    master_index=1;
    % All the other field 1st index lengths must be the same, otherwise try the
    % second index
    if(sum(field_rows(2:end)==nrows)~=(ncols-1))
     nrows=field_cols(1);
     master_index=2;
     % All the other field 2nd index lengths must be the same, otherwise we
     % are out of options
     if(sum(field_cols(2:end)==nrows)~=(ncols-1))
      error('I cannot make a table out of the data structure without a common dimension')
     end
    end % if the 1st index lengths are not the same
   end % Primary case test
  end % if there is more than one field

  %END of the sanity check to determine the correct layout of the table

  disp(['table with ',int2str(nrows),' row(s). Master index:',int2str(master_index)])

  % Get the number of elements in each field.
  for i=1:ncols
   dummy=getfield(data,fields{i});
   if(master_index==1)
    NElems(i)=length(dummy(1,:));
   else
    NElems(i)=length(dummy(:,1));
   end
  end

  %Get the data type for each field.  Default to double.
  % Add up the bytes per table row, at the same time.
  fmtstr=char(uint8(zeros(ncols,7)));
  NBytes=0;
  for i=1:ncols
   % Read in the type label, stripping off trailing whitespaces
   TypeLabel=sscanf(FITSfindkeystring(header,['TFORM',int2str(i)]),'%s');
   % Default to double.
   if(isempty(TypeLabel)), TypeLabel='D'; end
   Type=TypeLabel(end); % This is not exactly strict, since 100Integer is also
                        % acceptable, according to the standard.
   switch Type
    case 'L', % 1-byte logical
     fmt='uchar  '; 
     NBytes=NBytes+1*NElems(i);
    case 'X', % Bit.  This is tricky, since Matlab can't read bits.
     % If the bits are stored in a grouping of 8, 16, 32, or 64, then
     % read them in as integers
     if(NElems(i)== 8)
      fmt='uint8  ';
      NElems(i)=1;
      NBytes=NBytes+1;
     elseif(NElems(i)==16)
      fmt='uint16 ';
      NElems(i)=1;
      NBytes=NBytes+2;
     elseif(NElems(i)==32)
      fmt='uint32 ';
      NElems(i)=1;
      NBytes=NBytes+4;
     elseif(NElems(i)==64),
      fmt='uint64 ';
      NElems(i)=1;
      NBytes=NBytes+8;
     else
      disp('Sorry, Matlab can not write bits unless they are in blocks of 8, 16, 32, or 64');
      return;
     end
    case 'B', % unsigned byte
     fmt='uchar  ';
     NBytes=NBytes+1*NElems(i);
    case 'A', % 1-byte character
     fmt='uchar  ';
     NBytes=NBytes+1*NElems(i);
    case 'I', % 2-byte integer
     fmt='int16  ';
     NBytes=NBytes+2*NElems(i);
    case 'J', % 4-byte integer
     fmt='int32  ';
     NBytes=NBytes+4*NElems(i);
    case 'E', % 4-byte real
     fmt='float32';
     NBytes=NBytes+4*NElems(i);
    case 'D', % 8-byte double
     fmt='float64';
     NBytes=NBytes+8*NElems(i);
    case 'C', disp('Sorry, I can not yet write complex'); return; % Complex
    case 'M', disp('Sorry, I can not yet write complex'); return; % Complex Double
    otherwise
     error(['I do not know what to do with the TTYPE ',Type])
   end % Switch
   fmtstr(i,:)=fmt;
   key=['TTYPE',int2str(i)];
   header=FITSremovekey(header,key);
   header=FITSaddkey(header,key,fields{i},0);
   key=['TFORM',int2str(i)];
   header=FITSremovekey(header,key);
   header=FITSaddkey(header,key,[int2str(NElems(i)),Type],0);
  end % end of loop through fields

  % Remove old keywords
  header=FITSremovekey(header,'SIMPLE');
  header=FITSremovekey(header,'XTENSION');
  header=FITSremovekey(header,'BITPIX');
  header=FITSremovekey(header,'NAXIS');
  header=FITSremovekey(header,'NAXIS1');
  header=FITSremovekey(header,'NAXIS2');
  header=FITSremovekey(header,'PCOUNT');
  header=FITSremovekey(header,'GCOUNT');
  header=FITSremovekey(header,'TFIELDS');
  header=FITSremovekey(header,'END');

  %Add new keywords.
  if(rec==0)
   header=FITSaddkey(header,'SIMPLE','T',   1);
  else
   header=FITSaddkey(header,'XTENSION','BINTABLE',1);
  end
  header=FITSaddkey(header,'BITPIX' ,8,     2);
  header=FITSaddkey(header,'NAXIS'  ,2,     3);
  header=FITSaddkey(header,'NAXIS1' ,NBytes,4);
  header=FITSaddkey(header,'NAXIS2' ,nrows, 5);
  header=FITSaddkey(header,'PCOUNT' ,0,     6);
  header=FITSaddkey(header,'GCOUNT' ,1,     7);
  header=FITSaddkey(header,'TFIELDS',ncols, 8);

  % WRITE THE HEADER
  status=local_write_header(fid,header,rec);
  if(status~=0)
   error('unable to write header')
  end

  % Now to the data
   for row=1:nrows
    if(nrows>5000)
     if(~rem(row,floor(nrows/25)))
      disp([int2str(row),' of ',int2str(nrows)])
     end
    end
    for i=1:ncols
     if(master_index==1)
      dummy=getfield(data,fields{i},{row,1:NElems(i)});
     else
      dummy=getfield(data,fields{i},{1:NElems(i),row});
     end
     fwrite(fid,dummy,sscanf(fmtstr(i,:),'%s'));
    end % loop over fields
   end % loop over rows
 
 otherwise,
  error(['I do not know how to deal with records of the form ',DataType])
end

%Pad out to a 2880 boundary
local_zero_pad(fid,0);

% Close the file
fclose(fid);

% End of main programme
%-------------------------------------------------------------------------

function status=local_write_header(fid,header,rec)
 
header(end+1,1:80)=['END',blanks(77)];  % Add end line.
header=header(:,1:80);     % Cut width of text matrix to 80.

if(rec==0)
 % Is it necessarily true that the Primary header cannot exceed 36 rows?
 % The standard states:
 %  The header of a primary HDU  shall consist of a series of card images  in
 %  ASCII  text. All header records shall consist of 36 card images. Card
 %  images without information shall be filled  with ASCII blanks
 %  (hexadecimal 20). 
 %% Ensure that the Primary header is composed of 36 rows of 80 characters
 [nrows,ncols]=size(header);
 padding=rem(nrows,36);
 if(padding==0), padding=36; end
 header=[header;char(32*ones(36-padding,80))];
 %end
end

% Write header
fprintf(fid,'%s',header');
% Zero pad
local_zero_pad(fid,1);

% Set the status
% At the moment, this is useless, since it can never not return 0
status=0;

%%%% End of subroutine local_write_header

function local_zero_pad(fid,header_flag)
% Find number of bytes left to end of this 2880 block.
% (this sorta repeats the step done previously for the primary header to ensure
% that the header is composed of 36 x 80 characters
% If it is a header, fill with ASCII character 32, else fill with ASCII 0.
if(header_flag)
 BlankChar=32;
else
 BlankChar=0;
end
i=mod(2880-mod(ftell(fid),2880),2880);
fwrite(fid,char(BlankChar*ones(i,1)),'uchar');

%End of function local_zero_pad
