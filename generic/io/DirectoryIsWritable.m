% DirectoryIsWritable: Test whether we can write to a directory
%
% IsWritable = DirectoryIsWritable(Directory)
%
% ARGUMENTS
%  Directory    Path to the directory to test
% RETURNS
%  IsWritable   1 => Is writable
%               0 => Is not writable
%
% SEE ALSO
%  fileattrib

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  130524 First version

function IsWritable = DirectoryIsWritable(Directory)

% Check the directory exists
[status,message,messageid] = fileattrib(Directory);
if(status==0)
 error(['No such directory ',Directory])
elseif(message.directory==0)
 error([Directory,' is a file, not a directory'])
end

% create a random folder name so no existing folders are affected
[status,TestFile]=system(['mktemp --tmpdir=',Directory]);

if(status==1)
 % Error in creating temp file.  Directory is not writable
 IsWritable=0;
else
 IsWritable=1;
 % Clean up. Remove the test file
 [status,testDir]=system(['rm ',TestFile]);
 if(status~=0)
  error('Unable to delete ',TestFile,', which is odd')
 end
end
