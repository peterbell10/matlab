% readFortranRecord: Read a record from an unformatted Fortran binary file
%
% D=readFortranRecord(FileID,precision);
%
% ARGUMENTS
%  FileID       The file id, as returned by fopen(file,'rb')
%  precision    The precision of the values stored in the record. See fread
%
% RETURNS
%  D            The data read from the record.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function D=readFortranRecord(FileID,precision)

switch precision
 case { 'uchar', 'schar', 'uint8' }
  BytesPerElement=1;
 case { 'int16', 'uint16' }
  BytesPerElement=2;
 case { 'int32', 'uint32', 'single', 'float32'}
  BytesPerElement=4;
 case { 'int64', 'uint64', 'double', 'float64'}
  BytesPerElement=8;
 otherwise
  disp(['Unknown precision: ',precision])
end

ArrayLengthStart=fread(FileID,1,'*int'); % bytes
nElements=ArrayLengthStart/BytesPerElement;
D=fread(FileID,nElements,precision);
ArrayLengthEnd=fread(FileID,1,'*int');

if( ArrayLengthStart ~= ArrayLengthEnd )
 disp('ERROR: readFortranRecord: Something amiss with the record')
 disp(['ArrayLengthStart=',int2str(ArrayLengthStart), ...
       '; ArrayLengthEnd=',int2str(ArrayLengthEnd)])
end
