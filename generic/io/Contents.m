% io: Routines for Input/Output of generic data types
%
% write_cell_array_to_ascii  Write a cell array to and ASCII file
%
% DirectoryIsWritable        Test whether we can write to a directory
%
% readFortranRecord          Read a record from an unformatted Fortran binary
%                            file
