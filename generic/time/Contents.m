% Time functions (Eric's library)
%
% Chandra_date	Determines the number of days after launch an obs took place
%
% Greg2Jul	Convert Gregorian to Julian date (-2.4e6)
%
% sec2dhms	Convert seconds to Days:Hours:Minutes:Seconds
