function Chandra_date(file);
% Chandra_date: The number of days after launch a Chandra observation took place.
%
% This program determines how many days after Chandra's Launch,
% a particular observation took place.  Most headers have the start
% and end date of the observation, so just about any file will do,
% but shorter files take less time to be read in.
%
% syntax Chandra_date(file);
%
% ARGUMENTS
%  file		The FITS file containing observational data.
%
% RETURNS
%  The number of days after the launch of Chandra the observation took place.
%
% NOTE: "apply_acisabs" gives a time of about ~0.2 days earlier than
% 	 what this program calculates for time between launch and 
%	start of the observation.
%
% REQUIRES
%  fitsload, fitsfindkeyvalue
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  07 10 27 Mature version

% read in the zero header of the file, because it
% contains all the information and is the quickest
% to read in
[h,d] = fitsload(file);

% Determine the MJD of the observation
ZERO = fitsfindkeyvalue(h,'MJDREF');		% THE MJD reference day

START = fitsfindkeyvalue(h,'TSTART')/3600/24;	% Time of the start of 
						% observation in seconds 
						% after MJD reference day

END = fitsfindkeyvalue(h,'TSTOP')/3600/24;	% Time of the end of 
						% observation in seconds 
						% after MJD reference day


% Launch date is 1999-07-23
% in MJD that's 51382 (From Web, date conversion utility 
% http://heasarc.gsfc.nasa.gov/cgi-bin/Tools/DateConv/dateconv.pl
LNCH = 51382;

START = ZERO+START;				% Add MJD reference day to start time
END = ZERO+END;					% Add MJD reference day to end time

MID = (START + END)/2;				% MID is the MJD half-way through the observation

fprintf('Your Observation took place %g days after the launch.\n\n',MID-LNCH);
fprintf('It started at %g and ended at %g days after the launch.\n',START-LNCH,END-LNCH);
