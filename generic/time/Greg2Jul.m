function JulianDate=Greg2Jul(year,month,day,hr,min,sec)
% Greg2Jul: Convert Gregorian to Julian date (-2400000d)
%
%  Given the year, month number, day number, hour, minute, and second
%  Return the Julian Day number
%
% JulianDate=Greg2Jul(year,month,day,hr,min,sec)
%
% ARGUMENTS
%  year		The year
%  month	The month (Jan=1, Feb=2, ... Dec=12)
%  day		The day (>=1, <=31)
%  hr		The hour (>=0, <=23)
%  min		The minute (>=0, <=59)
%  sec		The second (>=0, <60)
%
%  These may be vectors of numbers.
%
% RETURNS
%  Julian date, less the base of 2 400 000
%
% RETURNS: Nothing
%
% BUGS
%  Has not been tested for all times.
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  02-02-12 Mature version.
%  07 10 27 Fixed leap year test.

% Do the easy stuff first:
Day_fraction=(hr+(min+sec/60)/60)/24;

% Find leap years:
% Rules for leap years:
%   1. Years divisible by four are leap years, unless...
%   2. Years also divisible by 100 are not leap years, except...
%   3. Years divisible by 400 are leap years.
leap_year=  ~rem(year,4) & ( rem(year,100) | ~rem(year,400) ) ;
past_March_0=month>=3;

Days_elapsed=[0,31,31+28,31+28+31,31+28+31+30,31+28+31+30+31,31+28+31+30+31+30,31+28+31+30+31+30+31,31+28+31+30+31+30+31+31,31+28+31+30+31+30+31+31+30,31+28+31+30+31+30+31+31+30+31,31+28+31+30+31+30+31+31+30+31+30];

Day_of_the_year=Days_elapsed(month)'+(leap_year&past_March_0)+(day-1)+Day_fraction;


% Now for the hard part: the Julian date

Julian_date_1980=44238.5; % Note: this was a leap year

Num_years_since_1980=year-1980;

Num_leap_years_since_1980=floor((Num_years_since_1980-1)/4)+1;

JulianDate= Julian_date_1980+365*Num_years_since_1980+Num_leap_years_since_1980+Day_of_the_year;
