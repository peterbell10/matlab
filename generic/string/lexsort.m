% lexsort: Sort alphabetically
%
% OUT = lexsort(A)
%
% ARGUMENTS
%  A	String matrix of lexical names
%
% RETURNS
%  OUT	Alphabetically sorted version of A
%	(sort is case INSENSITIVE)

% AUTHOR
%	M. Louis Lauzon and Craig Jones
%
%	Imaging Research Laboratories         phone: (519) 663-3833
%	Robarts Research Institute            FAX:   (519) 663-3789
%	100 Perth Drive, P.O. Box 5015
%	London, Ontario, CANADA  N6A 5K8
%	
%	email: LLAUZON@IRUS.RRI.UWO.CA
%	email: CRAIGJ@IRUS.RRI.UWO.CA
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  01 03 01 Mature version
%
%  07 10 27 Regularised header comments

% LEXSORT transforms all words into a numerical value,
% where a-z (and A-Z, since case insensitive) get mapped 
% to their ascii value minus 32 (subtracting 32 maps blank 
% spaces to zeros). The range of ascii values is now 65-90.
% The word then gets mapped to a number base 26. These 
% base-26 numbers are then sorted from which A is then sorted
% along its rows, and the result is put into OUT.
%
%	lower(A)-32			maps a-Z (and A-Z) --> 65:90
%	size(A,2)			represents the length of a word
%	A'~=32 				is a matrix of ones and zeros with
%					zeros only where blanks show up
%					(the words are column-wise in A')
%	cumsum(A'~=32)			yields a cumulative sum 
%					(down the columns)
%	(26.^(wordlength-cumsum(*)))	is our mapping matrix to
%					map words to numbers in base 26
%	sum( (lower(A')-32) .* (26.^(w-cumsum(*))) )
%					represents the words in base 26
%	[OUT,j] = sort(words base 26)	sorts the base 26 values
%					with sorted values in X, 
%					and sorted indices in j
%	OUT=A(j,:) 			reorders the rows of A 
%					(i.e. the lexical words) in 
%					to alphabetical order

function OUT = lexsort(A)

[OUT, j] = sort( sum( (lower(A')-32) .* ...
			(32 .^ (size(A,2)-cumsum(A'~=32)) ) ) );
OUT = A(j,:);

