% isoctave: True if running under Octave
%
% test = isoctave();
%
% ARGUMENTS
%  none
%
% RETURNS
%  1 if Octave (version number < 5)
%  0 otherwise (version number >=5) which implies Matlab
%
% NOTES
%  This is very crude and makes a couple of nasty assumptions:
%   No Matlab user would use a version less than 5
%   Octave will never get to version >= 5
%   Matlab and Octave won't fuck with the version function.
%
%  However, the simple API means if Octave ever provides the same
%  functionality as Matlab's ver(), then it will be easy to modify the code.
%
% SEE ALSO
%  version ver

% AUTHOR: Eric Tittley
%
% HISTORY
%  080118 First version
%
% COMPATIBILITY: Matlab, Octave

function test = isoctave()
v = version;
test = str2num(v(1))<5;
