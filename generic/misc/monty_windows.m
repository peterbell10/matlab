function [num_winners_if_keep,num_winners_if_swap]=monty_windows(num_attempts)
winners=ceil(3*rand(num_attempts,1));
windows=0*winners*[1:3];
windows((winners-1)*num_attempts+[1:num_attempts]')=1&winners;
rand_choice_of_losers=ceil(2*rand(num_attempts,1));
first_choice=ceil(3*rand(num_attempts,1));
for k=1:num_attempts
 losers=find(~windows(k,:));
 if windows(k,first_choice(k))
  revealed_door(k)=losers(rand_choice_of_losers(k));
 else
  if first_choice(k)==losers(1)
   revealed_door(k)=losers(2);
  else revealed_door(k)=losers(1);
  end
 end  
end
exposed_doors=0*windows;
exposed_doors((first_choice-1)*num_attempts+[1:num_attempts]')=1&first_choice;
exposed_doors((revealed_door'-1)*num_attempts+[1:num_attempts]')=1&revealed_door';
num_winners_if_keep=sum(windows((first_choice-1)*num_attempts+[1:num_attempts]'));
num_winners_if_swap=sum(windows(find(~exposed_doors)));
