function gamma = nu2gamma(nu,B);
% nu2gamma: The lorentz factor for radiation in a magnetic field.
%
% An estimate of the lorentz factor, gamma, for radiation of frequency,
% nu (in MHz), in a magnetic field of B microgauss.
%
% syntax gamma = nu2gamma(nu,B);
%
% ARGUMENTS
%  nu	The frequency (MHz)
%  B	The magnetic field (microgauss)
%
% RETURNS
%  gamma	The Lorentz factor.
%
% NOTES:
%  Theory from page 177 of Shu, 'Physics of Astrophysics,
%  Volume 1, Radiation.'  Synchrotron Theory simple version.
%
%  This is an order of magnitude calculation only, hopefully
%  a more advanced version of this program will be written later.
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: D Hudson
%
% HISTORY:
%  02 22 07 First version
%  07 10 29 Modified comments.
%
% COMPATIBILITY: Matlab, Octave

% Constants
c = 2.99792458e10; % speed of light cm/s
m_e = 9.109389754e-28; % electron mass in grams
e = 1.6021773349e-19*c*1e-1; % electron charge in esu => esu = c x 1e-1 Coulombs

gamma = sqrt((2*pi*m_e*c*1e12/e)*nu./B); 
