% physics/cooling: Cooling functions for gas physics
%
% CollisionalCoolingSW91: Cooling via Collisional Excitation of Neutral Hydrogen
%                         Scholz & Waters '91 (ApJ 380 302)
