% CollisionalCoolingSW91: Cooling via Collisional Excitation of Neutral Hydrogen
%                         Scholz & Waters '91 (ApJ 380 302)
%
%  Lambda=CollisionalCoolingSW91(T)
%
% ARGUMENTS
%  T     Temperature [K]
%
% RETURNS
%  Cooling Rate factor which must be multiplied by n_e * n_H1
%  In CGS units.
%  Multiply by 1e-13 to convert to MKS

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function Lambda=CollisionalCoolingSW91(T)



IndexLo=find(T<=1e5);
IndexHi=find(T>1e5);

dlo = [2.137913e2 -1.139492e2 2.506062e1 -2.762755 1.515352e-1 -3.290382e-3];
dhi = [2.7125446e2 -9.8019455e1 +1.4007276e1 -9.7808421e-1 +3.3562891e-2 ...
       -4.55332321e-4];

Lambda=0*T;

y1=log(T(IndexLo)); % ln(T)
y2=y1.*y1;
y3=y1.*y2;
y4=y1.*y3;
y5=y1.*y4;
Lambda(IndexLo)=1e-20*exp(dlo(1) + dlo(2)*y1 + dlo(3)*y2 + dlo(4)*y3 ...
                         +dlo(5)*y4 + dlo(6)*y5 - 1.184156e5./T(IndexLo));

y1=log(T(IndexHi)); % ln(T)
y2=y1.*y1;
y3=y1.*y2;
y4=y1.*y3;
y5=y1.*y4;
Lambda(IndexHi)=1e-20*exp(dlo(1) + dlo(2)*y1 + dlo(3)*y2 + dlo(4)*y3 ...
                         +dlo(5)*y4 + dlo(6)*y5 - 1.184156e5./T(IndexHi));

end % function
