function R=Reffective(Drag,Rguess,V,rho,T,A,Z)
% Reffective: The effective radius of an object experiencing a given drag.
%
% R=Reff(Drag,Rguess,V,rho,T,A);   % Neutral gas
% R=Reff(Drag,Rguess,V,rho,T,A,Z); % Ionized gas
%
% ARGUMENTS
%  Drag		The force of drag being experienced
%  Rguess	An initial guess at the effective radius
%  V	Velocity of the sphere wrt the medium [cm/s]
%  rho	Density of the medium [g/cm^3]
%  T		Temperature [K]
%  A		Atomic mass of the ion [amu]
%  Z		Charge per ion [optional] [esu]
%
% RETURNS
%  The effective radius such that a sphere of that radius would experience the
%  drag measured.
%
% REQUIRES
%  drag_force
%
% SEE ALSO
%  drag_coef drag_force dynamic_viscosity impact_parameter Reynolds_number

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 07 31 First Version
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

if(nargin==6)
 R=fminsearch(@Reff_diff,Rguess,[],Drag,V,rho,T,A);
elseif(nargin==7)
 R=fminsearch(@Reff_diff,Rguess,[],Drag,V,rho,T,A,Z);
else
 error('need 6 or 7 arguments')
end
%END

function err=Reff_diff(R,Drag,V,rho,T,A,Z)
%
% err=Reff_diff(R,Drag,V,rho,T,A,[Z]);

if(nargin==6)% neutral
 F=drag_force(V,R,rho,T,A);
elseif(nargin==7)% ionized
 F=drag_force(V,R,rho,T,A,Z);
else
 error('need 6 or 7 arguments')
end

err=abs(Drag-F);

%END
