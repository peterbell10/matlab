% Reynolds_number: The Reynold's number for flow around a sphere
%
% Re=Reynolds_number(diameter,velocity,density,T,A);   % neutral gas
% Re=Reynolds_number(diameter,velocity,density,T,A,Z); % ionized gas
%
% ARGUMENTS
%  diameter	Diameter of the sphere [cm]
%  velocity	Velocity of the sphere wrt the medium [cm/s]
%  density	Density of the medium [g/cm^3]
%  T		Temperature [K]
%  A		Atomic mass of the ion [amu]
%  Z		Charge per ion [optional] [esu]
%
% RETURNS
%  The Reynold's number (dimensionless)
%
% REQUIRES
%  drag_coef drag_force dynamic_viscosity impact_parameter Reffective
%
% SEE ALSO
%  dynamic_viscosity

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 07 31 First Version
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

function Re=Reynolds_number(diameter,velocity,density,T,A,Z)

% If T, Z, and Ai are given, then the gas is presumed to be ionized
%  and mu is not used
%
% eta is the dynamic viscosity [mass/( dist time)]
if(nargin==5) % the gas is unionized
 eta=dynamic_viscosity(T,density,A);
elseif(nargin==6)  % The gas is ionized
 eta=dynamic_viscosity(T,density,A,Z);
else
 error('need 5 or 6 arguments')
end
Re=diameter*velocity*density/eta;
