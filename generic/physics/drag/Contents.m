% physics/drag: Functions related to drag in ionised and neutral gasses.
%
% drag_coef		The coefficient of drag, given the Reynold's number
%
% drag_force		Force of drag on a sphere
%
% dynamic_viscosity	Dynamic viscosity of a medium
%
% impact_parameter	Debye radius divided by the true impact parameter for an
%                   	ionized gas.
%
% Reffective		The effective radius of an object experiencing a given drag.
%
% Reynolds_number	The Reynold's number for flow around a sphere
