% ionisation_cross_section: The ionisation cross sections for HI, HeI, & HeII
%
% sigma=ionisation_cross_section(nu)
%
% ARGUMENTS
%  nu   Frequency at which to measure the cross sections [Hz]
%
% RETURNS
%  [sigma_HI, sigma_HeI, sigma_HeII]    The cross sections, in m^2

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab
%
% HISTORY:
%  080812 Mature version
%  120116 Comments added

function sigma=ionisation_cross_section(nu)

sigma_T = [6.30 7.83 1.58]*1e-22;
nu_T    = [3.282 5.933 13.13]*1e15;
beta=[1.34 1.66 1.34];
s   =[2.99 2.05 2.99];

sigma = zeros([length(nu) 3]);
for i=1:3
 mask=find(nu>=nu_T(i));
 if( ~isempty(mask) )
  sigma(mask,i)=sigma_T(i)*(  beta(i)*(nu(mask)/nu_T(i)).^(-s(i)) ...
                            + (1-beta(i))*(nu(mask)/nu_T(i)).^(-s(i)-1) );
 end
end
