function S_out = Convert_Radio_fluxes(S_in,alpha,freq_in,freq_out);
% Convert_Radio_fluxes: Convert powerlaw radio fluxes from one frequency to another
%
% syntax S_out = Convert_Radio_fluxes(S_in,alpha,freq_in,freq_out);
%
% ARGUMENTS
%  S_in		The flux at freq_in (Jy)
%  alpha	The radio spectral index
%  freq_in	The frequency from which to convert (GHz)
%  freq_out	The frequency to which to convert (GHz)
%
% RETURNS
%  S_out	The flux at freq_out.
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 07 30 First version
%  07 10 29 Modified comments
%
% COMPATIBILITY: Matlab, Octave

Jy = 1e-23;  % 1 Jy = 10^-23 ergs cm^-2 s^-2 Hz^-1

S_out = exp(-alpha*(log(freq_out*1e9)-log(freq_in*1e9)) + log(S_in*Jy));
S_out = S_out/Jy;

%fprintf('\nFor %g Jy at %g GHz with a spectral index of %g,\n',S_in,freq_in,alpha)
%fprintf('the output flux is %g Jy at %g GHz.\n\n',S_out,freq_out)
