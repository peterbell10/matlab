% blackbody: Emissivity of a black body as set by Planck's Law
%
%  B=blackbody(x,T,WavelengthFlag)
%
% ARGUMENTS
%  x    Frequency/wavelength at which to sample the black body [Hz/m] (vector)
%  T    Temperature of the black body [K] (scalar)
%  WavelengthFlag       [Optional] Selects x to be:
%               frequency (WavelengthFlag=0)
%               wavelength (WavelengthFlag=1) [Default 0]
%
% RETURNS
%  B    The spectral irradiance,
%        Energy per Time per Area per Solid Angle per frequency
%       WavelengthFlag=0 => J m^-2      = W m^-2 s
%        Energy per Time per Area per Solid Angle per wavelength
%       WavelengthFlag=1 => J m^-3 s^-1 = W m^-2 m^-1

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function B=blackbody(x,T,WavelengthFlag)

% Physical Constants
h=6.62606957e-34; % J s
c=299792458; % m/s
k=1.3806488e-23; % J / K

if( nargin < 3 )
 WavelengthFlag=0;
end

switch WavelengthFlag
 case 0
  % In terms of frequency
  B = (2*h/c^2)*x.^3 ./ (exp((h/(k*T))*x) - 1 ); % W m^-2 s
 case 1
  % in terms of wavelength
  B = (2*h*c^2)./x.^5 ./ (exp((h*c/(k*T))./x) - 1 ); % W m^-2 m^-1
 otherwise
  disp(['Unknown case: WavelengthFlag=',num2str(WavelengthFlag)])
  B=nan;
  return
end
