% physics: Tools and functions based on physics.
%
% blackbody            Emissivity of a black body as set by Planck's Law
%
% cooling              Cooling functions for gas physics
%
% drag                 Functions related to drag in ionised and neutral gasses.
%
% ionisation_cross_sections Functions related to ionising radiation for H & He
%
% recombination        More function related to ionising radiation for H & He
%
% Convert_Radio_fluxes Converts radio fluxes from one freq to another.
%
% Determine_alpha      Finds the radio spectral index for two radio observations
%
% flux_eng_conv	       Converts energy of a powerlaw from one range to another
%
% nu2gamma 	       Estimates the Lorentz factor for synchrotron radiation
%
% SEE ALSO
%  astro
