function [flux,N] = flux_eng_conv(Gamma,flux_o,lo_o,hi_o,lo_f,hi_f);
% flux_eng_conv: Converts powerlaw energy from one range to another.
%
% syntax [flux,N] = flux_eng_conv(Gamma,flux_o,lo_o,hi_o,lo_f,hi_f);
%
% ARGUMENTS
%  Gamma	The photon index, 
%  flux_o	The original flux for the orginal energy range (erg/s cm^-2)
%  lo_o		The original lower energy bound (keV)
%  hi_o		The original high energy bound (keV) 
%  lo_f		The desired output low energy bound (keV)
%  hi_f		The desired output high energy bound (keV)
%
% RETURNS
%  flux		The output flux (ergs cm^-2 s^-1)
%  N		The Number of photons @ 1 keV
%
% SEE ALSO

% AUTHOR: Danny Hudson (?)
%
% HISTORY
%  020730 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

keV = 1.60217733e-09; % 1keV = 1.60217733e-09 ergs

% INTEGRAL(N@1keV*E^(-Gamma) * E dE) = Flux
% Since N @1keV this integral must be done in keV and converted to ergs.
% N@1keV (E_HI^(-Gamma+2)/(-Gamma+2) - E_LO^(-Gamma+2)/(-Gamma+2)) = Flux
% Convert to keV N = Flux / ((E_HI^(-Gamma+2)/(-Gamma+2) - E_LO^(-Gamma+2)/(-Gamma+2)) * 1.602e-9 ergs/keV)
N = flux_o/(hi_o^(-Gamma+2)/(-Gamma+2) - lo_o^(-Gamma+2)/(-Gamma+2))/keV;

flux = N*(hi_f^(-Gamma+2)/(-Gamma+2) - lo_f^(-Gamma+2)/(-Gamma+2))*keV;
