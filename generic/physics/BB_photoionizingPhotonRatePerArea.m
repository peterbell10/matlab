function N_dA=BB_photoionizingPhotonRatePerArea(T)

 nu0 = 3.282e15;
 N_dA = pi*integral(@(x)N_dA_dnu(x,T),nu0,1e21);

function x=N_dA_dnu(nu,T)
 h=6.62606957e-34; % J s
 x=blackbody(nu,T)./(h*nu);


