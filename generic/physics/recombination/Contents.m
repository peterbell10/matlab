% recombination: Recombination coefficients for Hydrogen and Helium
%
% alpha_A_HII: HII-->HI recombination coefficient:     Case A
%
% alpha_B_HII: HII-->HI recombination coefficient:     Case B
%
% alpha_A_HeII: HeII-->HeI recombination coefficient:   Case A
%
% alpha_B_HeII: HeII-->HeI recombination coefficient:     Case B
%
% alpha_A_HeIII: HeIII-->HeII recombination coefficient: Case A
%
% alpha_B_HeIII: See alpha_B_HII
