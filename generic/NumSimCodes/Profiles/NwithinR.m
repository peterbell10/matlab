function radius=NwithinR(dist,N);
%
% syntax: function radius=NwithinR(dist,N);
%
% For particles at a distance dist from a point, find the radius
% such that the number of particles within the radius is N;

[y,i]=sort(dist);
radius=(y(N+1)+y(N))/2;
