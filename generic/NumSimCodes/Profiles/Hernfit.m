function yfit=Hernfit(r,delta,rs)
% yfit=Hernfit(r,delta,rs)
%
% Returns the generalized density profile given by Hernquist.
% yfit is the density profile at the points given by the vector, r
% using the parameters delta and rs.

yfit=delta*rs^4./(r.*(rs+r).^3);
