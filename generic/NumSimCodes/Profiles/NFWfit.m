function yfit=NFWfit(r,delta,rs)
% yfit=NFWfit(r,delta,rs)
%
% Returns the generalized density profile given by Navarro, Frenk, and White.
% yfit is the density profile at the points given by the vector, r
% using the parameters delta and rs.

yfit=delta*rs^3./(r.*(rs+r).^2);
