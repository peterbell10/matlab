function sqrs=ETdiff(parameters,rfit,actual,sigma)
% ETdiff: Return the error for a set of ET parameters and a density profile.
%
% sqrs=ETdiff(parameters,rfit,actual,sigma)
%
% ARGUMENTS
%  parameters	parameters(1) log10(delta)
%		parameters(2) rs
%		parameters(3) alpha
%		parameters(4) beta
%  rfit		The distances
%  actual	rho(rfit) from the data
%  sigma	(optional) A vector of sigmas of length(rfit) to determine the
%		chisqr.  They should have units log10(rho).
%
% RETURNS
%  sqrs		The ChiSqr
%
% USAGE
%  P=fminsearch('ETdiff',[1 1],[],rfit,actual,sigma);
%
% SEE ALSO
%  ETfit

% AUTHOR: Eric Tittley
%
% HISTORY: 03 06 30 Mature version.  Added comments.

actual_log=log10(actual);
fit=ETfit(rfit,10^parameters(1),parameters(2),parameters(3),parameters(4));
if(nargin==3)
 sqrs=sum((log10(fit)-actual_log).^2);
else
 sqrs=chisqr(actual_log,log10(fit),sigma);
end

