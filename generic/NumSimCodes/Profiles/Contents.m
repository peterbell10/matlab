% Profiles: Routines to create and analyse halo profiles from N-body simulations
%
% The following are general profiles:
%
% Betafit	Beta model, for gas.
%
% ETfit		A discontinuous fit.
%
% ET2fit	A discontinous fit with a constant-value core and an exterior
%		dependancy that goes as r^(-log10(r)/2-1).
%
% ET3fit	A discontinous fit with a constant-value core.
%
% Hernfit	A Hernquist profile.
%
% NFWfit	NFW profile.
%
% SWfit		The generalised Syer and White profile.
%
% SWfit2	The same as SWfit, but r_s^beta has been incorporated
%		into the normalisation, to make it easier to fit.
% 
% The following are passed to a routine like FMINS to fit profiles
%
% Betadiff  ET2diff  ET3diff ETdiff  Herndiff  NFWdiff  SWdiff
%
% 
% bin_profile	The mean value and standard deviation of a data tag, binned,
%               with interpolation across empty bins
%
% FindSigmas    Tags data used to create a profile with the standard deviation
%
% interp_profiles
%		Interpolates profile data over those regions where there are
%		no particles.
%
% mean_profile	Combines a series of profiles and creates a mean profile
%               ignoring zeros
%
% ParticlesInCone Find the particles in a cone.
%
% rebin_profiles
%		Takes a series of profiles with different x vectors and
%		rebins/resamples to a new, common x vector.	
