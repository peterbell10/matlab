function [R,N,profile,sigma,dV] = ProfileHalo(r,z,centre,bins)

r_recentred = recentre(r,centre) - 0.5;
distance = sqrt(sum(r_recentred.^2));

[N,Index]=histc(distance,bins);

dV = [4/3*pi*(bins(2:end).^3 - bins(1:end-1).^3)]; 
R  = mean([bins(2:end) ; bins(1:end-1)]);

profile=0*N(1:end-1);
sigma=profile;
for i=1:length(profile)
 if(N(i)>0)
  extract = z(Index==i);
  profile(i) = mean(extract);
  sigma(i) = std(extract);
 end
end
N=N(1:end-1);
