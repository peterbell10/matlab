clear

% Isothermal, so set the temperature here
T=5e4; % K

dr=1e-4; % kpc

% M_gas / (M_gas + M_DM)
GasFraction = 0.04;

hdr.h100=0.7; SI='MKS'; Units
m_p=mum/mu;
k_b=1.38e-23;

r=[dr:dr:10]*kpc; % m
rs=kpc; % m
rho_DM=1e6*Rho_c*rs^3./(r.*(rs+r).^2); % kg
% The total Dark Matter Mass
m_DM_in_shell=4*pi*rho_DM.*r.^2*dr*kpc; % kg
sum(m_DM_in_shell) / Mo

dr=dr*kpc; % m

% Initial guess at RhoGasCore
RhoGasCore = rho_DM(1)*GasFraction;

rho_g = 0.0*rho_DM;
rho_g(1) = RhoGasCore;

RhoError=1;
rho_g(1) = RhoGasCore;
while(RhoError > 0.1 || abs(ScaleFactor-1)>0.1)

 rho_g_old = rho_g;


 m_in_shell=4*pi*(rho_DM+rho_g).*r.^2*dr; % kg
 CumMass=cumsum(m_in_shell); % kg
 %CumMass(end)
 % The total Dark Matter Mass
 %m_DM_in_shell=4*pi*rho_DM.*r.^2*dr; % kg
 %sum(m_DM_in_shell) / Mo
 %m_DM_in_shell(end)
 
 GravAccel = G*CumMass./r.^2; % m s^-2 

 for i=2:length(r)
  rho_g(i) = rho_g(i-1) * (1 - dr * (mu * m_p / (k_b * T)) * GravAccel(i) );
  if(rho_g(i)<0)
   rho_g(i)=0;
  end
 end

 RhoError = sum(abs(rho_g - rho_g_old))/sum(rho_g)

 % Scale the central gas 
 m_gas_in_shell=4*pi*(rho_g).*r.^2*dr; % kg
 Mass_gas = sum(m_gas_in_shell); % kg
 MassFraction = Mass_gas/CumMass(end)
 ScaleFactor = GasFraction * CumMass(end)/Mass_gas
 if(ScaleFactor<0.5)
  ScaleFactor=0.5;
 elseif(ScaleFactor>2)
  ScaleFactor=2;
 end
 rho_g = rho_g * ScaleFactor;

end
loglog(r,rho_g)

A=[r;rho_g];
save -ASCII GasProfile.dat A
