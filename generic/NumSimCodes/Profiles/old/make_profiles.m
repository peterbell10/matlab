function d=make_profiles(datafile,centres,tho,rhoo)
% d=make_profiles(datafile,centres,tho,rhoo)
%
% see also: profile_cluster

% Must be run in the directory with 'profile'

[num,n]=size(centres);

% set array size
d_temp=profile_cluster(datafile,centres(1,:),tho,rhoo);
d=zeros([size(d_temp),num]);
d(:,:,1)=d_temp;

for i=2:num
d(:,:,i)=profile_cluster(datafile,centres(i,:),tho,rhoo);
% cmd=['profile ',datafile,' ',num2str([centres(i,:),tho,rhoo]),' >! /tmp/temp_profile']
% eval(['!',cmd])
% load /tmp/temp_profile
% d(:,:,i)=temp_profile;
end
