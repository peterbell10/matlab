function [mean_prof,std_prof]=mean_profile(x_new,x_old,y_old,flag)
% [mean_prof,std_prof]=mean_profile(x_new,x_old,y_old)
% Given the [n,m] array,  y_old, of profiles (m profiles of length n)
%           [n,m] array,  x_old, of profile radii
%     and the [l] vector, x_new, of new radial bins
% an [n] vector, mean_prof, is returned which is the mean profile.
% An [n] vector, std_prof, is optionally returned giving the stddev
% of the sample.
%
% Notes: if the value at any one place is exactly 0, then the data
% point is ignored.  If this value is inappropriate, one may be chosen
% by setting the otional value, flag, to the value to skip.

%parameters
len=length(x_new);
if nargin==3, flag=0; end

%Set up the new bins:
bins=[0,(x_new(1:len-1)+x_new(2:len))/2,x_new(len)+(x_new(len)-x_new(len-1))/2];

for i=1:len
 mask=find(x_old>bins(i) & x_old<bins(i+1) & y_old~=flag);
 if(size(mask)>0)
  mean_prof(i)=mean(y_old(mask));
  std_prof(i) = std(y_old(mask));
 else
  mean_prof(i)=NaN;
  std_prof(i)=NaN;
 end
end
