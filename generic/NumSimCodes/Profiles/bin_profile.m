function [centres,new,new_std]=bin_profile(x_new,x_old,y_old)
% bin_profile: Bin data into profiles, returning standard error as well.
%
% [centres,new,std_new]=bin_profile(x_new,x_old,y_old)
%
% Given y_old		Data values (vector)
%       x_old		Data positions (vector)
%       x_new		Radial bin boundaries (vector)
%
% Return centres	The bin centres (vector)
%        new		The mean value of the values of y_old that fall
%                       in the each bin (vector)
%        new_std	The standard deviation of the values which fall
%                       in each bin (vector)

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 02 08 Error out when the input bins don't overlap the data
%  00 11 11 Interpolate new_std, as well as new.
%  03 06 30 Added comments.

badflag=-99999;

num_bins=length(x_new);
centres=(x_new(1:end-1)+x_new(2:end))/2;

%initialize arrays
new=0*centres;

% For each bin, find all the x positions that fall in the bin
% and use the values of these positions to create a mean and std dev
% for that bin.
% If we find nothing in the bin, flag it as bad
for i=1:num_bins-1
 sample=find(x_old>x_new(i) & x_old<x_new(i+1));
 if(~isempty(sample))
  new(i)=mean(y_old(sample));
  new_std(i)=std(y_old(sample));
 else
  new(i)=badflag;
 end
end

% For each of the bins marked as bad (no values were found for this
% bin), interpolate a value using the bins for which values were found.
bad=find(new==badflag);
if(~isempty(bad))
 good=find(new~=badflag);
 if(~isempty(good))
  new(bad)=interp1( centres(good), new(good), centres(bad) );
  new_std(bad)=interp1( centres(good), new_std(good), centres(bad) );
 else
  error('bin_profile: the input data does not overlap with the bins')
 end
end
