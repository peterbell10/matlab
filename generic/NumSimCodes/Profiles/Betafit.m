function yfit=Betafit(r,delta,rs,beta)
% yfit=Betafit(r,delta,rs,beta)
%
% Returns the generalized Beta-fit density profile.
% yfit is the density profile at the points given by the vector,
% r, using the parameters delta, rs, alpha, and beta.
% This is generally used to fit to gas halos.

yfit=delta*(1+(r/rs).^2).^(-3*beta/2);
