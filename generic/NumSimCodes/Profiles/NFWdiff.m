function sqrs=NFWdiff(parameters,rfit,actual,sigma)
% NFWdiff: Return the error for a set of NFW parameters and a density profile.
%
% sqrs=NFWdiff(parameters,rfit,actual,sigma)
%
% ARGUMENTS
%  parameters	parameters(1) log10(delta)
%		parameters(2) rs
%  rfit		The distances
%  actual	rho(rfit) from the data
%  sigma	A vector of sigmas of length(rfit) to determine the chisqr
%
% RETURNS
%  sqrs		The ChiSqr
%
% USAGE
%  P=fminsearch('NFWdiff',[1 1],[],rfit,actual,sigma);
%
% SEE ALSO
%  NFWfit

% AUTHOR: Eric Tittley
%
% HISTORY: 03 06 26 Mature version.  Added comments.

actual_log=log10(actual);
fit=NFWfit(rfit,10^parameters(1),parameters(2));
%sqrs=mean(abs(log10(fit)-actual_log));
sqrs=chisqr(actual_log,log10(fit),sigma);
