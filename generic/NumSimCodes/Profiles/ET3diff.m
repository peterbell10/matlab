function sqrs=ET3diff(parameters,rfit,actual,sigma)

actual_log=log10(actual);
fit=ET3fit(rfit,10^parameters(1),parameters(2),parameters(3));
sqrs=chisqr(actual_log,log10(fit),sigma);
