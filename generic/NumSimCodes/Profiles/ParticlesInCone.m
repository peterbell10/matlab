function [index,u]=ParticlesInCone(r,x0,v,phi)
% ParticlesInCone: Find the particles in a cone.
%
% [index,u]=ParticlesInCone(r,x0,v,phi)
%
% ARGUMENTS
%  r	The particle positions (3,N)
%  x0	The origin of the cone (x,y,z)
%  v	The vector direction of the centre of the cone openning.
%  phi	The angle of openning of the cone, in degrees.

% AUTHOR: Eric Tittley
%
% HISTORY
%  040331 First version

% Convert to radians
phi = phi/180*pi;

% Find a second point on the line.
x1=x0+v;

% Find the distances for all the particles from the line.
% d(i) = distance of point r(:,i) to the line defined by x0 & x1
% u(i) = distance from x0 to the point on the line closest to r(:,i)
[d,u]=DistanceFromPointToLine(x0,x1,r');

% Find the angle for each point away from the centre of the cone.
phis=atan(d./u);

% Find all the points in the cone.
index=find( u>0 & phis<phi );

% Return just the distances of the particles in the cone.
u=u(index);
