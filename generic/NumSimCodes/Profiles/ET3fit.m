function yfit=ET3fit(r,tau,rs,beta)
% yfit=ET3fit(r,tau,rs,beta)
%
% Returns the generalized density profile given by Eric Tittley.
% yfit is the density profile at the points given by the vector,
% r, using the parameters delta, rs, alpha, and beta.

beta=-1*beta;

delta_beta=tau*rs^(-beta);
mask=r<rs;
yfit=0*r;
yfit(mask)=tau*(1&r(mask));
yfit(~mask)=delta_beta*r(~mask).^beta;
