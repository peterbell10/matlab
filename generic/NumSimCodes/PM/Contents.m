% PM: Tools for Martin White's PM code
%
% loadPMic         Read a PM inititial conditions file containing the PM
%                  (particle) info.
%
% loadPMic_double  Read a PM inititial conditions file containing the PM
%                  (particle) info. Double precision.
%
% writePMic        Write a PM inititial conditions file.
