function plot_cell_mean_C_fraction(Times,Fractions,Cell,LOS)

mask=[1:length(Times)-1];

clf
sum=zeros(length(mask),1);
for i=9:15
 sum = sum + (i-8)*squeeze(Fractions(i,Cell,LOS,mask));
end
plot(Times(mask),sum)
