% MovieDiffMeanFraction: Generate a movie of the difference in mean species state from Timedep output
%
% MovieDiffMeanFraction(F1,F2,SpeciesRange,MovieName,[PolarFlag],[Redshifts])
%
% ARGUMENTS
%  F1 [Nspecies,NCells,NLOS,Ntimes]
%  F2 [Nspecies,NCells,NLOS,Ntimes]
%
% RETURNS
%  Nothing
%
% REQUIRES
%  movies (Ocvate only)
%  movies requires the ffmpeg system tools installed.
%
% SEE ALSO
%  movies (Octave only), addframe, aviobj (Matlab only), MovieMeanFraction

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab, though the output varies
%
% HISTORY
%  100831 First version, based on MovieMeanFraction

function MeanState=MovieDiffMeanFraction(F1,F2,SpeciesRange,MovieName,PolarFlag,Redshifts)

if(nargin==4)
 PolarFlag=0;
 RedshiftFlag=0;
end
if(nargin==5)
 RedshiftFlag=0;
end
if(nargin==6)
 RedshiftFlag=1;
end

if(ndims(F1)==4)
 [NSpecies,NCells,NLOS,NTimes]=size(F1);
else
 error('Please pass F1 in the form F1(NSpecies,NCells,NLOS,NTimes)')
end

if(ndims(F2)==4)
 [NSpecies,NCells,NLOS,NTimes]=size(F2);
else
 error('Please pass F1 in the form F2(NSpecies,NCells,NLOS,NTimes)')
end

% Extract the range of Species we want to display
[Nspecies,Nlos,Ncells,Ntimes]=size(F1);
if(Nspecies ~= length(SpeciesRange))
 F1=F1(SpeciesRange,:,:,:);
 F2=F2(SpeciesRange,:,:,:);
end

% If any are <0, set to 0 ( usually a small fraction are slightly < 0 )
F1(F1<0)=0;
F2(F2<0)=0;

MeanState=zeros(NCells,NLOS,NTimes-1);
NSpecies = length(SpeciesRange);
% The fractions are the weights, and we're averaging the States (CI, CII, etc..)
for i=1:NSpecies
 F1(i,:,:,:)=F1(i,:,:,:)*i;
 F2(i,:,:,:)=F2(i,:,:,:)*i;
end

% Weighted means, where the fractions in each state are the weights (sum to 1)
for i=1:NTimes-1
 Diff(:,:,i)=sum(F2(:,:,:,i),1)-sum(F1(:,:,:,i),1);
end

Range=[-2 2];

% Display one frame to initialise the figure
clf
if(PolarFlag)
 ph=imagesc_polar(squeeze(Diff(:,:,1)),Range);
else
 imagesc(squeeze(Diff(:,:,1)),Range)
end

% Initialise the movie
if(isoctave)
 movie('init',MovieName)
else
 aviobj = avifile(MovieName);
end

th=[];

for i=1:NTimes-1
 % Display the frame
 if(PolarFlag)
  ph=imagesc_polar(squeeze(Diff(:,:,i)),Range);
  cb=colorbar('Ylim',Range);
  cb_im=get(cb,'child');
  set(cb_im,'Ydata',Range);
  ca=gca;
  axes(cb);
  ylabel('\Delta <State>')
  axes(ca);
  % Need a list of redshifts for the following to work
  %text(0,1,['z=',num2str('1.3')]);
 else
  imagesc(squeeze(Diff(:,:,i)),Range)
  cb=colorbar;
  ca=gca;
  axes(cb);
  ylabel('\Delta <State>')
  axes(ca);
 end
 
 if(RedshiftFlag)
  % erase the previous text
  if(~ isempty(th) )
   set(th,'Visible','off')
  end
  % print the redshift
  th=text(-1,1.05,['z=',num2str(Redshifts(i))]);
 end

 % Grab the frame for the movie
 if(isoctave)
  movie('add',MovieName);
 else
  Frame=getframe(gcf);
  aviobj = addframe(aviobj,Frame);
 end
end

% Close the movie
if(isoctave)
 movie('close',MovieName,3);
else
 aviobj = close(aviobj);
end
