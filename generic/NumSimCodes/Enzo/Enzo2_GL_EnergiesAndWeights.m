% Enzo2_GL_EnergiesAndWeights: The energies and weights to pass to Enzo2 for
%                              photon-packet radiative transfer.
%
% [Es,Is]=Enzo2_GL_EnergiesAndWeights(Levels,alpha);
%
% ARGUMENTS
%  Levels	The number of levels in each interval [L1 L2 L3]
%  alpha	The powerlaw dependency L/Lo = (nu/nu_o)^alpha
%               Note: Since Enzo2 expects numbers of photons, the SED
%               will go as (nu/nu_o)^(alpha-1)
%
% RETURNS
%  Es		The energies to set PhotonTestSourceSED to
%  Is		The intensities to set PhotonTestSourceEnergy to

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab/Octave
%
% TODO

function [Es,Is]=Enzo2_GL_EnergiesAndWeights(Levels,alpha)

% Threshold Energies [eV]
% EThresh=[14.6 25.6 56.4]; % These come from Enzo's docs via misinterpretation
EThresh=[13.59843449 24.58738880 54.4177650];

% First interval
[xs,ws]=GaussLegendreWeights(Levels(1));
Es=EThresh(1)+(EThresh(2)-EThresh(1))*(xs+1)/2;
Ws = (EThresh(2)-EThresh(1))/EThresh(1) * ws;

% Second interval
[xs,ws]=GaussLegendreWeights(Levels(2));
Es=[Es,EThresh(2)+(EThresh(3)-EThresh(2))*(xs+1)/2];
Ws=[Ws, (EThresh(3)-EThresh(2))/EThresh(1) * ws];

% Third interval
[xs,ws]=GaussLaguerreWeights(Levels(3));
Es=[Es,EThresh(3)*(xs+1)];
Ws=[Ws, EThresh(3)/EThresh(1) * ws];

% Multiply the spectrum by the weights
Is = Ws.*(Es/EThresh(1)).^(alpha-1);

% And normalise. Need to do this because Enzo2 expects the SED to sum to unity.
% But note that this will throw off the integral normalisation
Is = Is/sum(Is);

% Or will it?
% If I have done this right, sum(PhotonTestSourceLuminosity*Is) should equal
% PhotonTestSourceLuminosity (which is total # of photons),
% i.e. sum(Is) should equal 1.
