% Enzo_WriteICs: Write data to Enzo-ready IC files
%
% Enzo_WriteICs(Data,Attributes,Dir,[GasGridOnly])
%
% ARGUMENTS
%  Data 	Structure with elements:
%   r            Particle positions [3xM] [0, 1) or junk         Code Units
%   v            Particle velocities [3xM] or [Res x Res x Res x 3] Code Units
%   rho_gas      Grid of gas densities  [Res x Res x Res]        Code Units
%   v_gas        Grid of gas velocities [Res x Res x Res x 3]    Code Units
%  Attributes   Structure of attribute (header) values
%   GridDensity
%   GridVelocities
%   ParticlePositions
%   ParticleVelocities
%  Dir          The directory in which to write the IC files
%
% NOTE
%  Write all values as doubles and long integers. Enzo must be compiled accordingly.
%
%  Attributes can be read from other IC files via:
%   Info=h5info(GasDensityFile,'/');
%   Attributes.GridDensity=Info.Attributes;
%
%  Read the files with h5read, like:
%   D=h5read('GridDensity','/GridDensity');
%
% RETURNS
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab
%
% TODO
%  Octave compatibility


function Enzo_WriteICs(Data, Attributes, Dir, GasGridOnly)

if(nargin==3)
 GasGridOnly =1;
end

D=dir(Dir);
if(isempty(D))
 [SUCCESS,MESSAGE,MESSAGEID] = mkdir(Dir)
end

% Odd how all are plural except GridDensity
ParticlePositions  = ['/ParticlePositions' ];
ParticleVelocities = ['/ParticleVelocities'];
GridDensity        = ['/GridDensity'       ];
GridVelocities     = ['/GridVelocities'    ];

ParticlePositionFile=[Dir,ParticlePositions ];
ParticleVelocityFile=[Dir,ParticleVelocities];
GridDensityFile     =[Dir,GridDensity       ];
GridVelocityFile    =[Dir,GridVelocities    ];

if(~GasGridOnly)
 Size=size(Data.r);
 if(length(Size)==2)
  WriteListData(ParticlePositionFile, ParticlePositions,  Data.r);
 else
  WriteGridData(ParticlePositionFile, ParticlePositions,  Data.r);
 end
 WriteRootAttributesToFile(ParticlePositionFile,Attributes.ParticlePositions);

 if(length(Size)==2)
  WriteListData(ParticleVelocityFile, ParticleVelocities,  Data.r);
 else
  WriteGridData(ParticleVelocityFile, ParticleVelocities, Data.v);
 end
 WriteRootAttributesToFile(ParticleVelocityFile,Attributes.ParticleVelocities);
end % not GasGridOnly

WriteGridData(GridDensityFile,GridDensity,Data.rho_gas);
WriteRootAttributesToFile(GridDensityFile,Attributes.GridDensity);

WriteGridData(GridVelocityFile,GridVelocities,Data.v_gas);
WriteRootAttributesToFile(GridVelocityFile,Attributes.GridVelocities);

% END main

function WriteGridData(File,Label,Data)
 Size=size(Data);
 if( exist(File,'file') )
  delete(File);
 end
 h5create(File, Label, Size, 'Datatype','double');
 h5write( File, Label, Data);
 % Add four attributes
 if(length(Size)==3)
  Size=[Size 1];
 end
 h5writeatt(File,Label,'Component_Rank',Size(4));
 h5writeatt(File,Label,'Component_Size',prod(Size(1:3)));
 h5writeatt(File,Label,'Dimensions',Size(1:3));
 h5writeatt(File,Label,'Rank',length(Size(1:3)));
 % I think these are just fillers
 h5writeatt(File,Label,'TopGridStart',[0 0 0]);
 h5writeatt(File,Label,'TopGridEnd',-100000*[1 1 1]);
 h5writeatt(File,Label,'TopGridDims',-99999*[1 1 1]);
% END WriteListData
% END WriteGridData

function WriteListData(File,Label,Data)
 % For data that is a list of particle positions or velocities
 % [N,3]
 Size=size(Data);
 if(length(Size)~=2)
  disp('ERROR: WriteListData expects Data to be [N,3]')
  return
 end
 if( Size(1)==3 & Size(2)~=3 )
  Data=Data';
  Size=size(Data);
 end
 if( exist(File,'file') )
  delete(File);
 end
 h5create(File, Label, Size, 'Datatype','double');
 h5write( File, Label, Data);
 % Add four attributes
 h5writeatt(File,Label,'Component_Rank',Size(2));
 h5writeatt(File,Label,'Component_Size',Size(1));
 h5writeatt(File,Label,'Rank',1);
 h5writeatt(File,Label,'Dimensions',Size(1));
 % I think these are just fillers
 h5writeatt(File,Label,'TopGridStart',[0 0 0]);
 h5writeatt(File,Label,'TopGridEnd',-100000*[1 1 1]);
 h5writeatt(File,Label,'TopGridDims',-99999*[1 1 1]);
% END WriteListData

function WriteRootAttributesToFile(File,Attributes)
 for iAttribute=1:length(Attributes)
  h5writeatt(File, '/', Attributes(iAttribute).Name, Attributes(iAttribute).Value);
 end
% END WriteRootAttributesToFile
