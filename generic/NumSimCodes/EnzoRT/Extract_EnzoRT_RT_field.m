% Extract_EnzoRT_RT_field: Extract a field from a set of EnzoRT RT files.
%
% Field = Extract_EnzoRT_RT_field(z, grids, span, field, Dir)
%
% EnzoRT generates multiple RT files for the same timestep, each corresponding
% to a node. Extract_EnzoRT_RT_field() extracts a single field from the
% multiple RT files.
%
% ARGUMENTS
%  z          Redshift
%  grids      The layout of the grids. See note
%  span       If only a subset is required. Otherwise set to 0.
%             This is a tricky one, depending on the subgrid layout
%             and the total grid size
%             Set it to the range of LOSs in each Subgrid that you want.
%             For example, a 512^3 grid on 8 processors will be split into
%             8 256^3 grids
%             If there are three slices, and you only want the middle slice from
%             each, then slice=257:512
%             
%  field      The field requested.
%  Dir        The directory the files are located in [optional]
%
% RETURNS
%  Field
%
% NOTES
%  Grid layout
%   grids will change depending on the LOSs used.  But here are some guesses
%   to get you going.
% case 8
%  grids=[0 4; 3 7] or [0 5; 6 7]
% case 16
%  grids=[0 12 8 4; 15 11 7 3];
%
% REQUIRES
%  loadPMRTdata
%
% SEE ALSO
%  loadPMRTdata, enzo_merge_outputs

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% HISTORY
%  08 06 12 First version.
%  08 08 11 Brought into the main library tree. Added comments.
%  08 11 10 Added geometry for 8 processors.
%  10 07 20 Modified to take in grids, not guess at it. This is a major
%       change to the API to match the now redundant Merge_EnzoRT_RT_files.

function Field = Extract_EnzoRT_RT_field(z, grids, span, field, Dir)

if(nargin==4)
 Dir='./';
end

CWD=pwd;

cd(Dir)
% Read in the data files
for i=1:numel(grids)
 file=['RT_Z',num2str(z,'%.3f'),'_P',int2str_zeropad(grids(i),2),'.data'];
 D(i).D=loadPMRTdata(file);
end
cd(CWD)

% Merge the fields
Dims=size(grids);
i=0;
Field=[];
for col=1:Dims(2)
 Col=[];
 for row=1:Dims(1)
  i=i+1;
  if( isscalar(span) & (span==0) )
   Col=[Col;[D(i).D.(field)]];
  else
   Col=[Col;[D(i).D(span).(field)]];
  end
 end
 Field=[Field,Col];
end
