% riPMRTIonSumsFile: Number of photoionizations in a RT file.
%
% [rf,Nhiif,Nheiif,Nheiiif] = riPMRTIonSumsFile(filename)
%
% ARGUMENTS
%  filename       Filename for results from Enzo run (results*.dat)
%
% RETURNS
% rf              Fluid zone (kpc)
% Nhiif           Cumulative number of hydrogen ions.
% Nheiif          Cumulative number of He+ ions.
% Nheiiif         Cumulative number of He++ ions.
%   All variables returned as column arrays.
%
% SEE ALSO
%  riPMRTESumsFile

% COMPATIBILITY: Matlab(?), Octave
%
% AUTHOR: Avery Meiksin
%
% HISTORY:
%  29 11 12 Creation date. (After riCDRTIonSumsFile.m.)
%
function [rf,Nhiif,Nheiif,Nheiiif] = riPMRTIonSumsFile(filename)
kpc = 3.0856e19;        %kpc in m
dat = loadPMRTdata(filename);
rf = dat.R/ kpc;        %convert m to kpc
hdenf = dat.n_H;        %total hydrogen density (per m^3)
hedenf = dat.n_He;      %total helium density (per m^3)
XHII = dat.f_H2;        %HII fraction
XHeII = dat.f_He2;      %HeII fraction
XHeIII = dat.f_He3;     %HeIII fraction
hiidenf = max(0,XHII).*hdenf;
heiidenf = max(0,XHeII).*hedenf;
heiiidenf = max(0,XHeIII).*hedenf;
Volf = (pi/0.75)*(rf.*rf.*rf);                      %Vol in kpc^3
dVolf = kpc*kpc*kpc*diff(cat(1,[0],Volf));          %dVol in m^3
Nhiif = cumsum(hiidenf.*dVolf);
Nheiif = cumsum(heiidenf.*dVolf);
Nheiiif = cumsum(heiiidenf.*dVolf);
