% RT_Gamma_probabilistic: The local ionisation/ionization rate in radiative
%                         transfer.  Probabilistic formulation, for tau >= 1.
%
% [Gamma_H1,Gamma_He1,Gamma_He2]=RT_Gamma_probabilistic(N_H1_cum,N_He1_cum,N_He2_cum,R,z,Spectrum)
% [Gamma_H1,Gamma_He1,Gamma_He2]=RT_Gamma_probabilistic(Data,z,Spectrum)
%
% ARGUMENTS
%  N_H1_cum     Cumulative column density to the cell of H1  [m^-2]
%  N_He1_cum    Cumulative column density to the cell of He1 [m^-2]
%  N_He2_cum    Cumulative column density to the cell of He2 [m^-2]
%  n_H1         Number density of HI in the cell [m^-3]
%  n_He1        Number density of HeI in the cell [m^-3]
%  n_He2        Number density of HeII in the cell [m^-3]
%  R            Distance from the source to the cell         [m]
%  z            Redshift (used only for providing a time ref for the source)
%  Spectrum     The spectrum of the source.  See RT_Luminosity [struct]
%
% or
%
%  Data         A structure with elements:
%                column_H1
%                column_He1
%                column_He2
%                R
%  z            Redshift (used only for providing a time ref for the source)
%  Spectrum     The spectrum of the source.  See RT_Luminosity [struct]
%  
% RETURNS
%  Gamma_H1         Ionisation rate for H1  [s^-1]
%  Gamma_He1        Ionisation rate for He1 [s^-1]
%  Gamma_He2        Ionisation rate for He2 [s^-1]
%
% REQUIRES
%  RT_Luminosity
%
% SEE ALSO
%  RT_Gamma, RT_Luminosity

% AUTHOR: Eric Tittley
%
% HISTORY
%  110614 First version.
%
% COMPATIBILITY: Matlab, Octave

function [Gamma_H1,Gamma_He1,Gamma_He2]=RT_Gamma_probabilistic(varargin)

switch length(varargin)
 case 3
  Data      = varargin{1};
  N_H1_cum  = [Data.column_H1];
  N_He1_cum = [Data.column_He1];
  N_He2_cum = [Data.column_He2];
  n_H1      = [Data.f_H1] .*[Data.n_H] ; % m^-3
  n_He1     = [Data.f_He1].*[Data.n_He]; % m^-3
  n_He2     = [Data.f_He2].*[Data.n_He]; % m^-3
  R         = [Data.R];
  z         = varargin{2};
  Spectrum  = varargin{3};
 case 6
  N_H1_cum  = varargin{1};
  N_He1_cum = varargin{2};
  N_He2_cum = varargin{3};
  n_H1      = varargin{4};
  n_He1     = varargin{5};
  n_He2     = varargin{6};
  R         = varargin{7};
  z         = varargin{8};
  Spectrum  = varargin{9};
 otherwise
  error(['RT_Gamma_probabilistic: Incorrect number of input arguments: ', ...
        int2str(length(varargin))])
end

% Check the arguments

% We can only work with redshift being a scalar
if(~isscalar(z))
 error('RT_Gamma_probabilistic: z must be a scalar')
end

% Now, were we given a list of cells, or a single cell?
%  No matter what they are, we only need them as vectors
%  But first remember what size they were
[NArr,MArr]=size(N_H1_cum);
N_H1_cum=N_H1_cum(:);
N_He1_cum=N_He1_cum(:);
N_He2_cum=N_He2_cum(:);
R=R(:);
%  How many?
N=length(N_H1_cum);
if(length(N_He1_cum)~=N)
 error('RT_Gamma_probabilistic: N_H1_cum, N_He1_cum, N_He2_cum, and R must be the same size')
end
if(length(N_He2_cum)~=N)
 error('RT_Gamma_probabilistic: N_H1_cum, N_He1_cum, N_He2_cum, and R must be the same size')
end
if(length(R)~=N)
 error('RT_Gamma_probabilistic: N_H1_cum, N_He1_cum, N_He2_cum, and R must be the same size')
end

% Get column densities in the cell(s)
if(N==1)
 error('Sorry, I can only handle vectors at the moment')
end
% Add a final bin edge to R, which is currently a list of the lower edges of
% the cell boundaries
R=[R; R(end,:)+(R(end,:)-R(end-1,:))]; % m
dR=diff(R); % m
N_H1  = n_H1  .* dR; % m^-2
N_He1 = n_He1 .* dR; % m^-2
N_He2 = n_He2 .* dR; % m^-2

%  If we have been given a list of cells, re-assign, since the nested functions
%  expect R & N_* to be scalars
if(N>1)
 N_H1_cum_vec  = N_H1_cum;
 N_He1_cum_vec = N_He1_cum;
 N_He2_cum_vec = N_He2_cum;
 N_H1_vec      = N_H1 ;
 N_He1_vec     = N_He1;
 N_He2_vec     = N_He2;
 R_vec         = R;
end

% Globals
% Constant
h_p = 6.6260688e-34; % Planck constant. [J s]
% Ionisation thresholds
nu_0 = 3.282e15; % Hz
nu_1 = 5.933e15; % Hz
nu_2 = 1.313e16; % Hz

nu_inf = 1000*nu_2;

if(N==1)
 % Use Gauss-Kronrod
 IntFun=@quadgk;
 Gamma_H1  = IntFun(@dGamma_dnu_H1_H2,  nu_0,nu_inf);
 Gamma_He1 = IntFun(@dGamma_dnu_He1_He2,nu_1,nu_inf);
 Gamma_He2 = IntFun(@dGamma_dnu_He2_He3,nu_2,nu_inf);
else
 % Allocate
 Gamma_H1 =0*N_H1_cum;
 Gamma_He1=0*N_H1_cum;
 Gamma_He2=0*N_H1_cum;
 for i=1:N
  N_H1_cum  = N_H1_cum_vec(i);
  N_He1_cum = N_He1_cum_vec(i);
  N_He2_cum = N_He2_cum_vec(i);
  N_H1      = N_H1_vec(i);
  N_He1     = N_He1_vec(i);
  N_He2     = N_He2_vec(i);
  R         = R_vec(i);
  % Use Gauss-Kronrod to get Gamma * n * dR
  IntFun=@quadgk;
  Gamma_H1(i)  = IntFun(@dGamma_dnu_H1_H2,  nu_0,nu_inf);
  Gamma_He1(i) = IntFun(@dGamma_dnu_He1_He2,nu_1,nu_inf);
  Gamma_He2(i) = IntFun(@dGamma_dnu_He2_He3,nu_2,nu_inf);
 end
 % Scale by (n * dR)=N (cell column density)
 Gamma_H1  = Gamma_H1  ./ N_H1_vec;
 Gamma_He1 = Gamma_He1 ./ N_He1_vec;
 Gamma_He2 = Gamma_He2 ./ N_He2_vec;
 % Return as the same shape as input
 Gamma_H1  = reshape(Gamma_H1, NArr,MArr);
 Gamma_He1 = reshape(Gamma_He1,NArr,MArr);
 Gamma_He2 = reshape(Gamma_He2,NArr,MArr);
end

% Nested functions

function y = dGamma_dnu_H1_H2(nu)
 y=0*nu;
 mask=find(nu>nu_0);
 if(~isempty(mask))
  [L,e1,e2,e3,norm]=CommonIntegral(nu(mask));
  y(mask) = L .* ((1 - e1 ) .* e2 .* e3 .* norm) / h_p;
 end
end

function y = dGamma_dnu_He1_He2(nu)
 y=0*nu;
 mask=find(nu>nu_1);
 if(~isempty(mask))
  [L,e1,e2,e3,norm]=CommonIntegral(nu(mask));
  y(mask) = L .* ((1 - e2) .* e1 .* e3 .* norm) / h_p;
 end
end

function y = dGamma_dnu_He2_He3(nu)
 y=0*nu;
 mask=find(nu>nu_2);
 if(~isempty(mask))
  [L,e1,e2,e3,norm]=CommonIntegral(nu(mask));
  y(mask) = L .* ((1 - e3) .* e1 .* e2 .* norm) / h_p;
 end
end

function [L,e1,e2,e3,norm]=CommonIntegral(nu)
 tau_1 = H1_H2(nu)   * N_H1;
 tau_2 = He1_He2(nu) * N_He1;
 tau_3 = He2_He3(nu) * N_He2;
 e1 = exp(-tau_1);
 e2 = exp(-tau_2);
 e3 = exp(-tau_3);

 % The transmission factor.  What fraction of light has reached this cell.
 Trans = exp(-(  H1_H2(nu)   * N_H1_cum  ...
               + He1_He2(nu) * N_He1_cum ...
	       + He2_He3(nu) * N_He2_cum ));
 
 % Normalization
 enumu = (1 - e1.*e2.*e3); % Always between 0 and 1
 denom =  ((1 - e1) .* e2 .* e3 ) ...
        + ((1 - e2) .* e1 .* e3 ) ...
        + ((1 - e3) .* e1 .* e2 );
 norm=0*enumu;
 mask=find(denom > realmin);
 if(~isempty(mask))
  norm(mask) = enumu(mask) / denom(mask);
 end
 mask=find(denom <= realmin);
 if(~isempty(mask))
  norm(mask) = enumu(mask) * realmax;
 end

 L=RT_Luminosity(nu,z,Spectrum)./nu .* Trans;
 if(Spectrum.PLANE_WAVE)
  % Intensity is luminosity attentuated by 1/R_0^2 and optical depth.
  % R_0 is actually R_0 * aa * Mpc / h  The calling function needs to set this
  % correctly
  L = L./(4*pi*(Spectrum.Ro)^2);
 else
  % Intensity is luminosity attentuated by 1/r^2 and optical depth.
  L = L./(4*pi*R^2);
 end

end % CommonIntegral

function cs=H1_H2(nu)
 sigma = 6.3e-22;
 beta_o = 1.34;
 s = 2.99;
 cs=0*nu;
 mask=nu>nu_0;
 cs(mask)=sigma * (   beta_o*(nu(mask)/nu_0).^-s ...
                   + (1-beta_o)*(nu(mask)/nu_0).^(-s-1) );
end

function cs=He1_He2(nu)
 sigma = 7.83e-22;
 beta_o = 1.66;
 s = 2.05;
 cs=0*nu;
 mask=nu>nu_1;
 cs(mask)=sigma * (   beta_o*(nu(mask)/nu_1).^-s ...
                   + (1-beta_o)*(nu(mask)/nu_1).^(-s-1) );
end

function cs=He2_He3(nu)
 sigma = 1.58e-22;
 beta_o = 1.34;
 s = 2.99;
 cs=0*nu;
 mask=nu>nu_2;
 cs(mask)=sigma * (  beta_o*(nu(mask)/nu_2).^-s ...
                   + (1-beta_o)*(nu(mask)/nu_2).^(-s-1) );
end

% End of nested functions

end % RT_Gamma()
