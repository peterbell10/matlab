function A=getRTdata(Data,element,VolFlag)
% getRTdata: Extract all the data for a structure element from the data structure.
%
% A=getRTdata(Data,element,VolFlag)
%
% ARGUMENTS
%  Data		The structure containing the RT data for the redshift dump.
%  element	The member of the structure, such as T, n_H1, etc
%  VolFlag	(optional) If true, the data is assumed to be a volume and
%		is re-shaped.  Otherwise, it is a slice of singe LOS.
%		Default is true.
%
% OUTPUT
%  A		A 3 dimensional array of the same size as the simulation volume
%		filled with the data for the selected element.
%
% REQUIRES
%
% SEE ALSO
%  PMRT
%  loadPMRTdata for a list of possible elements.

% AUTHOR Eric Tittley
%
% HISTORY
%  04 01 15 First version
%  04 02 05 Minor change to comments.
%  07 10 29 Modified comments
%
% COMPATIBILITY: Matlab, Octave (?)

if(nargin<3)
 VolFlag=1;
end

NLOS=size(Data,2);
NFLUX=size(Data(1).G,1);
A=zeros(NFLUX,NLOS);

for los=1:NLOS
 str=['A(:,los)=Data(los).',element,';'];
 eval(str);
end

if(VolFlag)
 A = reshape(A,[NFLUX,sqrt(NLOS),sqrt(NLOS)]);
end
