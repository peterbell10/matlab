% RT: Tools for manipulating RT data
%
% getRTdata	Extract all the data for a data structure element from the
%		data structure.
%
% loadPMdensity	Load a density file produced by the RT part of the PMRT code.
%
% loadPMRTdata	Load a PMRT data set from file (obsoleted by readRTdata)
%
% R_ifront_estimate The position of an ionisation front, plane wave
%
% R_ifront_estimate_polar  The position of an ionisation front, polar slab
%
% R_ifront_estimate_spherical The position of an ionisation front, spherical
%
% readRTdata    Load the RT data structure output by the RT code
%
% readRTrates   Load the RT rates produced by the RT code
%
% riPMRTESumsFile    Energy from photoionizations in an RT file.
%
% riPMRTIonSumsFile  Number of photoionizations in an RT file.
%
% RT_Gamma      The local ionisation/ionization rate in radiative transfer.
%
% RT_Gamma_probabilistic  The local ionisation/ionization rate in radiative
%                         transfer.  Probabilistic formulation, for tau >= 1.
%
% RT_IonisationHeating    The local heating rate due to ionisation/ionizations.
%
% RT_Luminosity The source spectrum.
%
% RT_Spectrum	The transmitted spectrum.
%
% RT_spectral_index  Estimate the spectral index in a cell.  (nu/nu_0)^alpha_p
%
% writeRTdata   Save the gas data structure to a form used by the RT library.
