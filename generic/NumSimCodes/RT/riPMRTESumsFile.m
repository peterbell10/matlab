% riPMRTESumsFile: Energy from photoionizations in a RT file.
%
% [rf,Ethermf,Ehiif,Eheiif,Eheiiif] = riPMRTESumsFile(filename)
%
% ARGUMENTS
%  filename       Filename for results from Enzo run (results*.dat)
%
% RETURNS
% rf              Fluid zone (kpc)
% Ethermf         Cumulative thermal energy (J)
% Ehiif           Cumulative energy of hydrogen ionizations (J)
% Eheiif          Cumulative energy of He+ ionizations (J)
% Eheiiif         Cumulative enert of He++ ionizations (J)
%   All variables returned as column arrays.
%
% SEE ALSO
%  riPMRTIonSumsFile

% COMPATIBILITY: Matlab(?), Octave
%
% AUTHOR: Avery Meiksin
%
% HISTORY:
%  29 11 12 Creation date. (After riCDRTESumsFile.)
%
function [rf,Ethermf,Ehiif,Eheiif,Eheiiif] = riPMRTESumsFile(filename)
kboltz = 1.3806504e-23;     %Boltzmann constant (CODATA06; in J/ K)
kpc = 3.0856e19;            %kpc in m
IP_HI = 0.5*4.35974394e-18; %IP HI (J) (CODAT06)
IP_HeI = 1.807*IP_HI;       %IP HeI (J) (CODAT06)
IP_HeII = 4*IP_HI;          %IP HeII (J) (CODAT06)
dat = loadPMRTdata(filename);
rf = dat.R/ kpc;            %convert m to kpc
hdenf = dat.n_H;            %total hydrogen density (per m^3)
hedenf = dat.n_He;          %total helium density (per m^3)
XHI = dat.f_H1;             %HI fraction
XHII = dat.f_H2;            %HII fraction
XHeI = dat.f_He1;           %HeI fraction
XHeII = dat.f_He2;          %HeII fraction
XHeIII = dat.f_He3;         %HeIII fraction
tempkf = dat.T;             %temperature (K)
npartf = (XHI + 2*XHII).*hdenf + (XHeI + 2*XHeII + 3*XHeIII).*hedenf;
pf = kboltz*(npartf.*tempkf);  %(J/ m^3)
Volf = (pi/0.75)*(rf.*rf.*rf);                      %Vol in kpc^3
dVolf = kpc*kpc*kpc*diff(cat(1,[0],Volf));          %dVol in m^3
Ethermf = 1.5*cumsum(pf.*dVolf);
hiidenf = max(0,XHII).*hdenf;
heiidenf = max(0,XHeII).*hedenf;
heiiidenf = max(0,XHeIII).*hedenf;
Nhiif = cumsum(hiidenf.*dVolf);
Nheiif = cumsum(heiidenf.*dVolf);
Nheiiif = cumsum(heiiidenf.*dVolf);
Ehiif = IP_HI*Nhiif;
Eheiif = IP_HeI*Nheiif;
Eheiiif = IP_HeII*Nheiiif;
