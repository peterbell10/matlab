function writeRTdata(Data,filename)
% writeRTdata: Save the gas data structure to a form used by the RT library.
%
% writeRTdata(Data,filename)
%
% ARGUMENTS
%  Data         Structure containing the data to write.
%  filename     Name of the file to be read in.
%
% OUTPUT
%  None
%
% NOTES
%  Structure elements:
%       G    
%       n    
%       T    
%       L    
%       n_H  
%       n_H1 
%       n_H2 
%       n_He 
%       n_He1
%       n_He2
%       n_He3
%       R    
%       D    
%       tau_H1
%       G_H1 
%       G_He1
%       G_He2
%       I_H1
%       I_He1
%       I_He2
%       L_H1 
%       L_He1
%       L_He2
%       L_eH 
%       L_C  
%       E_H1 
%       E_He1
%       E_He2
%       column_H1
%       column_He1
%       column_He2
%       CellBufferIndex
%       Cell
%       NumCells
%
% REQUIRES
%
% SEE ALSO
%  loadPMRTdata
%  getRTdata    Extracts volumes of data from PMRT data structure.

% AUTHOR: Eric Tittley
%
% HISTORY
%  090702 First version. Based on loadPMRTdata
%
% COMPATIBILITY: Matlab, Octave
%
% TODO
%  Write refinements, if required.

% Read the header, checking the byte-order
fid=fopen(filename,'w');
if(fid == -1)
 error(['writeRTdata: unable to open file ',filename,' for writing'])
end

Ver=3.2;
Ver_lo = floor(Ver);
Ver_hi = round( (Ver-Ver_lo)*10 );
% Write the header
[NFLUX,NLOS]=size([Data.T]);
hdr=int32(zeros(1,256));
hdr(1)=1;
hdr(2)=Ver_lo;
hdr(3)=Ver_hi;
hdr(4)=NFLUX;
hdr(5)=NLOS;
fwrite(fid,hdr,'int32');

WriteRTStructures(Data,fid);

fclose(fid);

% END of loadPMRTdata()

% Read in an RT data structure.
% Function private to loadPMRTdata
function WriteRTStructures(Data,fid)

Data_VER = 3.2;

[NFLUX,NLOS]=size([Data.T]);

switch Data_VER
 case 3.2
  for los=1:NLOS
   fwrite(fid,Data(los).NumCells     ,'int32');
   fwrite(fid,Data(los).Cell         ,'int32');
   fwrite(fid,Data(los).NBytes       ,'int32');
   fwrite(fid,Data(los).R            ,'double');
   fwrite(fid,Data(los).D            ,'double');
   fwrite(fid,Data(los).Dold         ,'double');
   fwrite(fid,Data(los).Entropy      ,'double');
   fwrite(fid,Data(los).T            ,'double');
   fwrite(fid,Data(los).tau_H1       ,'double');
   fwrite(fid,Data(los).n            ,'double');
   fwrite(fid,Data(los).n_H          ,'double');
   fwrite(fid,Data(los).f_H1         ,'double');
   fwrite(fid,Data(los).f_H2         ,'double');
   fwrite(fid,Data(los).n_He         ,'double');
   fwrite(fid,Data(los).f_He1        ,'double');
   fwrite(fid,Data(los).f_He2        ,'double');
   fwrite(fid,Data(los).f_He3        ,'double');
   % Parts not included in RT_DATA_SHORT
   fwrite(fid,Data(los).G            ,'double');
   fwrite(fid,Data(los).G_H1         ,'double');
   fwrite(fid,Data(los).G_He1        ,'double');
   fwrite(fid,Data(los).G_He2        ,'double');
   fwrite(fid,Data(los).I_H1         ,'double');
   fwrite(fid,Data(los).I_He1        ,'double');
   fwrite(fid,Data(los).I_He2        ,'double');
   fwrite(fid,Data(los).L            ,'double');
   fwrite(fid,Data(los).L_H1         ,'double');
   fwrite(fid,Data(los).L_He1        ,'double');
   fwrite(fid,Data(los).L_He2        ,'double');
   fwrite(fid,Data(los).L_eH         ,'double');
   fwrite(fid,Data(los).L_C          ,'double');
   fwrite(fid,Data(los).E_H1         ,'double');
   fwrite(fid,Data(los).E_He1        ,'double');
   fwrite(fid,Data(los).E_He2        ,'double');
   % End of long parts
   fwrite(fid,Data(los).column_H1    ,'double');
   fwrite(fid,Data(los).column_He1   ,'double');
   fwrite(fid,Data(los).column_He2   ,'double');
   fwrite(fid,Data(los).v_z          ,'double');
   fwrite(fid,Data(los).v_x          ,'double');
   fwrite(fid,Data(los).NCols        ,'double');
   fwrite(fid,Data(los).CellBufferIndex ,'int64');
  end
 otherwise
  error(['Unknown version ',num2str(Data_VER)])
end % switch - case

% END ReadRTStructures()
