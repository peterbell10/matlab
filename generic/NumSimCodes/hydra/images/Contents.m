% Images of hydra data 
%
% A_scale	Scales an image matrix
%
% DeformationImage
%		The degree of deformation between two particle distributions.
%
% P_proj	The projected pressure.
%
% P_proj_multi	The projected pressure maps for a series of data sets.
%
% T_proj	The projected temperature.
%
% T_proj_multi	The projected temperature maps for a series of data sets.
%
% X_ray_luminosities
%		X-ray luminosity
%
% h_proj	Projects the data onto a 2-D plane.
%
% h_proj_3d	Projects the data onto a regular mesh.
%
% h_proj_multi	Projects the data in 2-D for a series of simulations.
%
% makegifs_temp	Makes a series of GIF images from Hydra data
%		Only gas particles are shown and they are
%  		colour-coded according to temperature.
%
% plot_T	4-panel image (T all gas, hot, core, and very hot)
%
% plot_T_quick	Particles coded by temperature.  Made obsolete by plotXYZ.
%
% plot_gas_temp	4-panel image (T all gas, hot, core, and very hot)
%
% show_A	Scales a 2-D matrix distribution.
