[rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,time));
[gasdn,darkdn,gash,darkh]=readdensities([dataname(irun,time),'.densities']);
dark=find(~itype);
if(~ exist(['centres.',num2str(irun),'.mat']))
 % Find the Centres
 good=find(darkdn>MinOverDensity*hdr.ndark);
 centres=find_centres(irun,time,Res, r(:,dark(good)),rm(dark(good)),darkdn(good),darkh(good) );
 cmd=['save centres.',num2str(irun),'.mat centres']
 eval(cmd)
else
 cmd=['load centres.',num2str(irun),'.mat']
 eval(cmd)
end

% Get the Units
%load Units
munit=hdr.munit; lunit=hdr.lunit; Kunit=hdr.Kunit;
Units

if(~ exist(['overdensities.',num2str(irun),'.mat']))
 % Find the overdensities
 [mgas,rad,Tx,mdark,ndark,ngas,centres]=overdensities(centres,rm,r,th,itype,hdr.ngas);
 good=find(ngas(:,2)>Nmin);
 Tx=Tx(good)';
 rad=rad(good,:); centres=centres(good,:);
 mgas=mgas(good,:); mdark=mdark(good,:);
 ngas=ngas(good,:); ndark=ndark(good,:);
 mgas=mgas*munit; mdark=mdark*munit;     %Mo
 rad=rad*lunit/Mpc;                      %Mpc (note: corrected for h^-1)
 Tx=Tx*Kunit;                            %K
 % Filter out all the Substructure
 [HaloStructure,Cluster]=RemoveHaloStructure(centres,rad(:,1)*Mpc/lunit,mgas(:,1)+mdark(:,1));
 Tx=Tx(Cluster);
 rad=rad(Cluster,:);
 centres=centres(Cluster,:);
 mgas=mgas(Cluster,:); mdark=mdark(Cluster,:);
 ngas=ngas(Cluster,:); ndark=ndark(Cluster,:);
 cmd=['save overdensities.',num2str(irun),'.mat mgas rad Tx mdark ndark ngas centres']
 eval(cmd)
else
 cmd=['load overdensities.',num2str(irun),'.mat']
 eval(cmd)
end



if(~ exist(['profiles.',num2str(time),'.mat']))
 %Profile the data
 d=make_profiles(dataname(irun,time),centres,1,1);
 cmd=['save profiles.',num2str(time),'.mat d']
 eval(cmd)
else
 cmd=['load profiles.',num2str(time),'.mat']
 eval(cmd)
end

if(~ exist(['mean_density_profile.',num2str(irun),'.mat']))
%Get the mean profile ( a longer process )
% extract the radius 'profile'
 r=squeeze(d(:,1,:))*lunit/Mpc; % units of Mpc
% extract the density profile
 rho=squeeze(d(:,5,:))*munit*Mo/(lunit^3)/Rho_c; % units of rho critical
 [N,dummy]=size(rad);
 if(dummy~=2), error('rad must be N x 2'), end
% find r/R_200 for each profile
 r_r200=0*r; % r/R_200
 for i=1:N
  r_r200(:,i)=r(:,i)/rad(i,1);
 end
% interpolate over profile bins with contents 0
 d_interp=interp_profiles(rho);
% rebin the profiles to a common set of bins
 r_r200_bins=[-1.4:0.02:1.3];
 rho_rebinned=rebin_profiles(r_r200_bins,log10(r_r200),d_interp);
% again, remove zeros by interpolating
 rho_rebinned_interp=interp_profiles(rho_rebinned);
% find the mean profile (in log space)
 for i=1:length(r_r200_bins)
  nonzero=find(rho_rebinned_interp(i,:)~=0);
  log_rho_mean(i)=mean(log10(rho_rebinned_interp(i,nonzero)));
  log_rho_std(i) = std(log10(rho_rebinned_interp(i,nonzero)));
 end
 cmd=['save mean_density_profile.',num2str(irun),'.mat r_r200_bins log_rho_mean log_rho_std']
 eval(cmd)
else
 cmd=['load mean_density_profile.',num2str(irun),'.mat']
 eval(cmd)
end

%plot the results
loglog(10.^r_r200_bins,10.^log_rho_mean,'k-')
hold on
loglog(10.^r_r200_bins,10.^(log_rho_mean+log_rho_std),'k:')
loglog(10.^r_r200_bins,10.^(log_rho_mean-log_rho_std),'k:')
hold off
