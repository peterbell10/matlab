load Units.mat
load profiles.mat % These need to be converted to 'real' units
load mass_rad.mat % These are already converted to 'real' units

% extract the radius 'profile'
r=squeeze(d(:,1,:))*lunit/Mpc; % units of Mpc

% extract the density profile
rho=squeeze(d(:,5,:))*munit*Mo/(lunit^3)/Rho_c; % units of rho critical

N=length(rad);

% find r/R_200 for each profile
r_r200=0*r; % r/R_200
for i=1:N
 r_r200(:,i)=r(:,i)/rad(i,1);
end

% interpolate over profile bins with contents 0
d_interp=interp_profiles(rho);

% rebin the profiles to a common set of bins
r_r200_bins=[-1.4:0.02:1.3];
rho_rebinned=rebin_profiles(r_r200_bins,log10(r_r200),rho);

% again, remove zeros by interpolating
rho_rebinned_interp=interp_profiles(rho_rebinned);

% find the mean profile (in log space)
log_rho_mean=mean(log10(rho_rebinned_interp'));
log_rho_std = std(log10(rho_rebinned_interp'));

%plot the results
loglog(10.^r_r200_bins,10.^log_rho_mean,'b-')
hold on
loglog(10.^r_r200_bins,10.^(log_rho_mean+log_rho_std),'r:')
loglog(10.^r_r200_bins,10.^(log_rho_mean-log_rho_std),'r:')

save mean_density_profile r_r200_bins log_rho_mean log_rho_std
