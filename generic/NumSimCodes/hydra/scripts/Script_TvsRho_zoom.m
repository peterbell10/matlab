disp('Do not use this, just plot the points (T vs Rho)')
[rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,itime));
Units
[gasdn,darkdn,gash,darkh]=readdensities([dataname(irun,itime),'.densities']);
L=BoxLimits;
gas=find(itype&r(:,1)>L(1)&r(:,1)<L(2)&r(:,2)>L(3)&r(:,2)<L(4)&r(:,3)>L(5)&r(:,3)<L(6));
centre=r(gas(dn(gas)==max(dn(gas))),:);
L=[centre(1)-0.1 centre(1)+0.1 centre(2)-0.1 centre(2)+0.1 centre(3)-0.1 centre(3)+0.1];
gas=find(itype&r(:,1)>L(1)&r(:,1)<L(2)&r(:,2)>L(3)&r(:,2)<L(4)&r(:,3)>L(5)&r(:,3)<L(6));
th=th(gas)*hdr.Kunit*hdr.atime^2;
dn=gasdn(gas) * hdr.munit*Mo /(hdr.lunit*hdr.atime)^3;
TvsRho=numdensity(log10(dn),log10(th),Nsrch,Res,log10(AxLimits));
cmd=['save TvsRho.zoom.',int2str(irun),'.',int2str_zeropad(itime,4),'.mat TvsRho'];
eval(cmd)
