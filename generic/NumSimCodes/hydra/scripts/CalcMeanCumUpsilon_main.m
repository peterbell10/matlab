hdr=readhydra_field(datafile,'hdr',Version);
load profiles.1000.mat
load overdensities.1.mat
Units
Nclus=length(Tx);

x=squeeze(d(:,1,:))*hdr.lunit/Mpc;
ndark=squeeze(d(:,3,:));
ngas =squeeze(d(:,4,:));
ndark_cum=cumsum(ndark);
ngas_cum =cumsum(ngas );

Ups=0*ngas; Ups_cum=Ups;

nall=ngas*0.1+ndark*0.9;
nall_cum=ngas_cum*0.1+ndark_cum*0.9;

Ups(nall~=0)=ngas(nall~=0)./nall(nall~=0);
Ups_cum(nall_cum~=0)=ngas_cum(nall_cum~=0)./nall_cum(nall_cum~=0);

for i=1:Nclus
 x(:,i)=x(:,i)/rad(i,1);
end

long_x=mat2vect(x);
long_Ups=mat2vect(Ups);
long_Ups_cum=mat2vect(Ups_cum);

[centres,new,std_new]=bin_profile(xfit,...
                                  log10(long_x(long_Ups~=0)),...
                                  log10(long_Ups(long_Ups~=0)) );

log_Ups_all(:,Model)=new';
log_Ups_all_std(:,Model)=std_new';

[centres,new,std_new]=bin_profile(xfit,...
                                  log10(long_x(long_Ups_cum~=0)),...
                                  log10(long_Ups_cum(long_Ups_cum~=0)) );

log_Ups_cum_all(:,Model)=new';
log_Ups_cum_all_std(:,Model)=std_new';

eval(['cd ',Base])

