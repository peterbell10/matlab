function multimass4(datafile_in,Version,centre,datafile_out)
%
% multimass4(datafile_in,Version,centre,datafile_out)
%
% Takes the initial conditions in datafile_in, of version, Version,
% centres the data file on centre,
% and makes the following configuration:
%
% ----------------------------------
% |    /                      \    |
% |   /                        \   |
% |  /        __________        \  |
% | /        /          \        \ |
% |/        /            \        \|
% |         |    ____    |         |
% |         |   /    \   |         |
% |         |   | Res|   |         |
% |         |   \____/   |         |
% |         |            |         |
% |\        \    Res/2   /        /|
% | \        \__________/        / |
% |  \                          /  |
% |Res\          Res/4         /   |
% | 8  \                      /    |
% ----------------------------------
%
% That is, 3 concentric spheres in a cubic volume:
%  an inner sphere of radius BOX/8 with Resolution Res
%  an intermediate sphere of radius BOX/4 with resolution Res/2
%  an outer sphere of radius BOX/2 with resolution Res/4
%  and everything outside the outer sphere is at resolution Res/8
%
% This leads to N = Res^3/12 particles
%
% Masses are scaled appropriately
%
% The particles outside the central sphere are sampled at random from the
% base initial conditions.
%
% The number of particles need not be a power of 2
%
% The particles should lie on the grid, (0,1)
%
% See READHYDRA for help on the Hydra Version argument

% Read in the initial perturbed file
[rm,r,v,th,itype,dn,h,hdr]=readhydra(datafile_in,Version);

% The number of particle in each of the 4 resolutions
Nobj  =length(itype);
Nobj2 =round(Nobj /2);
Nobj4 =round(Nobj2/2);
Nobj8 =round(Nobj4/2);

% Sample the distributions:
Samp2 = ceil(Nobj*rand(Nobj2,1));
Samp4 = ceil(Nobj*rand(Nobj4,1));
Samp8 = ceil(Nobj*rand(Nobj8,1));

% Recentre the distribution and move to the origin
r=recentre(r,centre)-0.5;

% Find the distance of each particle from the origin
dist=sqrt( sum( r.^2 ) );

% Find those particles in each sphere/shell
Part1 =find(dist<=(1/8))';
Part2 =Samp2(find(dist(Samp2)<=(1/4) & dist(Samp2)>(1/8)));
Part4 =Samp4(find(dist(Samp4)<=(1/2) & dist(Samp4)>(1/4)));
Part8 =Samp8(find(dist(Samp8)>(1/2)));
size(Part1)
size(Part2)
size(Part4)
size(Part8)
Parts = [Part1;Part2;Part4;Part8];
PartsGas =Parts(find( itype(Parts)));
PartsDark=Parts(find(~itype(Parts)));

hdr.ngas=length(PartsGas);
hdr.ndark=length(PartsDark);
hdr.nobj=hdr.ngas+hdr.ndark;

% How many particles were extracted per shell?
Nobj1 = length(Part1);
Nobj2 = length(Part2);
Nobj4 = length(Part4);
Nobj8 = length(Part8);

if( (Nobj1+Nobj2++Nobj4+Nobj8) ~= hdr.nobj )
 error('Ooops: something is awry in multimass')
end

% Now create the data arrays
if(round(hdr.Version*100)==402)
 % The input file has constant masses per particles.  This is not good.
 rm_new=hdr.rmgas*[   ones(1,Nobj1),...
                   2^1*ones(1,Nobj2),...
                   4^1*ones(1,Nobj4),...
                   8^1*ones(1,Nobj8) ];
 hdr.Version=4.01;
else
 rm_new=[rm(Part1);2^3*rm(Part2);4^3*rm(Part4);8^3*rm(Part8)];
end
r_new=r(:,Parts)+0.5; % recentre to (0.5,0.5,0.5) at the same time
v_new=v(:,Parts);
if(hdr.ngas>0)
 th_new=th(PartsGas);
 dn_new=dn(PartsGas);
 h_new = h(PartsGas);
else
 th_new=[];
 dn_new=[];
 h_new=[];
end
itype_new=itype(Parts);

%output the data file
writehydra(datafile_out,rm_new,r_new,v_new,th_new,itype_new,dn_new,h_new,hdr,0)
