function Rs=Flow2(irun,start,fin,clump)
% syntax: Rs=Flow2(irun,start,fin,clump)

count=0;
for i=start:fin
 count=count+1;
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,i));
 centre=mean(r(clump,:));
 for j=1:3
  Rs(:,j,count)=r(:,j)-centre(j);
 end
end
