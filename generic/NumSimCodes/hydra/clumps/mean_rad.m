function [mean_rad,max_rad]=mean_rad(r);

centre=mean(r);
r(:,1)=r(:,1)-centre(1);
r(:,2)=r(:,2)-centre(2);
r(:,3)=r(:,3)-centre(3);

dist=sqrt(sum((r.^2)'));
mean_rad=mean(dist);
max_rad=max(dist);
