function Vfinal=V_func(t,to,Vo,M,R,rho_g,T,A,Z)
% Vfinal=V_func(t,to,Vo,M,R,rho_g,T,A,[Z]);
%
% Returns the velocity, Vfinal, at given time, t, of a clump 
% moving through a uniform medium of gas.
% Clump paramters: M = mass
%                  R = radius
%                  Vo = initial velocity at time, to
% Gas parameter:   rho_g = gas density
%		   T = Temperature
%		   A = mean atomic mass number
%                  Z = mean charge per ion (optional)
%
% Note: if Z is set, then the gas is presumed to be a plasma
%       which uses an entirely different coef of dyn. viscosity
%
% Returns a scalar if t is a scalar, and a vector if
% t is a vector.  All other quantities should be scalars.

if( nargin~=8 & nargin~=9 )
 error('Insufficient number of arguments')
end

if(R<0), R=0+1e-6; end

%l=M/(2*pi*R^2*rho_g);
%tl=to-l/Vo;
%V=l./(t-tl);

%%%% ORIGINAL CORE %%%%%
%t_half=M/(Vo*2*pi*R^2*rho_g);
%V=Vo*((t-to)./t_half +1).^(-1/2);
%%%%%%%%%%%%%%%%%%%%%%%%

% New method: integrate the drag force from t_o to t(1), then t(1) to t(2), ...
% Need the extra parameters T, A, and Z

Tol=5e-5;

for i=1:length(t);
 if(i>1)
  t_o=t(i-1);
  V_o=Vfinal(i-1);
 else
  t_o=to;
  V_o=Vo;
 end
 Nslices=1;
 Err=Tol+1;
 previous_Vfinal = Vo;
% iter=0;
 if(nargin==8) % Neutral gas
  while(Err>Tol);
%  iter=iter+1;
   Nslices=Nslices*2;
   dt=(t(i)-t_o)/Nslices;
   V=V_o;
   for t_now=t_o:dt:t(i)
% Euler method (faster than Runge-Kutta, and they converge to the same answers)
    V = V - dt*drag_force(V,R,rho_g,T,A)/M;
% End Euler method
%    % 4th order Runge-Kutta
%    dV1 = -1*dt*drag_force(V      ,R,rho_g,T,A)/M;
%    dV2 = -1*dt*drag_force(V+dV1/2,R,rho_g,T,A)/M;
%    dV3 = -1*dt*drag_force(V+dV2/2,R,rho_g,T,A)/M;
%    dV4 = -1*dt*drag_force(V+dV3  ,R,rho_g,T,A)/M;
%    dV  = dV1/6 + dV2/3 + dV3/3 + dV4/6;
%    V = V + dV;
   end
   Vfinal(i) = V;
   Err = abs((Vfinal(i) - previous_Vfinal)/Vfinal(i));
   previous_Vfinal = Vfinal(i);
  end % End while
 else % nargin==9 -> Plasma
  while(Err>Tol);
%  iter=iter+1;
   Nslices=Nslices*2;
   dt=(t(i)-t_o)/Nslices;
   V=V_o;
   for t_now=t_o:dt:t(i)
% Euler method
    V = V - dt*drag_force(V,R,rho_g,T,A,Z)/M;
% End Euler method
%    % 4th order Runge-Kutta
%    dV1 = -1*dt*drag_force(V      ,R,rho_g,T,A,Z)/M;
%    dV2 = -1*dt*drag_force(V+dV1/2,R,rho_g,T,A,Z)/M;
%    dV3 = -1*dt*drag_force(V+dV2/2,R,rho_g,T,A,Z)/M;
%    dV4 = -1*dt*drag_force(V+dV3  ,R,rho_g,T,A,Z)/M;
%    dV  = dV1/6 + dV2/3 + dV3/3 + dV4/6;
%    V = V + dV;
   end
   Vfinal(i) = V;
   Err = abs((Vfinal(i) - previous_Vfinal)/Vfinal(i));
   previous_Vfinal = Vfinal(i);
  end % while Err > Tol
 end

%disp(iter)

end % loop over all t's
