function err=V_diff_R(R,V_actual,time_actual,to,Vo,M,rho_g,T,A,Z)
if(nargin==9) % Neutral
 err=V_diff(V_actual,time_actual,to,Vo,M,R,rho_g,T,A);
else %Plasma
 err=V_diff(V_actual,time_actual,to,Vo,M,R,rho_g,T,A,Z);
end

function err=V_diff(V_actual,time_actual,to,Vo,M,R,rho_g,T,A,Z)
if(nargin==9) % Neutral Gas
 V=V_func(time_actual,to,Vo,M,R,rho_g,T,A)';
else % Plasma
 V=V_func(time_actual,to,Vo,M,R,rho_g,T,A,Z)';
end
err=sum(sqrt( (V-V_actual).^2 ));
