function [MPS,seps]=mean_part_sep(r)
% Find the mean particle separation for the given particle positions
%
% [MPS,seps]=mean_part_sep(r);

% AUTHOR: Eric Tittley
%
% HISTORY
%  02-03-27 Mature version
%  	Added comments.
%	Modified to use r(3,n) data format.
%	Return the separations.

m=size(r,2);
seps=zeros((m*m-m)/2,1);

marker=1;
for i=1:m
 positions=r(:,i)*ones(1,m-i);
 marker_end=marker+m-i-1;
 seps(marker:marker_end)=sqrt(sum(((r(:,i+1:m)-positions).^2))); 
 marker=marker_end+1;
end

MPS=mean(seps);
