% Hydra data io routines
%
%
% These read, write, and modify hydra data files
%
% dataname		The datafile name, given the run # and extension #.
%			Does the zero-padding.
%
% get_file_names	A list of datafile names.
%
% readhydra		Read a hydra data file.
%
% readhydra_field	Read a single field from a hydra data file.
%
% (readhydra_header	A private function for reading the header.)
%
% writehydra		Write hydra data files.
%
% editheader		Edit the datafile header using a GUI.  Not very robust.
%  (uses readcb and writecb)
%
% cleardatafile	Clears the datafile header of a lot of junk.  Good for
%		creating initial conditions from previous datafiles.
%
% resample_hydra
%               Resamples hydra data to a lower resolution by randomly
%               choosing particles.
%
%
% These read in files produced by the fortran tools for analysing hydra data
%
% readdensities		Produced by calcdens
%
% readdispersions	Produced by v_dispersions (obsoleted by v_disp)
%
% readskid		Produced by skid
%
%
% readinitpert	Read in initial perturbation files that are to be used by
% 		the initial conditions generator
%
%
% Tools to connect with Mike Seymours wavelet analysis stuff
%
% DumpPositions
%
% ReadWavelets
