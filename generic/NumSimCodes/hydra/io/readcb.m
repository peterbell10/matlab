function readcb(gcbf)
% callback function for the read button of editheader

% get the handles for all the edit boxes
handle=gethandles(gcbf);

% retrieve the file name	
inputfile=get(handle.inputfile,'String');

% read in the hydra file
[hydradata.rm,hydradata.r,hydradata.v,hydradata.th,hydradata.itype,hydradata.dn,hydradata.h,hydradata.hdr]=readhydra(inputfile);

% set all the edit boxes to the header values	

set(handle.itime,		'String',num2str(hydradata.hdr.itime));
set(handle.itstop,		'String',num2str(hydradata.hdr.itstop));
set(handle.itdump,		'String',num2str(hydradata.hdr.itdump));
set(handle.itout,		'String',num2str(hydradata.hdr.itout));
set(handle.time,		'String',num2str(hydradata.hdr.time));
set(handle.atime,		'String',num2str(hydradata.hdr.atime));
set(handle.htime,		'String',num2str(hydradata.hdr.htime));
set(handle.dtime,		'String',num2str(hydradata.hdr.dtime));
set(handle.Est,			'String',num2str(hydradata.hdr.Est));
set(handle.T,			'String',num2str(hydradata.hdr.T));
set(handle.Th,			'String',num2str(hydradata.hdr.Th));
set(handle.U,			'String',num2str(hydradata.hdr.U));
set(handle.Radiation,	'String',num2str(hydradata.hdr.Radiation));
set(handle.Esum,		'String',num2str(hydradata.hdr.Esum));
set(handle.Rsum,		'String',num2str(hydradata.hdr.Rsum));
set(handle.irun,		'String',num2str(hydradata.hdr.irun));
set(handle.nobj,		'String',num2str(hydradata.hdr.nobj));
set(handle.ngas,		'String',num2str(hydradata.hdr.ngas));
set(handle.ndark,		'String',num2str(hydradata.hdr.ndark));
set(handle.L,			'String',num2str(hydradata.hdr.L));
set(handle.intl,		'String',num2str(hydradata.hdr.intl));
set(handle.nlmx,		'String',num2str(hydradata.hdr.nlmx));
set(handle.perr,		'String',num2str(hydradata.hdr.perr));
set(handle.dtnorm,		'String',num2str(hydradata.hdr.dtnorm));
set(handle.sft0,		'String',num2str(hydradata.hdr.sft0));
set(handle.sftmin,		'String',num2str(hydradata.hdr.sftmin));
set(handle.sftmax,		'String',num2str(hydradata.hdr.sftmax));
set(handle.h100,		'String',num2str(hydradata.hdr.h100));
set(handle.box,			'String',num2str(hydradata.hdr.box));
set(handle.zmet,		'String',num2str(hydradata.hdr.zmet));
set(handle.spc0,		'String',num2str(hydradata.hdr.spc0));
set(handle.lcool,		'String',num2str(hydradata.hdr.lcool));
set(handle.rmgas,		'String',num2str(hydradata.hdr.rmgas));
set(handle.rmdark,		'String',num2str(hydradata.hdr.rmdark));
set(handle.rmnorm,		'String',num2str(hydradata.hdr.rmnorm));
set(handle.tstart,		'String',num2str(hydradata.hdr.tstart));
set(handle.omega0,		'String',num2str(hydradata.hdr.omega0));
set(handle.xlambda0,	'String',num2str(hydradata.hdr.xlambda0));
set(handle.h0t0,		'String',num2str(hydradata.hdr.h0t0));

%start off with the first dump time
set(handle.dumptimeidx,	'String',num2str(1));
set(handle.dumptime, 	'String',num2str(hydradata.hdr.times(1)));

set(handle.readbutton,  'UserData',hydradata);

return;	
