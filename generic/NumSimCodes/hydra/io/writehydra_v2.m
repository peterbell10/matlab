function writehydra_v2(filename,rm,r,v,th,itype,dn,h,hdr,mach);
%writehydra     Write hydra data files
%   Syntax: writehydra(filename,rm,r,v,th,itype,dn,h,hdr,mach)
%            where mach is 'big' or 'little', depending on the byte
%            structure.  The default is 'native'.

if(nargin==9) mach='n';
else mach=mach(1);
end

%These have to be rescaled
hdr.lunit=hdr.lunit/hdr.L;
hdr.vunit=hdr.vunit/hdr.L;
hdr.nunit=hdr.nunit*hdr.L^3;
hdr.eunit=hdr.eunit/hdr.L^2;
hdr.Kunit=hdr.Kunit/hdr.L^2;

fid=fopen(filename,'wb',mach);

% Header
% ibuf

%rec size
fwrite(fid,400*4,'int');
fwrite(fid,hdr.times,'float');

% ibuf1
fwrite(fid,hdr.itime	,'uint');
fwrite(fid,hdr.itstop 	,'uint');
fwrite(fid,hdr.itdump 	,'uint');
fwrite(fid,hdr.itout 	,'uint');
fwrite(fid,hdr.time 	,'float');
fwrite(fid,hdr.atime 	,'float');
fwrite(fid,hdr.htime 	,'float');
fwrite(fid,hdr.dtime 	,'float');

fwrite(fid,hdr.Est  	,'float');
fwrite(fid,hdr.T 	,'float');
fwrite(fid,hdr.Th 	,'float');
fwrite(fid,hdr.U 	,'float');
fwrite(fid,hdr.Radiation,'float');
fwrite(fid,hdr.Esum 	,'float');
fwrite(fid,hdr.Rsum 	,'float');
fwrite(fid,hdr.cpu	,'float');

fwrite(fid,hdr.tstop	,'float');
fwrite(fid,hdr.tout	,'float');
fwrite(fid,hdr.icdump	,'uint');
fwrite(fid,hdr.padding	,'float');
fwrite(fid,hdr.Tlost	,'float');
fwrite(fid,hdr.Glost	,'float');
fwrite(fid,hdr.Ulost	,'float');

fwrite(fid,hdr.Tdark	,'float');
fwrite(fid,hdr.Udd	,'float');
fwrite(fid,hdr.Ugg	,'float');
fwrite(fid,hdr.Udg	,'float');

fwrite(fid,0		,'float'); % padding for alignment
fwrite(fid,hdr.lunit	,'real*8');
fwrite(fid,hdr.munit	,'real*8');
fwrite(fid,hdr.tunit	,'real*8');
fwrite(fid,hdr.vunit	,'real*8');
fwrite(fid,hdr.nunit	,'real*8');
fwrite(fid,hdr.Kunit	,'real*8');
fwrite(fid,hdr.eunit	,'real*8');
fwrite(fid,hdr.cunit	,'real*8');

for i=45:100
	fwrite(fid,0,'integer*4');
end

% ibuf2
fwrite(fid,hdr.irun 	,'uint');
fwrite(fid,hdr.nobj 	,'uint');
fwrite(fid,hdr.ngas 	,'uint');
fwrite(fid,hdr.ndark 	,'uint');
fwrite(fid,hdr.L 	,'uint');
fwrite(fid,hdr.intl 	,'uint');
fwrite(fid,hdr.nlmx 	,'uint');
fwrite(fid,hdr.perr 	,'float');

fwrite(fid,hdr.dtnorm 	,'float');
fwrite(fid,hdr.sft0 	,'float');
fwrite(fid,hdr.sftmin 	,'float');
fwrite(fid,hdr.sftmax 	,'float');
fwrite(fid,hdr.h100 	,'float');
fwrite(fid,hdr.box  	,'float');
fwrite(fid,hdr.zmet 	,'float');
fwrite(fid,hdr.spc0 	,'float');

fwrite(fid,hdr.lcool 	,'uint');
fwrite(fid,hdr.rmgas 	,'float');
fwrite(fid,hdr.rmdark 	,'float');
fwrite(fid,hdr.rmnorm 	,'float');
fwrite(fid,hdr.tstart 	,'float');
fwrite(fid,hdr.omega0 	,'float');
fwrite(fid,hdr.xlambda0 ,'float');

fwrite(fid,hdr.h0t0 	,'float');
for i=25:100
	fwrite(fid,0,'integer*4');
end

% rec size
fwrite(fid,400*4,'int');

% end of header

% now write out the particle data
write_fort(fid,rm,'float');
write_fort(fid,r','float');
write_fort(fid,v','float');
write_fort(fid,h,'float');
%e=th.*(hdr.atime^2);
e=th;
write_fort(fid,e,'float');
write_fort(fid,itype,'uint');
write_fort(fid,dn,'float');


function write_fort(fid,data,vartype)
numelements=size(data,1)*size(data,2);
fwrite(fid,numelements*4,'int');
fwrite(fid,data,vartype);
fwrite(fid,numelements*4,'int');
return;
