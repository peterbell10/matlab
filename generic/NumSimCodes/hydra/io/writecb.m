function writecb(gcbf)
% callback function for the write button of editheader

% get the handles for all the edit boxes
handle=gethandles(gcbf);

% get the data
hydradata=get(handle.readbutton,'UserData');

% set the header values to the edit boxes
hydradata.hdr.itime	 	=str2num(get(handle.itime,   	'String'));
hydradata.hdr.itstop   	=str2num(get(handle.itstop,  	'String'));
hydradata.hdr.itdump   	=str2num(get(handle.itdump,  	'String'));
hydradata.hdr.itout 	=str2num(get(handle.itout,   	'String'));
hydradata.hdr.time 	 	=str2num(get(handle.time,	 	'String'));
hydradata.hdr.atime 	=str2num(get(handle.atime,   	'String'));
hydradata.hdr.htime 	=str2num(get(handle.htime,   	'String'));
hydradata.hdr.dtime 	=str2num(get(handle.dtime,   	'String'));
hydradata.hdr.Est 	 	=str2num(get(handle.Est,	 	'String'));
hydradata.hdr.T 		=str2num(get(handle.T,  	 	'String'));
hydradata.hdr.Th  	 	=str2num(get(handle.Th, 	 	'String'));
hydradata.hdr.U 		=str2num(get(handle.U,  	 	'String'));
hydradata.hdr.Radiation	=str2num(get(handle.Radiation,	'String'));
hydradata.hdr.Esum 	 	=str2num(get(handle.Esum,	 	'String'));
hydradata.hdr.Rsum 	 	=str2num(get(handle.Rsum,	 	'String'));
hydradata.hdr.irun 	 	=str2num(get(handle.irun,	 	'String'));
hydradata.hdr.nobj 	 	=str2num(get(handle.nobj,	 	'String'));
hydradata.hdr.ngas 	 	=str2num(get(handle.ngas,	 	'String'));
hydradata.hdr.ndark 	=str2num(get(handle.ndark,   	'String'));
hydradata.hdr.L 		=str2num(get(handle.L,  	 	'String'));
hydradata.hdr.intl 	 	=str2num(get(handle.intl,	 	'String'));
hydradata.hdr.nlmx 	 	=str2num(get(handle.nlmx,	 	'String'));
hydradata.hdr.perr 	 	=str2num(get(handle.perr,	 	'String'));
hydradata.hdr.dtnorm   	=str2num(get(handle.dtnorm,  	'String'));
hydradata.hdr.sft0 	 	=str2num(get(handle.sft0,	 	'String'));
hydradata.hdr.sftmin   	=str2num(get(handle.sftmin,  	'String'));
hydradata.hdr.sftmax   	=str2num(get(handle.sftmax,  	'String'));
hydradata.hdr.h100 	 	=str2num(get(handle.h100,	 	'String'));
hydradata.hdr.box 	 	=str2num(get(handle.box,	 	'String'));
hydradata.hdr.zmet 	 	=str2num(get(handle.zmet,	 	'String'));
hydradata.hdr.spc0 	 	=str2num(get(handle.spc0,	 	'String'));
hydradata.hdr.lcool 	=str2num(get(handle.lcool,   	'String'));
hydradata.hdr.rmgas 	=str2num(get(handle.rmgas,   	'String'));
hydradata.hdr.rmdark   	=str2num(get(handle.rmdark,  	'String'));
hydradata.hdr.rmnorm   	=str2num(get(handle.rmnorm,  	'String'));
hydradata.hdr.tstart   	=str2num(get(handle.tstart,  	'String'));
hydradata.hdr.omega0   	=str2num(get(handle.omega0,  	'String'));
hydradata.hdr.xlambda0 	=str2num(get(handle.xlambda0,	'String'));
hydradata.hdr.h0t0	 	=str2num(get(handle.h0t0,	 	'String'));

inputfile=get(handle.inputfile,'String');
outputfile=get(handle.outputfile,'String');

% write out the new file
writehydra(outputfile,hydradata.rm,hydradata.r,hydradata.v,hydradata.th,hydradata.itype,hydradata.dn,hydradata.h,hydradata.hdr);
