function writehydra(filename,rm,r,v,th,itype,dn,h,hdr,CommonMassFlag);
%writehydra     Write hydra data files
%   Syntax: writehydra(filename,rm,r,v,th,itype,dn,h,hdr,[CommonMassFlag])
%            where the optional CommonMassFlag can be 1 or 0 (default)
%            If it is 1, then rm is ignored.
%
% This version presumes hydra4.0 data file format.
%
% Use writehydra_v2 for the older format

% HISTORY
% 00 05 09 Changed itype from uint to int, since 'lost' particles have type -2
% 00 07 24 Modified to use 'official' Hydra 4.0 format
% 01 08 29 Bug fix.  rcen was being treated as a single float, when in fact
%	it is a real(3).  The only consequence is that rmax2 was being ignored.

if(nargin<10), CommonMassFlag=0; end
% Convert Versioning information to new format
if(length(hdr.Version)==1)
 Ver(1)=round(hdr.Version);
 Ver(2)=round((hdr.Version-Ver(1))*10);
 Ver(3)=round((hdr.Version-Ver(1)-Ver(2)/10)*100);
 hdr.Version=Ver;
end
Version=hdr.Version(1)*100+hdr.Version(2)*10+hdr.Version(3);
if(Version==402), CommonMassFlag=1; end

%These have to be rescaled
%hdr.lunit=hdr.lunit/hdr.L;
%hdr.vunit=hdr.vunit/hdr.L;
%hdr.nunit=hdr.nunit*hdr.L^3;
%hdr.eunit=hdr.eunit/hdr.L^2;
%hdr.Kunit=hdr.Kunit/hdr.L^2;

machineformat='native';

fid=fopen(filename,'wb',machineformat);

disp(hdr.Version)

% Versioning information
fwrite(fid,3*4,'int');
fwrite(fid,hdr.Version,'int');
fwrite(fid,3*4,'int');

padding=0.0;
% Header
% ibuf

%rec size
fwrite(fid,400*4,'int');
fwrite(fid,hdr.times,'float');

count=0;
% ibuf1
fwrite(fid,hdr.itime	,'uint'); count=count+1;
fwrite(fid,hdr.itstop 	,'uint'); count=count+1;
fwrite(fid,hdr.itdump 	,'uint'); count=count+1;
fwrite(fid,hdr.itout 	,'uint'); count=count+1;
fwrite(fid,hdr.time 	,'float'); count=count+1;
fwrite(fid,hdr.atime 	,'float'); count=count+1;
fwrite(fid,hdr.htime 	,'float'); count=count+1;
fwrite(fid,hdr.dtime 	,'float'); count=count+1;

fwrite(fid,hdr.Est  	,'float'); count=count+1;
fwrite(fid,hdr.T 	,'float'); count=count+1;
fwrite(fid,hdr.Th 	,'float'); count=count+1;
fwrite(fid,hdr.U 	,'float'); count=count+1;
fwrite(fid,hdr.Radiation,'float'); count=count+1;
fwrite(fid,hdr.Esum 	,'float'); count=count+1;
fwrite(fid,hdr.Rsum 	,'float'); count=count+1;
fwrite(fid,hdr.cpu	,'float'); count=count+1;

fwrite(fid,hdr.tstop	,'float'); count=count+1;
fwrite(fid,hdr.tout	,'float'); count=count+1;
fwrite(fid,hdr.icdump	,'uint');  count=count+1;
fwrite(fid,padding	,'float'); count=count+1;
fwrite(fid,hdr.Tlost	,'float'); count=count+1;
fwrite(fid,hdr.Qlost	,'float'); count=count+1;
fwrite(fid,hdr.Ulost	,'float'); count=count+1;
if(Version>=400), fwrite(fid,padding,'float'); count=count+1; end

if(Version>=400)
 fwrite(fid,hdr.soft2	,'float'); count=count+1;
 fwrite(fid,hdr.dta	,'float'); count=count+1;
 fwrite(fid,hdr.dtcs	,'float'); count=count+1;
 fwrite(fid,hdr.dtv	,'float'); count=count+1;
end
if(Version==211)
 fwrite(fid,hdr.Tdark	,'float'); count=count+1;
 fwrite(fid,hdr.Udd	,'float'); count=count+1;
 fwrite(fid,hdr.Ugg	,'float'); count=count+1;
 fwrite(fid,hdr.Udg	,'float'); count=count+1;
end

if(Version==401 | Version==402 | Version==211)
 fwrite(fid,hdr.lunit	,'real*8'); count=count+2;
 fwrite(fid,hdr.munit	,'real*8'); count=count+2;
 fwrite(fid,hdr.tunit	,'real*8'); count=count+2;
 fwrite(fid,hdr.vunit	,'real*8'); count=count+2;
 fwrite(fid,hdr.nunit	,'real*8'); count=count+2;
 fwrite(fid,hdr.Kunit	,'real*8'); count=count+2;
 fwrite(fid,hdr.eunit	,'real*8'); count=count+2;
 fwrite(fid,hdr.cunit	,'real*8'); count=count+2;
end

% rest of ibuf1
for i=count+1:100
 fwrite(fid,0,'integer*4');
end

count=0;
% ibuf2
fwrite(fid,hdr.irun 	,'uint'); count=count+1;
fwrite(fid,hdr.nobj 	,'uint'); count=count+1;
fwrite(fid,hdr.ngas 	,'uint'); count=count+1;
fwrite(fid,hdr.ndark 	,'uint'); count=count+1;
if(Version<400)
 fwrite(fid,hdr.L	,'uint'); count=count+1;
end
fwrite(fid,hdr.intl 	,'uint'); count=count+1;
fwrite(fid,hdr.nlmx 	,'uint'); count=count+1;
fwrite(fid,hdr.perr 	,'float'); count=count+1;

fwrite(fid,hdr.dtnorm 	,'float'); count=count+1;
fwrite(fid,hdr.sft0 	,'float'); count=count+1;
fwrite(fid,hdr.sftmin 	,'float'); count=count+1;
fwrite(fid,hdr.sftmax 	,'float'); count=count+1;
if(Version>=400)
 fwrite(fid,padding	,'float'); count=count+1;
end

fwrite(fid,hdr.h100 	,'float'); count=count+1;
fwrite(fid,hdr.box  	,'float'); count=count+1;
fwrite(fid,hdr.zmet 	,'float'); count=count+1;
if(Version<400);
 fwrite(fid,hdr.spc0	,'float'); count=count+1;
end
fwrite(fid,hdr.lcool 	,'uint'); count=count+1;

fwrite(fid,hdr.rmgas 	,'float'); count=count+1;
if(Version<400)
 fwrite(fid,hdr.rmdark	,'float'); count=count+1;
end
fwrite(fid,hdr.rmnorm	,'float'); count=count+1;
if(Version>=400)
 fwrite(fid,padding,'float'); count=count+1;
 fwrite(fid,padding,'float'); count=count+1;
end

fwrite(fid,hdr.tstart 	,'float'); count=count+1;
fwrite(fid,hdr.omega0 	,'float'); count=count+1;
fwrite(fid,hdr.xlambda0 ,'float'); count=count+1;
fwrite(fid,hdr.h0t0 	,'float'); count=count+1;

%fwrite(fid,hdr.Version	,'float'); count=count+1;
if(Version>=400)
 % These are used for isolated boundary conditions
 if(length(hdr.rcen)~=3)
  error('hdr.rcen must have 3 elements')
 end
 fwrite(fid,hdr.rcen	,'float'); count=count+3;
 fwrite(fid,hdr.rmax2	,'float'); count=count+1;
end

%rest of ibuf2 
for i=count+1:100
 fwrite(fid,0,'integer*4');
end

% rec size
fwrite(fid,400*4,'int');

% end of header

% now write out the particle data
write_fort(fid,itype,'int');
if(~CommonMassFlag)
 write_fort(fid,rm,'float');
end
write_fort(fid,r,'float');
write_fort(fid,v,'float');
if(hdr.ngas>0);
 write_fort(fid,h,'float');
 write_fort(fid,th,'float');
 write_fort(fid,dn,'float');
end
%_________________________________________________

function write_fort(fid,data,vartype)
numelements=size(data,1)*size(data,2);
fwrite(fid,numelements*4,'int');
fwrite(fid,data,vartype);
fwrite(fid,numelements*4,'int');
return;
