function [grp,num]=grp_list(grp_indx)
% syntax: [grp,num]=grp_list(grp_indx)
%
% Given the group index (from readgroup), grp_indx,
% returns the groups, sorted by size of groups from largest to smallest
% with grp being the group number, and size the group, num
%
% See also readgroup, grp_members

[num,grp]=histc(grp_indx,[0:max(grp_indx)]);

[num,i]=sort(num);
grp=grp(i);
num=flipud(num');
grp=flipud(grp');
