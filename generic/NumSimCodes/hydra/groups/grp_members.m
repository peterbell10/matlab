function members=grp_members(grp_indx,grp)
% syntax: members=grp_members(grp_indx,grp)
%
% Given the group index, grp_indx, return the members of group, grp.
% grp_indx is produced from readgroup
%
% See also readgroup, grp_list

members=find(grp_indx==grp);
