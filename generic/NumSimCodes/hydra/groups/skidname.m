function filename=skidname(irun,iter)
% syntax: filename=skidname(irun,iter)
%
% Returns the filename of a skid file, given the run number, irun, and
% the iteration, iter.
%
% Note: this assumes a name format of skid.irun.iter.grp
%
% See also: readgroups

is=num2str(iter);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end

filename=['skid.',num2str(irun),'.',label,'.grp'];
