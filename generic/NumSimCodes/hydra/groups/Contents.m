% Tools to work with cluster membership data produced by SKID.
%
% cluster_history	Finds the clustering history of the particles.
%
% clustering_movie	Creates a quick movie, M, of the clustering
%			for the cluster.
%
% grp_list		The groups sorted by size given the group index.
%
% grp_members		The group members, given the group index.
%
% grps_from_list	The list of grps for which particles in a list
%			are members.
%
% readgroups		Reads the group index file produced by SKID.
%
% skidname		Returns the filename of a skid file.
