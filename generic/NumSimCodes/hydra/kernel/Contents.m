% Routines to analyse the hydra kernel
%
% kernel	The value of the kernel at x.
%
% kernel_2d	The kernel contracted into two dimensions.
%		Good for projections onto image planes.
%
% kernel_1d	The kernel contracted into one dimension.
%		Can't remember what this is good for.
%
% I_dr_dphi, kern_int_1d, kern_int_2d
%		Routines used to calculate the above.

