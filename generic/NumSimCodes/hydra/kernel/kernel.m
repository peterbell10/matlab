% kernel: The SPH kernel function (as used by hydra)
%
% W=kernel(x)
%
% ARGUMENTS
%  x	(r-r_i)/h  [0,2]
%
% RETURNS
%  W    The SPH weight for x = r/h, in the range x=0 to 2
%
% USAGE
%  Normalized such that the integral of kernel(x) dV over all volume == h^3
%  i.e., if h=1, integral of kernel(x) dV = 1
%
%  So,
%   W_i = h^-3 * kernel(x) where x=r/h
%
% SEE ALSO
%  kernel_2d, kernel_1d

% AUTHOR: Eric Tittley
%
% COMPATIBILTY: Matlab and Octave

function W=kernel(x)

W=0*x;
mask1=(x>=0)&(x<=1);
mask2=(x>1)&(x<=2);
W(mask1)=1/(4*pi)*(4-6*x(mask1).^2+3*x(mask1).^3);
W(mask2)=1/(4*pi)*(2-x(mask2)).^3;
