% kernel_1d: SPH smoothing kernel (hyda) projected onto a line.
%
% W=kernel_1d(x)
%
% ARGUMENTS
%  x	(r-r_i)/h  [0,2]
%  
% RETURNS
%  W    The integrated SPH weight, integrated twice, first in straight parallel
%       lines, i.e projected onto a plane, then along parallel lines in the
%       plane, onto a line.  x = r/h, in the range x=0 to 2.
%
% USAGE
%  A note about normalization.
%  Normalized such that for h=1, integral of kernel_2d(x) dx = 1
%  i.e. sum(kernel_1d(abs(x))*dx) 
%  x=[-2:dx:2]
%  Also force min(kernel_2d(x)) == 0
%
%  More generally, this means that for x=r/h, integral of kernel_2d(x) dr = h
%
%  So,
%   W_i = h^-1 * kernel_1d(x)
%   where x = r/h
%
% SEE ALSO
%	kernel, kernel_2d

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

function W=kernel_1d(x)

p1=[0.699834064542440 0.034959644666720 -1.249690776078230 0.666265577374100];
p2=[1.465294746111800 -2.338947218701440 1.245296783997620 -0.221115217205790];
Fudge=1-0.001412189134939;
p1=p1*Fudge;
p2=p2*Fudge;

W=0*x;
mask1=(x>=0)&(x<=1);
mask2=(x>1)&(x<=2);
W=mask1.*(p1(1)+p1(2)*x+p1(3)*x.^2+p1(4)*x.^3)+mask2.*(p2(1)+p2(2)*x+p2(3)*x.^2+p2(4)*x.^3);
W(W<0)=W(W<0)*0;
