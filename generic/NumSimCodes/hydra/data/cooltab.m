function [ttab,ctab0]=cooltab(cunit)
% syntax: [ttab,ctab0]=cooltab(cunit)
%
% cunit=nunit*tunit

zmet=0;
kb=1.38e-16;
nctab=61;
ctab0=zeros(nctab,1);
ttab=ctab0;
ctab0(1)=0;
ttab(1)=1e4;
for i=2:nctab
 ttab(i)=ttab(i-1)*10^0.1;
 lambda=Lambda((ttab(i)+ttab(i-1))/2.,zmet);
 ctab0(i)=ctab0(i-1)+1.5*kb*(ttab(i)-ttab(i-1))/lambda/cunit;
end

function lambda=Lambda(t,zmet)
if(t<1e5)
 lambda=(1.4e-28+1.7e-27*zmet)*t;
else
 lambda=5.2e-28*t^0.5+1.4e-18*t^(-1)+1.7e-18*t^(-0.8)*zmet;
end


