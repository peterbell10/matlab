function [time,value]=ExtractPartParam(irun,itimes,particle,parameter)
% [time,value]=ExtractPartParam(irun,itimes,particle,parameter);
%
% where parameter is one of 'rm','r','v','th','dn','h'

if( ~(  strcmp(Parameter,'rm') ...
      | strcmp(Parameter,'r') ...
      | strcmp(Parameter,'v') ...
      | strcmp(Parameter,'th') ...
      | strcmp(Parameter,'dn') ...
      | strcmp(Parameter,'h') ) )
 error('Parameter must be one of ''rm'',''r'',''v'',''th'',''dn'',or, ''h'' ')
end

hdr.h100=1; Units
n_times=length(itimes);

for i=1:n_times
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,itimes(i)));
 if(strcmp(parameter,'r') | strcmp(parameter,'v') )
  cmd=['value(:,i)=',parameter,'(:,particle);'];
 else
  cmd=['value(i)=',parameter,'(particle);'];
 end
 eval(cmd)
 if(strcmp(parameter,'rm')), value(i)=value(i)  *hdr.munit*Mo; end % g
 if(strcmp(parameter,'r' )), value(i)=value(i,:)*hdr.lunit*hdr.atime; end % cm
 if(strcmp(parameter,'v' )), value(i)=value(i,:)*hdr.lunit*hdr.atime/hdr.tunit; end % cm
 if(strcmp(parameter,'th')), value(i)=value(i)  *hdr.Kunit*hdr.atime^2; end % K
 if(strcmp(parameter,'dn')), value(i)=value(i)  *hdr.munit*Mo/(hdr.lunit*hdr.atime)^3; end % g/cm^3
 time(i)=hdr.time;
end
