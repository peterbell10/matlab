function GasToGalaxies(filein,fileout)
% Merge the cold gas clumps into galaxies (single dark matter particle).
%
% GasToGalaxies(filein,fileout)
%
% Arguments
%  filein	The data file to take as input.
%  fileout	The new file to create, containing merged particles.
%
% This procedure is used when creating new initial conditions from
% previous data.  The presence of the clumps slows the code dramatically
% since the motions of the gas particles within the clumps dominates the
% timestep criterion.  The clumps are sub-resolution, so it is no loss
% in converting them to dark matter particles, particularly when the
% SPH calculations between the hot and cold particles are decoupled.
%
% See also:
%  readhydra writehydra fof

% AUTHOR: Eric Tittley
%
% HISTORY
%  02-02-06 First version.

%%% Read in input
if(~ exist(filein))
 error('Input file does not exist')
end
[rm,r,v,th,itype,dn,h,hdr]=readhydra(filein);

%%% Cool gas has T<20 000K
cold=find(th*hdr.Kunit<20000);

%%% Group the cool gas into clumps using FOF (friend-of-friend) method
% Thes parameters should be user-selectable.  Perhaps with an OPTIONS argument.
epsilon=min(h)/3;
MinMembers=10;
Period=1;
Verb_flag=0;
[GroupNum,NGroups]=fof(r(:,cold),epsilon,MinMembers,Period,Verb_flag);

%%% Loop though the groups, creating single dark matter particles for each
%%% group.
%% Initialise arrays
rm_gals=zeros(NGroups,1);
r_gals=zeros(3,NGroups);
v_gals=r_gals;
itype_gals=rm_gals;  % We won't have to update this.
%% Start loop
for i=1:NGroups
 % find the index for the particles in this clump
 clump_members=cold(find(GroupNum==i));
 % Calculate the parameters for this new galaxy
 rm_gals(i)=sum(rm(clump_members));
 r_gals(:,i)=mean(r(:,clump_members)')';
 v_gals(:,i)=mean(v(:,clump_members)')';
end

%%% Create a list of the gas particles to keep.
%%% There is probably a more efficient way of doing the following.
gas=find(itype==1);
dark=find(itype==0);
mask=1&gas;
mask(cold(find(GroupNum~=0)))=0*mask(cold(find(GroupNum~=0)));
new_gas=find(mask);

%%% Create output arrays
%%% Gas goes first, then dark matter.  New galaxies will be at the end, even
%%% though computationally they will be dark matter.
rm=[rm(new_gas);rm(dark);rm_gals];
itype=[itype(new_gas);itype(dark);itype_gals];
r=[r(:,new_gas),r(:,dark),r_gals];
v=[v(:,new_gas),v(:,dark),v_gals];
th=th(new_gas);
dn=dn(new_gas);
h=h(new_gas);

%%% Update header
hdr.ngas=sum(itype==1);
hdr.ndark=sum(itype==0);
hdr.nobj=hdr.ngas+hdr.ndark;

%%% Write the file out
writehydra(fileout,rm,r,v,th,itype,dn,h,hdr);

%%%%%%%%%%%%%%% END of GasToGalaxies %%%%%%%%%%%%%%%%%
