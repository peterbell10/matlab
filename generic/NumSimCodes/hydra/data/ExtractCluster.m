function ExtractCluster(source,cluster_centre,cluster_radius,New_Box_Size, ...
                        NewFilename)
BufferSize=2^20;
[rm,r,v,th,itype,dn,h,hdr_cluster]=readhydra(source);
clear v th itype dn h

disp('recentring')
% Centre on (0,0,0)
for i=1:BufferSize:length(rm)
 hi=min([i+BufferSize-1,length(rm)]);
 r(:,i:hi)=recentre(r(:,i:hi),cluster_centre)-0.5;
end
disp('done recentring')

% Find the objects in the cluster
disp('finding distances')
dist2=0*rm;
for i=1:BufferSize:length(rm)
 hi=min([i+BufferSize-1,length(rm)])
 dist2(i:hi)=sum(r(:,i:hi).^2);
end
disp('finding cluster elements')
in_cluster=find(dist2<cluster_radius^2);
clear dist2

r=r(:,in_cluster);

[rm,dum,v,th,itype,dn,h,hdr_cluster]=readhydra(source);
clear dum

% Extract the particles;
v=v(:,in_cluster);
rm=rm(in_cluster);
itype=itype(in_cluster);
gas_in_cluster=in_cluster(find(itype==1));
th=th(gas_in_cluster);
dn=dn(gas_in_cluster);
h=h(gas_in_cluster);

%%% Initialise the Header
hdr=hdr_cluster;

%%% Remove any bulk motion in the cluster
mean_v=mean(v');
for i=1:3
 v(i,:)=v(i,:)-mean_v(i);
end

%%%% Scale the rm, r, v, th
Units;
Scale=(hdr.lunit/Mpc)/New_Box_Size
r=r*Scale;
hdr.box=New_Box_Size;
hdr.h100=1;
hdr.lunit=New_Box_Size*Mpc;
% v is initially in the units of (box size 0->1) / (time units (0->1)
% That is, vunit=lunit/tunit
v=v*hdr.vunit; % cm/s
% The new tunit (for isolated BCs) is 10 Ga
hdr.tunit=10*Ga; % s
vunit_old=hdr.vunit;
hdr.vunit=hdr.lunit/hdr.tunit; % cm/s
v=v/hdr.vunit; % (sim units grid position)/(sim units time)
% munit for isolated BCs is 10 GMo
rm=rm*hdr.munit/1e10;
hdr.munit=1e10;
% eunit and Kunit scale as vunit^2
hdr.eunit=hdr.vunit^2; % (cm/s)^2
hdr.Kunit=hdr.Kunit*(hdr.vunit/vunit_old)^2; % K
th=th*(vunit_old/hdr.vunit)^2;

% Update hdr
hdr.nobj=length(itype);
hdr.ngas =sum(itype==1);
hdr.ndark=sum(itype==0);
hdr.times=0*hdr.times;
hdr.times(1:11)=[0:0.01:0.1];
hdr.itime=1;
hdr.time=1e-6; % can't set it to zero, or hydra cops out after the 1st iter.
hdr.rcen=[0.5 0.5 0.5]; % The centre of the simulation
hdr.rmax2=1.0; % This sets the maximum distance from the centre of the cluster
	       % that a particle can get before it is deemed `lost'

% Reposition r
for i=1:3
 r(i,:)=r(i,:)+0.5;
end

writehydra(NewFilename,rm,r,v,th,itype,dn,h,hdr)

clear
