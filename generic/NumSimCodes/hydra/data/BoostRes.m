function BoostRes(file_in,file_out,factor)
% Boost the resolution of a Hydra file, conserving local dark matter dispersion.
%
%  BoostRes(file_in,file_out,factor)
%
% ARGUMENTS
%  file_in	The input file.
%  file_out	The file to create.
%  factor	The factor by which to boost the resolution (positive integer).
%
% SEE ALSO
%  BoostResolution

% AUTHOR: Eric Tittley
%
% HISTORY
%  02-10-18 Final first version.
%  02-11-18 Added comments and cleaned up code a bit.
%  02-11-20 Removed a stray 'toc'.

[rm,r,v,th,itype,dn,h,hdr]=readhydra(file_in);
[gasdn,darkdn,gash,darkh]=readdensities([file_in,'.densities']);
dark=find(itype==0);

%% First pass, which is pretty quick.  Boost the resolution by the given
%% factor.
if(~ exist(file_out) )
 disp('Boosting the resolution...')
 tic
 BoostResolution(file_in,factor,file_out)
 toc
end
%% At this point, all is fine except the DM velocities in file_out.  We must
%% Recalculate them

%% Start by calculating the local DM velocity dispersions.
if(~ exist('sigma.mat') )
 disp('Calculating DM velocity dispersions...')
 tic
 sigma=v_disp(r(:,dark),v(:,dark),darkh);
 toc
 save sigma.mat sigma
else
 disp('Loading DM velocity dispersions...')
 load sigma
end

%%% Initialise the new vector of V's
% Preliminaries
N=factor;
options = optimset('Display','notify',...
                   'TolX',0.0001,...
                   'TolFun',0.01,...
                   'MaxFunEvals',50001,...
                   'MaxIter',50000);
disp('Calculating new values for v...')
if(exist('v_new.mat'))
 disp('Loading a previous dump')
 clear i % Otherwise loading in the value of i won't override i as complex
 load v_new.mat
 Start=i;
else
 Start=1;
 i=1;
 v_new=zeros(3,length(dark)*factor);
end

TStart=clock;
iStart=i;
% Main loop through all dark matter particles.
for i=Start:length(dark)
 if(rem(i,100)==0)
  Now=clock;
  Elapsed=etime(Now,TStart);
  %if(i>iStart>0)
  if(i>iStart)
   TotalTime=Elapsed/(i-iStart)*(length(dark)-iStart); %s
   HMS=dms2mat(deg2dms(TotalTime/3600));
   TimeStr=[' Total time: ', ...
                int2str_zeropad(HMS(1),2), ...
            ':',int2str_zeropad(HMS(2),2), ...
            ':',int2str_zeropad(HMS(3),2)];
   TimeLeft=Elapsed/(i-iStart)*(length(dark)-i);
   HMS=dms2mat(deg2dms(TimeLeft/3600));
   TimeStr=[TimeStr, '  Time left: ', ...
                     int2str_zeropad(HMS(1),2), ...
                 ':',int2str_zeropad(HMS(2),2), ...
                 ':',int2str_zeropad(HMS(3),2)];
  else
   TimeStr='';
  end % i > iStart
  disp([int2str(i),' of ',int2str(length(dark)),TimeStr ])
  if(rem(i,1000)==0)
   save v_new.mat v_new i
  end
 end
 V=v(:,dark(i))';
 P=[N,V,sigma(i)];
 vo=randn(3,N-1)*sigma(i)/sqrt(3)+V'*ones(1,N-1);	% Initial guess at the new velocities
 v_dummy=N*V'-sum(vo,2);	% Conserve total momentum
 vo=[vo,v_dummy];
 span=(i-1)*N+[1:N];
 v_new(:,span)=fminsearch('ErrorInSystem',vo,options,P);
end

if(i==0), i=length(dark); end
save v_new.mat v_new i

% Now we have the new v's, put them in the new file
disp('Writing final output file')
[rm,r,v,th,itype,dn,h,hdr]=readhydra(file_out);
dark=find(itype==0);
v(:,dark)=v_new;
writehydra(file_out,rm,r,v,th,itype,dn,h,hdr);

!rm sigma.mat v_new.mat
