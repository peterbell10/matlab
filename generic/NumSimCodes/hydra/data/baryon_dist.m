function [num,x]=baryon_dist(datafile)

[rm,r,v,th,itype,dn,h,time,atime]=readdata(datafile);

%min_density=2^(-8);
%max_density=2^15;

min_exp=-7;
max_exp=15;

exps=[min_exp:0.2:max_exp];

x=(2*(1&(1:length(exps)))).^exps;

[num,x]=hist(dn(1:length(dn)/2),x);

num=num*2/length(dn);
