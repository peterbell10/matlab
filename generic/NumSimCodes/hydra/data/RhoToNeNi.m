function [ne,ni]=RhoToNeNi(rho,Z)
% Determine ne and ni for a fully ionized plasma
%
% [ne,ni]=RhoToNeNi(rho,Z);
%
% Arguments:
%  rho	The Gas density		(g cm^-3)
%  Z	The metalicity
%
% Output:
%  ne	The electron density	(cm^-3)
%  ni	The ion density		(cm^-3)
%
% Note: uses the Solar abundances of Anders and Grevesse (1989)
%
% See also:
%   Lx cooltab2

% Author: Eric Tittley

% HISTORY
%  02-02-01 First version

%%%%% The solar abundance table. %%%%%
%% They are sorted by atomic number, so A(1) is for H, A(20) is Ca
%% Atomic numbers span 1 to 90
%% They are in log_10.  -10 => nil contribution.
%A=( 12.00 10.99 1.16 1.15 2.6  ...
%     8.56  8.05 8.93 4.56 8.09 ...
%     6.33  7.58 6.47 7.55 5.45 ...
%     7.21  5.5  6.56 5.12 6.36 ...
%     3.10  4.99 4.00 5.67 5.39 ...
%     7.54  4.92 6.25 4.21 4.60 ...
%     2.88  3.41 -10  -10  -10  ...
%     -10   2.60 2.90 2.24 2.60 ...
%     1.42  1.92 -10  1.84 1.12 ...
%     1.69  0.94 1.86 1.66 2.0  ...
%     1.0   -10  -10  -10  -10  ...
%     2.13  1.22 1.55 0.71 1.50 ...
%     -10   1.00 0.51 1.12 -0.1 ...
%     1.1   0.26 0.93 0.00 1.08 ...
%     0.76  0.88 -10  1.11 -10  ...
%     1.45  1.35 1.8  1.01 -10  ...
%     0.9   1.85 -10  -10  -10  ...
%     -10   -10  -10  -10  0.12 );
%
%% Normalise the Abundances by H's
%A=A-A(1);
%
%%%%%%%%%%%%%  Don't need this %%%%%%%%%%%%%


%%%%%%%% From Allen's, 4th edition %%%%%%%%%%%%
%%                                Stripped   %%
%% Element group Number   Mass    electrons  %%
%%					     %%
%%  H		 100	  100	  100	     %%
%%  He		   9.8	   39	   20	     %%
%%  C,N,O,Ne	   0.145    2.19    1.1	     %%
%%  Other	   0.013    0.44    0.21     %%
%%					     %%
%%  Total	 109.96   141.63  121.3	     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% So, for every 1 H, there are
%% 1+Z*(0.098+0.00145+0.00013) ions
%% 1+Z*(0.20+0.011+0.0021) electrons
%% For every 1g H, there are
%% 1+Z*(0.39+0.0219+0.0044) g of ions

rho_H = rho/(1+Z*(0.39+0.0219+0.0044));		% g cm^-3 of H
nH = rho_H / 1.627623e-24 ;			% cm^-3 for H
ni = nH * (1+Z*(0.098+0.00145+0.00013)) ;	% cm^-3 for all ions
ne = nH * (1+Z*(0.20+0.011+0.0021)) ;		% cm^-3 for electrons

%%% END OF RhoToNeNi %%%
