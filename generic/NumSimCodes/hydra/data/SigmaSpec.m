function [sigma,meanN]=SigmaSpec(r,radii,Nsamples,PeriodicFlag)
%
% [sigma,meanN]=SigmaSpec(r,radii,Nsamples,PeriodicFlag)
%
% r and radii should be on the scale 0 to 1
% Repeating boundary conditions are not assumed if PeriodicFlag is not
% set to 1.

if (nargin==3), PeriodicFlag=0; end

Nradii=length(radii);

if(length(Nsamples)~=Nradii), Nsamples=Nsamples(1)*(1&radii); end

%initialise the random number generator
rand('state',sum(100*clock));

[Nparticles,dummy]=size(r);

mask=ones(Nparticles,1);
sigma=0*radii;
meanN=sigma;

for i=1:Nradii
 radius=radii(i)
% Find the random centres
 if(PeriodicFlag==0)
  span=1-2*radius;
 else 
  span=1;
 end
 centres=radius+span*rand(Nsamples(i),3);
 Num=zeros(1,Nsamples(i));
 for j=1:Nsamples(i)
  dummy=mask*centres(j,:);
  if(PeriodicFlag==0)
   r_temp=r-dummy;
  else
   r_temp=mod((r-dummy)+0.5,1)-0.5;
  end
  dist=sqrt( sum(r_temp'.^2) );
  Num(j)=length(find(dist<radius));
 end
 Num=Num/Nparticles/(4/3*pi*radius^3);
 meanN(i)=mean(Num);
 sigma(i)=std(Num);
end
