function StripLost(filein,fileout)
% Removed particles flagged as 'lost'
%
% StripLost(filein,fileout)
%
% filein	The name of the file to process.
% fileout	The name of the file to which to save the cleaned data.
%
% See also:
%  readhydra

% AUTHOR: Eric Tittley
%
% HISTORY: 02-02-03 First version

if(~ exist(filein))
 error('file does not exist')
end

% Read the input file
[rm,r,v,th,itype,dn,h,hdr]=readhydra(filein);

% lost particles have itype==-2
dark=find(itype==0);
gas=find(itype==1);

% Create new arrays
rm=[rm(gas);rm(dark)];
r=[r(:,gas),r(:dark)];
v=[v(:,gas),v(:dark)];
th=th(gas);
itype=[itype(gas);itype(dark)];
dn=dn(gas);
h=h(gas);

% Update header
hdr.ngas=length(gas);
hdr.ndark=length(dark);
hdr.nobj=hdr.ngas+hdr.ndark;

% Write the output file
writehydra(fileout,rm,r,v,th,itype,dn,h,hdr);

