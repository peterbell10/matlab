function VirFactor=virial_param(d,rad,r_cut,unitsfile,atime)
%VirFactor=virial_param(d,rad,r_cut,unitsfile,atime)
%
%Calculates the Virial Factor (P/P_grav) at the overdensity radii
% for the clusters whose profiles are in d and overdensity radii in rad.
% The profile cutoff radii are in r_cut.
% The unitsfile is the file to read in to get the units relevant for the
% simulation.
% atime is the expansion (1 for present).
%
%See also: Euler, get_cutoffs

Mpc=3.085678e24;       %Mpc in cm

[num_bins,num_fields,num_profiles]=size(d);

for i=1:num_profiles
 [r,P,dPdr,Rho_dPhi_dr,Rho_dPhi,Pres_g,P_pres,Pk]=Euler(d(:,:,i),unitsfile,atime,r_cut(i));
 for k=1:2
  dummy=abs(r-rad(i,k)*Mpc);
  bin=find(dummy==min(dummy));
  if(bin>5 & bin<length(P)-2)
%   size(P),bin
   VirFactor(i,k)=mean(P(bin-4:bin+2)./Pres_g(bin-4:bin+2));
  else
   VirFactor(i,k)=0;
  end
 end
end
