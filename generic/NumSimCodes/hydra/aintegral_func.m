function y=aintegral_func(t,Omega_o,Lambda_o)
y=1./(sqrt( 1 + Omega_o*(1./t - 1) + Lambda_o*(t.^2 - 1) ) );
