% Tools for analysing gradients in SPH
%
% constgrad		Creates a constant density gradient.
%
% meanSPHprofile	Finds the mean density of particles in slice bins.
%
% sphgradient		Creates a hydra datavolume this has a density profile
%			with rho = rho_o r^-1
