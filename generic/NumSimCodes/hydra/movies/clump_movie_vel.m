function M=clump_movie(run,start,spacing,finish,gas_index,clump_index,Vmin,window)
%M=clump_movie(run,start,spacing,finish,gas_index,clump_index,Vmin,window)

disp('Initializing')
num=(finish-start)/spacing+1;
M=moviein(num,gcf);

if(nargin==7), window=[0 1 0 1]; end

disp('Starting main loop')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 file=dataname(run,i);

 disp(['Reading file ',file])
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(file,'l');

 speed=sqrt( sum(v'.^2) );
 fast_clump=find(speed(clump_index)>Vmin);
 fast_gas=  find(speed(  gas_index)>Vmin);

 disp('Plotting')
 clf
 hold on
 h=plot(r(clump_index(fast_clump),1),r(clump_index(fast_clump),2),'b.');
 h=plot(r(gas_index(fast_gas),1),r(gas_index(fast_gas),2),'r.');
 set(h,'markersize',1);
 axis(window)
 hold off

% Label the time.
 disp('Labelling the time.')
 text(0,1.02,num2str(time));

% Grab the frame for the movie
 disp('Grabbing the frame.')
 M(:,j)=getframe(gcf);
end

disp('Compiling the movie into MPEG.');
map=colormap;
command=['mpgwrite(M,map,''run',base(2:5),'.mpg'')']
eval(command)
