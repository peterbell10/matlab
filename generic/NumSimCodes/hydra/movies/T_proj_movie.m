function M=T_proj_movie(run,start,spacing,finish,resolution)

disp('initializing')

num=(finish-start)/spacing;
M=moviein(num+1,gcf);

% Load in the final state to determine
%  the location of peak density
disp('Loading in final state');
is=num2str(finish);
if length(is)==3 label=['0',is];
elseif length(is)==2 label=['00',is];
elseif length(is)==1 label=['000',is];
else label=is;
end
file=['d0',num2str(run),'.',label]
[rm,r,v,th,itype,dn,h,time,atime]=readdata(file);

% Find all the gas particles
disp('Finding the gas particles')
gas=find(itype);

dn=dn(gas);
th=th(gas);
r=r(gas,:);

disp('Setting the temperature max cutoff to 10keV')
%thmax=max(th)/10;
numgas=length(dn);
if(numgas==32768) Kunit=1738935160;
else error(['Help! I do not know what units to use if numgas=',int2str(numgas)])
end
% set Tmax to be 10keV where 1keV=11.6059e6 K
thmax=10/(Kunit/11.6059e6);

disp('Finding the location of the highest density')
centre=centre_quick(r,dn,0);
shift=[1.5,1.5,1.5]-centre;

disp('Starting the main loop')
j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 disp(' Reading in the datafile')
 is=num2str(i);
 if length(is)==3 label=['0',is];
 elseif length(is)==2 label=['00',is];
 elseif length(is)==1 label=['000',is];
 else label=is;
 end
 file=['d0',num2str(run),'.',label]
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(file);

% We only want the gas particles
 disp(' Isolating the gas and centring')
 r=r(gas,:);
 th=th(gas)*atime^2;

% Shift the points
 r=rem(r+(1&gas)*shift,1);

 disp(' Finding the projected temperatures')
 [R,G,B]=T_proj(r(:,1),r(:,2),th,resolution,thmax,1);

 disp(' Plotting')
 imshow(R,G,B) 

% Label the time.
 disp(' Labelling the time')
 text(0,-5,['Run ',num2str(run),' Time:',num2str(time)]);

% Grab the frame for the movie
 disp(' Grabbing the frame')
 M(:,j)=getframe(gcf);
end

disp('End of main loop, making movie')
map=colormap;
mpgwrite(M,map,['run0',num2str(run),'.proj_gas_temp.mpg'],[1 0 1 0 10 8 10 25])
