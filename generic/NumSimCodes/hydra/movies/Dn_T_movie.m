function M=Dn_T_movie(run,start,spacing,finish)
% M=Dn_T_movie(run,start,spacing,finish)
%
hdr.h100=1; Units
num=(finish-start)/spacing;
M=moviein(num+1,gcf);

mask=0;
centre=0;

AxisV=[1e-32 1e-24 1e4 1e9];

j=num+2;
for i=finish:-spacing:start
 j=j-1;

% Read in the next datafile
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(run,i));

% We only want the gas particles
% disp(' Isolating the gas')
 gas=find(itype);
 dn=dn(gas);
 th=th(gas);

% Convert to real units
% Kunit=1.7389e9;
% disp(['Using ',num2str(Kunit),' for Kunit'])
 th=th*hdr.Kunit*hdr.atime^2;
% disp(['Density in units of Rho_critical'])
 dn=dn*hdr.munit*Mo/(hdr.lunit*hdr.atime)^3;

% disp(' Plotting')
 h=loglog(dn,th,'.','markersize',1);
 axis(AxisV);

% Label the time.
% disp(' Labelling the time')
 text(AxisV(1),AxisV(4)*2,'Run ',num2str(run),' Time:',num2str(hdr.time)]);
 xlabel('g/cm^3'); ylabel('K');

 M(:,j)=getframe(gcf);
end

map=colormap;
command=['mpgwrite(M,map,''run0',num2str(run),'.Dn_T.mpg'')']
eval(command)
