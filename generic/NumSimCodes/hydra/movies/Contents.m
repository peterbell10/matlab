% Routines to make MPEG movies out of Hydra data.  Many are obsolete or
% could use improvement.  Ultimately, all movies should be in GIF format.
%
% Dn_T_movie		rho-T space evolution
%
% T_hist_mov		T distribution evolution
%
% T_proj_movie		Projected temperature
%
% clump_movie		Plots a clump in a separate colour
%
% clump_movie_vel	Same a clump_movie, but only plots particles
%			above a certain speed
%
% clus_mov		Two frames, one for dark matter and one for gas
%
% h_proj_movie		Projected mass evolution. Produces an image array.
%
% images2movies		Turns an image array into an MPEG.
%
% makemovie_S_prof	The entropy profile evolution of a cluster.
%
% makemovie_T_prof	The T profile evolution of a cluster.
%
% makemovie_rho_prof	The rho profile evolution of a cluster.
%
% makemovie_shock	2-panel movie (T and S)
%
% makemovie_temp	4-panel plot (T, hottest, densest, very hottest)
%
% makemovie_zoom_temp	4-panel plot (T for all, hottest for all,
%			T for the core particles, Very hottest core)
%
% quick_movie		Makes a movie, centring on the densest peak
%
% simple_movie		Particle positions.
