function M=T_hist_mov(run,start,spacing,finish,bins,index,Kunit,max)
%M=T_hist_mov(run,start,spacing,finish,bins,index,Kunit,max)

lo=bins(1); hi=bins(end);

num=(finish-start)/spacing+1;
M=moviein(num,gcf);

j=0;
for i=start:spacing:finish
 j=j+1;

% Read in the next datafile
 file=dataname(run,i);

 disp(['Reading file ',file])
 [rm,r,v,th,itype,dn,h,time,atime]=readdata(file);

 histc(log10(th(index)*Kunit),bins);

 axis([lo hi 0 max])

% Label the time.
% disp('Labelling the time.')
 text(lo,-max/10,num2str(time));

% Grab the frame for the movie
 disp('Grabbing the frame.')
 M(:,j)=getframe(gcf);
end

disp('Compiling the movie into MPEG.');
map=colormap;
command=['mpgwrite(M,map,''run',num2str(run),'.T_hist.mpg'')']
eval(command)

