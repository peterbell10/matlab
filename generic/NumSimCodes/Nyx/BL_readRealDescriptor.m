% BL_readRealDescriptor: Read a BoxLib RealDescriptor structure from file stream
%
% RealDescriptor=BL_readRealDescriptor(fid)
%
% RealDescriptor format (from FabConv.H)
%          format[1] = number of bits per number
%          format[2] = number of bits in exponent
%          format[3] = number of bits in mantissa
%          format[4] = start bit of sign
%          format[5] = start bit of exponent
%          format[6] = start bit of mantissa
%          format[7] = high order mantissa bit (CRAY needs this)
%          format[8] = bias of exponent
% I *think* order has to do with big-endian or little endian.
%
% So if order is increasing, then it is big endian; if order is decreasing then
% little endian.

function RealDescriptor=BL_readRealDescriptor(fid)

RealDescriptor.format=BL_readArray(fid);
dummy=fscanf(fid,'%c',1); %','
RealDescriptor.order=BL_readArray(fid);
dummy=fscanf(fid,'%c',1); %')'
