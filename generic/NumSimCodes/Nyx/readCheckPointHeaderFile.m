% readCheckPointHeaderFile: Read a CheckPoint Header file
%
% Header=readCheckPointHeaderFile(file)

function Header=readCheckPointHeaderFile(file)

fid=fopen(file,'r');

fileTypeStr=fscanf(fid,'%s',1);
if(isempty(strfind(fileTypeStr,'CheckPoint')))
 error([file,' is not a CheckPoint header file. It is ',fileTypeStr]);
end

Header.spdim       = fscanf(fid,'%i',1);
Header.cumtime     = fscanf(fid,'%f',1);
Header.mx_lev      = fscanf(fid,'%i',1);
Header.finestLevel = fscanf(fid,'%i',1);
