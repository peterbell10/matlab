% Nyx_writeCellH: Write the Nyx Cell_H file
%
% Nyx_writeCellH(file,CellH)
%
% ARGUMENTS
%  file     File to write 'path/to/Level_0/Cell_H'
%
%  CellH    Structure with the fields
%            vers
%            how
%            ncomp
%            ngrow
%            Box
%             .smallEnd
%             .bigEnd
%             .type
%            ncomp

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function Nyx_writeCellH(file,CellH);

fid=fopen(file,'w');

fprintf(fid,'%i\n',CellH.vers);
fprintf(fid,'%i\n',CellH.how);
fprintf(fid,'%i\n',CellH.ncomp);
fprintf(fid,'%i\n',CellH.ngrow);
fprintf(fid,'(1 0\n');
BL_writeBox(fid,CellH.Box);
fprintf(fid,'\n)\n1\nFabOnDisk: Cell_D_0000 0\n\n');
fprintf(fid,'1,%i\n',CellH.ncomp);
for i=1:CellH.ncomp
 fprintf(fid,'%e,',CellH.min(i));
end
fprintf(fid,'\n\n');
fprintf(fid,'1,%i\n',CellH.ncomp);
for i=1:CellH.ncomp
 fprintf(fid,'%e,',CellH.max(i));
end
fprintf(fid,'\n');

fclose(fid);
