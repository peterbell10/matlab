% Nyx: Tools to manipulate Nyx AMR files 
%
% Nyx is based on the AMReX (nee BoxLib) library, so many functions are
% specific to the more general AMReX (BL) library.  And some of the Nyx
% functions should be renamed to indicate they actually relate to the AMReX
% Library
%
% Nyx-specific
%
% convertEnzoICtoNyxIC  Skeleton script for generating Nyx ICs from Enzo ICs
%
% Nyx_FAB_readHeader    Read n FAB file header
%
% Nyx_FAB_writeHeader   Write a FAB file header
%
% Nyx_writeCellH        Write the Nyx Cell_H file
%
% NyxReadRunlog         Read the runlog file produced by Nyx at runtime
%
% readHyperCLawFile     Read a HyperCLaw-format file
%
% readNyxGrid		Read Nyx "plot" files
%
% writeNyxGrid		Write Nyx gas grid file
%
%
% AMReX-specific (nee BoxLib. Rebranded as AMReX in 2017)
%
% BL_readArray            Read a BoxLib Array structure from file stream
%
% BL_readBox              Read a BoxLib Box structure from file stream
%
% BL_readRealDescriptor   Read a BoxLib RealDescriptor structure from file
%                         stream
%
% BL_writeArray           Write a BoxLib Array structure to file stream
%
% BL_writeBox             Write a BoxLib Box structure to file stream
%
% BL_writeRealDescriptor  Write a BoxLib RealDescriptor structure to file
%                         stream


