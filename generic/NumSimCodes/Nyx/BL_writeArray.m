% BL_writeArray: write a BoxLib Array structure to file stream
%
% BL_writeArray(fid,Array)
function BL_writeArray(fid,Array)

fprintf(fid,'(%i',length(Array));
fprintf(fid,', (');
fprintf(fid,'%i',Array(1));
for i=2:length(Array)
 fprintf(fid,' %i',Array(i));
end
fprintf(fid,'))'); % ))
