% BL_writeRealDescriptor: write a BoxLib RealDescriptor structure to file stream
%
% RealDescriptor=BL_writeRealDescriptor(fid,RealDescriptor)
%
% RealDescriptor format (from FabConv.H)
%          format[1] = number of bits per number
%          format[2] = number of bits in exponent
%          format[3] = number of bits in mantissa
%          format[4] = start bit of sign
%          format[5] = start bit of exponent
%          format[6] = start bit of mantissa
%          format[7] = high order mantissa bit (CRAY needs this)
%          format[8] = bias of exponent
% I *think* order has to do with big-endian or little endian.
%
% So if order is increasing, then it is big endian; if order is decreasing then
% little endian.

function BL_writeRealDescriptor(fid,RealDescriptor)

BL_writeArray(fid,RealDescriptor.format);
fprintf(fid,',');
BL_writeArray(fid,RealDescriptor.order);
fprintf(fid,')');
