clear
% convertEnzoICtoNyxIC: skeleton script for generating Nyx ICs from Enzo ICs

% As best as I can tell, Nyx expects the following units
%  density           Mo/Mpc^3
%  [xyz]mom          Mo (km/s)
%  rho_[Ee]          Mo (km/s)^2
%  Particle Mass     Mo
%  Particle Velocity km/s

ParticleFileIsASCIIFlag=0;

Omega_m=0.28;
%Omega_b=0.046;
Omega_b=0.279; % For Omega_b == Omega_m
h=0.7;
hdr.h100=h;
Boxsize=64; % Comoving Mpc
z_i=63;
z=63;
To=100; % K
Y = 0.2486;
SI='MKS';
a=1/(z_i+1); % Expansion Factor
[rhoU,LU,TU,tU,vU]=Enzo_CosmologyGetUnits(Omega_m,h,Boxsize,z_i,z,SI);
Units

% EnergyPerCell = MassPerCell / MassPerParticle * EnergyPerParticle(T)
%  MassPerCell will be D.density
MassH  = 1.00794*amu; % kg
MassHe = 4.002602*amu; % kg
MassPerParticle = 1/((1-Y)/MassH+Y/MassHe); % kg [Note: Mass per gas particle]
% EnergyPerCellScale is still small by a factor of two. I've stuck in a h^-2
% but can't prove it should be there.
EnergyPerCellScale = (3/2) * kb*To / MassPerParticle / h^2 / 1e6; % Mo (km/s)^2

% Gas Grids

D.density = h5read('GridDensity','/GridDensity');
Dims      = size(D.density)';
CellSize  = Boxsize./Dims(:)'; % Force a row vector
DensityScaleUnit=rhoU*(a*Mpc)^3/Mo; % kg/m^3 * (m/Mpc)^3 Converts to Mo/coMpc^3
D.density = DensityScaleUnit*D.density; % Mo coMpc^-3
Velocities=h5read('GridVelocities','/GridVelocities');
dV = Boxsize^3/prod(Dims); % coMpc^3
VelocityScale = vU*a/1e3 * dV;
D.xmom=VelocityScale*Velocities(:,:,:,1).*D.density; % Mo km/s
D.ymom=VelocityScale*Velocities(:,:,:,2).*D.density;
D.zmom=VelocityScale*Velocities(:,:,:,3).*D.density;
D.Temp=0*D.density+To;

rho_e=D.density*EnergyPerCellScale;
MomMag=sqrt(D.xmom.^2+D.ymom.^2+D.zmom.^2); % Mo km/s
V=MomMag./D.density; % km/s
% Add the kinetic energy to get rho_E
D.rho_E=rho_e+0.5*MomMag.*V; % Mo (km/s)^2
D.rho_e = rho_e; % rho_E must be written before rho_e
clear rho_e

% Default settings for H.  No need to change these
H.Content='FAB';
H.RealDescriptor.format = [64 11 52 0 1 12 0 1023];
H.RealDescriptor.order  = [8 7 6 5 4 3 2 1];

% Grid Dimensions (C is zero-index-based)
H.Box.smallEnd = [0 0 0];
H.Box.bigEnd   = Dims-1;
H.Box.type     = [0 0 0];

% The number of fields
H.nFields=length(fieldnames(D));

TestDirExists=dir('InitialGasGrid');
if(length(TestDirExists)>0)
 rmdir('InitialGasGrid','s');
end
mkdir('.','InitialGasGrid');
mkdir('./InitialGasGrid','Level_0');
writeNyxGrid('InitialGasGrid/Level_0/Cell_D_0000',D,H);



% The Cell_H file
% From VisMF.H
%        int                  m_vers;  // The version # of the Header.
%        How                  m_how;   // How the MF was written to disk.
%        int                  m_ncomp; // Number of components in MF.
%        int                  m_ngrow; // The number of ghost cells in MF.
%        BoxArray             m_ba;    // The BoxArray of the MF.
%        Array< FabOnDisk >   m_fod;   // FabOnDisk info for contained FABs.
%        Array< Array<Real> > m_min;   // The min()s of each component of FABs.
%        Array< Array<Real> > m_max;   // The max()s of each component of FABs.
CellH.vers=1;
CellH.how =0;
CellH.ncomp=H.nFields;
CellH.ngrow=0;
CellH.Box.smallEnd=[0 0 0];
CellH.Box.bigEnd  = Dims-1;
CellH.Box.type    =[0 0 0];
CellH.min=zeros(H.nFields,1);
CellH.max=zeros(H.nFields,1);
Fields=fieldnames(D);
for i=1:H.nFields
 Data=getfield(D,Fields{i});
 CellH.min(i)=min(Data(:));
 CellH.max(i)=max(Data(:));
end
Nyx_writeCellH('InitialGasGrid/Level_0/Cell_H',CellH);

% Dark Matter particles
% The following is what the documentation suggests, but produces small velocities
VelocityUnit    = vU*a/1e3; % a km/s (internally, u = a v)

% Temmporarily save in ASCII format until I figure out the Binary format
%  Format for each particle is (X Y Z Mass Vx Vy Vz)
Positions  = h5read('ParticlePositions','/ParticlePositions');
Positions  = Boxsize * Positions; % Mpc
Velocities = h5read('ParticleVelocities','/ParticleVelocities');
Velocities = VelocityUnit * Velocities ; % a^-1 km/s
NParticles = length(Positions);
MassPerDMParticle = (Boxsize*Mpc)^3*Rho_c*(Omega_m-Omega_b)/NParticles/Mo;
Mass       = MassPerDMParticle*ones(NParticles,1);

if(ParticleFileIsASCIIFlag==1)
 fid=fopen('InitialParticles.dat','w');
 fprintf(fid,'%i\n',NParticles);
 fprintf(fid,'%e %e %e %e %13.6e %13.6e %13.6e\n',[Positions, Mass, Velocities]');
 fclose(fid);
else % binary
 NP = NParticles;
 DM = length(Dims);
 NX = DM+1;  % Number of floats to write, other than the three position values.
             % In our case it is 4 (mass + Velocity)
 fid=fopen('InitialParticles.bin','w');
 fwrite(fid,NP,'int64');
 fwrite(fid,DM,'int32');
 fwrite(fid,NX,'int32');
 Array=[Positions, Mass, Velocities]';
 fwrite(fid,Array,'single');
 fclose(fid);
end


% Nyx doesn't need the Header File, but readNyxGrid does
Header.Fields        = fieldnames(D);
Header.BL_SPACEDIM   = 3;
Header.cumTime       = 0;
Header.finestLevel   = 0;
Header.ProbLo        = [0 0 0];
Header.ProbHi        = Boxsize*[1 1 1];
Header.refRatio      = [];
Header.Geom.smallEnd = [0 0 0];
Header.Geom.bigEnd   = Dims-1;
Header.Geom.type     = [0 0 0];
Header.levelSteps    = 0;
Header.CellSize      = CellSize;
Header.Coord         = 0;
Header.bndry         = 0;
Header.level         = 0;
Header.gridSize      = 1;
Header.cur_time      = 0;
Header.gridLocLo     = [0 0 0];
Header.gridLocHi     = Boxsize*[1 1 1];
Header.PathNameInHeader = 'Level_0/Cell';

writeHyperCLawFile('InitialGasGrid/Header',Header);
