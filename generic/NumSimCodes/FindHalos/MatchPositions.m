% MatchPositions:  Match a list of positions with another list
%
% [Index,IndexMatch] = MatchPositions(Positions,MatchPositions,tol,[L_MAX])
%
% ARGUMENTS
%  Positions       The list of positions to see if they have a match [3xN] [0,1)
%  MatchPositions  The list of positions to match to [3xM] [0,1)
%  tol		   The tolerance.  If two positions are separated by less
%		   than tol, then they are considered the same position.
%  L_MAX	   (optional) Maximum number of chaining cells per dimension.
%
% RETURNS
%  Index        The index into Positions which have matches in MatchPositions
%  IndexMatch   The index into MatchPositions which have matches in Positions
%
% USAGE NOTES
%  Positions(Index) == MatchPositions(IndexMatch)

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab
%
% HISTORY
%  120702 First vesion.
%  120706 Added IndexMatch as output

function [Index,IndexMatch] = MatchPositions(Positions,MatchPositions,tol,L_MAX)

%%% Set L_MAX if it was not passed in.
if(nargin<4)
 L_MAX=32;
end

%%% Check the size of Positions to match.  We can only deal with 3xN
[Ndim,NumPos]=size(Positions);
if(Ndim~=3)
 if(NumPos==3)
  Positions=Positions';
  NumPos=Ndim;
 else
  error(' I do not know what to do with other than 3 dimensions')
 end
end

%%% Check the size of Positions to match to.  We can only deal with 3xM
[Ndim,NumMatch]=size(MatchPositions);
if(Ndim~=3)
 if(NumMatch==3)
  MatchPositions=MatchPositions';
  NumMatch=Ndim;
 else
  error(' I do not know what to do with other than 3 dimensions')
 end
end

% Start with assuming no matches
MatchListIn = zeros(1,NumPos);
MatchListOut= zeros(1,NumPos);

%%%% NOTE: Assume all centres are between 0 and 1
%% Find the mesh size to use for gridding into cells
if(ceil(1/tol)<=L_MAX)
 L=ceil(1/tol);
else
 L=L_MAX;
end

 %% Index the particles into cells
[StartPos  ,llPos  ]=LL_Create(Positions,L);
[StartMatch,llMatch]=LL_Create(MatchPositions,L);

% Only search those cells that are not empty
CellsNotEmpty=find(StartPos>0);

for c=1:length(CellsNotEmpty)
 [I,J,K]=ind2sub([L,L,L],CellsNotEmpty(c)); % Convert to index
 
 % Get the elements to find matches for in the main cell
 Positions_in_cell      = LL_ElementsInCell(StartPos,llPos,I,J,K);
 
 %% Get the elements to match to in this cell, and the neighbouring cells
 spanI=mod( [-1:1]+I-1,L)+1;
 spanJ=mod( [-1:1]+J-1,L)+1;
 spanK=mod( [-1:1]+K-1,L)+1;
 neighbours=[];
 for i=spanI
  for j=spanJ
   for k=spanK
    if(StartMatch(i,j,k)>0)
     neighbours=[neighbours;LL_ElementsInCell(StartMatch,llMatch,i,j,k)];
    end
   end
  end
 end

 Nneighbours=length(neighbours);

 if(Nneighbours>0)
  % For each element in the main cell, find the distance to other elements
  % in the cell and the neighbouring cells.
  for n=1:length(Positions_in_cell);
   particle=Positions_in_cell(n);
   % Find the distance to each neighbour
   centre=Positions(:,particle);
   centre_long=centre*ones(1,Nneighbours);
   dist2=sum(( MatchPositions(:,neighbours) - centre_long ).^2);
   match=find(dist2<tol^2);
   if(~isempty(match))
    % I found a match.
    MatchListIn(particle)=1;
    % Who did I match?  Just take the first.
    MatchListOut(particle)=neighbours(match(1));
   end
  end % if their were MatchPositions in the neighbouring cells
 end % loop over elements in main cell

end % loop through non-empty cells

Index=find(MatchListIn);
if(max(Index)>length(MatchListIn))
 disp(['I fucked up'])
end
IndexMatch=MatchListOut(Index);
