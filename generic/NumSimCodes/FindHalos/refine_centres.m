% refine_centres: From a list of approximate centres, calculate better centres.
%
% new_centres=refine_centres(r,dn,old_centres,rad)
%
% ARGUMENTS
%  r		Particle positions [3,N]
%  dn		Local particle density [N]
%  old_centres	List of approximate centres [3,M]
%  rad		The radius within which distance from the approximate centre to
%               search for the refined centre.
%
% RETURNS
%  new_centres	Refined centres [3,M]
%
% REQUIRES
%  LL_Create, LL_ElementsInCell, recentre
%
% SEE ALSO
%  find_clusters toolbox

% AUTHOR: Eric Tittley
%
% HISTORY
% 00 05 09 Flipped r(N,3) to r(3,N)
%
% 02 04 29 The loop through the centres was slow, so I boosted it's performance.
%	Most of the time was being spent re-centering nobj positions, even though
%	we only needed the nearby particles.  So I implemented a link-list.
%	The link-list takes a while to set up, but then the loop goes quickly.
% 02 04 30 Was: for i1=1:3, I=spanI(i1);   NOW: for I=spanI
% 02 05 09 Added line: ll=ll(:); % Make sure ll is a single column
%	and: 
% +    if(Start(I,J,K)>0)
%       Elements=[Elements;LL_ElementsInCell(Start,ll,I,J,K)];
% +    end
% 04 07 29 Comment changes.
% 04 11 29 Obvious bug.  How did this ever work?
%  Was:
%   max_x=x(nx==max(nx)); max_x=max_x(1);
%   max_y=x(ny==max(ny)); max_y=max_y(1);
%   max_z=x(nz==max(nz)); max_z=max_z(1);
%  Now:
%   max_x=x(nx==max(nx)); max_x=max_x(1);
%   max_y=y(ny==max(ny)); max_y=max_y(1);
%   max_z=z(nz==max(nz)); max_z=max_z(1);
%  07 10 31 Enforce a maximum L
%  07 11 02 Deal better with small halos.
%	Modified comments
%  11 01 10 Why was the input argument "rad" listed as being a vector in the
%       comments? It is just a scalar.
%       Test that the estimated centre is not [NaN NaN NaN].  This happens how?
%  11 02 15 centres must be [3xN], not [N,3]
%
% COMPATIBILITY: Matlab, Octave (?)

function new_centres=refine_centres(r,dn,old_centres,rad)

% METHOD
%  Recentre the particles around the old_centre.
%  Cull to only those particles within the radius rad of old_centre.
%  Find the x, y, & z distribution of particles.
%  Find the peaks in each of the 3 distributions.
%  Find the particles within distance rad/3 of all the peaks.
%  Return the mean position of those particles.

Lmax=512;
SearchNumber=32;

%%% Get the number of objects, transpose if necessary
nobj=size(r,2);
if(nobj==3)
 if(size(r,1)~=3)
  r=r';
  nobj=size(r,2);
 end
end

%%% The number of centres to refine, transpose if necessary
nr=size(old_centres,2);
if(nr==3)
 if(size(old_centres,1)~=3)
  old_centres=old_centres';
  nr=size(old_centres,2);
 end
end

%%% Initialize the output matrix
new_centres=0*old_centres;

%%% Create a link-list of particles in each cell
L=ceil(1/rad);
L=min([L Lmax]);
[Start,ll]=LL_Create(r,L);
ll=ll(:); % Make sure ll is a single column

%%% MAIN LOOP
for i=1:nr % loop through all centres to refine

 %% Status chatter
 if(mod(i,floor(nr/10))==0)
  disp(['In refine_centres: refining centre number ',num2str(i),' of ',num2str(nr)])
 end

 %% Get the elements in this cell, and the neighbouring cells
 if(sum(isnan(old_centres(:,i))) > 0 )
  disp(['WARNING: old_centres(',int2str(i),')=',num2str(old_centres(i,:))])
 else
  Cell=mod(floor(old_centres(:,i)*L),L)+1;
  spanI=mod( [-1:1]+Cell(1)-1,L)+1;
  spanJ=mod( [-1:1]+Cell(2)-1,L)+1;
  spanK=mod( [-1:1]+Cell(3)-1,L)+1;
  Elements=[];
  for I=spanI
   for J=spanJ
    for K=spanK
     if(isnan(I))
      disp(old_centres(:,i))
     end
     if(Start(I,J,K)>0)
      Elements=[Elements;LL_ElementsInCell(Start,ll,I,J,K)];
     end
    end
   end
  end
 end % if isnan ... else ... end

 if(length(Elements)>SearchNumber)
  %%% Get the r's centred on the estimated centre. It becomes (0,0,0).
  r_temp=recentre(r(:,Elements),old_centres(:,i))-0.5;

  %%% Find the objects within rad of the estimated cluster centre
  dist2=sum( r_temp.^2 );
  [SortedDist,Index]=sort(dist2);
  spheremask=Index(1:SearchNumber);
  %spheremask=find(dist2 < rad^2);
 else
  %disp('WARNING: refine_centres: < SearchNumber particles in LL cells. Increase rad?')
  %disp([' Only ',int2str(length(Elements)),' particles found'])
  spheremask=[];
 end

 %%% Find the centre
 if(~isempty(spheremask))

  %%% Extract just the core particles
  r_clus=r_temp(:,spheremask);
  dn_local=dn(Elements(spheremask));

  %find the density peak within a sphere around the estimated cluster centre

  %first find the objects within 40% of the maximum density
  peakmask=spheremask( dn_local>(max(dn_local)*0.4) );

  if(isempty(peakmask))
   disp('WARNING: refine_centres: peakmask empty. Using all the local particles.')
   peakmask=spheremask;
  end
  %find the location of the peak in the x-y-z direction
  [nx,x]=hist(r_temp(1,peakmask),10);
  [ny,y]=hist(r_temp(2,peakmask),10);
  [nz,z]=hist(r_temp(3,peakmask),10);
  max_x=x(nx==max(nx)); max_x=max_x(1);
  max_y=y(ny==max(ny)); max_y=max_y(1);
  max_z=z(nz==max(nz)); max_z=max_z(1);

  %find those particles in all three of these peaks
  %dx=rad/3; %dy=dx, dz=dx
  % Mean bin size
  dx = mean([x(2)-x(1) y(2)-y(1) z(2)-z(1)]);
  core=find(  r_clus(1,:)>(max_x-dx) & r_clus(1,:)<(max_x+dx) ...
            & r_clus(2,:)>(max_y-dx) & r_clus(2,:)<(max_y+dx) ...
            & r_clus(3,:)>(max_z-dx) & r_clus(3,:)<(max_z+dx) );
  if(~isempty(core))
   %find the mean location of this core.
   centre=mean(r_clus(:,core)')';
   %recentre and save
   new_centres(:,i)=rem(old_centres(:,i)+centre+1,1);
  else
   dx=rad; %dy=dx, dz=dx
   core=find(  r_clus(1,:)>(max_x-dx) & r_clus(1,:)<(max_x+dx) ...
             & r_clus(2,:)>(max_y-dx) & r_clus(2,:)<(max_y+dx) ...
             & r_clus(3,:)>(max_z-dx) & r_clus(3,:)<(max_z+dx) );
   %find the mean location of this core.
   if(~isempty(core))
    centre=mean(r_clus(:,core)')';
    %recentre and save
    new_centres(:,i)=rem(old_centres(:,i)+centre+1,1);
   else
    %disp('WARNING: refine_centres: core still empty.')
    new_centres(:,i)=-[1; 1; 1];
   end
  end

 else
  new_centres(:,i)=-[1; 1; 1];
 end % if spheremask not empty

end % loop over all centres

%%% Cull out the false centres
new_centres=new_centres(:,find(sum(new_centres)>0));

%%%%%%%%%%%%%%  END OF refine_centres %%%%%%%%%%%%%%%%
