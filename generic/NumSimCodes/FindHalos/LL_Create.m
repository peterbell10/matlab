function [Start,ll]=LL_Create(r,L)
% LL_Create: Create a Link-List
%
% [Start,ll]=LL_Create(r,L);
%
% ARGUMENTS
%
%  r	Positions (3xN array), spanning [0:1)
%  L	# of cells per dimension
%
% OUTPUT
%
%  Start	LxLxL array of starting positions
%  ll		The link list (vector of length N)
%
% USAGE
%  r=rand(3,10000);
%  L=10;
%  [Start,ll]=LL_Create(r,L);
%  elements=LL_ElementsInCell(Start,ll,3,2,8);
%  disp(r(:,elements));
%
% NOTES:	If a cell has no elements, then Start(I,J,K)==0
%
% SEE ALSO 
%  LL_ElementsInCell

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 04 30 First version
%  02 10 09 Added comment indicating that r must span [0,1)
%  04 07 29 Minor comments changes.
%  07 11 02 Modified comments.
%
% COMPATIBILITY: Matlab, Octave
%
% TODO
%  r must be 3xN.  Should check this

nobj=size(r,2);
Start=zeros(L,L,L);
ll=zeros(nobj,1);
index=mod(floor(r*L),L)+1; % Assume r spans [0,1)
for i=1:nobj
 I=index(1,i);
 J=index(2,i);
 K=index(3,i);
 ll(i)=Start(I,J,K);
 Start(I,J,K)=i;
end
