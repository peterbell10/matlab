function [HaloStructure,Cluster]=RemoveHaloStructure(centres,R200,M200)
% Sort clusters into primary clusters and halo substructure.
%
% [HaloStructure,Cluster]=RemoveHaloStructure(centres,R200,M200);
%
% ARGUMENTS
%  centres	An Nx3 list of object centres.
%  R200		A length N vector of overdensity radii for delta=200.
%  M200		A lenght N vector of total mass withing R200.
%
% OUTPUT
%  HaloStructure   An index into the list of N clusters which selects
%		   the objects that are substructure.
%  Cluster	   An index into the list of N clusters which selects
%		   the objects that are primary clusters.
%
% For the structure data given, sorts the structures into those that are
% sub-structure of larger halos and those that aren't
%
% The criterion for distinction is whether a structure is within a halo
% (as defined by the distance, R200) of a more massive cluster.
%
% SEE ALSO
%  overdensities

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 04 30 Mature version.  Added comments.
%  02 05 02 Added comments.

N=length(R200);

% Our template mask
IsCluster=1+0*M200; % 0==false, 1==true

% Sort the masses
[dummy,I]=sort(M200);

% We want the most massive, first
I=flipud(I);

% This looping business is not efficient, but then there should not be a huge
% number of structure (typically < 10000 )
for i=1:N
 if(IsCluster(I(i))==1) % then find all structures within R200 and remove their
                        % cluster flag
  for j=i+1:N
   % (dx,dy,dz)
   dr=centres(I(i),:)-centres(I(j),:);
   % we have to account for repeating boundary conditions
   dr=abs(dr);
   dr(dr>0.5)=dr(dr>0.5)-1;
   dist=sqrt( sum( dr.^2 ) );
   if(dist<R200(I(i)))
    IsCluster(I(j))=0;
   end
  end %loop over smaller structures
 end %if a cluster
end

HaloStructure=find(~IsCluster);
Cluster=find(IsCluster);
