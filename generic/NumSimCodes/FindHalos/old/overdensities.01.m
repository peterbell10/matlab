function [mgas,rad,Tx,mdark,ndark,ngas,centres]=overdensities(centres,rm,r,th,itype,num_munits)
% Returns that details of the overdensity radius (at overdensities
% of 200 and 500) for the clusters centred on centres.
%
% [mgas,rad,Tx,mdark,ndark,ngas,centres]=overdensities(centres,rm,r,th,itype,num_munits);
%
% The details should be converted to real units via:
%
% mgas=mgas*munit; mdark=mdark*munit;	%Mo
% rad=rad*lunit/Mpc;			%Mpc (note: corrected for h^-1)
% Tx=Tx*Kunit;				%K
%
% SEE ALSO
%  overdensity_radius.

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 06 06 Modified to use r(3,N) convention.
%  00 11 10 Changed Num_clusters=length(centres); to
%	[Num_clusters,dummy]=size(centres);
%	if(dummy~=3), error('centres must be Nx3'), end
%  02 04 30 Modified comments.

over=[200,500];
[Num_clusters,dummy]=size(centres);
if(dummy~=3), error('centres must be Nx3'), end

for i=1:Num_clusters
 disp([num2str(i),' of ',num2str(Num_clusters)])
 for j=2:-1:1
  [rad(i,j),index]=overdensity_radius(rm,r,centres(i,:),over(j),num_munits);
  gas=index(find(itype(index))); dark=index(find(~itype(index)));
  ngas(i,j)=length(gas);  ndark(i,j)=length(dark);
  mgas(i,j)=sum(rm(gas)); mdark(i,j)=sum(rm(dark));
 end %j
% if(length(dark)>0)
%  [Ldark(i,:),Ldarkmag(i)]=cluster_L(centres(i,:),r(dark,:),v(dark,:),rm(dark));
% end
 if(length(gas)>0)
%  [ Lgas(i,:), Lgasmag(i)]=cluster_L(centres(i,:),r( gas,:),v( gas,:),rm( gas));
  cent=centres(i,:)'*(1&gas);
  r_temp=rem(r(:,gas)-cent+1.5,1)-.5;
  dist=sqrt(sum(r_temp.^2));
%  core_gas=gas(find(dist<.05));
  core_gas=gas(find(dist/rad(i,1)<.2));
  if(length(core_gas))
   Tx(i)=mean(th(core_gas));
  else
   Tx(i)=0;
  end %if there is core_gas
 else
  disp('Warning, No gas in Overdensity=200')
  Tx(i)=0;
 end %if
end %i


