function [centres,index]=clean_centres(centres,tol,L_MAX)
% Remove the degenereate (repeated) centres in a list of centres.
%
% syntax: [centres,index]=clean_centres(centres,tol,[L_MAX]);
%
% ARGUMENTS
%  centres	A (Nx3) array of centres.
%  tol		The tolerance.  If two centres are separated by less
%		than tol, then they are considered the same cluster.
%  L_MAX	(optional) Maximum number of chaining cells per dimension.
%
% OUTPUT
%  centres	The new list of cleaned centres.
%  index	The index into the original list of centres from which
%		the new list is derived.
%
% This is an order N^2 problem, so beware of large lists of objects.
%
% To help optimize the speed, two extra parameters may be passed:
%  L_MAX = maximum number of chaining cells per dimension
%  A large value of L_MAX means lots of individual cells
%  There will be an optimum value for L_MAX.  Too large a value and 
%  the routine will spend too much time finding particles in a cell.
%  Particularly if many of the cells are empty. Time goes as L^3 (or L^2
%  for 2D data).
%  Too small a value and there will be too many positions per cell for the
%  core routine to process, which goes as N_positions_per_cell^2

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 04 05 Version mature
%  01 04 05 Fixed bug (for [i,j,k]=1:L changed to for [i,j,k]=0:L) which
%   prevented the lowest positions to be cleaned.
%  01 04 05 Sped up significantly by looping through each cell, instead of
%   the possible (LxLxL) postitions of a cell.
%  02 04 30 Added comments.

%MAX_NUM=1024;
if(nargin<3)
 L_MAX=32;
end
if(nargin<4)
 OptFlag=0;
end

[Num,Ndim]=size(centres);
if(Ndim~=2 & Ndim~=3)
 error(' I do not know what to do with other than 2 or 3 dimensions')
end

% Initialise the array to 1
good=ones(1,Num);

if(Ndim==3)
 %%%% NOTE: Assuming all centres are between 0 and 1
 %% Find the mesh size to use for gridding into cells
 mins=min(centres);
 maxs=max(centres);
 max_span=ceil(max( [(maxs(1)-mins(1))/tol,(maxs(2)-mins(2))/tol] ));
 if(max_span<=L_MAX)
  L=max_span;
 else
  L=L_MAX;
 end
 divisor=tol*max_span/L;
 
 %% Index the particles into cells
 [Start,ll]=LL_Create(centres,L);

 %% Loop through all cells
 %%  If the cell is not empty then
 %%   Find all the elements in the cell and neighbouring cells
 %%   Find the distance to each neighbour
 %%  

 % Only search those cells that are not empty
 CellsNotEmpty=find(Start>0);
 
 for c=1:length(CellsNotEmpty)
  [I,J,K]=ind2sub([L,L,L],CellsNotEmpty(c)); % Convert to index
  
  % Get the elements in the main cell
  particles_in_cell=LL_ElementsInCell(Start,ll,I,J,K);
  
  %% Get the elements in this cell, and the neighbouring cells
  spanI=mod( [-1:1]+I-1,L)+1;
  spanJ=mod( [-1:1]+J-1,L)+1;
  spanK=mod( [-1:1]+K-1,L)+1;
  neighbours=[];
  for i1=1:3
   i=spanI(i1);
   for i2=1:3
    j=spanJ(i2);
    for i3=1:3
     k=spanK(i3);
     neighbours=[neighbours,LL_ElementsInCell(Start,ll,i,j,k)];
    end
   end
  end

  % For each element in the main cell, find the distance to other elements
  % in this cell and the neighbouring cells.
  for n=1:length(particles_in_cell);
   if(good(particles_in_cell(n))) % if this positions is presently unique
    % Find the distance to each neighbour
    centre=centres(particles_in_cell(n),:);
    centre_long=ones(length(neighbours),1)*centre;
    dist=sqrt(sum(((centres(neighbours,:)-centre_long).^2)')');
    match=find(dist<tol);
    if(isempty(match))
     error('I should have at least found one match')
    end
    if(length(match)>1)
     centres(particles_in_cell(n),:)=mean(centres(neighbours(match),:));
     good(neighbours(match))=0*good(neighbours(match));
     good(particles_in_cell(n))=1;
    end % if more than one match
   end % if this position was presently unique
  end % loop over elements in main cell

 end % loop through non-empty cells

else % if Ndim==3, else, (Ndims==2)

 %% ( Do it the old way for now )

 %%% Index the particles into boxes and process one box at a time
 % Index the particles into cells
 mins=min(centres);
 maxs=max(centres);
 max_span=ceil(max( [(maxs(1)-mins(1))/tol,(maxs(2)-mins(2))/tol] ));
 if(max_span<=L_MAX)
  L=max_span;
 else
  L=L_MAX;
 end
 divisor=tol*max_span/L;
 cells=ceil([(centres(:,1)-mins(1))/divisor,...
             (centres(:,2)-mins(2))/divisor] );

 % Now loop through each cell;  loop through all particles in a cell;
 %  loop through neighbour particles
 for c=1:size(cells,1)
  i=cells(c,1);
  j=cells(c,2);
  particles_in_cell=find(cells(:,1)==i & cells(:,2)==j );
  if(~ isempty(particles_in_cell) ) % if there are any positions in the cell
   % find all the neighbouring positions
   neighbours=find(  cells(:,1)>=(i-1) & cells(:,1)<=(i+1) ...
                   & cells(:,2)>=(j-1) & cells(:,2)<=(j+1) );
   for n=1:length(particles_in_cell) % loop through positions in cell
    if(good(particles_in_cell(n))) % if this positions is unique
     centre=centres(particles_in_cell(n),:);
     centre_long=ones(length(neighbours),1)*centre;
     dist=sqrt(sum(((centres(neighbours,:)-centre_long).^2)')');
     match=find(dist<tol);
     if(isempty(match))
      error('I should have at least found at least one match')
     end
     if(length(match)>1)
      centres(particles_in_cell(n),:)...
       =mean(centres(neighbours(match),:));
      good(neighbours(match))=~good(neighbours(match));
      good(particles_in_cell(n))=1;
     end % if more than one match
    end % if unique
   end % loop through positions in cell
  end % if not empty cell
 end % loop through cells

end % if Ndim==3, else, end

index=find(good);
centres=centres(index,:);
