% FindHalosInGriddedData: Given gridded density field, break it up into halos
%
% [HaloCentres,HaloCells] = FindHalosInGriddedData(DensityField)
%
% ARGUMENTS
%  DensityField  A grid of densities
%
% RETURNS
%  HaloCentres   List of centres found [3 x NHalos]
%  HaleCells     The cells belonging to each of the NHalos. Cell array
%
% NOTES
%  This should produce the same results as using HOP and converting cells into
%  a set of particles with density rho_i at r_i=(x_i,y_i,z_i) and searching over
%  the nearest 27 particles.
%
% SEE ALSO

% TODO
%  Parallelize.  This is doable by breaking up problem into subvolumes (8 or
%  64 etc), performing the task, then dealing with all halos found to hit an
%  edge.
%  For halos abutting at an edge, either merge (if my max density at the edge
%  is greater than any of your densities, then I absorb you) or verify assignment
%  of edge cells by simply assigning to the maximum of the local 27 densities.


% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

% CURRENTLY INCOMPLETE

%function [HaloCentres,HaloCells] = FindHalosInGriddedData(DensityField)
function HaloIndex = FindHalosInGriddedData(DensityField)

% Steps
%  1) Find all the peaks.
%  2) Sort the densities of each cell
%  3) Loop over all cells in order of decreasing density
%      If cell is centre, assign halo number to cell and all unassigned cells
%       within the 26 surrounding cells.
%      If cell is not a centre, assign given halo number to all unassigned cells
%       within the 26 surrounding cells.

Dim=size(DensityField);
if( (Dim(1)~=Dim(2)) | (Dim(1)~=Dim(3)) )
 error('FindHalosInGriddedData: Sorry, I prefer if boxes are square')
end
Dimension        = Dim(1);
DimensionSquared = Dimension^2;

% Find all the peaks
PeakIndices=FindPeaksInField(DensityField); % Function not yet written
[iX,iY,iZ]=ind2sub(Dim,PeakIndices);
NHalos=length(PeakIndices);

% Sort the cells
[SortedDensities,IndexHiToLo]=sort(DensityField(:),1,'descend');
clear SortedDensities

% The halo to which each cell will be assigned.
HaloIndex=0*DensityField(:); % Initially blank

% Predefine. These are [-1:1], so be used in span = iCell -1 + [-1:1].
[di,dj,dk]=meshgrid([-2:0],[-2:0],[-2:0]);
di=di(:);
dj=dj(:);
dk=dk(:);

% Loop over all cells
for i=1:length(IndexHiToLo)
 CellIndex=IndexHiToLo(i);
 % Find if cell is a peak cell.
 % If this cell is a centre, assign halo number to this cell
 % Otherwise, this cell would have necessarily been already assigned someone
 % else's centre number, since we are going down in order of decreasing density
 CentreNumber=find(CellIndex==PeakIndices);
 if(isempty(CentreNumber))
  CentreNumber=0;
 end
 % Sanity check
 if( (CentreNumber==0) & (HaloIndex(CellIndex)==0) )
  error(['FindHalosInGriddedData: Unreachable state'])
 end
 if( CentreNumber )
  HaloIndex(CellIndex)=CentreNumber;
 end
 % Assign surrounding 26 cells this CentreNumber if not already assigned
 CentreNumber=HaloIndex(CellIndex);
 % Need to inline ind2sub because it is a killer
 %[iCell,jCell,kCell]=ind2sub(Dim,CellIndex);
 % Inlined version, tuned for all Dims==Dimension
 ndx=CellIndex;
 vi = rem(ndx-1, DimensionSquared) + 1;
 vj = (ndx - vi)/DimensionSquared + 1;
 kCell = vj;
 ndx = vi;
 vi = rem(ndx-1, Dimension) + 1;
 vj = (ndx - vi)/Dimension + 1;
 jCell = vj;
 iCell = vi;
 %end of inlining
 X=mod(iCell+di,Dimension)+1;
 Y=mod(jCell+dj,Dimension); % Would need to subtract one, anyway, in (Y-1)*Dim
 Z=mod(kCell+dk,Dimension); % Would need to subtract one, anyway, in (Z-1)*Dim^2
 %NeighbourCellIndex=sub2ind(Dim,X,Y,Z);
 NeighbourCellIndex = X + Y*Dimension + Z*DimensionSquared;
 NeighbourUnassignedMask = find(HaloIndex(NeighbourCellIndex)==0);
 HaloIndex(NeighbourCellIndex(NeighbourUnassignedMask))=CentreNumber;
end

% Now each element in HaloIndex should be non-zero.
% i.e., each cell should have been assigned to some halo
% Sanity Check
if(sum(HaloIndex==0)>0)
 disp(['WARNING: FindHalosInGriddedData: some cells are unassigned'])
end

% Now reshape into a grid
HaloIndex=reshape(HaloIndex,Dim);

%  END MAIN FUNCTION
