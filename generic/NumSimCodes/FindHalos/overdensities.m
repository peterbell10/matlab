function [rad,mdark,ndark,mgas,ngas,Tx]=overdensities(centres,rm,r,th,itype,num_munits,L)
% overdensities: Details of the clusters within overdensity radiii of 200 and 500.
%
% [rad,mdark,ndark,mgas,ngas,Tx]= ...
%   overdensities(centres,rm,r,th,itype,num_munits,[L]);
%
% ARGUMENTS
%
%  centres      3xM array of cluster centres.
%  rm           Particle masses. (length N)
%  r            Particle positions. Span the range (0,1]. (3xN)
%  th           Particle temperatures for the gas. (length ngas)
%  itype        Particle type. (0=>dark, 1=>gas) (length N)
%  num_munits   The mass factor.  Should be the mass contained in the
%               simulation volume when the mean density (total mass/volume)
%               is equal to an overdensity of 1.
%  L            (optional) The initial number of cells per dimension. Used in
%               the indexing of the particles into cells.  Default is 256.
%               A larger L will speed things up.  But too large an L will
%               both suck up too much memory and produce erroneous output.
%               A warning will be issued if the latter is occuring.
%
% OUTPUT
%
%  rad          The overdensity radii. (Nx2)
%  mdark        The mass of dark matter within the overdensity radii. (Nx2)
%  ndark        The number of dark matter particles within the overdensity
%               radii. (Nx2)
%  mgas         The mass of gas within the overdensity radii. (Nx2)
%  ngas         The number of gas particles within the overdensity
%               radii. (Nx2)
%  Tx           The mean temperature within the first overdensity radii.
%               The temperature is mass-weighted. (Length N)
%
%
% The details can be converted to real units via:
%
% mgas=mgas*munit; mdark=mdark*munit;   %Mo
% rad=rad*lunit/Mpc;                    %Mpc (note: corrected for h^-1)
% Tx=Tx*Kunit;                          %K
%
% ERROR MESSAGES
%  "Warning: Overdensity Radius > Mesh size"
%       The cell size is too small. The values derived could be in error.
%       Decrease L and redo.
%
%  "Warning: overdensity_radius: cluster not dense enough. over_density=500"
%       The cluster is too small.  It doesn't have any part that is more dense
%       than an overdensity of 500. Not fatal.
%
%  "Warning: No gas in Overdensity=200"
%       There is no gas in the cluster. Not fatal.
%
% SEE ALSO
%  overdensity_radius

% AUTHOR: Eric Tittley

%%% The default value for L
if(nargin<7), L=256; end

%%% The overdensities
over=[200,500];

%%% Ls to try
Ls = [1024 800 640 512 400 320 256 200 160 128 100 80 64 50 40 32 25 20 16 10 8];
Lindex = find(Ls >= L);
if(isempty(Lindex))
 Lindex=1;
else
 Lindex = Lindex(end);
end

%%% Check the arguments
[dummy,Num_clusters]=size(centres);
if(dummy~=3)
 centres=centres';
 Num_clusters=dummy;
end

% Determine if there is gas
if(sum(itype)>0)
 GasFlag=1;
else
 GasFlag=0;
end

%%% Initialise the products
rad=zeros(Num_clusters,2);
mdark=rad;
ndark=rad;
if(GasFlag)
 mgas=rad;
 ngas=rad;
 Tx=zeros(Num_clusters,1);
else
 mgas=[];
 ngas=[];
 Tx=[];
end

HaloIsConverged=zeros(Num_clusters,1);

NumOverdensityRadiusExceedsMeshSize = Num_clusters;
while(NumOverdensityRadiusExceedsMeshSize > 0)

 % Initialise Counters
 NumOverdensityRadiusExceedsMeshSize=0;
 NumNoGas=0;
 NumNoDark=0;

 L=Ls(Lindex);

 disp(['Using L=',int2str(L)])

 %%% Index the particles into cells using a link list
 [Start,ll]=LL_Create(r,L);

 tBase = clock;

 %%% MAIN LOOP
 for i=1:Num_clusters % Loop through the centres

  %% Diagnostic output so the user doesn't get itchy.
  if(etime(clock,tBase) > 10) % if more than 10s has elapsed
   disp([num2str(i),' of ',num2str(Num_clusters)])
   tBase = clock;
  end

  if(~HaloIsConverged(i))
   %% Get the particles in the 27 neighbouring cells
   Cell=mod(floor(centres(:,i)*L),L)+1;
   spanI=mod( [-1:1]+Cell(1)-1,L)+1;
   spanJ=mod( [-1:1]+Cell(2)-1,L)+1;
   spanK=mod( [-1:1]+Cell(3)-1,L)+1;
   Elements=[];
   for I=spanI
    for J=spanJ
     for K=spanK
      Elements=[Elements;LL_ElementsInCell(Start,ll,I,J,K)];
     end
    end
   end

   %% Calculate the overdensity radii for each overdensity, and get the index
   %% of particles for this cluster.
   for j=2:-1:1
    [rad(i,j),index]=overdensity_radius(rm(Elements),r(:,Elements), ...
                      centres(:,i),over(j),num_munits);
    index=Elements(index);
    dark=index(find(itype(index)==0));
    ndark(i,j)=length(dark);
    mdark(i,j)=sum(rm(dark));
    if(GasFlag)
     gas=index(find(itype(index)==1));
     ngas(i,j)=length(gas);
     mgas(i,j)=sum(rm(gas));
    end
   end %j

   %% We sort particles on a mesh of cells to speed things. But if the mesh
   %% is too fine, then the results will be wrong.  Warn if this is the case.
   if( max(rad(i,:))>1/L )
    NumOverdensityRadiusExceedsMeshSize=NumOverdensityRadiusExceedsMeshSize+1;
   else
    HaloIsConverged(i)=1;
   end

   if(GasFlag)
    %% Derive Tx for the gas, but only if there is gas
    if(length(gas)>0)
     cent=centres(:,i)*ones(1,length(gas));
     r_temp=rem(r(:,gas)-cent+1.5,1)-.5;
     dist2=sum(r_temp.^2);
     core_gas=gas(find(dist2/(rad(i,1)^2)<.2));
     if(length(core_gas))
      Tx(i)=mean(th(core_gas));
     else
      Tx(i)=0;
      NumNoGas=NumNoGas+1;
     end %if there is core_gas
    else
     Tx(i)=0;
    end %if there is any gas
   end

  end % if Halo is not converged

 end %i

 if(NumOverdensityRadiusExceedsMeshSize>0)
  disp(['Warning: Overdensity Radius > Mesh size for ',...
        int2str(NumOverdensityRadiusExceedsMeshSize),' centres'])
 end

 if(NumNoGas>0)
  disp(['Warning: No gas in Overdensity=200 for ',...
        int2str(NumNoGas),' centres'])
 end

 Lindex = Lindex+1;
end % while NumOverdensityRadiusExceedsMeshSize>0
