function Elements=LL_ElementsInCell(Start,ll,I,J,K)
% LL_ElementsInCell: Get the elements in a cell from a link list
%
% ARGUMENTS
%
%  Start	LxLxL array of starting positions
%  ll		The link list (vector of length N)
%  I,J,K	The cell coordinates
%
% OUTPUT
%
%  Elements	The list of elements in that cell
%
% USAGE
%  r=rand(3,10000);
%  L=10;
%  [Start,ll]=LL_Create(r,L);
%  elements=LL_ElementsInCell(Start,ll,3,2,8);
%  disp(r(:,elements);
%
% NOTES
%  Link Lists can dramatically improve the performance of code by allowing
%  for the selection of only that data that is useful.  But a performance
%  analysis of code rewritten to use Link Lists will often find that the
%  dramatic gains in performance will be ofset in part by overhead in
%  manipulating the link lists.  This function often becomes the slowest
%  part of the code.
%
% SEE ALSO 
%  LL_Create

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 04 30 First version.
%  02 05 02 Added comments concerning the performance.
%  04 07 29 Minor comments changes.

% It is very performace-killing to gradually accumulate a vector in
% Matlab (or any other language).  So here I'm going to increase it
% in increments of 1024.  Space is allocated for 1024 elements. When
% that becomes full, then space for 1024 more elements is assigned and
% so on.  At the end, I'll extract only those elements that needed.
%
% The reason that this routine is so time-consuming is that it is
% inherently not vectorizable.  This should be turned into a MEX
% routine.

i=Start(I,J,K);
if(i>0)
 NElements=1024;
 Elements=zeros(1024,1);
 j=0;
 while(i>0)
  j=j+1;
  if(j>NElements)
   Elements=[Elements;zeros(1024,1)];
   NElements=NElements+1024;
  end
  Elements(j)=i;
  i=ll(i);
 end
 Elements=Elements(1:j);
else
 Elements=[];
end