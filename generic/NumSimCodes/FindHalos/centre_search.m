function [centre,dev]=centre_search(r)
% centre_search: Find the region of highest number density.
%
% [centre,dev]=centre_search(r);
%
% INPUT ARGUMENTS
%  r	the locations of the particles
%
% OUTPUT ARGUMENTS
%  centre	The location of the highest density.
%  dev		The standard deviation of the highest density region.
%		(almost meaningless in most cases)
%
% METHOD
%  On a 10x10x10 mesh, determine the number counts of the particles
%  If the cell with the largest number of particles has fewer than 32 particles,
%   then return the mean position of the particles.
%   otherwise call centre_search recursively for those particles.

% HISTORY
%  01 04 04 Mature Code
%	Modified to accept new r=r(3,N) format for data
%	Added syntax comments
%  02 03 27 Added comments and renamed centre_search.  The file was already
%	called centre_search, so this just affects the comments.
%	Modified to deal with the case in which there is only 1 particle in
%	the cell with the most counts.
%  03 06 20 Added errors if r is empty or has only 1 particle.
%  04 07 29 Minor comments changes.

N=size(r,2);
if(N==0)
 error('Empty r matrix')
end
if(N==1)
 error('Only 1 particle in r matrix')
end

xmin=min(r(1,:)); xmax=max(r(1,:));
ymin=min(r(2,:)); ymax=max(r(2,:));
zmin=min(r(3,:)); zmax=max(r(3,:));

% make the particles span the range [0,1] in every direction.
r_temp(1,:)=(r(1,:)-xmin)/(xmax-xmin);
r_temp(2,:)=(r(2,:)-ymin)/(ymax-ymin);
r_temp(3,:)=(r(3,:)-zmin)/(zmax-zmin);

% make a 3D index.  Numbers will range from 0 to 9
index3d=floor(r_temp*9.999);

% make a 1d index so that index1d=234 means index3d=[2 3 4]
index1d=index3d(1,:)*100+index3d(2,:)*10+index3d(3,:);

% now count the number in each cell.
count=0*[1:1000];
if(length(index1d)>1000)
 for i=0:999
  count(i+1)=sum(i==index1d);
 end
else
 for i=1:length(index1d)
  count(index1d(i)+1)=count(index1d(i)+1)+1;
 end
end

% find the cell with the most particles
countmax=max(count);
cell=find(count==countmax);
if(length(cell)>1)  %DEGENERATE CASE, WE MUST BREAK THE SYMMETRY
 % for each of the degenerate cells, add up the neighbouring cells and
 % use that number as a discriminant.  If THAT is degenerate, then we're
 % SOL. 
 for i=1:length(cell)
  new_count(i)=0;
  cellx=floor((cell(i)      )/100)*100;
  celly=floor((cell(i)-cellx)/10 )* 10;
  cellz=cell(i)-cellx-celly;
  for dx=-100:100:100
   indexx=cellx+dx;
   if(indexx>0 & indexx<=900)
    for dy=-10:10:10
     indexy=celly+dy;
     if(indexy>0 & indexy<=90)
      for dz=-1:1:1
       indexz=cellz+dz;
       if(indexz>0 & indexz<=9)
        new_count(i)=new_count(i)+count(indexx+indexy+indexz);
       end %indexz
      end %dz
     end %indexy
    end %dy
   end %indexx
  end %dx
 end %i
 cell=cell(find(new_count==max(new_count)));
end %if length>1

% find those particles
 clump=find(index1d==(cell(1)-1));
 r_clump=r(:,clump);

if(countmax==1)
 centre=r_clump';
 dev=[0 0 0];
elseif(countmax < 32)
 centre=mean(r_clump');
 dev=std(r_clump');
else %refine
 disp(['Going to refinement with ',int2str(countmax),' particles'])
 [centre,dev]=centre_search(r_clump);
end
