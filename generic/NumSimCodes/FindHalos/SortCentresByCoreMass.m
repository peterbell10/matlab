function centres_sorted=SortCentresByCoreMass(centres,r,rm,dn,rcut,dncut)
% SortCentresByCoreMass: Sort a list of centres by the mass of the cores
%
% centres_sorted=SortCentresByCoreMass(centres,r,rm,dn,rcut,dncut)
%
% ARGUMENTS
%  centres	N rows of (x,y,z) cluster centre positions.
%  r		A list of Nobj columns of (x,y,z) particle positions.
%  rm		A list of Nobj particle masses.
%  dn		A list of Nobj particle densities.
%  rcut		The radius of the `core' of the cluster.  Only particles within
%		rcut of the cluster centre will be used to sort the masses of
%		the clusters.  Must be the same units as r.
%  dncut	The minimum allowed maximum density in a core.  Any core whose
%		maximum density is less than dncut will not be included.
%
% OUTPUT
%  centres_sorted	centres from above, sorted by mass in ascending order.

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 05 ?? First version.
%  02 09 06 Added arguments check and comments.
%  02 10 03 Added comments for OUTPUT
%  04 07 29 Minor comments changes.

% Check the arguments
if(size(r,2)~=length(rm) | length(rm)~=length(dn))
 error('r, rm, and dn must be equal lengths')
end 

MeshRes=min([ceil(1/rcut),256]);

% Index the particles into cells
disp(' Indexing particles...')
[Start,ll]=LL_Create(r,MeshRes);

%% Sort the centres by core mass
Ncentres=size(centres,1);
Mcore=zeros(Ncentres,1);
Maxdn=zeros(Ncentres,1);
for i=1:Ncentres
 if(mod(i,1000)==1)
  disp([' ',int2str(i),' of ',int2str(Ncentres)])
 end
 % Get all the particles in the 27 surrounding cells
 Cell=floor(centres(i,:)*MeshRes)+1;
 SpanI=mod(Cell(1)+[-2:0],MeshRes)+1;
 SpanJ=mod(Cell(2)+[-2:0],MeshRes)+1;
 SpanK=mod(Cell(3)+[-2:0],MeshRes)+1;
 Elements=[];
 for I=SpanI
  for J=SpanJ
   for K=SpanK
    if(Start(I,J,K)>0)
     Elements=[Elements;LL_ElementsInCell(Start,ll,I,J,K)];
    end
   end
  end
 end

 % Centre all the particles on the cluster centre
 r_centred=recentre(r(:,Elements),centres(i,:))-0.5;
 dist2=sum( r_centred.^2 );
 core=find(dist2<rcut^2);
 if(~ isempty(core))
  List=Elements(core);
  Mcore(i)=sum(rm(List));
  Maxdn(i)=max(dn(List));
 else
  Mcore(i)=0;
  Maxdn(i)=0;
 end
end

% Cull out cores with central densities less than dncut.
Massive=find(Maxdn>=dncut);
Mcore=Mcore(Massive);
centres=centres(Massive,:);

[Msorted,index] = sort(Mcore);
centres_sorted=centres(index,:);

%%%%% DONE %%%%%
