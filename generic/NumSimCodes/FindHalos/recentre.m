function r_recentred=recentre(r,centre)
% recentre: Recentre the positions r around position centre.
%
%  r_recentred=recentre(r,centre);
%
% ARGUMENTS
%  r            3xN array of positions, all in the range [0,1)
%  centre       [x y z] position to centre around
%
% RETURNS
%  3xN array of positions shifted so that [x y z] is now at [0.5 0.5 0.5]
%
% EXAMPLE
%  % Find the distance of all particles from point centre.
%  r_recentred=recentre(r,centre)-0.5;
%  distance = sqrt(sum(r_recentred.^2));

% AUTHOR: Eric Tittley
%
% HISTORY
%  02-11-13 Mature version.
%	Make centre a column vector, regardless of the input.
%	Add comments.
%  04 07 29 Minor changes to the comments
%  11 06 24 Comments modified to clarify expected input and output

% Make column vector
centre=centre(:);

n=size(r,2);
r_recentred=rem(r+([1.5;1.5;1.5]-centre)*ones(1,n),1);
