% BuildClusterFromBound: Find cluster members by building them up one at a time
% from bound particles.
%
% core=BuildClusteFromBound(centre,r,v,rm,hdr);
%
% ARGUMENTS
%  centre	cluster centre	 	[1 x 3]    Range=[0,1)
%  r		particle positions 	[3 x Nobj] Range=[0,1)
%  v		particle velocities	[3 x Nobj]
%  rm		particle masses		[Nobj x 1]
%  hdr		Header containing required parameters:
%		hdr.lunit Converts code units to cm
%		hdr.tunit Converts code units to time (s)
%		hdr.lunit/hdr.tunit Converts code units to velocity (cm/s)
%		hdr.munit Converts code units to Mo
%		hdr.sft0  Gravitational softening (code units).
%
% OUTPUT
%  core		Members of the structure [Ncore x 1]
%
% USAGE
%  This routine is normally called from BuildClustersFromBound
%
% REQUIRES
%  recentre
%
% SEE ALSO
%  BuildClusterFromBound

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 05 15 First version based on BuildClustersFromBound
%  02 10 03 Added minor comments
%  04 07 29 Minor comment changes
%  07 10 31 Modified comments.
%  11 01 21 Units() requires SI to be defined in the environment.
%
% COMPATIBILITY: Matlab, Octave (?)

% Terms:
%  core		1) particles within 5 softening lengths of the core centre
%		2) core members which are known to be bound

function core=BuildClusterFromBound(centre,r,v,rm,hdr)

% We search for bound particles in order of increasing distance from the centre
% stopping when more then nNotBoundInARowThreshold particles have been found
% to be unbound.  Clearly this should be Infinity (or at least the number of
% particles).  But that would make the algorithm an N^2.  But the threshold
% number is also dangerous, since it will trip if an unbound substructure or
% a subtructure with members bound to the substructure but not the primary halo
% are encountered.
% nNotBoundInARowThreshold should probably be an input parameter:
%  hdr.nNotBoundInARowThreshold
nNotBoundInARowThreshold=5000;
% Perhaps the real limit should be radius which could be, say, twice the
% maximum virial radius.

%% PRELIMINARIES:
% Find the distance from the centre for all the particles
r_centred=recentre(r,centre)-0.5;
dist2=sum( r_centred.^2 ); % dist^2
% Initialise the units
SI='CGS';
Units, G=6.6732e-8;
% Convert the input into real units
centre=centre*hdr.lunit;	% cm
r=r*hdr.lunit;			% cm
v=v*hdr.lunit/hdr.tunit;	% cm/s
rm=rm*hdr.munit*Mo;		% g
r_cut=20*hdr.sft0*hdr.lunit;	% cm
dist2=dist2*hdr.lunit^2;	% cm^2
soft=hdr.sft0*hdr.lunit;	% cm
% How many particles?
N=length(rm);
% Set a flag for each particle stating that it is not bound.
bound=zeros(N,1);

%% PHASE 1:
%% Find all the particles within n softening lengths of the centre.
%% For each of these particles, test to see if it is bound to the other
%% particles.
%% At the end, the core will consist of only those particles that are
%% bound to the other core particles.
core=find(dist2<r_cut^2);
Ncore=length(core);
if(Ncore>1)
 % Determine which core members are bound
 r_core=r_centred(:,core);
 rm_core=rm(core);
 v_core=v(:,core);
 % Subtract the mean mass-weighted velocity of the core
 MeanV=mean(v_core,2);
 v_core(1,:)=v_core(1,:)-MeanV(1);
 v_core(2,:)=v_core(2,:)-MeanV(2);
 v_core(3,:)=v_core(3,:)-MeanV(3);
 % For each core member, determine if it is bound to the core.
 for j=1:Ncore
 % Kinetic energy
  Ek=0.5*rm_core(j)*sum(v_core(:,j).^2,1); % g (cm/s)^2
 % The potential energy (sum of GmM/r over all core particles)
  d=zeros(3,Ncore);
  d(1,:)=r_core(1,:)-r_core(1,j);
  d(2,:)=r_core(2,:)-r_core(2,j);
  d(3,:)=r_core(3,:)-r_core(3,j);
  d=[d(:,1:j-1),d(:,j+1:end)]; % Remove the jth particle
  dist=sqrt( sum(d.^2) )';
  dummy=[rm_core(1:j-1);rm_core(j+1:end)]; % Remove the jth particle
  Eg=G*rm_core(j)*sum(dummy./(dist+soft)); % g (cm/2)^2
 % Test if particle is bound
  if(Ek<Eg)
   bound(core(j))=1;
  end
 end
 core=find(bound==1); % Indexes into the FULL data set.
 % Now core is the list of bound members within n softening lengths
else
 core=[];
end % If Ncore>1

%% PHASE 2:
%% Go through each particle, in order of increasing distance, testing to
%% see if the particle is bound to the core particles.
%% If it is bound, then it is added to the core particles.
%% Repeat until we don't add any more particles
if(~ isempty(core))
 Ncore=length(core);
 Ncore_o=Ncore-1; % set to trip first while loop
 while(Ncore>Ncore_o)
  Ncore_o=Ncore;
  Avail=find(bound==0);
  NAvail=length(Avail);
  nNotBoundInARow=0;
  % Sort all particles by distance
  [span,d_index]=sort(dist2(Avail));
   % test particles in order of increasing distance
  span=Avail(d_index)';
  for j=span
   MeanV=mean(v(:,core),2); % bulk velocity of cluster
   v_part=v(:,j)-MeanV; % Subtract the bulk velocity
   % Kinetic Energy
   Ek=0.5*rm(j)*sum(v_part.^2); % g (cm/s)^2
   % Potential Energy
   N_core=length(core);
   d=zeros(3,N_core);
   d(1,:)=r(1,core)-r(1,j);
   d(2,:)=r(2,core)-r(2,j);
   d(3,:)=r(3,core)-r(3,j);
   dist=sqrt( sum(d.^2,1) )';
   Eg=G*rm(j)*sum(rm(core)./(dist+soft)); % g (cm/2)^2
   if(Ek<Eg) % If bound
    bound(j)=1;
    core=[core;j];
    nNotBoundInARow=0;
   else
    nNotBoundInARow=nNotBoundInARow+1;
   end
   %% Quit the loop if we've found 10 particles in a row that aren't bound
   if(nNotBoundInARow>nNotBoundInARowThreshold)
    % If a preset number of particles in a row are not bound, then  break from
    % the for-loop but not the while-loop
    break
   end
  end % loop over all particles in order of increasing distance
  Ncore=length(core); % for the next while loop condition
 end % while loop
end % if core is not empty
