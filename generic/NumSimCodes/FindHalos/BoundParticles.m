% BoundParticles: Find particles in a simulation that are bound
%
% Index=BoundParticles(r,v,rm,hdr,MeshSize)
%
% ARGUMENTS
%  centre	cluster centre	 	[1 x 3] 
%  r		particle positions 	[3 x Nobj] [0 1)
%  v		particle velocities	[3 x Nobj]
%  rm		particle masses		[Nobj x 1]
%  hdr		Header containing required parameters:
%		hdr.lunit Converts code units to cm (code unit 1 => hdr.lunit)
%               hdr.vunit Converts code velocities to cm/s
%		hdr.munit Converts code units to Mo
%		hdr.sft0  Gravitational softening (code units).
%               hdr.GridDimension
%               hdr.Nsph
%               hdr.h_guess
%               hdr.time  Expansion factor

function [Potential,Ek]=BoundParticles(r,v,rm,hdr)

% Initialise the units
SI='CGS';
Units

% r must be in the interval [0 1)
if( min(r(:))< 0 | max(r(:))>=1 )
 error('r must be on the interval [0 1)')
end

% Find the Gravitational Potential of each of the particles
Potential=GravitationalPotential(r,rm,hdr,hdr.Method);
min(Potential(:)), mean(Potential(:)), max(Potential(:))

% Find the kinetic energy of each particle
Ek = (0.5*hdr.munit*Mo*hdr.vunit^2) * rm .* sum(v.^2);
min(Ek(:)), mean(Ek(:)), max(Ek(:))
