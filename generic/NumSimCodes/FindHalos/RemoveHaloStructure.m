function [HaloStructure,Cluster,Parent]=RemoveHaloStructure(centres,R200,M200)
% RemoveHaloStructure: Sort clusters into primary clusters & halo substructure.
%
% [HaloStructure,Cluster,Parent]=RemoveHaloStructure(centres,R200,M200);
%
% ARGUMENTS
%  centres	An Nx3 list of object centres.
%  R200		A length N vector of overdensity radii for delta=200.
%  M200		A length N vector of total mass withing R200.
%
% OUTPUT
%  HaloStructure   An index into the list of N clusters which selects
%		   the objects that are substructure.
%  Cluster	   An index into the list of N clusters which selects
%		   the objects that are primary clusters.
%  Parent	   For each of the elements in HaloStructure, the index
%		   of the parent cluster is stored.
%
% For the structure data given, sorts the structures into those that are
% sub-structure of larger halos and those that aren't
%
% The criterion for distinction is whether a structure is within a halo
% (as defined by the distance, R200) of a more massive cluster.
%
% USAGE:
%  [HaloStructure,Cluster,Parent]=RemoveHaloStructure(centres,rad(:,1), ...
%                                                     mdark(:,1)+mgas(:,1));
%  plot(centres(HaloStructure,1),centres(HaloStructure,2),'.')
%  plot(centres(Cluster,1),centres(Cluster,2),'.')
%  M=HaloStructure(find(Parent==Parent(1)))
%  plot(centres(M,1),centres(M,2),'.')
%
% SEE ALSO
%  overdensities

% AUTHOR: Eric Tittley
%
% HISTORY
%  02 04 30 Mature version.  Added comments.
%  02 05 02 Added comments.
%  02 05 02 Rewrote to increase performance by indexing the centres
%	into cells accessible via a link list.  This means we only
%	search for nearby centres over the neighbouring cells, not
%	the whole volume.  This change led to a 46x boost in
%	performance.
%  02 06 12 Added the output Parent, plus minor changes.
%  04 07 29 Minor comments changes.

N=length(R200);

% Our template mask
IsCluster=ones(N,1); % 0==false, 1==true

% Initialise the parent of the substructures.
Parent=zeros(N,1);

% Sort the masses.
[dummy,I]=sort(M200);

% We want the most massive, first
I=flipud(I)';

%%% Index the centres into cells using a link list
L=floor(1/max(R200));
[Start,ll]=LL_Create(centres',L);

% This looping business is not efficient, but then there should not be a huge
% number of structure (typically < 10000 )
for i=I % Loop through the centres in order of their mass (decreasing)
 if(IsCluster(i)==1) % All structures within R200 are substructure.
                     % Remove their cluster flag.
  %%% NOTE: This algorithm won't erroneously change the setting of a larger
  %%%       cluster if and only if it is alway true that a larger M200 implies
  %%%       a larger R200.  AFAIK, this is always true, but it is not
  %%%       necessarily true.
  %% Get all the centres in the neighbouring 27 cells
  Cell=mod(floor(centres(i,:)*L),L)+1;
  spanI=mod( [-1:1]+Cell(1)-1,L)+1;
  spanJ=mod( [-1:1]+Cell(2)-1,L)+1;
  spanK=mod( [-1:1]+Cell(3)-1,L)+1;
  Elements=[];
  for I=spanI
   for J=spanJ
    for K=spanK
     if(Start(I,J,K)>0) % Not a necessary test, but it helps performance.
      Elements=[Elements;LL_ElementsInCell(Start,ll,I,J,K)];
     end
    end
   end
  end

  %% One of the elments will be the cluster we are working on.
  %% Remove it from the list.
  cut=find(Elements==i);
  Elements=[Elements(1:cut-1);Elements(cut+1:end)];
  
  %% Examine the distances to all the neighbouring centres.
  %% If they are within R200, set them to 0.
  cent=ones(length(Elements),1)*centres(i,:);
  dr=rem(centres(Elements,:)-cent+1.5,1)-.5;
  dist=sqrt( sum( dr'.^2 ) );
  SubStructure=Elements(find(dist<R200(i)));
  NStructures=length(SubStructure);
  IsCluster(SubStructure)=zeros(NStructures,1);
  Parent(SubStructure)=i*ones(NStructures,1);

 end % if a cluster
end % loop through all centres in order of their mass (Largest first)

HaloStructure=find(~IsCluster);
Parent=Parent(HaloStructure);
Cluster=find(IsCluster);
