function centres=HaloFindViaDeprojection(varargin)
% HaloFindViaDeprojection: Find clusters using tomographic deprojection.
%
% syntax: centres=HaloFindViaDeprojection(irun,step,Res)
%     or: centres=HaloFindViaDeprojection(r,rm,dn,h,Res)
%
%
% centres=HaloFindViaDeprojection(irun,itime,Res,SaveFlag)
%  Read the data from a file.  The gas is used for finding clusters.
%
%  ARGUMENTS
%   irun	Run number.				See: dataname (irun)
%   itime	Iteration label.			See: dataname (step)
%   Res		The resolution of the projected masses.	See: h_proj (L)
%
%
% centres=HaloFindViaDeprojection(r,rm,dn,h,Res)
%  Use the data supplied.
%
%  ARGUMENTS
%   r            The (x,y,z) position of each particle.
%   rm           The mass of each particle.
%   h            The smoothing radii.                    See: h_proj (h)
%   Res          The resolution of the projected masses. See: h_proj (L)
%
%
% SEE ALSO:
%  find_centres readhydra dataname h_proj deproj refine_centres

% AUTHOR; Eric Tittley
%
% HISTORY:
% 00 05 05
%  Updated to use r(3,N) arrays instead of r(N,3)
%  and to be compatible with hydra4.0
% 00 11 12 Removed A=flipud(A) since that line was removed from h_proj
% 02 04 29 Redo to use variable argument lists.
% 04 07 27 Minor changes to the comments.
% 11 01 18 Strip refine_centres out of find_centres and rename to
%          HaloFindViaDeprojection
% 11 02 15 Centres returned is now 3xM instead of Mx3
% 11 02 16 Use a subfunction for the locating of peaks

if(length(varargin)==3)
 irun =varargin{1};
 itime=varargin{2};
 Res  =varargin{3};
 [rm,r,v,th,itype,dn,h,hdr]=readhydra(dataname(irun,itime));
% A mistake in hydra means that all particles that are 'lost' have their itype
% changed to -2.
% In Hydra 4.0, all gas particles must be found at the start of arrays.
 gas=find(itype(1:hdr.ngas) & dn>10*hdr.ngas); %dn==hdr.ngas => dn==rho_c
 clear v th itype
 r=r(:,gas);
 rm=rm(gas);
 dn=dn(gas);
 h=h(gas);
 clear gas
elseif(length(varargin)==5)
 r=  varargin{1};
 rm= varargin{2};
 dn= varargin{3};
 h=  varargin{4};
 Res=varargin{5};
else
 disp('syntax: centres=HaloFindViaDeprojection(irun,step,Res)')
 disp('    or: centres=HaloFindViaDeprojection(r,rm,dn,h,Res)')
 error('HaloFindViaDeprojection: invalid syntax')
end

%xy
Axy=h_proj(r(1,:),r(2,:),rm,h,Res,[0 1 0 1 0 1]);
Pos=FindPeaksInField(Axy);
[Y1,X1]=ind2sub(size(Axy),Pos);

%xz
Axz=h_proj(r(1,:),r(3,:),rm,h,Res,[0 1 0 1 0 1]);
Pos=FindPeaksInField(Axz);
[Z2,X2]=ind2sub(size(Axz),Pos);

%yz
Ayz=h_proj(r(2,:),r(3,:),rm,h,Res,[0 1 0 1 0 1]);
Pos=FindPeaksInField(Ayz);
[Z3,Y3]=ind2sub(size(Ayz),Pos);

clear Aleft Aright Aup Adown Aleft_up Aleft_down Aright_up Aright_down
%clear Axy Axz Ayz

disp('deprojecting...')
% deproj is very sensitive to max_tol, and how it should be treated is not obvious.
% deproj expands its tolerance iteratively, until it hits max_tol
%max_tol=round(Res*0.01); % pixels
max_tol=5; % pixels

[X,Y,Z,tol]=deproj(X1,Y1,X2,Z2,Y3,Z3,max_tol);
clear X1 Y1 X2 Z2 Y3 Z3 max_tol
centres=[X,Y,Z]'/Res;

%%%% END of main()
