% ParticlesInSphere: List of particles within a distance of a position
%
% Index = ParticlesInSphere(r,centre,rad)
%
% ARGUMENTS
%  r            Particle positions [3xN]
%  centre       Centre of sphere (1x3)
%  radius       Radius to search
%
% RETURNS
%  Index        List of particles within rad of centre
%
% SEE ALSO
%  recentre

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  120731 First version

function Index = ParticlesInSphere(r,centre,radius)

%%% Get the number of objects, transpose if necessary
nobj=size(r,2);
if(nobj==3)
 if(size(r,1)~=3)
  r=r';
  nobj=size(r,2);
 end
end

Centre=centre*ones(1,nobj);
dist2=sum((rem(r-Centre+1.5,1)-0.5).^2);
Index=find(dist2<=radius^2);
