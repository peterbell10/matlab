function centres=getcentres(r,n,centres)
% syntax: centres=getcentres(r,n,[centres])
%
% Returns a list of n centres interactively selected from the distribution
% of particles given by r.
%
% If the list is to be appended to an existing list, then the existing
% list can be passed as an optional 3rd argument.
%
% see also: refine_centres

if(nargin<3), centres=[]; end
num_centres=length(centres);
clf
for i=1:n

%Plot the x-y plane, first
 fig=plot(r(:,1),r(:,2),'.');
 set(fig,'markersize',5);
 if(length(centres)>0)
  hold on
  plot(centres(:,1),centres(:,2),'ro');
  hold off
 end

%Zoom in
 title('Click on a cluster to zoom in on')
 [x,y]=ginput(1);
 axis([x-0.1 x+0.1 y-0.1 y+0.1]);

%get the x-y coords for this cluster
 title('Click on the cluster centre, exactly')
 [x,y]=ginput(1);
 x1=x;

%Plot the x-z plane
 box=find(r(:,1)>x-0.1 & r(:,1)<x+0.1 & r(:,2)>y-0.1 & r(:,2)<y+0.1);
 if(~isempty(centres))
  box_centres=find(centres(:,1)>x-0.1 & centres(:,1)<x+0.1 & centres(:,2)>y-0.1 & centres(:,2)<y+0.1);
 end
 fig=plot(r(box,1),r(box,3),'.');
 set(fig,'markersize',5);
 if(~isempty(centres))
  hold on
  plot(centres(box_centres,1),centres(box_centres,3),'ro');
  hold off
 end
 axis([x-0.1 x+0.1 0 1])

%Zoom in
 title('Click on the same cluster to zoom in')
 [x,z]=ginput(1);
 axis([x-0.1 x+0.1 z-0.1 z+0.1]);

%get the x-z coords for this cluster
 title('Click on the cluster centre, exactly')
 [x,z]=ginput(1);
 x2=x;

%set the coordinates
 num_centres=num_centres+1;
 centres(num_centres,1)=(x1+x2)/2;
 centres(num_centres,2)=y;
 centres(num_centres,3)=z;

end
