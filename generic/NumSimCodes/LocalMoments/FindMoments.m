function [Mnorm,Mdp,MaxFacetD,Mqp,MaxFacetQ,a_res,Sphericity]=FindMoments(a)
% FindMoments: Determine the local mean, dipole, and quadropole of a particle
%	       distribution.
%
% [Mnorm,Mdp,MaxFacetD,Mqp,MaxFacetQ,a_res,Sphericity]=FindMoments(a);
%
% ARGUMENTS
%  a	The anisotropy array, as returned by anisotropy();
%
% RETURNS
%  Mnorm	The normal (mean of a(:,i));
%  Mdp		The dipole moment
%  MaxFacetD	The facet for which the dipole is a maximum
%  Mqp		The quadropole moment 
%  MaxFacetQ	The facet for which the quadropole is a maximum
%  a_res	The residual of a, after the monopole, dipole, and quadropoles
%		have been removed
%  Sphericity	The degree of circular symmetry of a 0==perfect symmetry,
%		>0 is increasingly less circular
%
% NOTES
%  The monopole moment is one for every particle, since the anisotropy for each
%  particle has been normalised by the mean anisotopy for the particle.
%
% REQUIRES
%  anisotropy
%
% SEE ALSO
%  anisotropy Classify

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

[dummy,N]=size(a);
disp('Normalizing');
Mnorm=mean(a)';

NonZeros=find(Mnorm);
Zeros=find(~Mnorm);
a_res=0*a;
a_res(:,NonZeros)=a(:,NonZeros)./(ones(12,1)*Mnorm(NonZeros)');

disp('Finding the residual after the 1st moment has been subtracted');
% The first moment is unity for every particle, since they have been 
% normalised
a_res=a_res-1;

%mean(mean(a_res))
mean(std(a_res)), %a_res

Sphericity=std(a_res)';

%The directions of the facets (in radians)
theta=pi/2-(63+26/60)/180*pi;
Dir=[
 0.0000  pi/2; ...
 0.0000  theta; ...
 1.2566  theta; ...
 2.5133  theta; ...
 3.7699  theta; ...
 5.0265  theta; ...
 0.6283 -theta; ...
 1.8850 -theta; ...
 3.1416 -theta; ...
 4.3982 -theta; ...
 5.6549 -theta; ...
 0.0000 -pi/2  ];

%Opos(i) is the opposite facet of facet, i
Opos=[12,9,10,11,7,8,5,6,2,3,4,1];

%Adj(i,:) is the 5 adjacent facets to facet i
Adj=zeros(12,5);
Adj( 1,:)=[ 2  3  4  5  6];
Adj( 2,:)=[ 1  3  6  7 11];
Adj( 3,:)=[ 1  2  4  7  8];
Adj( 4,:)=[ 1  3  5  8  9];
Adj( 5,:)=[ 1  4  6  9 10];
Adj( 6,:)=[ 1  2  5 10 11];
Adj( 7,:)=[ 2  3  8 11 12];
Adj( 8,:)=[ 3  4  7  9 12];
Adj( 9,:)=[ 4  5  8 10 12];
Adj(10,:)=[ 5  6  9 11 12];
Adj(11,:)=[ 2  6  7 10 12];
Adj(12,:)=[ 7  8  9 10 11];

Mdp_arr=0*a;


% alpha=pi/2-theta;
% beta=acot( cos(pi/5)*cos(alpha)/sin(alpha) );
% phi=(alpha+beta)/2;

% using phi=alpha is a good approximation,
% using phi=(alpha+beta)/2 is better
% using phi such that 2*pi(1-cos(phi/2))=4*pi/12 is best
% integrating over a pentagonal patch would be exact.

phi=2*acos(1-1/6);

%Dipole Basis vector
Coef1=0.5*(sin(phi/2))^2/(1-cos(phi/2));
Coef2=cos(phi)*Coef1;  % might look to simple, but work out the integeral. This is what you get
Bd=[Coef1 Coef2  Coef2  Coef2  Coef2  Coef2 ...
         -Coef2 -Coef2 -Coef2 -Coef2 -Coef2 -Coef1 ];
Normalization=sqrt( sum( Bd.^2 )/12 );
Bd=Bd/Normalization;

%Quadropole Basis Vector (Moment = 3cos(theta)^3-1 )
Coef1=(cos(phi/2)-cos(phi/2)^3 )/(1-cos(phi/2));
%Coef2= ( 3/2*cos(phi)^2*(1-cos(phi/2)^4) ...
%        +9/2*sin(phi)^2*cos(phi)*sin(phi/2)^4 ...
%        +2*(cos(phi/2)-1) ) / (2*(1-cos(phi/2)));
% which gives -0.2316
% the previous should equal this, but I must have made an integration error
% the next results is from numerical integration
%Coef2=-0.4964;
% the next is from simple symmetry arguments (since the sum has to equal zero)
Coef2=-1*Coef1/5;
% which gives -0.3056
Bq=[Coef1 Coef2 Coef2 Coef2 Coef2 Coef2 ...
          Coef2 Coef2 Coef2 Coef2 Coef2 Coef1 ];
Normalization=sqrt( sum( Bq.^2 )/12 );
Bq=Bq/Normalization;

% *** Lets make the array of permutations of each of the Basis vectors ***

PermBd=zeros(6,12);
PermBq=PermBd;

for i=1:6
 PermBd(i,i)		 =Bd(1);
 PermBd(i,Adj(i,:))	 =Bd(2:6);
 PermBd(i,Adj(Opos(i),:))=Bd(7:11);
 PermBd(i,Opos(i))	 =Bd(12);

 PermBq(i,i)		 =Bq(1);
 PermBq(i,Adj(i,:))	 =Bq(2:6);
 PermBq(i,Adj(Opos(i),:))=Bq(7:11);
 PermBq(i,Opos(i))	 =Bq(12);
end

PermBd=PermBd';
PermBq=PermBq';

%For each particle's anisotropy vector, there will be 36 different
% combinations of direction for the dipole and quadropole moments
%For each of these combinations, we will find the best-fit combination
% of Md and Mq, including direction.

MaxFacetD=zeros(N,1);
MaxFacetQ=MaxFacetD;
Coef_best=zeros(N,2);

resnorm_min=1e99;
for part=1:N
 if(~mod(part,1000)), disp(part), end
 resnorm_min=1e99;
 d=a_res(:,part);
 for comb_d=1:6
  dummy=PermBd(:,comb_d);
  for comb_q=1:6
   C=[dummy,PermBq(:,comb_q)];
   coef=C\d;
   residual = C*coef-d;
   resnorm = sum(residual.*residual);
   if(resnorm<resnorm_min) 
    resnorm_min=resnorm;
    MaxFacetD(part)=comb_d;
    MaxFacetQ(part)=comb_q;
    Coef_best(part,:)=coef';
   end
  end
 end
end

Mdp=zeros(N,1);
Mqp=Mdp;
for i=1:N
 if(Coef_best(i,1)<0)
  Mdp(i)=-1*Coef_best(i,1);
  MaxFacetD(i)=Opos(MaxFacetD(i));
 else
  Mdp(i)=Coef_best(i,1);
 end
end
Mqp=Coef_best(:,2);
