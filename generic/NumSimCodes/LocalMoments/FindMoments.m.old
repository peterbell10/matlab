function [Mnorm,Mdp,MaxFacetD,Mqp,MaxFacetQ,a_res,Sphericity]=FindMoments(a)
%
% [Mnorm,Mdp,MaxFacetD,Mqp,MaxFacetQ,a_res,Sphericity]=FindMoments(a);
%
% a	The anisotropy array, as returned by anisotropy();
%
% Mnorm	     The normal (mean of a(:,i));
% Mdp	     The dipole moment
% MaxFacetD  The facet for which the dipole is a maximum
% Mqp        The quadropole moment 
% MaxFacetQ  The facet for which the quadropole is a maximum
%
% a_res      The residual of a, after the monopole, dipole, and quadropoles
%            have been removed
%
% Sphericity The degree of circular symmetry of a 0==perfect symmetry,
%            >0 is increasingle less circular
%
% The monopole moment is one for every particle, since the anisotropy for each
% particle has been normalised by the mean anisotopy for the particle.

[dummy,N]=size(a);
disp('Normalizing');
Mnorm=mean(a);

NonZeros=find(Mnorm);
Zeros=find(~Mnorm);
a_res=0*a;
a_res(:,NonZeros)=a(:,NonZeros)./(ones(12,1)*Mnorm(NonZeros));

disp('Finding the residual after the 1st moment has been subtracted');
% The first moment is unity for every particle, since they have been 
% normalised
a_res=a_res-1;

%mean(mean(a_res))
mean(std(a_res)), %a_res

Sphericity=std(a_res);

%The directions of the facets (in radians)
Dir=[
 0.0000  1.5708; ...
 0.0000  0.4637; ...
 1.2566  0.4637; ...
 2.5133  0.4637; ...
 3.7699  0.4637; ...
 5.0265  0.4637; ...
 0.6283 -0.4637; ...
 1.8850 -0.4637; ...
 3.1416 -0.4637; ...
 4.3982 -0.4637; ...
 5.6549 -0.4637; ...
 0.0000 -1.5708  ];

%Opos(i) is the opposite facet of facet, i
Opos=[12,9,10,11,7,8,5,6,2,3,4,1];

%Adj(i,:) is the 5 adjacent facets to facet i
Adj=zeros(12,5);
Adj( 1,:)=[ 2  3  4  5  6];
Adj( 2,:)=[ 1  3  6  7 11];
Adj( 3,:)=[ 1  2  4  7  8];
Adj( 4,:)=[ 1  3  5  8  9];
Adj( 5,:)=[ 1  4  6  9 10];
Adj( 6,:)=[ 1  2  5 10 11];
Adj( 7,:)=[ 2  3  8 11 12];
Adj( 8,:)=[ 3  4  7  9 12];
Adj( 9,:)=[ 4  5  8 10 12];
Adj(10,:)=[ 5  6  9 11 12];
Adj(11,:)=[ 2  6  7 10 12];
Adj(12,:)=[ 7  8  9 10 11];

Mdp_arr=0*a;

CosFundAng=cos(Dir(2,2));

disp('finding the Dipole Moments')
normalization=10*sqrt(0.2+CosFundAng^2);
for part=1:N
 if(~mod(part,10000)), disp(part), end
 a_shrt=a_res(:,part);
 for facet=1:12
  Mdp_arr(facet,part)=( a_shrt(facet)       ...
                       -a_shrt(Opos(facet)) ...
                       +( sum(a_shrt(Adj(facet,:))) ...
                         -sum(a_shrt(Adj(Opos(facet),:))) )*CosFundAng ...
                       )/normalization;
 end
end

disp('Finding the maximum Mdp for each particle')
MaxFacet=0*Mnorm;
for part=1:N
 Max=find(Mdp_arr(:,part)==max(Mdp_arr(:,part)));
 MaxFacet(part)=Max(1);
end

Mdp=0*Mnorm;
for part=1:N
 Mdp(part)=Mdp_arr(MaxFacet(part),part);
end

clear Mdp_arr

disp('Subtracting the Mdp from each particle');
da=0*a;
for part=1:N
 MF=MaxFacet(part);
 da(MF,part)             = 1;
 da(Opos(MF),part)       =-1;
 da(Adj(MF,:),part)      =   CosFundAng;
 da(Adj(Opos(MF),:),part)=-1*CosFundAng;
 da(:,part)=da(:,part)*Mdp(part);
end

a_res=a_res-da;
MaxFacetD=MaxFacet;

%mean(mean(a_res))
mean(std(a_res)), %a_res

disp('Finding the Quadopole Moments')
Mqp_arr=0*a;
%normalization=sqrt( 2*5^2 +10*CosFundAng^2 );
CosFundAng=1;
normalization=3*(2*5+10*CosFundAng);
for part=1:N
 if(~mod(part,10000)), disp(part), end
 a_shrt=a_res(:,part);
 for facet=1:12
  Mqp_arr(facet,part)=( 5*( a_shrt(facet) + a_shrt(Opos(facet)) )...
                       -sum(a_shrt([Adj(facet,:) Adj(Opos(facet),:)]))*CosFundAng ...
                       )/normalization;
 end
end

disp('Finding the maximum Mqp for each particle')
for part=1:N
 Max=find(Mqp_arr(:,part)==max(Mqp_arr(:,part)));
 MaxFacet(part)=Max(1);
end

Mqp=0*Mnorm;
for part=1:N
 Mqp(part)=Mqp_arr(MaxFacet(part),part);
end

clear Mqp_arr

disp('Subtracting the Mqp from each particle');
for part=1:N
 MF=MaxFacet(part);
 da(MF,part)                         = 5;
 da(Opos(MF),part)                   = 5;
 da([Adj(MF,:) Adj(Opos(MF),:)],part)=-1*CosFundAng;
 da(:,part)=da(:,part)*Mqp(part);
end

a_res=a_res-da;
clear da

MaxFacetQ=MaxFacet;

%mean(mean(a_res))
mean(std(a_res)), % a_res
