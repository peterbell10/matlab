function Class=Classify(Sphericity,DP,QP)
% Classify        Classifies the particles into those with local spherical,
%                 planar, and filamentary structure
%
% function Class=Classify(Sphericity,DP,QP)
%
% ARGUMENTS
%  Sphericity	The degree of circular symmetry of a 0==perfect symmetry,
%		>0 is increasingly less circular
%  DP		The dipole moment
%  QP		The quadropole moment
%
% RETURNS
%  Class = 1 for a sphere
%          2 for a sheet
%          3 for a filament
%          4 for neither
%          5 for overlapping as a sphere, sheet, and filament
%
% NOTES
%  Sphericity, DP, and QP out output from FindMoments
%
% SEE ALSO
%  FindMoments

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

mask_Sphere=Sphericity<0.5 & DP<0.4 & QP<0.2; % and Sph>DP and Sph>QP
mask_Sheet =Sphericity>=0.5 & Sphericity<0.9 & QP<0.3; % and Sph>DP
mask_Filam =(Sphericity>0.9 & QP>=0.1 & DP>0.1) | (QP>=0.3);

Class=0*Sphericity;
Class(find(mask_Sphere))=1;
Class(find(mask_Sheet ))=2;
Class(find(mask_Filam ))=3;
Class(find(~mask_Filam & ~mask_Sheet & ~mask_Sphere ))=4;
Class(find(mask_Filam & mask_Sheet & mask_Sphere ))=5;
