% padNBody: Pad data around an NBody distribution
%
% [Rpadded,Index] = padNBody(R,BoxWidth,PadWidth)
%
% ARGUMENTS
%  R           The input particle positions. Either [NPart,NDim] or
%              [NDim,NPart] and assumed to be on the range [0,BoxWidth)
%  BoxWidth    The upper boundary position.
%
%  PadWidth    The width of the padding, in the same units as BoxWidth and R.               
%
% RETURNS
%  Rpadded     The input particle positions followed by the padding particles.
%              [M,NDim] where M>NPart. I.e., Rpadded(NPart,:)==R (or R');
%
%  Index       Index of particles so extra data elements can also be
%              constructed for the pad particles.
%              example: hnew = [h h(index)];	

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab and Octave  

function [Rpadded,Index] = padNBody(R,BoxWidth,PadWidth)

[NPart,NDim]=size(R);
if(NPart<NDim)
 R=R';
 tmp=NDim;
 NDim=NPart;
 NPart=NDim;
end

P=[]; % Container for the padding particles
Index=[]; % Container for the index
switch NDim
 case 2,
  for i=-1:1
   switch i
    case -1,
     I=find(R(:,1)>BoxWidth-PadWidth);
     S=R(I,:);
    case 0,
     I=[1:size(R,1)]';
     S=R;
    case 1,
     I=find(R(:,1)<         PadWidth);
     S=R(I,:);
   end
   N=length(I);
   S=S+i*[BoxWidth*ones(N,1),zeros(N,1)];
   for j=-1:1
    switch j
     case -1,
      mask=find(S(:,2)>BoxWidth-PadWidth);
      Ij=I(mask);
      Sj=S(mask,:);
     case 0,
      if(i~=0)
       Ij=I;
       Sj=S;
      else
       Ij=[];
       Sj=[];
      end
     case 1,
      mask=find(S(:,2)<         PadWidth);
      Ij=I(mask);
      Sj=S(mask,:);
    end
    N=length(Ij);
    if(N>0)
     Sj=Sj+j*[zeros(N,1),BoxWidth*ones(N,1)];
     P=[P;Sj];
     Index=[Index;Ij];
    end
   end
  end
 case 3,
  for i=-1:1
   switch i
    case -1,
     Ii=find(R(:,1)>BoxWidth-PadWidth);
     Si=R(Ii,:);
    case 0,
     Ii=[1:size(R,1)]';
     Si=R;
    case 1,
     Ii=find(R(:,1)<         PadWidth);
     Si=R(Ii,:);
   end
   N=length(Ii);
   Si=Si+i*[BoxWidth*ones(N,1),zeros(N,1),zeros(N,1)];
   for j=-1:1
    switch j
     case -1,
      mask=find(Si(:,2)>BoxWidth-PadWidth);
      Ij=Ii(mask);
      Sj=Si(mask,:);
     case 0,
      Ij=Ii;
      Sj=Si;
     case 1,
      mask=find(Si(:,2)<         PadWidth);
      Ij=Ii(mask);
      Sj=Si(mask,:);
    end
    N=length(Sj);
    Sj=Sj+j*[zeros(N,1),BoxWidth*ones(N,1),zeros(N,1)];
    for k=-1:1
     switch k
      case -1,
       mask=find(Sj(:,3)>BoxWidth-PadWidth);
       Ik=Ij(mask);
       Sk=Sj(mask,:);
      case 0,
       if(i~=0 | j~=0)
        Ik=Ij;
        Sk=Sj;
       else
        Ik=[];
        Sk=[];
       end
      case 1,
       mask=find(Sj(:,3)<         PadWidth);
       Ik=Ij(mask);
       Sk=Sj(mask,:);
     end
     N=length(Ik);
     if(N>0)
      Sk=Sk+k*[zeros(N,1),zeros(N,1),BoxWidth*ones(N,1)];
      P=[P;Sk];
      Index=[Index;Ik];
     end
    end % end loop over k
   end % end loop over j
  end % end loop over i
 otherwise,
  error('ERROR: Unable to deal with %i dimensions',NDim);
end

Rpadded = [R;P];

