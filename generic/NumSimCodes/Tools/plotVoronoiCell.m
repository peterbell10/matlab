% plotVoronoiCell: Plot a single cell for a Voronoi Cell
%
% plotVoronoiCell(V,C,cellNumber,Colour)
%
% ARGUMENTS
%  V Voronoi vertices (see usage)
%  C Voronoi cell     (see usage)
%
% RETURNS
%  nothing
%
% USAGE
%  [V,C]=voronoin(R); % R is N x 3 positions of N particles
%  % Plot the 10th particle's cell with colour index 1
%  plotVoronoiCell(V,C,10,1); 

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab 


function plotVoronoiCell(V,C,cellNumber,Colour)

VerticesOfCell = V(C{cellNumber},:);

K = convhulln(VerticesOfCell,{'Qt','QbB'});

for i=1:length(K)
 P=VerticesOfCell(K(i,:),:);
 patch(P(:,1),P(:,2),P(:,3),Colour)
end
