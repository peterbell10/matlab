function index=particlesInLimits(r,Limits)
% particlesInLimits: List of particles in a volume
%
% index=particlesInLimits(r,Limits)
%
% ARGUMENTS
%  r	  particle positions [3,N]
%  Limits The boundary of the volumne [x_low x_high y_low y_high z_low z_high]
%         Interpreted as x_low <= x < h_high.
%
% RETURNS
%  index  List of particles that are contained within Limits.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab

[M,N]=size(r);
if( M==3 & N~=3 )
 r=r';
end

index=find(   r(:,1)>=Limits(1) & r(:,1)<Limits(2) ...
            & r(:,2)>=Limits(3) & r(:,2)<Limits(4) ...
	    & r(:,3)>=Limits(5) & r(:,3)<Limits(6) );
