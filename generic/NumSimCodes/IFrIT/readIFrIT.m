% readIFrIT: Read Ionization FRont Interactive Tool (IFrIT) input files
%
% D=readIFrIT(file)
%
% ARGUMENTS
%  file The name of the file to read.
%
% RETURNS
%  D    The grid of data in the file.

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function D=readIFrIT(file)


FID=fopen(file,'rb');

Dimensions=readFortranRecord(FID,'int32');
Dimensions=Dimensions';

D=readFortranRecord(FID,'float32');

D=reshape(D,Dimensions);


