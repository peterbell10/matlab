%
%***************************************************************************
% [overdens,Tgas,v_x,v_y,v_z,f_HI,f_HeII,f_e] = gaReadGadgetDataFile(filename,ngrid,field) *
%***************************************************************************
%***************************************************************************
%
% ARGUMENTS
%  filename      Filename for gadget data file
%  ngrid         Grid size along one axis
%  field         One of 'overdensity', 'temperature', 'v_x', 'v_y', 'v_z', 'all'
%
% RETURNS
% overdens       Gas overdensity, Delta = rho/ <rho> = 1 + delta
% Tgas           Gas temperature (K)
% v_x            x-direction peculiar velocity
% v_y            y-direction peculiar velocity
% v_z            z-direction peculiar velocity
% f_HI           n_HI/ n_H (HI fraction)
% f_HeII         n_HeII/ n_H
% f_e            n_e/ n_H
%
% COMPATIBILITY: Matlab, Octave
%
% AUTHOR: Avery Meiksin
%
% HISTORY:
%  29 08 13 Creation date. (Fashiond after IDL routine read_gascube.pro.)
%  13 01 15 Rewritten to minimize memory usage. (aam)
%
% Reads output file from GadgetToGrid gridding routine of Gadget output.
%
function [overdens,Tgas,v_x,v_y,v_z,f_HI,f_HeII,f_e] = gaReadGadgetDataFile(filename,ngrid,field);
%
% Zero everything returned
overdens = 0;
Tgas = 0;
v_x = 0;
v_y = 0;
v_z = 0;
f_HI = 0;
f_HeII = 0;
f_e = 0;

ngrid2  = ngrid*ngrid;
ngrid3  = ngrid2*ngrid;

% Read in the file
% The input is a 1D array with ngrid^3 elements
fid = fopen(filename,'rb');
icase = 0;
if(strcmp(field,'overdensity'))
  icase = 1;
end
if(strcmp(field,'temperature'))
  icase = 2;
end
if(strcmp(field,'v_x'))
  icase = 3;
end
if(strcmp(field,'v_y'))
  icase = 4;
end
if(strcmp(field,'v_z'))
  icase = 5;
end
if(strcmp(field,'f_HI'))
  icase = 6;
end
if(strcmp(field,'f_HeII'))
  icase = 7;
end
if(strcmp(field,'f_e'))
  icase = 8;
end
if(strcmp(field,'all'))
  icase = 9;
end
if(icase > 0)
  odens   = zeros(1,ngrid3);    % Gas overdensity, Delta = rho/<rho> = 1+delta
  odens = fread(fid,ngrid3,'float');
  disp(' ');
  disp(filename);
  disp('Checking extrema:');
  if(icase == 1 || icase == 9)
    disp('log Delta min/max');
    min(log10(odens))
    max(log10(odens))
  end
  overdens = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    overdens(i,:,:) = reshape(odens(istart:iend),ngrid,ngrid)';
  end
  clear odens;
end
if(icase > 1)
  temp    = zeros(1,ngrid3);    % temperature [K]
  temp  = fread(fid,ngrid3,'float');
  if(icase == 2 || icase == 9)
    disp(' ');
    disp(filename);
    disp('Checking extrema:');
    disp('log T (K) min/max');
    min(log10(temp))
    max(log10(temp))
  end
  Tgas     = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    Tgas(i,:,:) = reshape(temp(istart:iend),ngrid,ngrid)';
  end
  clear temp;
end
if(icase > 2)
  velx    = zeros(1,ngrid3);    % x-direction peculiar velocity km/s
  velx  = fread(fid,ngrid3,'float');
  if(icase == 3 || icase == 9)
    disp(' ');
    disp(filename);
    disp('Checking extrema:');
    disp('vpec_x (km/s) min/max');
    min(velx)
    max(velx)
  end
  v_x      = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    v_x(i,:,:) = reshape(velx(istart:iend),ngrid,ngrid)';
  end
  clear velx;
end
if(icase > 3)
  vely    = zeros(1,ngrid3);    % y-direction peculiar velocity km/s
  vely  = fread(fid,ngrid3,'float');
  if(icase == 4 || icase == 9)
    disp(' ');
    disp(filename);
    disp('Checking extrema:');
    disp('vpec_y (km/s) min/max');
    min(vely)
    max(vely)
  end
  v_y      = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    v_y(i,:,:) = reshape(vely(istart:iend),ngrid,ngrid)';
  end
  clear vely;
end
if(icase > 4)
  velz    = zeros(1,ngrid3);    % z-direction peculiar velocity km/s
  velz  = fread(fid,ngrid3,'float');
  if(icase == 5 || icase == 9)
    disp(' ');
    disp(filename);
    disp('Checking extrema:');
    disp('vpec_z (km/s) min/max');
    min(velz)
    max(velz)
  end
  v_z      = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    v_z(i,:,:) = reshape(velz(istart:iend),ngrid,ngrid)';
  end
  clear velz;
end
if(icase > 5)
  fHI    = zeros(1,ngrid3);    % n_HI/ n_H
  fHI  = fread(fid,ngrid3,'float');
  if(icase == 6 || icase == 9)
    disp(' ');
    disp(filename);
    disp('Checking extrema:');
    disp('f_HI min/max');
    min(fHI)
    max(fHI)
  end
  f_HI      = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    f_HI(i,:,:) = reshape(fHI(istart:iend),ngrid,ngrid)';
  end
  clear fHI;
end
if(icase > 6)
  fHep    = zeros(1,ngrid3);    % n_HeII/ n_H
  fHep  = fread(fid,ngrid3,'float');
  if(icase == 7 || icase == 9)
    disp(' ');
    disp(filename);
    disp('Checking extrema:');
    disp('f_HeII min/max');
    min(fHep)
    max(fHep)
  end
  f_HeII      = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    f_HeII(i,:,:) = reshape(fHep(istart:iend),ngrid,ngrid)';
  end
  clear fHep;
end
if(icase > 7)
  fe    = zeros(1,ngrid3);    % n_e/ n_H
  fe  = fread(fid,ngrid3,'float');
  if(icase == 8 || icase == 9)
    disp(' ');
    disp(filename);
    disp('Checking extrema:');
    disp('f_e min/max');
    min(fe)
    max(fe)
  end
  f_e      = zeros(ngrid,ngrid,ngrid);
% Order in agreement with read_gascube.pro
  for i = 1:ngrid
    istart = (i-1)*ngrid2 + 1;
    iend   = i*ngrid2;
    f_e(i,:,:) = reshape(fe(istart:iend),ngrid,ngrid)';
  end
  clear fe;
end
fclose(fid);
