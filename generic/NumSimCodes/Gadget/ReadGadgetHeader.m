% ReadGadgetHeader: Read a Gadget header
%
% [D,hdr]=ReadGadgetHeader(fid)
%
% ARGUMENTS
%  Input
%   fid   File Identifier (as returned by fopen() )
%
% RETURNS
%  hdr     The header of the file, returned as a structure
%
%  Elements of hdr:
%   npart
%   mass
%   time
%   redshift
%   sft
%   feedback
%   npartTotal
%   flag_cooling
%   num_files
%   BoxSize
%   Omega0
%   OmegaLambda
%   HubbleParam
%   flag_stellarage
%   flag_metals
%   hashtabsize
%
% SEE ALSO
%  readgadget ReadGadgetField

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab Octave
%
% HISTORY
%  13 02 13 First version.

function hdr=ReadGadgetHeader(fid)

count=0; % # of words (4 bytes)
blocksize=fread(fid,1,'uint');
hdr.npart           =fread(fid,6,'uint');   count=count+6;
hdr.mass            =fread(fid,6,'real*8'); count=count+12;
hdr.time            =fread(fid,1,'real*8'); count=count+2;
hdr.redshift        =fread(fid,1,'real*8'); count=count+2;
hdr.sfr             =fread(fid,1,'uint');   count=count+1;
hdr.feedback        =fread(fid,1,'uint');   count=count+1;
hdr.npartTotal      =fread(fid,6,'uint');   count=count+6;
hdr.flag_cooling    =fread(fid,1,'uint');   count=count+1;
hdr.num_files       =fread(fid,1,'uint');   count=count+1;
hdr.BoxSize         =fread(fid,1,'real*8'); count=count+2;
hdr.Omega0          =fread(fid,1,'real*8'); count=count+2;
hdr.OmegaLambda     =fread(fid,1,'real*8'); count=count+2;
hdr.HubbleParam     =fread(fid,1,'real*8'); count=count+2;
hdr.flag_stellarage =fread(fid,1,'uint');   count=count+1;
hdr.flag_metals     =fread(fid,1,'uint');   count=count+1;
hdr.hashtabsize     =fread(fid,1,'uint');   count=count+1;

% Read in the rest of the 256 byte buffer
bytesleft = 256 - count*4;
buffer = fread(fid,bytesleft,'char');
blocksize=fread(fid,1,'uint');
