% Gadget2HydraUnpert: Convert from Gadget native file format to Hydra
%                     unperturbed file format
%
% Gadget2HydraUnpert(GadgetFile,HydraUnpertFile)
%
% ARGUMENTS
%  GadgetFile       The file from which to read the positions
%  HydraUnpertFile  The file to be written
%
% RETURNS
%  Nothing
%
% REQUIRES
%  ReadGadgetField
%
% SEE ALSO
%  readgadget ReadGadgetField readinitpert

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY: 111205 First version

function Gadget2HydraUnpert(GadgetFile,HydraUnpertFile)

% Read in the Gadget file (non-HDF5)
r=ReadGadgetField(GadgetFile,'r');

% The Hydra unperturbed file, used as input to createcosmo, is simply:
%  4-byte integer: Number of bytes in the record
%  3N 4-byte floats: The positions
%  4-byte integer: Number of bytes in the record

% The size of the unperturbed position data
[N,M]=size(r);
NBytes = N*M*4;

% Need to scale to [0:1)
Scale = ceil(max(r(:)));
r = r/Scale;

% Open, write, close
FID = fopen(HydraUnpertFile,'w');
Count = fwrite(FID,NBytes,'uint');
Count = fwrite(FID,r,'float');
Count = fwrite(FID,NBytes,'uint');
fclose(FID);

% DONE
