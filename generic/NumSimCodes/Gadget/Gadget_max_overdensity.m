% Gadget_max_overdensity: Find the statistics of a Gadget IC file
%
% [z,maxOD]=Gadget_max_overdensity(file,h_guess)
%
% ARGUMENTS
%  file     IC file for which to find the maximum Over density
%  h_guess  A guess at the smoothing length. Aim small and the routine will
%           grow the value. If you aim too small, time will be wasted growing
%           the smoothing length. Aim too high and the N^2 scaling will kill
%           performance. (scalar)
%
% RETURNS
%  z        The redshift of the file.
%  maxOD    The maximum overdensity.
%
% REQUIRES
%  ReadGadgetField calcdens_d
%
% SEE ALSO
%  ReadGadgetField readgadget calcdens calcdens_d

% AUTHOR: Eric TIttley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY
%  101116 New version based on hydra/ICs/overdensity_statistics.m

function [z,maxOD]=Gadget_max_overdensity(file,h_guess)

% Read in the header and the positions
hdr= ReadGadgetField(file,'hdr');
r  = ReadGadgetField(file,'r');

%Extract just the DM particles;
if(hdr.npartTotal(1)==0)
 r = r(:,1:hdr.npartTotal(2));
else
 r = r(:,hdr.npartTotal(1)+[1:hdr.npartTotal(2)]);
end

% Note, systematic error is about 0.6 for Nsph=32, 0.1 for 64, and 0.02 for 128
[h,dn]=calcdens_d(r/hdr.BoxSize,64,h_guess);

disp(['<h>=',num2str(mean(h))])

maxOD=max(dn)/length(dn)-1;
z=hdr.redshift;
