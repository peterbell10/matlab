clear

FID=fopen('rad.cds','r');

N = fread(FID,1,'uint64');

tx=fread(FID,N,'float64');
ty=fread(FID,N,'float64');
tz=fread(FID,N,'float64');
tw=fread(FID,N,'float64');

fclose(FID);

r=ReadGadgetField('Data_025','r');
r=r(:,[1:N]);
h=ReadGadgetField('Data_025','h');

hdr=ReadGadgetField('Data_025','hdr');

L=200;
Limits=[0 1 0 1 0 1];
r=r/hdr.BoxSize;
h=h/hdr.BoxSize;

NumDensity=h_proj_3d(r,ones(N,1),h,L,Limits);
tic, ColDensity=h_proj_3d(r,tw,h,L,Limits); toc
ColDensity=ColDensity./NumDensity;

imagesc(log10(ColDensity(:,:,100))), colorbar
