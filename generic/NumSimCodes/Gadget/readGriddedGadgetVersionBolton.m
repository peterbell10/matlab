% readGriddedGadgetVersionBolton: Read a grid file from Bolton's GadgetToGrid
%
% Grid=readGriddedGadgetVersionBolton(fname,Field)
%
% ARGUMENTS
%  Input
%   fname   The name of the file to read
%   Field   [Optional] A specific field. Default is 'all'
%
% RETURNS
%  Grid       The grid, returned as a structure
%  hdr     The header of the file, returned as a structure
%
%  Elements of Grid
%   gridsize
%   boxout
%   rho
%   temp
%   velx
%   vely
%   velz
%   fHI
%   fHeII
%   fe
%
% SEE ALSO
%  readGadgetVersionBolton readgadget ReadGadgetField

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab Octave

function Grid=readGriddedGadgetVersionBolton(fname,Field)

% Default behaviour
if(nargin < 2)
 Field='all';
end

% Open the file
fid=fopen(fname,'rb');
if(fid<=0)
 disp(['ERROR: readGriddedGadgetVersionBolton: unable to open file: ',fname])
 return
end

ierr = 0;
Grid.gridsize = fread(fid,1,'int32');
Grid.boxout   = fread(fid,1,'float32');
Dims=[Grid.gridsize,Grid.gridsize,Grid.gridsize];
cellsPerGrid=prod(Dims);
bytesPerGrid=4*cellsPerGrid;
switch Field
 case 'all'
  Grid.rho    = fread(fid,cellsPerGrid,'float32');
  Grid.temp   = fread(fid,cellsPerGrid,'float32');
  Grid.velx   = fread(fid,cellsPerGrid,'float32');
  Grid.vely   = fread(fid,cellsPerGrid,'float32');
  Grid.velz   = fread(fid,cellsPerGrid,'float32');
  Grid.fHI    = fread(fid,cellsPerGrid,'float32');
  Grid.fHeII  = fread(fid,cellsPerGrid,'float32');
  Grid.fe     = fread(fid,cellsPerGrid,'float32');
  Grid.rho    = reshape(Grid.rho  ,Dims);
  Grid.temp   = reshape(Grid.temp ,Dims);
  Grid.velx   = reshape(Grid.velx ,Dims);
  Grid.vely   = reshape(Grid.vely ,Dims);
  Grid.velz   = reshape(Grid.velz ,Dims);
  Grid.fHI    = reshape(Grid.fHI  ,Dims);
  Grid.fHeII  = reshape(Grid.fHeII,Dims);
  Grid.fe     = reshape(Grid.fe   ,Dims);
 case 'rho'
  Grid.rho    = fread(fid,cellsPerGrid,'float32');
  Grid.rho    = reshape(Grid.rho  ,Dims);
 case 'temp'
  ierr        = fseek(fid,1*bytesPerGrid,'cof');
  Grid.temp   = fread(fid,cellsPerGrid,'float32');
  Grid.temp   = reshape(Grid.temp ,Dims);
 case 'velx'
  ierr        = fseek(fid,2*bytesPerGrid,'cof');
  Grid.velx   = fread(fid,cellsPerGrid,'float32');
  Grid.velx   = reshape(Grid.velx ,Dims);
 case 'vely'
  ierr        = fseek(fid,3*bytesPerGrid,'cof');
  Grid.vely   = fread(fid,cellsPerGrid,'float32');
  Grid.vely   = reshape(Grid.vely ,Dims);
 case 'velz'
  ierr        = fseek(fid,4*bytesPerGrid,'cof');
  Grid.velz   = fread(fid,cellsPerGrid,'float32');
  Grid.velz   = reshape(Grid.velz ,Dims);
 case 'fHI'
  ierr        = fseek(fid,5*bytesPerGrid,'cof');
  Grid.fHI    = fread(fid,cellsPerGrid,'float32');
  Grid.fHI    = reshape(Grid.fHI  ,Dims);
 case 'fHeII'
  ierr        = fseek(fid,6*bytesPerGrid,'cof');
  Grid.fHeII  = fread(fid,cellsPerGrid,'float32');
  Grid.fHeII  = reshape(Grid.fHeII,Dims);
 case 'fe'
  ierr        = fseek(fid,7*bytesPerGrid,'cof');
  Grid.fe     = fread(fid,cellsPerGrid,'float32');
  Grid.fe     = reshape(Grid.fe   ,Dims);
 otherwise
  error(['ERROR: readGriddedGadgetFieldVersionBolton: unrecognized field: ',Field])
end

fclose(fid);

if(ierr  ==-1)
 error('ERROR: readGriddedGadgetFieldVersionBolton: Unsuccessful read. Corrupt file or incorrect format?')
end
