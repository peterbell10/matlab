% RebinCubeToHalf  Decrease the number of cells in a 3D array by 8 by merging
%			8 neighbouring cells into one.
%
% ARGUMENTS
%  B	The array to rebin down. [N,N,N]
%
% RETURNS
%  A	An array 1/8 the size. [N/2,N/2,N/2]
%
% SEE ALSO
%  rebin_img

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: matlab & octave
%
% TODO: Improve argument checks

function A=RebinCubeToHalf(B)

B1=B(1:2:end-1,1:2:end-1,1:2:end-1);
B2=B(1:2:end-1,1:2:end-1,2:2:end  );
B3=B(1:2:end-1,2:2:end  ,1:2:end-1);
B4=B(1:2:end-1,2:2:end  ,2:2:end  );
B5=B(2:2:end  ,1:2:end-1,1:2:end-1);
B6=B(2:2:end  ,1:2:end-1,2:2:end  );
B7=B(2:2:end  ,2:2:end  ,1:2:end-1);
B8=B(2:2:end  ,2:2:end  ,2:2:end  );

A=B1+B2+B3+B4+B5+B6+B7+B8;

A=A/8;
