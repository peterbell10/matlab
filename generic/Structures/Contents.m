% Structures/ Tools to manipulate structures.
%
% struct2arr: Extract an array from a single element of a structure array.
%             Logically equivalent to Struct(:).element(:)
%
% StructCat: Concatenate the elements of structures (and structure arrays).
