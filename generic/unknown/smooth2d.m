function array_out=smooth2d(array_in,radius)
% smooth2d: smooth a 2-d vector with a Gaussian.
%
% array_out=smooth2d(array_in,radius);
%
% ARGUMENTS
%  array_in	The array to smooth
%  radius	The width of the gaus
%
% RETURNS
%  A smoothed array
%
% NOTES
%  DUPLICATES FUNCTIONALITY OF SMOOTH.  DEPRECATED.
%
% REQUIRES
%
% SEE ALSO
%  smooth

% AUTHOR: Eric Tittley
%
% HISTORY
%  07 12 17 Regularised comments
%
% COMPATIBILITY: Matlab, Octave
%
% LICENSE
%  Copyright Eric Tittley
%  The GPL applies to the contents of this file.

[Res1,Res2]=size(array_in);
if(Res1~=Res2), error('smooth2d: array_in must be square'), end
Res=Res1;

gaussian=zeros(Res,Res);
for i=1:Res
 for j=1:Res
  gaussian(i,j)=sqrt( (i-Res/2)^2 + (j-Res/2)^2 );
 end
end
gaussian=gauss(1,radius,gaussian,0);
gaussian=gaussian/sum(sum(gaussian));
gaussian=shift(gaussian,-Res/2,1);
gaussian=shift(gaussian,-Res/2,2);
array_out=real( ifft2( fft2(array_in).* fft2(gaussian) ) );
