function p=tittley(span,order)

% Returns the Tittley values dependant on the vector values in SPAN
% for the orders 0 to ORDER.  Essentially, P is a matrix of size
% (ORDER+1) X (length of SPAN) with the lowest orders in the first rows.
% Written by Eric Tittley, 95 06 24

% Transform the sorted vector (lowest first), SPAN, to the interval [-1:1]
if rem(order,2)==0 error('Parameter ORDER must be odd in call to tittley'); end
a=span(1);
b=span(length(span));
x=(2*span-(b+a))/(b-a);

% Initialize the array, t
p=[1:order+1]'*span*0;

% Set the first term Po=1
p(1,:)=1&[1:length(span)];

% Set the second term P1=x;
p(2,:)=x;

for k=1:((order-1)/2)
% p(2*k+1,:)=(exp(k*x)-1)/((exp(k)-exp(-k))/k-2);
% p(2*k+2,:)=(exp(-k*x)-1)/((exp(k)-exp(-k))/k-2);
 p(2*k+1,:)=(exp(k*x)-1)/(exp(k)-1);
 p(2*k+2,:)=(exp(-k*x)-1)/(exp(k)-1);
end
