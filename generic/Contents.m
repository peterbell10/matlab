% Eric Tittley's Matlab/Octave library
%
% Coordinates		Coordinate conversion and pretty-printing.
%
% ROSAT_tools		Tools for ROSAT analysis
%
% SAX_tools		Tools for BeppoSAX analysis
%
% Scripts		Assorted scripts.
%
% Spectral Models	Creation and fitting of x-ray spectral models.
%
% astro			Astronomy-related functions.
%
% data			Basic data manipulation.
%
% fits			FITS file I/O and manipulation.
%
% graphics		Graphics routines.
%
% hydra			Hydra-software I/O and generic simulation manipulation.
%
% math			Mathematical functions.
%
% matrix		Matrix and image manipulation.
%
% mex			Assorted MEX functions.
%
% misc			Assorted unclassifiable functions.
%
% physics		Physics Calculations.
%
% string		String functions.
%
% time			Date and time conversion
