function index=FilterByRegion(x,y,Region)
% FilterByRegion: Filter as set of coordinates by a region
%
% index=FilterByRegion(x,y,Region)
%
% ARGUMENTS
%  x,y		Coordinates
%  Region	Structure containing regions by which to filter 
%
% RETURNS
%  The index to those (x,y) positions that satisfy the regions determined by
%  the structure array, Region.
%
% NOTES
%  Region is in the format as returned by ParseRegionFile.
%
% SEE ALSO
%  MaskFromRegion ParseRegionFile PlotRegion WriteRegionFile WriteRegionFile_SAS

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 01 18 First version
%  01 01 25 Added support for box.
%  01 02 16 Added support for rotbox.
%	Noticed that a second +shape doesn't really mean anything
%	after an orignal +shape.  The problem is that '+' and '-'
%	is not sufficient for determining regions.  Need 'and', 'or',
%	'not', and 'nor'.
%  01 06 19 Added support for annulus.
%	Fixed a disturbingly large number of bugs: (1) instead of (i),
%	x instead of x_new.
%  01 11 29 Bug in the subtractive case of rotbox.
%  07 12 22 Regularised comments
%  10 11 17 Added support for ellipse
%
% COMPATIBILITY: Matlab, Octave
%
% COPYRIGHT
%  Eric Tittley (2001).
%  Free for use, but you can't make money or receive services by redistributing
%   it in whole or in part.

N_Regions=length(Region);
index=[1:length(x)];

% The first region is a special one, since, if it is additive, then we
% take only those things that are within the region as the base.
% Otherwise, we take the whole data set.
if(Region(1).Sign>0)
 switch Region(1).Shape

  case 'circle'
   if(length(Region(1).P)~=3)
    error('circle must have 3 parameters')
   end
   % Find the distance from the centre of the region to each event
   dist=sqrt( (x-Region(1).P(1)).^2 + (y-Region(1).P(2)).^2 );
   InRegion=find(dist<=Region(1).P(3));

  case 'annulus'
   if(length(Region(1).P)~=4)
    error('annulus must have 4 parameters')
   end
   % Find the distance from the centre of the region to each event
   dist=sqrt( (x-Region(1).P(1)).^2 + (y-Region(1).P(2)).^2 );
   InRegion=find(dist>Region(1).P(3) & dist<Region(1).P(4));

  case 'box'
   if(length(Region(1).P)~=4)
    error('box must have 3 parameters')
   end
   centre=Region(1).P(1:2);
   width=Region(1).P(3);
   height=Region(1).P(4);
   InRegion=find(  x>=(centre(1)-width/2) & x<(centre(1)+width/2) ...
                 & y>=(centre(2)-height/2) & y<(centre(2)+height/2) );

  case 'rotbox'
   if(length(Region(1).P)~=5)
    error('rotbox must have 5 parameters')
   end
   centre=Region(1).P(1:2);
   width =Region(1).P(3);
   height=Region(1).P(4);
   theta =Region(1).P(5);
   % Rotate all the events around the centre theta degrees
   r=rot3d([x,y],theta/180*pi,3,centre);
   InRegion=find(  r(:,1)>=(centre(1)-width/2)  & r(:,1)<(centre(1)+width/2) ...
                 & r(:,2)>=(centre(2)-height/2) & r(:,2)<(centre(2)+height/2) );
   
  case 'ellipse'
   if(length(Region(1).P)~=5)
    error('ellipse must have 3 parameters')
   end
   centre=Region(1).P(1:2);
   a     =Region(1).P(3);
   b     =Region(1).P(4);
   theta =Region(1).P(5);
   % Rotate all the events around the centre theta degrees
   r=rot3d([x,y],theta/180*pi,3,centre);
   % Find the distance from the centre of the region to each event
   dist=sqrt( ((r(:,1)-centre(1))/a).^2 + ((r(:,2)-centre(2))/b).^2 );
   InRegion=find(dist<=1.0);

  otherwise
   error('Unsupported region')
 end %of switch, case

 x_new=x(InRegion);
 y_new=y(InRegion);
 index=index(InRegion);

else % The first region is subtractive
 switch Region(1).Shape

  case 'circle'
   if(length(Region(1).P)~=3)
    error('circle must have 3 parameters')
   end
   % Find the distance from the centre of the region to each event
   dist=sqrt( (x-Region(1).P(1)).^2 + (y-Region(1).P(2)).^2 );
   InRegion=find(dist>Region(1).P(3));

  case 'annulus'
   if(length(Region(1).P)~=4)
    error('annulus must have 4 parameters')
   end
   % Find the distance from the centre of the region to each event
   dist=sqrt( (x-Region(1).P(1)).^2 + (y-Region(1).P(2)).^2 );
   InRegion=find(dist<Region(1).P(3) | dist>Region(1).P(4));

  case 'box'
   if(length(Region(1).P)~=4)
    error('box must have 3 parameters')
   end
   centre=Region(1).P(1:2);
   width=Region(1).P(3);
   height=Region(1).P(4);
   InRegion=find(  x<(centre(1)-width/2) | x>=(centre(1)+width/2) ...
                 | y<(centre(2)-height/2) | y>=(centre(2)+height/2) );

  case 'rotbox'
   if(length(Region(1).P)~=5)
    error('rotbox must have 5 parameters')
   end
   centre=Region(1).P(1:2);
   width =Region(1).P(3);
   height=Region(1).P(4);
   theta =Region(1).P(5);
   % Rotate all the events around the centre theta degrees
   r=rot3d([x,y,0*x],theta/180*pi,3,[centre,0]);
   InRegion=find(  r(:,1)<(centre(1)-width/2)  | r(:,1)>=(centre(1)+width/2) ...
                 | r(:,2)<(centre(2)-height/2) | r(:,2)>=(centre(2)+height/2) );
   
  case 'ellipse'
   if(length(Region(1).P)~=5)
    error('circle must have 3 parameters')
   end
   centre=Region(1).P(1:2);
   a     =Region(1).P(3);
   b     =Region(1).P(4);
   theta =Region(1).P(5);
   % Rotate all the events around the centre theta degrees
   r=rot3d([x,y],theta/180*pi,3,centre);
   % Find the distance from the centre of the region to each event
   dist=sqrt( ((r(:,1)-centre(1))/a).^2 + ((r(:,2)-centre(2))/b).^2 );
   InRegion=find(dist>1.0);

  otherwise
   error('Unsupported region')

 end % of switch, case
 x_new=x(InRegion);
 y_new=y(InRegion);
 index=index(InRegion);

end % If first region additive, else, end

for i=2:N_Regions
 switch Region(i).Shape

  case 'circle'
   if(length(Region(i).P)~=3)
    error('circle must have 3 parameters')
   end
   % Find the distance from the centre of the region to each event
   dist=sqrt( (x_new-Region(i).P(1)).^2 + (y_new-Region(i).P(2)).^2 );
   if(Region(i).Sign>0) % Additive
    InRegion=find(dist<=Region(i).P(3));
   else % Subtractive
    InRegion=find(dist>Region(i).P(3));
   end

  case 'annulus'
   if(length(Region(i).P)~=4)
    error('annulus must have 4 parameters')
   end
   % Find the distance from the centre of the region to each event
   dist=sqrt( (x_new-Region(i).P(1)).^2 + (y_new-Region(i).P(2)).^2 );
   if(Region(i).Sign>0) % Additive
    InRegion=find(dist>Region(i).P(3) & dist<Region(i).P(4));
   else % Subtractive
    InRegion=find(dist<Region(i).P(3) | dist>Region(i).P(4));
   end

  case 'box'
   if(length(Region(i).P)~=4)
    error('box must have 3 parameters')
   end
   centre=Region(i).P(1:2);
   width=Region(i).P(3);
   height=Region(i).P(4);
   if(Region(i).Sign>0) % Additive
    InRegion=find(  x_new>=(centre(1)-width/2) & x_new<(centre(1)+width/2) ...
                  & y_new>=(centre(2)-height/2) & y_new<(centre(2)+height/2) );
   else % Subtractive
    InRegion=find(  x_new<(centre(1)-width/2) | x_new>=(centre(1)+width/2) ...
                  | y_new<(centre(2)-height/2) | y_new>=(centre(2)+height/2) );
   end

  case 'rotbox'
   if(length(Region(i).P)~=5)
    error('rotbox must have 5 parameters')
   end
   centre=Region(i).P(1:2);
   width =Region(i).P(3);
   height=Region(i).P(4);
   theta =Region(i).P(5);
   % Rotate all the events around the centre theta degrees
   r=rot3d([x_new,y_new],theta/180*pi,3,centre);
   if(Region(i).Sign>0) % Additive
    InRegion=find(  r(:,1)>=(centre(1)-width/2)  & r(:,1)<(centre(1)+width/2) ...
                  & r(:,2)>=(centre(2)-height/2) & r(:,2)<(centre(2)+height/2) );
   else % Subtractive
    InRegion=find(  r(:,1)<(centre(1)-width/2)  | r(:,1)>=(centre(1)+width/2) ...
                  | r(:,2)<(centre(2)-height/2) | r(:,2)>=(centre(2)+height/2) );
   end

  case 'ellipse'
   if(length(Region(1).P)~=5)
    error('circle must have 3 parameters')
   end
   centre=Region(i).P(1:2);
   a     =Region(i).P(3);
   b     =Region(i).P(4);
   theta =Region(i).P(5);
   % Rotate all the events around the centre theta degrees
   r=rot3d([x_new,y_new],theta/180*pi,3,centre);
   % Find the distance from the centre of the region to each event
   dist=sqrt( ((r(:,1)-centre(1))/a).^2 + ((r(:,2)-centre(2))/b).^2 );
   if(Region(i).Sign>0) % Additive
    InRegion=find(dist<=1.0);
   else % Subtractive
    InRegion=find(dist>1.0);
   end

  otherwise
   error('Unsupported region')
 end % end shape switch

 x_new=x_new(InRegion);
 y_new=y_new(InRegion);
 index=index(InRegion);

end % loop through regions 2:end
