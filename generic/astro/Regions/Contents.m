% astro/Regions: Region file manipulation, as used by Ciao and DS9
%
% Note: the format I use is very basic, akin to the Ciao format.
%       There is lots of room for extension to other formats and
%       addition of extra region objects.
%	Extra information in the region file such as colour is
%	presently ignored.
%
% I/O:
%  ParseRegionFile	Read in a region file.
%  WriteRegionFile	Write a region structure to a region file.
%  WriteRegionFile_SAS	Write a region structure to a region file. (SAS-compatible)
%
% Filtering
%  FilterByRegion	Filter (x,y) data using regions.
%  MaskFromRegion	Create an image mask which has 1 in included regions
%			and zero everywhere else.
%
% Visualisation
%  PlotRegion		Given a set of regions, plot them on the current figure.
