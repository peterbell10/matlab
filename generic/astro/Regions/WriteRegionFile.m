function WriteRegionFile(Region,regionfilename)
% WriteRegionFile: Write a SAOimage-compatible region file, given a region
%		   structure.
%
% WriteRegionFile(Region,regionfilename)
%
% ARGUMENTS
%  Region		Structure containing the regions
%  regionfilename	Name of the file to write
%
% RETURNS
%  Nothing
%
% SEE ALSO
%  FilterByRegion MaskFromRegion ParseRegionFile PlotRegion WriteRegionFile_SAS

% AUTHOR: Eric Tittley
%
% HISTORY
%  03 03 11 Mature version
%  07 12 22 Regularised comments
%
% TODO
%  Merge with WriteRegionFile_SAS
%
% COMPATIBILITY: Matlab, Octave

fid=fopen(regionfilename,'w');
if(fid<0)
 error('Unable to open region file')
end

fprintf(fid,'# Region file format: CIAO version 1.0\n');
for i=1:length(Region)
 if(Region(i).Sign<1)
  Sign='-';
 else
  Sign='+';
 end
 P_str=[];
 for j=1:length(Region(i).P)
  P_str=[P_str,num2str(Region(i).P(j)),','];
 end
 P_str=P_str(1:end-1);
 fprintf(fid,'%c%s(%s)\n',Sign,Region(i).Shape,P_str);
end % loop over Regions

fclose(fid);
