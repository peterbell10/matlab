function [new_wave,spec_out]=interpolate(wave,spec,spacing)
n_orig=length(wave);
#n_final=ceil(n_orig/spacing);
new_wave=[wave(1):spacing:wave(n_orig)];
n_final=length(new_wave);
spec_out=0*new_wave;
section_out_length=100;
section_orig_length=ceil(section_out_length*spacing);
nloops=floor(n_final/section_out_length)-2;

#Do the first segment
[p,yout]=spline(wave(1:section_orig_length+3),spec(1:section_orig_length+3),new_wave(1:section_out_length));
spec_out(1:section_out_length)=yout';
#Do the body
for k=1:nloops
 [p,yout]=spline(wave(section_orig_length*k-3:section_orig_length*(k+1)+3),spec(section_orig_length*k-3:section_orig_length*(k+1)+3),new_wave(section_out_length*k+1:section_out_length*(k+1)));
 spec_out(section_out_length*k+1:section_out_length*(k+1))=yout';
endfor
#Do the Tail
tail=[(nloops+1)*section_out_length:n_final];
[p,yout]=spline(wave(n_orig-section_orig_length-3:n_orig),spec(n_orig-section_orig_length-3:n_orig),new_wave(tail));
spec_out(tail)=yout';
