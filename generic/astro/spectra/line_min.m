function [centre,min]=line_min(wave,spec,line,interact_flag)
 if nargin<3 error('Too few parameters passed to line_min'), end
 if nargin<4 interact_flag=0; end
 if nargin>4 error('Too many parameters passed to line_min'), end
 wave=wave(:);
 spec=spec(:);
 cont=1;
 contnew=1;
 n=length(line);
 for k=1:n
  if (interact_flag)
   wave=[[-49:0],wave,[length(wave)+1:length(wave)+50]];
   spec=[0*[-49:0],spec,0*[length(spec)+1:length(spec)+50]];
   line=line+50;
   unity=1&[1:101];
   contnew=1.1;
   while((contnew~=1)&(contnew))
    plot(wave(line(k)-50:line(k)+50),[spec(line(k)-50:line(k)+50)/cont;unity])
    contnew=input('The new continuum value (1 to continue, 0 to skip line, -1 to exit w/o save)?');
    if contnew==-1 error('User break fron line_min'), end
    if contnew cont=cont*contnew; end
   end
  else cont=1;
  end
  if contnew
   pout=polyfit(wave(line(k)-1:line(k)+1),spec(line(k)-1:line(k)+1)/cont,2);
   centre(k)=-pout(2)/(2*pout(1));
   min(k)=pout(3)+pout(2)*centre(k)+pout(1)*centre(k)*centre(k);
  end
 end
