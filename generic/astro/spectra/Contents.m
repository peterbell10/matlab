% Functions to deal with spectral data
%
% bisect	Finds the bisector (centroid) of a spectral line.
%
% contfind	Fit a continnum to a spectrum.
%
% elginread	Read data produced by the Elginfield Observatory Reticon.
%
% line_min	Find the precise minimum of a line.
%
% linefind	Finds lines in a spectrum.
%
% mask2		Creates a mask for a spectrum to isolate regions of
%		continuum.
%
% opt_filt	Cleans a regularly-spaced data series of high frequency
%		noise by low-pass filtering the Fourier transform of the data.
%
% reticon_error	The error for the reticon at the Elginfield Observatory.
%
% spectrum.dat	A sample spectrum.
% 
% interpolate	Interpolates a spectrum onto a new set of wavelengths
% 
% nlf		???
%
% f1		Function used by nlf 
%
% tittley	Returns the Tittley values for a bisector.

