function linelist=linefind(wave,spec,level)
% Find spectral lines in a set of data.
% Actually, this function finds and returns the positions of local
% minima.  It is not a very complicated routine.
%
%Syntax:
%LINELIST=LINEFIND(WAVE,SPEC,LEVEL)
%Returns the positions of local minima in the vector SPEC that are found below a
%certain level, LEVEL.  WAVE is the wavelength vector for the spectrum.
%
% Eric Tittley (95 07) etittley@phobos.astro.uwo.ca

 if nargin~=3 error('Incorrect number of parameters passed to LINEFIND.'), end
 n=length(spec);
 temp=(spec(2:n-1)<level).*(spec(2:n-1)<spec(1:n-2)).*(spec(2:n-1)<spec(3:n));
 linelist=find(temp)+wave(1);
