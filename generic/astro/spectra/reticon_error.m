function pixel_error=reticon_error(di,intensity)
if(nargin~=2) error('Illegal number of parameters in call to reticon_error'), end
if(intensity<0) error('Parameter intensity less than zero in call to reticon_error'), end
if(intensity==0) intensity=0.00001; end
Nadu=di/4*intensity;
r2=1.3456;
pixel_error=sqrt(Nadu/520+r2)*intensity/Nadu;
