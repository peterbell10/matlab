function Hdr = ParseSBigHeader(header)
% ParseSBigHeader: Turns an SBIG header from Sinto a structure.
%
% Hdr = ParseSBigHeader(header)
%
% ARGUMENTS
%  header	A character string containing the header of an SBIG file
%
% RETURNS
%  The contents of the header in a structure with the format
%	Hdr.field_name=field_value
%
% NOTES
%  Usually called from SBIGread
%
% SEE ALSO
%  SBIGread

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 09 07 First version
%  07 12 22 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

% The header is 2048 bytes long.
% The first row of text is the camera information
% The last row of text is "END", followed by ASCII[10][13][26], then all zeros
% The middle rows of text are of the sort: Field = Value followed by ASCII[10][13]

%First, clean out all the null characters
header=header(header~=0);

%Find the carriage returns
CRs=find(header==10);
line_start=1;

Hdr.format = header(line_start:CRs(1)-1);
for i=2:length(CRs)-1
 row=header(CRs(i-1)+2:CRs(i)-1);
 separator = find(row==61); % the separator is '='
 field_name = row(1:separator-2);
 field_value = row(separator+2:end);
 if(sum(  field_value==43 | field_value==45 | field_value==46 ...
        | (field_value>=48 & field_value<=57 ) ))
  % then it is completely numeric
  field_value = sscanf(field_value,'%f',1);
% else
  % it must be kept at a string
 end
 eval(['Hdr.',field_name,'=field_value;']);
end
