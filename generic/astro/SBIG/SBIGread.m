function [Header,image]=SBIGread(filename,OldVersionFlag)
%SBIGread      Read in SBIG format CCD data files
%
%[Header,image]=SBIGread(filename,OldVersionFlag)
%
% ARGUMENTS
%  filename	File to read
%  OldVersionFlag [optional] 1 => ST-4 version of file. Prior to 2002
%
% RETURNS
%  Header	The image header
%  image	The image
%
% REQUIRES
%  ParseSBigHeader
%
% SEE ALSO
%  ParseSBigHeader

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 08 17 First version
%	based on a version written years ago Essentially nothing remains
%	except the name.
%  00 09 07
%	Corrected the header rem statements
%	Added the functionality of ParseSBigHeader
%  07 12 22 Regularised comments
%  09 07 21 Merged with older code and added the OldVersionFlag option
%
% COMPATIBILITY: Matlab, Octave

% Much of the documentation within this routing comes from the documents:
% "CCD Camera Operating Manual for the Model ST-7 and ST-8" and "ST-4 PC File
% Format" by the Santa Barabara Instrument Group (www.sbig.com)

if(nargin<2)
 OldVersionFlag=0;
end

fid=fopen(filename,'rb');

if(OldVersionFlag)
 image=fread(fid,[192,165],'int8');
 image=image';
 dummy =fread(fid,1,'uchar'); % Should just be a 'v'
 if(dummy ~= 118)
  disp('Warning: Unexpected character in header');
 end
 % After the 31680 bytes of image data, the telescope parameters are stored in
 % a "166th" line of 192 bytes, making the total file size be 31872 bytes. The
 % data on this 166th line is written in text using ASCII characters. ASCII was
 % chosen so that it could be easily interpreted using most programming
 % languages. The first byte of the 166th line is a lowercase 'v' character
 % (118 in decimal, which signifies the total length of ASCII data on the 166th
 % line). Bytes 2 through 79 (78 bytes total) are the file's annotation. The
 % annotation is left justified and padded with spaces on the right to fill out
 % the 78 character field width. The next 10 bytes (80 through 89) are the
 % exposure or integration time in 100 ths of a second. This field and the next
 % three fields are right justified, padded with spaces as required on the left
 % to fill out the 10 character field widths. The next 10 bytes (90 through 99)
 % are the focal length in inches. Bytes 100 through 109 are the aperture area
 % in square inches, and finally bytes 110 through 119 are the image
 % calibration factor. The remaining bytes (120 through 192) on the 166th line
 % are reserved for future use.
 Header.Annotation=char(fread(fid,78,'uchar')');
 Header.Exposure      = sscanf(char(fread(fid,10,'uchar')'),'%i',1)/100; % s
 Header.FocalLength   = sscanf(char(fread(fid,10,'uchar')'),'%f',1); % inch
 Header.AperatureArea = sscanf(char(fread(fid,10,'uchar')'),'%f',1); % inch^2
 Header.ImageCalibrationFactor ...
                      = sscanf(char(fread(fid,10,'uchar')'),'%f',1); % inch^2
else
 % There is a 2048 byte header
 Header=char(fread(fid,[1,2048],'int8'));

 Hdr=ParseSBigHeader(Header);

 nrows=Hdr.Height;
 ncols=Hdr.Width;

 for row=1:nrows
 %Each line consists of a two-byte integer (least significant byte first)
 %indicating the compressed length of the line in bytes
  bytes_in_line = fread(fid,1,'int16');

 % The row is stored uncompressed if its comressed length is greater than its
 % uncompressed length
  if( bytes_in_line/2 == ncols)
   image(row,:)=fread(fid,[1,ncols],'uint16');
  else
   n=0;
   % The 1st pixel of tha actual image data is written using 2 bytes and is
   % the actual pixel value.
   image(row,1)=fread(fid,1,'uint16');
   n=n+2;
   col=1;
   while (n<bytes_in_line)
    col=col+1;
    % Subsequent pixels are written as follows depending on the difference
    % between that pixel and the previous pixel (delta = pixel(m,n)-pixel(m,n-1)):
    % If -127<=delta<=127 then write delta as a single byte in 2's complement
    % format, otherwise write hex 80 (-128) followed by the actual pixel value
    % using two-bytes (least significant byte first).
    byte = fread(fid,1,'int8');
    n=n+1;
    if(byte>-128)
     image(row,col)=image(row,col-1)+byte;
    else
     image(row,col)=fread(fid,1,'uint16');
     n=n+2;
    end
   end % bytes_in_line
  end % compressed line
 end % rows

 % All images have a bias of 100 counts added to them to stop underflow due to
 % noise.  If you need to interpret pixel values or flat field correct an image
 % with your software the pixel value that is due to light is:
 %	Actual Counts = Image COunts - 100 + Pedestal
 % where Image Countes is the value read from the file and Actual Counts is the
 % light response.
 image=image-100+Hdr.Pedestal;

end % if OldVersionFlag else ...

fclose(fid);
