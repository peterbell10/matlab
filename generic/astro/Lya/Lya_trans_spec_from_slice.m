% Lya_trans_spec_from_slice: Generate a Lya spectrum from a simulation slice.
%
% [Trans,Amp,Mask,Skewer]=Lya_trans_spec_from_slice(D,z,dz,phi,Ystart,kms,tau_eff,Amp)
%
% ARGUMENTS
%  D       Slice structure:
%           .R     [m] Distance from source (N vector)
%           .n_H   [m^-3] Numberdensity (NxM array)
%           .n_He  [m^-3]
%           .f_H1
%           .f_He1
%           .T     [K]
%           .v_x   [km/s]
%           .v_z   [km/s]
%  z       Redshift of the data
%  dz      The span over redshift that we will examine
%  phi     The angle of the skewer to draw
%           phi=0    => Along a LOS, moving away from the source.
%           phi=pi/2 => Perpendicular to direction of source rays, moving to
%                       increasing LOS.
%           phi = atan(1./NR) => Wrap row by row
%  Ystart  The y-position to start the skewer.
%  kms     The velocities at which to calculate the transmission
% OPTIONAL ARGUMENTS
%
%  tau_eff An effective mean optical depth to normalize to. Ignored if <= 0.
%
%  Amp     The nomalization amplitude to use.
%       OR The guess at the normalization amplitude required to achieve tau_eff.
%          [default 1]
%        
%        If tau >0 then used as a first guess on the amplitude, then modified
%                  to be the normalization required to achieve tau_eff.
%        If tau <0 then the normalization to use. Left unmodified.
%
% RETURNS
%  Trans  The transmission fraction. Same dimensions as kms.
%  Amp    The normalization amplitude used to achieve tau_eff.
%  Mask   A mask indicating spectral bins that should be ignored in the analysis.
%  Skewer The skewer data from which the spectrum was generated.
%
% USAGE
%  Suppose you want to generate a transmission spectrum with a mean optical
%  depth of tau_eff for one slice of lines of sight and then use the same
%  amplitude normalization for subsequent calls.
%  Call with tau_eff set to the desired value and Amp set to 1 (as a guess).
%  That returns Amp, which can be used in subsequent calls, with tau_eff = -1
%   tau_eff = 0.3;
%   Amp     = 1;
%   [Trans,Amp,Mask,Skewer]=Lya_trans_spec_from_slice(D1,z,dz,phi,Ystart,kms,tau_eff,Amp);
%   tau_eff = -1;
%   [Trans,Amp,Mask,Skewer]=Lya_trans_spec_from_slice(D2,z,dz,phi,Ystart,kms,tau_eff,Amp);
%   
% REQUIRES
%  fake_Lya_spec
%  draw_skewer
%
% SEE ALSO
%  fake_Lya_spec draw_skewer

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Octave & Matlab
%
% HISTORY
%  080827 First version
%  110816 draw_skewer no longer requires the ix argument
%  130417 Make tau_eff an optional input
%  130417 Make Amp an optional input
%  130417 Return Amp if tau_eff is set
%
% TODO
%  Cosmological parameters should be input Omega_v, Omega_m, Ho

function [Trans,Amp,Mask,Skewer] = ...
         Lya_trans_spec_from_slice(D,z,dz,phi,Ystart,kms,tau_eff,Amp)

% ARGUMENT CHECKS
if( (nargin < 6) || (nargin > 8) )
 disp('syntax: [Trans,Mask,Skewer]=Lya_trans_spec_from_slice(D,z,dz,phi,Ystart,kms,[tau_eff],[Amp])')
 error('Insufficient number of arguments')
end

if(nargin <= 6)
 tau_eff = -1;
end
if(nargin <= 7)
 Amp     = 1;
end

if( nargin>6 )
 if( tau_eff <= 0 )
  tau_eff_flag = 0;
 else
  tau_eff_flag = 1;
 end
end

if( nargin==8 )
 if( Amp <= 0 )
  Amp_flag = 0;
 else
  Amp_flag = 1;
 end
end

if( (tau_eff>0) && (Amp<=0) )
 error('If tau_eff is set, then Amp must be set to the initial guess.')
end

% Various constants which should be input.
lambda_lya_HI   = 1215.67; % A
Omega_v = 0.764;
Omega_m = 0.236;
Ho = 73.0; %km s^-1 Mpc^-1
H = Ho * sqrt(Omega_m*(1. + z)^3 + Omega_v); % km s^-1 Mpc^-1

% The dimensions of the data
[NLos,NR]  = size(D.T);

% Default Mask
ShadowMask=0*D.f_H1;

% Find the shadowwed regions
%ShadowMask=(D.f_He1 < 0.1) & (D.f_H1 > 0.5);
%ShadowMask=(D.f_H1 > 0.1);

% Also mask the edges
ShadowMask(1 ,:   )=1;
ShadowMask(: ,1   )=1;
ShadowMask(NR,:   )=1;
ShadowMask(: ,NLos)=1;

hdr.h100=Ho/100;
SI='MKS';
Units

% ============== The skewer =============

% smax is the length of the skewer in Mpc
smax = dz*c/ (1.e3*H*(1. + z)); % Mpc
smax = smax*Mpc; % m

ibound = [1, NR];
jbound = [1, NLos];

%% The angle of the LOS%
sphi = sin(phi);
cphi = cos(phi);

% dRx and dRy are the x and y grid spacings.
% x is along a LOS, y separates different LOSs
dRx=D.R(2)-D.R(1,1); % m
Rmax = D.R(end)-D.R(1)+dRx; % m
dRy = Rmax/NLos; % m

% The starting point of the skewer
x = 0;          %m
y = Ystart*dRy; %m

%draw skewer SK
SK=draw_skewer(ibound, jbound, dRx, dRy, x, y, phi, smax);

%plot(SK.is,SK.js,'.')

% Along the LOS
Rmax = SK.s(end); % m

R     = (SK.s - SK.s(1)); % m
v_pec = zeros(SK.nel,1);
nH1   = zeros(SK.nel,1);
T     = zeros(SK.nel,1);
% These are only for outputting to the skewer file
Skewer.fH1      = zeros(SK.nel,1);
Skewer.mask_in  = zeros(SK.nel,1);
Skewer.R        = R(1:SK.nel);

% Ensure everything is a column vector
R=R(:);

dR = R(2:end) - R(1:end-1); % m
dR = [dR; dR(1)]; % Append an interval (m)

Skewer.dR       = dR(1:SK.nel);

% The following could be vectorised, but as yet is not a bottleneck
for nseg=1:SK.nel
 i = SK.is(nseg);
 j = SK.js(nseg);
 v_pec(nseg) = D.v_z(j,i)*cphi + D.v_x(j,i)*sphi; %km/s
 n(nseg)     = D.f_H1(j,i) * D.n_H(j,i) * dR(nseg); % m^-2
 T(nseg)     = D.T(j,i); % K
 % These are only for outputting to the skewer file
 Skewer.fH1(nseg)   = D.f_H1(j,i);
 Skewer.mask_in(nseg)  = ShadowMask(i,j);
end

v_pec=v_pec(:);
n=n(:);
T=T(:);

R = R/Mpc; % Mpc

% Add the Hubble flow component to the radial velocities.
v = v_pec + H*R; % km/s
%v = -v; % km/s  Stuff moving away from the source is moving toward us.

psi_shape=0; % = Voigt
species=0; % = HI

% Amp gets modified in the following call, if tau_eff > 0.
[Trans,Mask]=fake_Lya_spec(v, n, T, kms, psi_shape, species, tau_eff, Amp, Skewer.mask_in);


% NEED TO RETURN Skewer
Skewer.v=v;
Skewer.n=n;
Skewer.T=T;

