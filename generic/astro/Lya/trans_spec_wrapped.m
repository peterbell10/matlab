% trans_spec_wrapped: Generate a wrapped transmission spectrum from RT data
% 
% ARGUMENTS
%  INPUT, not modified
%   D        The RT data for the slice
%            Required are the elements:
%             R
%             v_z
%             v_x
%             f_H1
%             n_H
%             f_He2
%             n_He
%             T
%   LOS,Cell The ray starts at (LOS,Cell) in the slice.
%   zred     Redshift of the source
%   dz       The span over redshift that we will examine
%   delta_H  The factor by which to multiply f_ * n_* for Hydrogen
%   delta_He The factor by which to multiply f_ * n_* for Helium
%   kms      The range over which to calculate absorption
%   kms_for_tauEff The range over which to calculate the tau_eff.
%            Ignored if tau_eff < 0
%   tau_eff  [2-vector] Mean optical depth for HI and HeII
%            Ignored if tau_eff < 0
%   Cosmology Structure containing cosmological paramenters:
%            Omega_v
%            Omega_m
%            h100
%   phi      The angle of the LOS (radians)
%   Amp      (Optional) Amplitude used to:
%             if tau_eff < 0: scale the densities [AmpHI AmpHeII]
%             else:           provide a guess to set to tau_eff
%            Default: [1 1]
%
%  OUTPUT
%   flux_HI   The transmittance fraction at each velocity in kms
%             due to HI absorption
%   flux_HeII The transmittance fraction at each velocity in kms
%             due to HeII absorption
%   Skewer    (Optional) The Skewer passed to fake_Lya_spec
%   Amp       (Optional) Amplitude used to set tau to tau_eff [AmpHI AmpHeII]
%
% REQUIRES
%  draw_skewer

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

function [flux_HI, flux_HeII, Skewer, Amp] = ...
         trans_spec_wrapped(D, LOS, Cell, zred, dz, delta_H, delta_He, ...
                            kms, kms_for_tauEff, ...
                            tau_eff, Cosmology, phi, Amp)

% Crude argument check
if( (nargin<12) | (nargin>13) )
 error('syntax: [flux_HI, flux_HeII, Skewer, Amp] = trans_spec_wrapped(D, LOS, Cell, zred, dz, delta_H, delta_He, kms, kms_for_tauEff, tau_eff, Cosmology, phi, [Amp])')
end

if(nargin==12)
 % Guess at the normalisation amplitude required to achieve tau_eff.
 AmpHI   = 1;
 AmpHeII = 1;
else
 % Take from the input argument
 AmpHI   = Amp(1);
 AmpHeII = Amp(2);
end

% Get some physical constants
hdr.h100=Cosmology.h100;
SI='MKS';
Units

% The dimensions of the data
[NLos,NR]  = size([D.R]);

% Cosmology.
Cosmology.Ho = 100*Cosmology.h100; %km s^-1 Mpc^-1
H = Hubble(zred,Cosmology); % km s^-1 Mpc^-1
disp(['H=',num2str(H),' km s^-1 Mpc^-1'])

% smax is the length of the skewer in Mpc
smax = dz*c/ (1.e3*H*(1. + zred)); % Mpc
disp(['smax = ', num2str(smax), 'Mpc'])
smax = smax*Mpc; % m

% The index bounds of the box.
ibound = [1, NR];
jbound = [1, NLos];

sphi = sin(phi);
cphi = cos(phi);

% Lx and Ly are the x and y grid spacings.  I might have called them
% dRx, dRy.  x is along a LOS, y separates different LOSs
Lx=D(1).R(2)-D(1).R(1);
Rmax = D(1).R(NR)-D(1).R(1)+Lx;
Ly = Rmax/NLos;
disp(['Lx = ',num2str(Lx/Mpc),' Mpc; Ly = ',num2str(Ly/Mpc),' Mpc'])

% The starting point of the ray.  For my purposes this needs
% to be anywhere in the slice (LOS, Cell), but it seems it
% needs to be on the x or y axis (i or j == 0).
% Shift the order of the LOSs such that the LOS we want is at D(1)
D = shift(D,1-LOS,2);

x = (Cell-1)*Lx;
y = 0.;

%draw skewer Skewer
Skewer=draw_skewer(ibound, jbound, Lx, Ly, x, y, phi, smax);

% Along the LOS
Rmax = Skewer.s(Skewer.nel);

% Initialise arrays
R_los = (Skewer.s - Skewer.s(1))/Mpc;
v_los = zeros(1,Skewer.nel);
v     = zeros(1,Skewer.nel);
nH1   = zeros(1,Skewer.nel); % column density m^-2
nHe2  = zeros(1,Skewer.nel); % column density m^-2
T     = zeros(1,Skewer.nel);
nH    = zeros(1,Skewer.nel); % number density m^-3

% Generate the skewer data from the RT data for the slice
for nseg=1:Skewer.nel
 i = Skewer.is(nseg); % Cell number within the LOS
 j = Skewer.js(nseg); % Cell LOS
 v_los(nseg) = D(j).v_z(i)*cphi + D(j).v_x(i)*sphi; %km/s
 nH1(nseg)   = D(j).f_H1(i) *D(j).n_H(i) *Skewer.ds(nseg)*delta_H;  % m^-2
 nHe2(nseg)  = D(j).f_He2(i)*D(j).n_He(i)*Skewer.ds(nseg)*delta_He; % m^-2
 T(nseg)     = D(j).T(i); % K
 nH(nseg)    = D(j).n_H(i); % m^-3 (only used for output)
end
disp(['Skewer has ',num2str(Skewer.nel),' segments'])
disp(['Flux will have ',num2str(length(kms)),' bins'])

% Add the peculiar velocities to the Hubble flow, and put into recession.
v = v_los + H*R_los; % km/s
v = -v;

%%% Compute the observed fluxes at the velocities given in kms

% Effective mean optical depths to normalise to.
tau_eff_HI  =tau_eff(1);
tau_eff_HeII=tau_eff(2);

% Enumerations.
ProfileDoppler=0;
ProfileVoigt=1;
SpeciesHI=0;
SpeciesHeII=2;

flux_HI  =fake_Lya_spec(v, nH1,  T, kms_for_tauEff, ProfileVoigt, SpeciesHI, ...
                        tau_eff_HI, AmpHI);
disp(['For HI: Amp=',num2str(AmpHI)])
flux_HI  =fake_Lya_spec(v, AmpHI*nH1,  T, kms, ProfileVoigt, SpeciesHI, ...
                        -1, AmpHI);

flux_HeII=fake_Lya_spec(v, nHe2, T, kms_for_tauEff, ProfileDoppler, SpeciesHeII, ...
                        tau_eff_HeII, AmpHeII);
disp(['For HeII: Amp=',num2str(AmpHeII)])
flux_HeII=fake_Lya_spec(v, AmpHeII*nHe2, T, kms, ProfileDoppler, SpeciesHeII, ...
                        -1, AmpHeII);

if(nargout>2)
 Skewer.v    =v;
 Skewer.nH1  =nH1;  % column density m^-2
 Skewer.nHe2 =nHe2; % column density m^-2
 Skewer.T    =T;    % K
 Skewer.nH   =nH;   % number density m^-3
end

if(nargout>3)
 Amp=[AmpHI AmpHeII];
end
