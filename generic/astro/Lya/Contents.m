% astro/Lya: Tools for generating fake Lya forest transmission spectra
%
% draw_skewer: Create a skewer of connected cells along input angle phi
%
% draw_skewer_orig: Create a skewer of connected cells along input angle phi
%                   (deprecated)
%
% ExtractSkewer: Extract a skewer from a gridded dataset
%
% Lya_trans_spec_from_slice: Generate a Lya spectrum from a simulation slice.
% 
% SpectraWriteHDF5: Write transmission spectrum (or spectra) to an HDF5 file
%
% trans_spec_wrapped: Generate a wrapped transmission spectrum from RT data
%
% trans_spec_wrapped_Lyb: Generate a wrapped transmission spectrum from RT data
%       for Lybeta, using tau_eff for Lya as a basis.
