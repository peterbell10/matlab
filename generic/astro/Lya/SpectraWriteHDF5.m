% SpectraWriteHDF5: Write transmission spectrum (or spectra) to an HDF5 file
%
% SpectraWriteHDF5(File, Transmission, Velocity)
%
% ARGUMENTS
%  File          File to write the spectra to.
%  Transmission
%     .flux_HI   Transmission fraction from HI absorption
%     .flux_HeII Transmission fraction from HeII absorption
%  Velocity      Velocity of the transmission bins.
%
% RETURNS
%  Nothing
%
% SEE ALSO
%  fake_Lya_spec

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave

function SpectraWriteHDF5(File, Transmission, Velocity)

if(isoctave)
 flux_HI   = Transmission.flux_HI;
 flux_HeII = Transmission.flux_HeII;
 save('-hdf5',File,'Velocity','flux_HI','flux_HeII');
else
 % Assume matlab and use hdf5write()
 DetailsVelocity.Location  = '/';
 DetailsVelocity.Name      = 'Velocity';
 AttributeVelocity         = 'km/s';
 AttributeVelocity_details.Name = 'Units';
 AttributeVelocity_details.AttachedTo = '/Velocity';
 AttributeVelocity_details.AttachType = 'dataset';

 Detailsflux_HI.Location   = '/';
 Detailsflux_HI.Name       = 'flux_HI';
 Attributeflux_HI         = 'normalized';
 Attributeflux_HI_details.Name = 'Units';
 Attributeflux_HI_details.AttachedTo = '/flux_HI';
 Attributeflux_HI_details.AttachType = 'dataset';

 Detailsflux_HeII.Location = '/';
 Detailsflux_HeII.Name     = 'flux_HeII';
 Attributeflux_HeII         = 'normalized';
 Attributeflux_HeII_details.Name = 'Units';
 Attributeflux_HeII_details.AttachedTo = '/flux_HeII';
 Attributeflux_HeII_details.AttachType = 'dataset';

 hdf5write(File, DetailsVelocity, Velocity, ...
                 AttributeVelocity_details, AttributeVelocity, ...
                 Detailsflux_HI,  Transmission.flux_HI, ...
                 Attributeflux_HI_details, Attributeflux_HI, ...
                 Detailsflux_HeII,  Transmission.flux_HeII, ...
                 Attributeflux_HeII_details, Attributeflux_HeII);
end
