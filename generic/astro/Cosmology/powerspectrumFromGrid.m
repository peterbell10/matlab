function [PS,PSstd,k]=powerspectrumFromGrid(Grid)

Dims=size(Grid);
L = min(Dims);
if (L~=max(Dims))
 error('Grid must be cubic')
end

% The FFT
GridF=fft3(Grid);

% The Power
GridFP=abs(GridF);

GridFP=GridFP(1:L/2,1:L/2,1:L/2);

[X,Y,Z]=meshgrid([1:L/2],[1:L/2],[1:L/2]);

K=sqrt((X.^2+Y.^2+Z.^2));

bins=[0:L];
[PS,PSstd]=bindata_1d(K(:),GridFP(:),bins);
k=(bins(1:end-1)+bins(2:end))/2;
