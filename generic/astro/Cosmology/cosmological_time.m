% cosmological_time: Time (s) as a function of expansion factor
%
%  t = cosmological_time(a,H_0,omega_m)
%
% ARGUMENTS
%  a	Expansion factor
%  H_0	Hubble's constant at the present [s^-1]
%  omega_m	Matter density factor at the present
%
% RETURNS
%  The time since a=0 [s]
%
% NOTES
%  The equation reduces to  2/(3H_0) * a^1.5 for omega_m=1
%  I'm not sure this approximation is good for lambda != 0

% AUTHOR: Eric Tittley
%
% HISTORY
%  040819 Mature version
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

function t = cosmological_time(a,H_0,omega_m)

t = 2/(3*H_0*(1-omega_m)^0.5) * asinh( ((1-omega_m)/omega_m)^0.5 * a.^1.5 );
