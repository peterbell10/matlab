function H = Hubble(z,Cosmology)
% Hubble:  Hubble parameter as a function of redshift
%
% H = Hubble(z,Cosmology)
%
% ARGUMENTS
%  z            Redshift
%  Cosmology    Parameters pertaining to cosmology
%               Requires:
%                Ho  Hubble Constant now
%                Omega_m  Fraction of critical density in matter (dark + gas)
%                Omega_v  Fraction of critical density in vacuum energy
%
% RETURNS
%  H            Hubble parameter at the redshift, z [unit same as Ho]
%               [Dimensions same as z]

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab & Octave
%
% HISTORY: 130904 First version

H = Cosmology.Ho*sqrt(Cosmology.Omega_m * (1+z).^3 + Cosmology.Omega_v);
