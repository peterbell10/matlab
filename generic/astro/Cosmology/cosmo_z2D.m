function [D,DA,DL] = cosmo_z2D(z,H,OMEGA,OMEGA_V)
% cosmo_z2D: Distances from redshift
%
% [D,DA,DL] = cosmo_z2D(z,H,OMEGA,OMEGA_V)
%
% ARGUMENTS
%  z is the redshift
%  H is the hubble constant
%  OMEGA is the Matter density of the universe (OMEGA_M + OMEGA_R)
%  OMEGA_V is the vacuum density of the universe
%
% RETURNS
%  D	The "now" distance to an object
%  DA	Angular size distance
%  DL	Luminosity Distance
%
% NOTES
%  OMEGA_R = 4.1844e-5*(100/H)^2 (T_2.73)^4, where T is assumed to be 2.73 K.
%
%  These calculations are taken from Ned Wright's very helpful website...
%  see http://www.astro.ucla.edu/~wright/Distances_details.gif
%  http://www.astro.ucla.edu/~wright/cosmo_02.htm
%
% SEE ALSO

% AUTHOR: Danny Hudson, Eric Tittley
%
% HISTORY
%  030723 4th Anniversary of Chandra
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

%%%%%%%%%%%%% C O N S T A N T S %%%%%%%%%
% Speed of light from X-ray Data Booklet%
%					%
        c = 2.99792458e5; % km/s	%
%					%
%					%
%  OMEGA_R from "Allen's Astrophysical	%
%	         Quantities 4th ed."	%
%					%
        OMEGA_R = 4.1844e-5*(100/H)^2;  %
%	          (T_2.73)^4 (units of	%
%			      2.73 K) 	%
%		       			%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%% V A R I A B L E   D E F I N I T I O N S %%%%%%%%%%%%%%
%									%
% 		  Omega_m is the matter density				%
%									%
		  OMEGA_M = OMEGA - OMEGA_R;				%
%									%
%									%
% 	         Omega_v = LAMBDA*c^2/(3H^2) 	 			%
%									%
%									%
% 		Omega_r0 is the radiation density 			%
%									%
%									%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%% F O R M U L A E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%										%
% 			      D = (cZ(z)/H)					%
%										%
% 		DA = cZ(z)J([1-OMEGA_TOT]Z^2)/(H (1+z)), where			%
%										%
% 		Z(z) = int(da/a/sqrt(X),a=1/(1+z)...1) and			%
%										%
% 	X(a) = Omega_m0/a + Omega_r0/a^2 + Omega_v0*a^2 + (1-OMEGA_TOT)		%
%										%
% 	J(x) = sin(sqrt(-x)/sqrt(-x)		     for x<0			%
%            = 1 + x/6 + x^2/120 + ...x^n/(2n+1)!    for x~0			%
%            = sinh(sqrt(x)/sqrt(x)		     for x>0			%
%										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%------------------------------- Calculate Z.-----------------------------------%
%                   The integral is the only really tricky part			%
%										%
%										%
%------------------- Calculate the integral using Maple-------------------------%
%										%
%										%
%	CREATE A COMMAND LINE FOR THE INTEGRAL + AN QUIT COMMAND		%
%										%
%										%
%			        FIND MAPLE					%
%										%
	[DMY,MPL] = unix('locate maple | grep /bin/maple');			%
%										%
% 		      CHECK TO SEE IF MAPLE WAS FOUND				%
%										%
%										%
%_______________________________________________________________________________%
%										%
%		   IF "NO" DO THE INTEGRATION WITH MATALB			%
%										%
%										%
	if DMY == 1								%
	 fprintf('\nCouldn''t find Maple, doing the integral ');		%
         fprintf('numerically with matlab.\n\n\n');				%
%										%
%                create an inline function for the integrand			%
%										%
	 INT = inline(['1./a./sqrt(',num2str(OMEGA_M),'./a + '...		%
                                    ,num2str(OMEGA_R),'./(a.^2) + '...		%
				    ,num2str(OMEGA_V),'.*a.^2 + '...		%
				    ,num2str(1-(OMEGA+OMEGA_V)),')']);		%
%										%    
% 		Make a vector that defines the step size for			%
%		      the numerical integration					%
%		(2e-5 precision is good enough for now)				%
%										%
	  A = [1/(1+z):(1-1/(z+1))/(5e4-1):1];					%
	  N=length(A);								%
%										%
%										%
% 		     Integrate using the trapezoidal rule			%
           Z=sum((1/2*(INT(A(1:N-1)) + INT(A(2:N)))).*(A(2:N)-A(1:N-1)));	%
%										%
%										%
%										%
	 else									%										%
%_______________________________________________________________________________%
%										%
%		  IF "YES" USE MAPLE TO DO THE INTEGRAL				%
%										%
%										%
	  PST = findstr(MPL,'bin/maple');					%
	  DRCTRY = MPL(1:PST(1)+3);						%
%										%
%										%
%			   EXECUTE THE COMMAND					%
%										%
          cmds = ['evalf(int(1/a/sqrt(',num2str(OMEGA_M),'/a + '...		%
	                               ,num2str(OMEGA_R),'/(a^2) + '...		%
				       ,num2str(OMEGA_V),'*a^2 + '...		%
				       ,num2str(1-(OMEGA+OMEGA_V)),'),a='...	%
				       ,num2str(1/(1+z)),'..1),20); quit;'];	%
          [DMY,TEMP_Z] =  unix(['echo "',cmds,'" | ',DRCTRY,'maple -q']);	%
%										%
%										%
%		   PARSE THE OUTPUT FOR THE NUMERIC PART			%
%		   it should end 2 positions before the				%
%			    the first CR		 			%
%										%
	  PRSZ = findstr(TEMP_Z,'[');						%
	  Z = str2num(TEMP_Z(1,1:PRSZ(1)-2));					%
         end									%
%										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ----------------------Calculate D  D_A  D_L-----------------------------------%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   D   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%										%
                               D = c*Z/H;					%
%										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   D_A   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%										%
%		Calculate J based on the value of 1-(OMEGA+OMEGA_V)		%
%										%
% 				Flat universe					%
% 				  J = 1						% 
         if 1-(OMEGA+OMEGA_V) == 0						%
                           DA = c*Z/H/(1+z);					%
%										%
%										%
%										%
%										%
%				Almost Flat Universe				%
%										%
% if |(1-(OMEGA+LAMBDA))|Z^2 							%
% is less than 0.1 								%
% (ie close to a flat universe)							%
% then calculate a series (20 terms is more than enough)			%
        elseif abs(1-(OMEGA+OMEGA_V)).*Z.^2 < .1				%
%										%
            		   DA = c.*Z./H./(1+z).*...				%
			        sum(((1-(OMEGA+OMEGA_V)).*Z.^2).^[0:20]./...	%
				      FACTORIAL(2.*[0:20]+1));			%
%										%
%										%
%										%
%										%
%										%
% 				 Closed Universe				%
%										%
	elseif 1-(OMEGA+LAMBDA) < 0						%
 			   DA = c*Z/H/(1+z)*...					%
			        sin(sqrt(-1*(1-(OMEGA+OMEGA_V))*Z^2))/...	%
				sqrt(-1*(1-(OMEGA+OMEGA_V))*Z^2);		%
%										%
%										%
%										%
%										%
% 				 Open Universe					%										%
%										%
	else									%
			    DA = c*Z/H/(1+z)*...				%
			        sinh(sqrt((1-(OMEGA+OMEGA_V))*Z^2))/...		%
				sqrt((1-(OMEGA+OMEGA_V))*Z^2);			%
	end									%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   D_L   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%										%
				DL = (1+z)^2*DA;				%
%										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%------------------------------------O U T P U T--------------------------------%
fprintf('\nFor H = %g and a redshift of %g,\nDistance is %g Mpc,',H,z,D)	%
fprintf(' angular size distance is %g Mpc, \n',DA);				%
fprintf('and luminosity distance is %g Mpc.\n\n',DL);				%
fprintf('This gives %g h_%g^-1 kpc arcmin^-1 on the sky.\n'...			%
                                                     ,DA*1000*(1/60*pi/180),H);	%
fprintf('Luminosity is flux*4*pi*(luminosity distance)^2.\n\n');		%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






%-------------------F A C T O R I A L    S U B R O U T I N E--------------------%
% 	     This a little function I wrote to calculate a factorial for	%
%	        a vector of values that does not involve a "for loop"		%
%										%
%										%
			   function F = FACTORIAL(N);				%
%										%
%										%
%										%
%			 Determine the maximum factorial			%
% 			  that needs to be computed.				%
%										%
	MN = max(N);								%
%										%
%										%
%										%
%										%
% 			Create a lower triangular matrix 			%
%			with vlues of 1	and size MN x MN			%
% 			There is also a built-in MATLAB routine 		%
%				"qr" to do this					%
%										%
%										%
	A = fliplr([1:MN]'*[1:MN])...						%
	  - flipud([1:MN]'*[1:MN]);						%
	A(A>=0) = 1;								%
	A(A<0) = 0;								%
%										%
%										%
%										%
% 			Make a matrix of columns with values 			%
%			   going from MN down to 1				%
%										%
	B = [MN:-1:1]'*ones(1,MN);						%
%										%
%										%
%										%
%										%
% 			      Make a Matrix of columns 				%
%		      [[MN:-1:1]',0;[MN-1:-1:1]',0;0;[MN-2:-1:1]',...		%
%										%
	C=A.*B;									%
%										%
%										%
%										%
% 			  Change all the zeros in C to ones			%
	C(C==0)=1;								%
%										%
%										%
%										%
% 			Flip it left to right and take 				%
%			the product of all the columns				%
	TF = fliplr(prod(C));							%
%										%
%										%
%										%
% 			Extract the values of the vector N			%
	F = TF(N);								%
%										%
%										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
