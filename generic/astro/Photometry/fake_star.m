function fake=fake_star(MN,bias,sigma,ampl,x,y)
% fake_star: Returns a fake stellar image.
%
% fake=fake_star([M,N],bias,sigma,ampl,x,y);
%
% ARGUMENTS
%  [M,N]		The size of the frame image.
%  bias,sigma,ampl,x,y	Parameters of the Gaussian as returned
%  			by FIT_GAUSSIAN.
%
% RETURNS
%  A 2-D Gaussian (the fake star)
%
% SEE ALSO
%  fit_gaussian, gauss

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 12 14 First Version
%
% COMPATIBILITY: Matlab, Octave

map=zeros(MN(1),MN(2));
is=[1:MN(1)]';

% This is the principal loop
%for i=1:MN(1)
% for j=1:MN(2)
%  map(i,j) = sqrt( (i-y)^2 + (j-x)^2 );
% end
%end
% The principal loop vectorised
for j=1:MN(2)
 map(:,j)=sqrt( (is-y).^2 + (j-x)^2 );
end

fake=gauss(ampl,sigma,map,0)+bias;
