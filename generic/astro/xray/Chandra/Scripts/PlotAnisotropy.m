clear

%The minimum contour level

load -ascii MinLevel.dat
ContourLevels=2.^[MinLevel:0.5:0];

FontSize=16;

% The object name
fid=fopen('ObjName','r');
 Cluster=fscanf(fid,'%s');
fclose(fid);

more off;

load Image.mat
% The instrument for each observation
load Inst.mat
DecTicks=[-90:2/60:90];
RATicks=[-90:5/60:90];

figure(1)
clf
Xaxis = (Xaxis-mean(Xaxis))*deg_per_hr;
Yaxis = Yaxis-mean(Yaxis);
axis([Xaxis([end,1])-mean(Xaxis),Yaxis([1,end])])
ah=gca;
c=contourc(Xaxis,Yaxis,Image,ContourLevels);
set(gca,'ydir','nor','xdir','rev')
set(gca,'dataaspectratio',[1 1 1])
set(gca,'FontSize',FontSize)
set(gca,'Ytick',DecTicks)
set(gca,'yTickLabel',DEC2str(DecTicks,2))
set(gca,'Xtick',RATicks)
set(gca,'xTickLabel',DEC2str(RATicks,2))

Nelem=size(c,2);

i=1;
j=1;
while( i< Nelem )
 npoints=c(2,i);
 if(npoints>5)
  level(j)=c(1,i);
  line(c(1,i+1:i+npoints),c(2,i+1:i+npoints));
  ellipse_t(j) = fit_ellipse(c(1,i+1:i+npoints),c(2,i+1:i+npoints),ah);
  if(strcmp(ellipse_t(j).status,''))
   j=j+1;
  end
 end
 i=i+npoints+1;
end
% Check if the last ellipse is good.  Discard otherwise
if(~strcmp(ellipse_t(end).status,''))
 ellipse_t=ellipse_t(1:end-1);
 level=level(1:end-1);
end
eval(['print -deps2 ',Cluster,'_Fit_Ellipses.eps'])

% Each ellipse should encompass the "centre", i.e. brightest point.
BrightestLevel=max(level);
match = find(level==BrightestLevel);
if(length(match)>1)
 disp('PlotAnisotropy: WARNING: More that one brightest object found. Taking first')
 match = match(1);
end
ClusterCentreX0=ellipse_t(match).X0_in;
ClusterCentreY0=ellipse_t(match).Y0_in;
% Use FilterByRegion
Region.Shape='ellipse';
Region.Sign=1;
N = length(level);
j=1;
for i=1:N
 Region.P=[ellipse_t(i).X0_in ellipse_t(i).Y0_in ...
           ellipse_t(i).a ellipse_t(i).b ...
           ellipse_t(i).phi];
 IsCentreInEllipse = FilterByRegion(ClusterCentreX0,ClusterCentreY0,Region);
 if(~isempty(IsCentreInEllipse))
  ellip(j) = ellipse_t(i);
  levels(j)=level(i);
  j=j+1;
 end
end


figure(2)
clf
Nlevels=length(ellip);
for i=1:Nlevels
 ellipse(ellip(i).a,ellip(i).b,ellip(i).X0_in,ellip(i).Y0_in, ...
         -ellip(i).phi*180/pi,'r');
end
set(gca,'ydir','nor','xdir','rev')
set(gca,'dataaspectratio',[1 1 1])
set(gca,'FontSize',FontSize)
set(gca,'Ytick',DecTicks);
set(gca,'yTickLabel',DEC2str(DecTicks,2))
set(gca,'Xtick',RATicks);
set(gca,'xTickLabel',DEC2str(RATicks,2))
eval(['print -deps2 ',Cluster,'_Ellipses.eps'])

for i=1:Nlevels
 X(i)=ellip(i).X0_in;
 Y(i)=ellip(i).Y0_in;
 Phi(i)=ellip(i).phi;
 % radius of each isophote
 r(i) = sqrt(ellip(i).a*ellip(i).b);
end

figure(3)
clf

X=X-mean(X);
Y=Y-mean(Y);

plot(X*60,Y*60,'.')
axis equal

% Least-squares solve for the equation of the line connecting the centres.
%P = Y/[X;ones(size(X))]

% Fit a line which minimises the distance of the points to the line.
% fit_line fits P=[A,B,C] where Ax + By + C = 0.
PP=fit_line(X,Y);
if(abs(PP(2)) > 0) 
 P(1) = -PP(1)/PP(2);
 P(2) = -PP(3)/PP(2);
else
 warning('PlotAnisotropy: PP(2)==0')
end

Xlim=get(gca,'Xlim');
Ylim=get(gca,'Ylim');
Xfit = Xlim;
Yfit = P(1)*Xfit+P(2);
line( Xfit,Yfit)
set(gca,'Xlim',Xlim,'Ylim',Ylim);
set(gca,'FontSize',FontSize)
eval(['print -deps2 ',Cluster,'_Ellipse_Centres.eps'])

% Rotate the points to allign with the direction of oscillation
dr=rot3d([X;Y]',atan(P(1)),3,[0,0]);

% Scale by the effective radius of the isophote.
dr_a=dr(:,1)'./r;
dr_b=dr(:,2)'./r;

figure(4)
semilogx(levels,dr_a)
set(gca,'ylim',[-2 2])
set(gca,'FontSize',FontSize)
hold on
plot(levels,dr_b,'--')
hold off
xlabel('cts cm^{-2} s^{-1} arcmin^{-2}')
ylabel('fractional offset')
XLim=get(gca,'xlim');
line(XLim,[0,0],'color',[0 0 0])

%title([Cluster,': Bisector'])
eval(['print -deps2 ',Cluster,'_Bisector.eps'])

