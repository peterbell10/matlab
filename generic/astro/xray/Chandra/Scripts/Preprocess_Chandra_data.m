% Given a list of events files from different observations, and the
% regions of interest:
%   1) Calculate the limits in x and y for each observation.
%   2) Produce a GTI file for each.
%
% If another Obs is added, before runnint delete:
%  Inst.mat
%  EM.mat
%  Image_base.mat
%  Limits.mat (optional.  You may wish to leave it alone).
clear

% The area of interest
Res=512;
save -ASCII Res Res

% Base directory where the observations are stored
BASE_DIR='../';

%%%% END OF USER-SERVICABLE PARTS

more off

% Get the ObsID's
j=0;
D=dir('../');
for i=1:length(D)
 if(D(i).isdir)
  ID = sscanf(D(i).name,'%i');
  % Test to see if it is a number.  Not a number produces a string in
  % Matlab R2009a and produces an empty array in R2012b
  if(~isstr(ID) & ~isempty(ID))
   j=j+1;
   ObsID(j)=ID;
  end
 end
end

% Sort them in increasing numeric order (not essential, but pretty)
ObsID=sort(ObsID);

% Record the ObsID in a file
fid=fopen('ObsID','w');
fprintf(fid,'%i\n',ObsID);
fclose(fid);

%if(0)

%%% Find the events and Aspect Solution files and set up symbolic links
for i=1:length(ObsID)
 ObsStr=int2str(ObsID(i));
 Evt2_link=['evt2_',ObsStr,'.fits'];
 if(~ exist(Evt2_link) )
  % Make symbolic links for each evt file
  Evt2_file=ls([BASE_DIR,ObsStr,'/primary/*evt2*.fits']);
  Evt2_file=Evt2_file(1:end-1); % Strip the trailing \n
  eval(['! ln -s ',Evt2_file,' ',Evt2_link]);
 end
end

%%% Load the data into matlab format
for i=1:length(ObsID)
 ObsStr=int2str(ObsID(i));
 Evts_file=['evt2_',ObsStr,'.mat'];
 if(~ exist(Evts_file) )
  disp(ObsStr)
  Evt2_link=['evt2_',ObsStr,'.fits'];
  [hdr,data]=fitsload(Evt2_link,1);
  data = filter_fields(data);
  disp(['save ',Evts_file,' hdr data'])
  eval(['save ',Evts_file,' hdr data'])
 end
end

%%%% Filter out only the fields we need
%disp('Extracting only the fields we need...')
%for i=1:length(ObsID)
% clear data hdr
% ObsStr=int2str(ObsID(i));
% Evts_file=['evt2_',ObsStr,'.mat'];
% if(~ exist(Evts_file) )
%  disp(ObsStr)
%  Evts_all_file=['evt2_',ObsStr,'-all.mat'];
%  eval(['load ',Evts_all_file,' hdr data'])
%  eval(['save ',Evts_file,' hdr data'])
% end
%end

% What instrument?
disp('What instrument?')
if(~ exist('Inst.mat'))
 for i=1:length(ObsID)
  clear data hdr
  disp(ObsID(i))
  ObsStr=int2str(ObsID(i));
  Evts_file=['evt2_',ObsStr,'.mat'];
  eval(['load ',Evts_file])
  str=fitsfindkeystring(hdr,'DETNAM');
  if(findstr(str,'0123'))
   Inst(i)='I';
  elseif(findstr(str,'7'))
   Inst(i)='S';
  else
   error('Unknown Instrument')
  end
 end 
 % Record the Inst in a file (one ascii, one .mat)
 fid=fopen('Inst','w');
 fprintf(fid,'%c\n',Inst);
 fclose(fid);
 save Inst.mat Inst
else
 load Inst.mat
 if(length(Inst) ~= length(ObsID))
  error('You have added another Observation.  Please delete Inst.mat, EM.mat, Image_base.mat')
 end  
end

% Find the limits
if(~ exist('Limits.mat'))
 disp('Finding limits...')
 NObsID=length(ObsID);
 RA_lo  = zeros(NObsID,1);
 RA_hi  = zeros(NObsID,1);
 Dec_lo = zeros(NObsID,1);
 Dec_hi = zeros(NObsID,1);
 for i=1:NObsID
  clear data hdr
  disp(ObsID(i))
  ObsStr=int2str(ObsID(i));
  Evts_file=['evt2_',ObsStr,'.mat'];
  eval(['load ',Evts_file])
  if(Inst(i)=='S')
   % Filter out the chips other than 7
   index=find(data.ccd_id==7);
  elseif(Inst(i)=='I')
   index=find(data.ccd_id<=3);
  else
   error('Unknown Instrument')
  end
  x=double(data.x(index));
  y=double(data.y(index));
  % Very Rarely (well ObsID 7924), a handful of x's and y's are <0 by a large
  % margin.  Don't know why. detx & dety are fine.
  InRange=find(x>0 & y>0);
  x=x(InRange);
  y=y(InRange);
  % Get the data limits
  left=min(x);
  right=max(x);
  top=max(y);
  bottom=min(y);
  % Convert to RA and Dec
  x0=fitsfindkeyvalue(hdr,'TCRPX11');
  y0=fitsfindkeyvalue(hdr,'TCRPX12');
  dx=fitsfindkeyvalue(hdr,'TCDLT11');
  dy=fitsfindkeyvalue(hdr,'TCDLT12');
  alpha0=fitsfindkeyvalue(hdr,'TCRVL11');
  dec0=fitsfindkeyvalue(hdr,'TCRVL12');
  RA_lo(i) = (dx*(left-x0)/cos(dec0*pi/180)+alpha0)/15;
  RA_hi(i) = (dx*(right-x0)/cos(dec0*pi/180)+alpha0)/15;
  Dec_lo(i) = dy*(bottom-y0)+dec0;
  Dec_hi(i) = dy*(top-y0)+dec0;
 end
 % For Debugging
 disp(sprintf('%6i %6.3f %6.3f %7.2f %7.2f\n',[ObsID' RA_lo RA_hi Dec_lo Dec_hi]'))
 % "Lo" in RA is left, which is actually the highest RA. :p
 Lo(1) = max(RA_lo);
 Lo(2) = min(Dec_lo);
 Hi(1) = min(RA_hi);
 Hi(2) = max(Dec_hi);
 width = max([(Lo(1)-Hi(1))*cos(dec0*pi/180)*15,Hi(2)-Lo(2)])*60;
 % Set Hi to Lo+width (scaled appropriately for RA & Dec)
 Hi(1) = Lo(1) - (width/60)/(cos(dec0*pi/180)*15);
 Hi(2) = Lo(2) + width/60;
 save Limits.mat Lo Hi width
else
 load Limits.mat
end

%%% Calculate the image limits for each observation.
%%% Going from RA and Dec to limits in X and Y
% This loop bothers me.  It assumes Lo Hi and Width have been predefined
% in Limits.mat, then resets them.  WHY?!?!? ERT
disp('Calculate the image limits for each observation...')
for i=1:length(ObsID)
 clear data hdr
 disp(ObsID(i))
 ObsStr=int2str(ObsID(i));
 Evts_file=['evt2_',ObsStr,'.mat'];
 eval(['load ',Evts_file])
 x0=fitsfindkeyvalue(hdr,'TCRPX11');
 y0=fitsfindkeyvalue(hdr,'TCRPX12');
 dx=fitsfindkeyvalue(hdr,'TCDLT11');
 dy=fitsfindkeyvalue(hdr,'TCDLT12');
 alpha0=fitsfindkeyvalue(hdr,'TCRVL11');
 dec0=fitsfindkeyvalue(hdr,'TCRVL12');
 Limits(1)=(Lo(1)*15-alpha0)*cos(dec0*pi/180)/dx+x0;
 Limits(3)=(Lo(2)-dec0)/dy+y0;
 width_pixels=width/60/dy;
 % width_pixels needs to be an integer number that is a multiple of Res
 width_pixels=ceil(width_pixels/Res)*Res;
 Limits(2)=Limits(1)+width_pixels;
 Limits(4)=Limits(3)+width_pixels;
 eval(['save -ASCII Limits_',ObsStr,'.dat Limits']);
 % Update Limits.mat (Lo Hi width) (Lo doesn't change) WHY?!?! Why update?
 width=width_pixels*dy*60;
 Hi(1) = ((Limits(2)-x0)*dx/cos(dec0*pi/180)+alpha0)/15;
 Hi(2) = (Limits(4)-y0)*dy+dec0;
 save Limits Lo Hi width
end

% To set the limits by hand:
% After the pipeline has been run:
% matlab> Create_Chandra_Image
% matlab> figure(2)
% matlab> [Lo, Hi, width] = Get_Limits_Manually();
% matlab> save Limits.mat Lo Hi width
% matlab> Preprocess_Chandra_data
% csh> rm EM.mat Image_base.mat Image.mat
% csh> ../../scripts/ProcessChandraDataWithCIAO.csh
% matlab> Create_Chandra_Image
% matlab> PlotAnisotropy

%%% Calculate the GTI
%for i=1:length(ObsID)
% ObsStr=int2str(ObsID(i));
% Evts_file=['evt2_',ObsStr,'.mat'];
% eval(['load ',Evts_file])
% good=find(data.ccd_id~=7);  % Only those not on the main chip
% [GTI.START,GTI.STOP]=FindGTI(data.time(good));
% GTIfile=['GTI_',ObsStr,'.fits']
% if(exist(GTIfile))
%  eval(['!rm ',GTIfile])
% end
% GTI_write(GTI,hdr,GTIfile)
%end

% Create the directory for the Regions
if(~ exist('./Regions','dir') )
 mkdir Regions
end

% Find the Chip regions.
disp('Find the Chip regions')
load Inst.mat
for i=1:length(ObsID)
 clear data hdr
 disp(ObsID(i))
 ObsStr=int2str(ObsID(i));
 Chips_RegionFile=['Regions/Chips_',ObsStr,'.reg'];
 if(~ exist(Chips_RegionFile) )
  Evts_file=['evt2_',ObsStr,'.mat'];
  eval(['load ',Evts_file])
  FindChipRegion(data,Inst(i),Chips_RegionFile)
 end
end
