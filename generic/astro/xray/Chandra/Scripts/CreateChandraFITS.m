clear
more off

% User Servicable parts.

fid=fopen('ObjName','r');
Cluster=fscanf(fid,'%s');
fclose(fid);
CPS_min=0;

% Also note that I need the following Region files:
%  Regions/PointSources.reg ( a list of sources to remove)
%  FindPointSources() will produce this.

%%%% End of User Servicable parts

% The list of observation numbers
load ObsID

% The instrument for each observation
load Inst.mat

ContourLevels=2.^[-16.5:0.5:0];

% The smoothing parameters
NSearch=200;
BeamWidth_arcmins=2/60; % arcmins

FontSize=10;

%%% Read in Exposure maps and combine (need Res before next step).
for i=1:length(ObsID)
 [hdr_EM,EM_tmp]=FITSload(['product/expmap_',int2str(ObsID(i)),'.fits']);
 if(i==1)
  EM=EM_tmp;
 else
  EM=EM+EM_tmp;
 end
end
Res=size(EM,2);

%%% Read in the Events data.
%%% Filter
%%% Put into a common coordinate system (Physical, with Lower Left = 0,0)
%%% Combine

%%%%% Get the limits of the image %%%%%
% Limits can be generated via:
%  figure(2)
%  [Lo, Hi, width] = Get_Limits_Manually()
%  save Limits Lo Hi width
% Get the Limits in RA and DEC
load Limits
L=zeros(1,4);
L(1)=Lo(1);
L(2)=Hi(1);
L(3)=Lo(2);
L(4)=Hi(2);
BeamHalfWidth= BeamWidth_arcmins/2 /60*Res/(L(4)-L(3));
% Get the size of the image in pixels
ObsStr=int2str(ObsID(1));
Lphysical=load(['Limits_',ObsStr,'.dat']); % Physical Units
L_pix=[0 Lphysical(2)-Lphysical(1) 0 Lphysical(4)-Lphysical(3)];
% Tick labels
possible_tic_seps = [1/60 1/30 1/12 1/6 1/4 1/3 1/2 1 2 5 10 15 20 30 60 ...
                     120 300 600 900 1200 1800 3600 7200 18000];
dec_range = (Hi(2)-Lo(2))*60; % Minutes
ideal_tic_sep = dec_range/10; % Minutes
tic_sep=possible_tic_seps(interp1(possible_tic_seps,[1:length(possible_tic_seps)],ideal_tic_sep,'nearest'));
DecTicks=[floor(Lo(2)):tic_sep/60:ceil(Hi(2))]; %Degrees
%if(strcmp(Inst(1),'S'))
% RA_step=20; % seconds
%else
% RA_step=30;
%end
RA_range = (Lo(1)-Hi(1))*3600; % Seconds
ideal_tic_sep = RA_range/4; % Seconds
tic_sep=possible_tic_seps(interp1(possible_tic_seps,[1:length(possible_tic_seps)],ideal_tic_sep,'nearest'));
RATicks=[floor(Hi(1)*60)*60:tic_sep:ceil(Lo(1)*60)*60]/3600; % Hours


%%%%% Get the base image %%%%%
disp('Getting the base image...')
% Image has units cts/pix
mat_filename = 'Image_base.mat';
if(~ exist(mat_filename) )
 disp(' Accumulating events...')
 x=[];
 y=[];
 for i=1:length(ObsID)
  ObsStr=int2str(ObsID(i));
  disp(['  ',ObsStr])
  %% Read in the events data
  eval(['load evt2_',ObsStr,'.mat'])
  
  %% Filter

  % GTI
  [hdr_GTI,GTI]=FITSload(['product/evt2_',ObsStr,'.gti'],2);
  index=[];
  t=data.time;
  for interval=1:length(GTI.START)
   index=[index;find(t>=GTI.START(interval) & t<=GTI.STOP(interval))];
  end 
  data.x=data.x(index);
  data.y=data.y(index);
  data.energy=data.energy(index);
  data.ccd_id=data.ccd_id(index);

  %Filter out the point sources
  PointSources=ParseRegionFile(['Regions/PointSources_',ObsStr,'.reg']);
  index=FilterByRegion(data.x,data.y,PointSources);
  data.x=data.x(index);
  data.y=data.y(index);
  data.energy=data.energy(index);
  data.ccd_id=data.ccd_id(index);

  %Filter out the high-energy events, which are almost exclusively
  % background (restrict to 0.6 - 7 keV)
  % index=find(data.energy>600 & data.energy<7000);
  % Actually, the lower boundary is not necessary
  index=find(data.energy>100 & data.energy<7000);
  data.x=data.x(index);
  data.y=data.y(index);
  data.ccd_id=data.ccd_id(index);

  % Filter the chips
  if(Inst(i)=='S')
   % Filter out the chips other than 7
   index=find(data.ccd_id==7);
  elseif(Inst(i)=='I')
   index=find(data.ccd_id<=3);
  else
   error('Unknown Instrument')
  end
  data.x=data.x(index);
  data.y=data.y(index);

  % Put into common coordinate system, with Lower Left = 0,0 
  %x0=FITSfindkeyvalue(hdr,'TCRPX11');
  %y0=FITSfindkeyvalue(hdr,'TCRPX12');
  %dx=FITSfindkeyvalue(hdr,'TCDLT11');
  %dy=FITSfindkeyvalue(hdr,'TCDLT12');
  %alpha0=FITSfindkeyvalue(hdr,'TCRVL11');
  %dec0=FITSfindkeyvalue(hdr,'TCRVL12');

  % Put the data on common grid
  Lphysical=load(['Limits_',ObsStr,'.dat']); % Physical Units
  x=[x;data.x-Lphysical(1)];
  y=[y;data.y-Lphysical(3)];

 end % loop over ObsID

 clear t index

 disp('Creating the image...')
 % ensure double
 x=double(x);
 y=double(y);
 L_pix=double(L_pix);
 disp([num2str(length(x)),' events to map'])
 %save Temp.mat x y NSearch Res L_pix Nmax
 %plot(x,y,'.','markersize',1);
 %pause
 set(gca,'XLim',L_pix(1:2),'YLim',L_pix(3:4));
 if(0)
  Nmax=200^3;
  %Nmax=5e6;
  Image_base=numdensity_segmented(x,y,NSearch,Res,L_pix,Nmax)'; % Units of cts/frame
 else
  Image_base=numdensity(x,y,NSearch,Res,L_pix)'; % Units of cts/(frame area)
 end
 Image_cts_per_pixel=Image_base/Res^2; % Units of cts/pixel
 Image_base=Image_base/width^2; % Units of cts/arcminute^2
 eval(['save ',mat_filename,' Image_base Image_cts_per_pixel'])
else
 disp('Base mage already created, loading...')
 eval(['load ',mat_filename])
end

%%%%% Correct by the exposure map
disp('Correcting by the exposure map...')
% EM has units ( cm^2 s )
if(~ exist('EM.mat'))
 disp(' Creating exposure map...')
 radius=0*Image_cts_per_pixel;
 good=find(Image_cts_per_pixel>0.01);
 radius(good)=sqrt(NSearch./(pi*Image_cts_per_pixel(good)));
 % radius=2*radius;
 % radius=radius/2; % Need this line when there is kernel smoothing
 disp('  Smoothing the exposure map...')
 EM_smooth=SmoothOverRadius(EM,radius);
 save EM EM_smooth EM
else
 disp('  Loading exposure map...')
 load EM
end
Image_scaled=0*Image_base;
good=find(EM_smooth>0);
Image_scaled(good)=Image_base(good)./EM_smooth(good);

% Smooth the image by a Gaussian to the beam width
disp('Smoothing the image...')
Image=Smooth(Image_scaled,BeamHalfWidth,'gauss');

% Clip the image where the exposure map is zero
Image=Image.*(1&EM);

% Current units for Image are cts cm^-2 s^-1 pix^-2
% Such that sum(sum(Image))*(exposure time)*(mean eff. area) = # of events
% Let's convert to cts cm^-2 s^-1 arcmin^-2
%pix=(L(4)-L(3))*60/(Res-1); % one pixel = 'x' arcmin
%Image=Image/pix^2; % Image is now in units cts cm^-2 s^-1 arcmin^-2

% Set the aspect ratio for the axes
dec0=mean(L(3:4));
deg_per_hr=cos(dec0*pi/180)*360/24;

% END OF EVERYTHING

%%%%%% BEGIN IMPORTANT PART %%%%%%%

filename=[Cluster,'.fits'];

% Write the image to FITS format
hdr_out=[];
% Mandatory keywords
hdr_out=FITSaddkey(hdr_out,'XTENSION','IMAGE',0);
hdr_out=FITSaddkey(hdr_out,'BITPIX',-32,0);
hdr_out=FITSaddkey(hdr_out,'NAXIS',2,0);
hdr_out=FITSaddkey(hdr_out,'NAXIS1',Res,0);
hdr_out=FITSaddkey(hdr_out,'NAXIS2',Res,0);
hdr_out=FITSaddkey(hdr_out,'PCOUNT',0,0);
hdr_out=FITSaddkey(hdr_out,'GCOUNT',1,0);

% Some useful info pertaining to this writing of the file
D=datevec(now);
DATE=[int2str(D(1)),'-',int2str(D(2)),'-',int2str(D(3)),...
      'T',int2str(D(4)),':',int2str(D(5)),':',int2str(D(6))];
hdr_out=FITSaddkey(hdr_out,'CREATOR','Matlab script CreateChandraFITS.m, using FITSwrite',0);

% Useful info from the original hdr (everything between LONGSTRN
% and HISTNUM, not including HISTNUM)
%LineStart=FITSfindkey(hdr,'LONGSTRN');
%LineEnd=FITSfindkey(hdr,'HISTNUM')-1;
%hdr_out=[hdr_out;hdr(LineStart:LineEnd,1:80)];

% Position
hdr_out=FITSaddkey(hdr_out,'CTYPE1','RA---TAN',0);
hdr_out=FITSaddkey(hdr_out,'CTYPE2','DEC--TAN',0);
hdr_out=FITSaddkey(hdr_out,'CUNIT1','deg',0);
hdr_out=FITSaddkey(hdr_out,'CUNIT2','deg',0);
hdr_out=FITSaddkey(hdr_out,'CRPIX1',1.0,0);
hdr_out=FITSaddkey(hdr_out,'CRPIX2',1.0,0);
hdr_out=FITSaddkey(hdr_out,'CRVAL1',L(1)*15,0);
hdr_out=FITSaddkey(hdr_out,'CRVAL2',L(3),0);
%hdr_out=FITSaddkey(hdr_out,'CDELT1',-1*(L(2)-L(1))*15/Res,0);
hdr_out=FITSaddkey(hdr_out,'CDELT1',   (L(2)-L(1))*deg_per_hr/Res,0);
hdr_out=FITSaddkey(hdr_out,'CDELT2',   (L(4)-L(3))/Res,0);

% This will not be an XTENSION
hdr_out=FITSremovekey(hdr_out,'XTENSION');

% Write the FITS file
if(exist(filename))
 eval(['!rm ',filename])
end
status=FITSwrite(hdr_out,Image,filename);
