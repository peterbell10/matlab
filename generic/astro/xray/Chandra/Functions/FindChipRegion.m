% FindChipRegion: Find a region which encompasses the events for Chandra.
%
% FindChipRegion(data,Instrument,RegionFile)
%
% ARGUMENTS
%  data		The events data.
%  Instrument	The Chandra detector
%  RegionFile	The file to which to write the regions.
%
% RETURNS: Nothing.
%
% SEE ALSO
%  FindPointSources

% AUTHOR: Eric Tittley
%
% HISTORY
%  050218 First version.
%
% COMPATIBILITY: Matlab, Octave

function FindChipRegion(data,Inst,RegionFile)

switch Inst
 case 'I'
  good=find(data.ccd_id<=3);
 case 'S'
  good=find(data.ccd_id==7);
 otherwise
  disp('Unknown instrument')
end

x = data.x(good);
y = data.y(good);
clear data

left=find(x==min(x));
right=find(x==max(x));
top=find(y==max(y));
bottom=find(y==min(y));

% Four points
x1 = x(left);    y1 = y(left);
x2 = x(top);     y2 = y(top);
x3 = x(right);   y3 = y(right);
x4 = x(bottom);  y4 = y(bottom);

% Four lengths
L(1)=sqrt((x2-x1)^2+(y2-y1)^2);
L(2)=sqrt((x3-x2)^2+(y3-y2)^2);
L(3)=sqrt((x4-x3)^2+(y4-y3)^2);
L(4)=sqrt((x1-x4)^2+(y1-y4)^2);
L = max(L); % Take the largest
L = L + 100; % Padding

% The centre
x0 = mean([x1,x2,x3,x4]);
y0 = mean([y1,y2,y3,y4]);

% The Rotation
theta(1) = atan((y2-y1)/(x2-x1));
theta(2) = atan((x3-x2)/(y2-y3));
theta(3) = atan((y3-y4)/(x3-x4));
theta(4) = atan((x4-x1)/(y1-y4));
theta = mean(theta)/pi*180;

Region.Shape='rotbox';
Region.Sign=1;
Region.P=[x0,y0,L,L,theta];

WriteRegionFile(Region,RegionFile)
