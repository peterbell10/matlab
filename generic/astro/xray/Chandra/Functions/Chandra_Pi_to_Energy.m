function Energy=Chandra_Pi_to_Energy(Pi)
% Chandra_Pi_to_Energy: Convert event Pi to approximate energy
%
% Energy=Chandra_Pi_to_Energy(Pi)
%
% ARGUMENTS
%  Pi	The event Pi (energy bin)
%
% RETURNS
%  The energy (in what units?)
%
% NOTES
%  The energy can only ever be an approximation on account of the non-trivial
%  detector response function.

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  080116 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

Energy=0.0146*Pi-7.2972e-3;
