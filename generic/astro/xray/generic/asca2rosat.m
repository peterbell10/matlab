% asca2rosat: ROSAT coordinates and radius for an ASCA (SKY) extract region
%
% [rosat_x,rosat_y,rosat_r] = asca2rosat(asca_h,rosat_h,asca_x,asca_y,asca_r)
%
% ARGUMENTS
%  asca_h
%  rosat_h
%  asca_x
%  asca_y
%  asca_r
%
% RETURNS
%  rosat_x
%  rosat_y
%  rosat_r
%
% NOTES
%  The header is the zero entry header from the image file
%
% REQUIRES
%  fitsfindkeyvalue
%
% SEE ALSO
%  sax2asca, sax2rosat

% AUTHOR: Danny Hudson
%
% HISTORY
%  02-07-30 Mature version
%  07 10 05 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function [ro_x,ro_y,ro_r] = asca2rosat(rh,ah,as_x,as_y,as_r);

% Get information about the asca data
ax_0 = fitsfindkeyvalue(ah,'CRPIX1');
ay_0 = fitsfindkeyvalue(ah,'CRPIX2');
ara_0 = fitsfindkeyvalue(ah,'CRVAL1');
adec_0 = fitsfindkeyvalue(ah,'CRVAL2');
a_delx = fitsfindkeyvalue(ah,'CDELT1');
a_dely = fitsfindkeyvalue(ah,'CDELT2');
ax_max = fitsfindkeyvalue(ah,'NAXIS1');
ay_max = fitsfindkeyvalue(ah,'NAXIS2');

if ax_0 ~= fix(ax_0)
 ara_0 = (fix(ax_0) - ax_0)*a_delx + ara_0;
 ax_0 = fix(ax_0);
end

if ay_0 ~= fix(ay_0)
 adec_0 = (fix(ay_0) - ay_0)*a_dely + adec_0;
 ay_0 = fix(ay_0);
end

dec = adec_0 + a_dely*(as_y - ay_0);
ra = ara_0 + a_delx*(as_x - ay_0);


% Get information about rosat data from the header
rx_0 = fitsfindkeyvalue(rh,'CRPIX1');
ry_0 = fitsfindkeyvalue(rh,'CRPIX2');
rra_0 = fitsfindkeyvalue(rh,'CRVAL1');
rdec_0 = fitsfindkeyvalue(rh,'CRVAL2');
r_delx = fitsfindkeyvalue(rh,'CDELT1');
r_dely = fitsfindkeyvalue(rh,'CDELT2');
rx_max = fitsfindkeyvalue(rh,'NAXIS1');
ry_max = fitsfindkeyvalue(rh,'NAXIS2');

if rx_0 ~= fix(rx_0)
 rra_0 = (fix(rx_0) - rx_0)*r_delx + rra_0;
 rx_0 = fix(rx_0);
end

if ry_0 ~= fix(ry_0)
 rdec_0 = (fix(ry_0) - ry_0)*r_dely + rdec_0;
 ry_0 = fix(ry_0);
end

rra(1:rx_max) = rra_0 + r_delx.*([1:rx_max]-rx_0);
rdec(1:ry_max)= rdec_0 + r_dely*([1:ry_max]-ry_0);

ro_x = interp1(rra,[1:rx_max],ra);
ro_y = interp1(rdec,[1:ry_max],dec);
ro_r = as_r*a_dely/r_dely;
