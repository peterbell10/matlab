% filter_fields: From a data structure of x-ray events, remove all but the
%                useful.
%
% data = filter_fields(data)
%
% ARGUMENTS
%  data		Data structure to be filtered.
%
% RETURNS
%  data		A data structure containing only the following fields:
%		 ccd_id
%		 x
%		 y
%		 energy
%		 time
%
% NOTES
%  Rather mundane little task
%
% SEE ALSO
%  Preprocess_Chandra_data (the only script or function that uses this)

% AUTHOR: Eric Tittley
%
% HISTORY
%  060303 First version
%  071222 Added comments
%
% COMPATIBILITY: Matlab, Octave

function data = filter_fields(data)

data_new.ccd_id = data.ccd_id;
data_new.x      = data.x;
data_new.y      = data.y;
data_new.energy = data.energy;
data_new.time   = data.time;

data = data_new;
