% sax2asca: ASCA coordinates and radius for a BeppoSAX (SKY) extract region
%
% [asca_x,asca_y,asca_r] = sax2asca(sax_h,asca_h,sax_x,sax_y,sax_r)
%
% ARGUMENTS
%  sax_h
%  asca_h
%  sax_x
%  sax_y
%  sax_r
%
% RETURNS
%  asca_x
%  asca_y
%  asca_r
%
% NOTES
%  The header is the zero entry header from the image file
%
% REQUIRES
%  fitsfindkeyvalue
%
% SEE ALSO
%  sax2rosat, asca2rosat

% AUTHOR: Danny Hudson
%
% HISTORY
%  02-07-30 Mature version
%  07 10 05 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function [as_x,as_y,as_r] = sax2asca(bsh,ah,bs_x,bs_y,bs_r);

% Get information about BeppoSAX data from the header
bsx_0 = fitsfindkeyvalue(bsh,'CRPIX1');
bsy_0 = fitsfindkeyvalue(bsh,'CRPIX2');
bsra_0 = fitsfindkeyvalue(bsh,'CRVAL1');
bsdec_0 = fitsfindkeyvalue(bsh,'CRVAL2');
bs_delx = fitsfindkeyvalue(bsh,'CDELT1');
bs_dely = fitsfindkeyvalue(bsh,'CDELT2');
bsx_max = fitsfindkeyvalue(bsh,'NAXIS1');
bsy_max = fitsfindkeyvalue(bsh,'NAXIS2');


% Shift x and y if they are not integers
if bsx_0 ~= fix(bsx_0)
 bsra_0 = (fix(bsx_0) - bsx_0)*bs_delx + bsra_0;
 bsx_0 = fix(bsx_0);
end

if bsy_0 ~= fix(bsy_0)
 bsdec_0 = (fix(bsy_0) - bsy_0)*bs_dely + bsdec_0;
 bsy_0 = fix(bsy_0);
end

% Determine the ra and dec vectors
dec = bsdec_0 + bs_dely*(bs_y - bsy_0);
ra = bsra_0 + bs_delx*(bs_x - bsy_0)*cos(bsdec_0*pi/180);

% Get information about the asca data
ax_0 = fitsfindkeyvalue(ah,'CRPIX1');
ay_0 = fitsfindkeyvalue(ah,'CRPIX2');
ara_0 = fitsfindkeyvalue(ah,'CRVAL1');
adec_0 = fitsfindkeyvalue(ah,'CRVAL2');
a_delx = fitsfindkeyvalue(ah,'CDELT1');
a_dely = fitsfindkeyvalue(ah,'CDELT2');
ax_max = fitsfindkeyvalue(ah,'NAXIS1');
ay_max = fitsfindkeyvalue(ah,'NAXIS2');

% Shift x and y if they are not integers
if ax_0 ~= fix(ax_0)
 ara_0 = (fix(ax_0) - ax_0)*a_delx + ara_0;
 ax_0 = fix(ax_0);
end

if ay_0 ~= fix(ay_0)
 adec_0 = (fix(ay_0) - ay_0)*a_dely + adec_0;
 ay_0 = fix(ay_0);
end


ara(1:ax_max) = ara_0 + a_delx.*([1:ax_max]-ax_0)/cos(adec_0*pi/180);
adec(1:ay_max)= adec_0 + a_dely*([1:ay_max]-ay_0);

as_x = interp1(ara,[1:ax_max],ra);
as_y = interp1(adec,[1:ay_max],dec);
as_r = bs_r*bs_dely/a_dely;
