function GTI_write(GTI,base_hdr,filename)
% GTI_write: Write a GTI to FITS file
%
% GTI_write(GTI,base_hdr,filename)
%
% ARGUMENTS
%  GTI		A structure composed of GTI.START and GTI.STOP, both of which
%		are vectors of length N, where there are N intervals.
%  base_hdr	The header of the events.  Used to pick out some required
%		keywords.
%  filename	The file to write.
%
% NOTE
% 	BEWARE: fitswrite appends to a fits file if it already exists.
%
% SEE ALSO
%  fitsaddkey fitswrite fitsload FindGTI

% AUTHOR: Eric Tittley
%
% HISTORY
%  03 06 07 First version.
%  03 06 09
%	Added comment for FindGTI.
%	Modified to accept a GTI file name.
%  08 01 16 Verified Octave compatibility
%
% COMPATIBILITY: Matlab, Octave

%%% Prepare the header
H=fitsaddkey([],'XTENSION','BINTABLE',0,'binary table extension');
H=fitsaddkey(H,'BITPIX',8,0,'8-bit bytes');
H=fitsaddkey(H,'NAXIS',2,0,'2-dimensional binary table');
H=fitsaddkey(H,'NAXIS1',16,0,'width of table in bytes');
H=fitsaddkey(H,'NAXIS2',length(GTI.START),0,'number of rows in table');
H=fitsaddkey(H,'PCOUNT',0,0,'size of special data area');
H=fitsaddkey(H,'GCOUNT',1,0,'one data group (required keyword)');
H=fitsaddkey(H,'TFIELDS',2,0,'number of fields in each row');
H=fitsaddkey(H,'EXTNAME','GTI',0,'name of this binary table extension');
H=fitsaddkey(H,'HDUNAME','GTI',0,'ASCDM block name');
H=fitsaddkey(H,'TTYPE1','START',0,'label for field');
H=fitsaddkey(H,'TFORM1','1D',0,'format of field');
H=fitsaddkey(H,'TUNIT1','s',0,'unit of field');
H=fitsaddkey(H,'TTYPE2','STOP',0,'label for field');
H=fitsaddkey(H,'TFORM2','1D',0,'format of field');
H=fitsaddkey(H,'TUNIT2','s',0,'unit of field');
H=fitsaddkey(H,'MJDREF',fitsfindkeyvalue(base_hdr,'MJDREF'),0,'MJD zero point for times');
H=fitsaddkey(H,'TSTART',fitsfindkeyvalue(base_hdr,'TSTART'),0,'Observation start time');
H=fitsaddkey(H,'TSTOP',fitsfindkeyvalue(base_hdr,'TSTOP'),0,'Observation end time');
H=fitsaddkey(H,'TIMESYS','TT',0,'Time system');
H=fitsaddkey(H,'TIMEUNIT','s','Time unit');
H=fitsaddkey(H,'DATE-OBS',fitsfindkeystring(base_hdr,'DATE-OBS'),0,'Date and time of observation start');
H=fitsaddkey(H,'DATE-END',fitsfindkeystring(base_hdr,'DATE-END'),0,'Date and time of observation stop');
H=fitsaddkey(H,'TIMEZERO',fitsfindkeyvalue(base_hdr,'TIMEZERO'),0,'Clock correction');
H=fitsaddkey(H,'CONTENT','GTI',0,'Data product identification');
H=fitsaddkey(H,'DSTYP1','TIME',0,'DM Keyword: Descriptor name');
H=fitsaddkey(H,'DSVAL1','TABLE',0,'[s]');
H=fitsaddkey(H,'DSFORM1','D',0,'DM Keyword: Descriptor datatype');
H=fitsaddkey(H,'DSUNIT1','s',0,'DM Keyword: Descriptor unit');
H=fitsaddkey(H,'DSREF1',':GTI',0);

%%% Write HDU 0
H_head=fitsaddkey([],'EXTEND','T',0,'FITS dataset may contain extensions');
fitswrite(H_head,[],filename);

%%% Write the file
fitswrite(H,GTI,filename);
