function [n_H,N_H,rho,rho_e] = est_Density(rad,z,H,norm,Z)
% est_Density: Galaxy cluster density estimate given a normalization from a
%	       Mekal or Raymond-Smith model
%
% [n_H,N_H,rho,rho_e] = est_Density(rad,z,H,norm,Z)
%
% ARGUMENTS
%  rad is the angular radius on the extraction region in arcmin
%  z is the redshift of the cluster
%  H is assumed hubble constant (km Mpc^-1 s^-1)
%  norm is the normalization of the mekal or raymond-smith model
%  Z is the metallicity
%
% RETURNS
%  n_H is the average hydrogen density (cm^-3)
%  N_H is the number of hydrogen atoms in the region
%  rho is the density if grams cm^(-3)
%
% NOTES
%  This tool is only valid for near-by (low z) objects
%
% REQUIRES
%  RhoToNeNi
%
% SEE ALSO
%  est_Density_cosmo

% AUTHOR: Danny Hudson, Eric Tittley
%
% HISTORY
%  030723 4th Anniversary of Chandra
%  071222 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

%__________________U_N_I_V_E_R_S_A_L__C_O_N_S_T_A_N_T_S_________________________%
% From Allen's Astrophysical quantities 4th Ed.					%
c = 2.99792458e5; 		% speed of light in km s^-1			%
%mass_H = 1.6735344e-24;	% mass of hydrogen atom in grams		%
mass_H = 1.672623110e-24; 	% mass of a stripped hydrogen atom in grams	%
mass_e = 9.109389754e-28; 	% mass of electron in grams			%
%_______________________________________________________________________________%

%_______________________________________________________________%
% If we assume uniform density, we can used the normalization	%
% of our mekal model to determine the density			%
%								%
% According to XSPEC						%
% Norm = 1e-14/(4*pi(D_A(1+z))^2)*int(n_e n_h dV)		%
%								%
% where D_A is the angular size distance			%
% z is the redshift						%
% and n_e and n_h are the electron and hydrogen densities 	%
% respectively							%
%_______________________________________________________________%

%_______________________________________________________________________%
% We need n_h, and from the metallicity, we should be able		%
% to calculate the density						%
% rho = n_h*mass_h + Y*n_h*mass_He + Z*n_h*avg(mass_others)		%
%									%
% If we assume uniform density						%
% int(n_e n_h dV) = V*ratio*(N_h)^2,					%
% where N_h is the number of hydrogen ions and ratio is the ratio of	%
% electrons to hydrogen ions (n_e/n_h)					%
% so, that								%
%  N_H =sqrt(Norm*4*pi*(D_A(1+z))^2/(V*ratio)				%			
%_______________________________________________________________________%

%______________C_A_L_C_U_L_A_T_I_N_G__D_A_(A_N_G_U_L_A_R__S_I_Z_E__D_I_S_T_A_N_C_E______________%
%												%
% z = v/c => v = cz	=> Distance = v/H							%
%												%
% cluster distance in h^-1 Mpc									%
%												%
   					dist = c*z/H;						%
%												%
% From geometry tan(theta) = radius/Distance => radius = Distance*tan(theta);			%
%												%
% radius of region in kpc h^-1									%
%												%
  				r = dist*tan(rad/60*pi/180) * 1e3;				%
%												%
% D_A = size/theta										%
% where "size" is the transverse extent of an object and "theta" is the angle (in radians) that %
% it subtends on the sky.									%
% From http://www.astro.ucla.edu/~wright/cosmo_02.htm						%
%												%
% The angular size distance is the distance/radian so,  					%
% r(kpc) * 3.09e21 (cm/kpc) / (rad(arcmin)/60(arcmin/degree) * pi(radians)/180(degrees)) 	%
%										= cm/radian	%
%												%
% angular size distance in h^-1 cm								%
%												%
  				D_A = r*3.09e21/(rad/60*pi/180);				%
%_______________________________________________________________________________________________%


% We assume the the mekal model uses n_H and not n_i, (I'll have to check on this)

[ne,ni] = RhoToNeNi(1,Z);	% density doesn't matter because we just want the ratio.

%%%%%%%% From Allen's, 4th edition %%%%%%%%%%%%
%%                                Stripped   %%
%% Element group Number   Mass    electrons  %%
%%                                           %%
%%  H            100      100     100        %%
%%  He             9.8     39      20        %%
%%  C,N,O,Ne       0.145    2.19    1.1      %%
%%  Other          0.013    0.44    0.21     %%
%%                                           %%
%%  Total        109.96   141.63  121.3      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%so 
nh = ni*100/(100+9.8 + Z*.145 + Z*.013);	% ni is the number of ions Z is the solar metallicity
ratio = ne/nh;

N_H = sqrt(norm*4*pi*(D_A*(1+z))^2/1e-14/ratio); 			             % h^(-1) hydrogen atoms in region
N_e = N_H*ratio; 			             				     % h^(-1) electrons in region

n_H = N_H/(4/3 * pi * (r * 3.09e21)^3)^(1/2);	  				     % hydrogen density (h^(1/2) cm^-3)
n_e = n_H*ratio;	  				     			     % electron density (h^(1/2) cm^-3)

rho = n_H*mass_H;	  				     % hydrogen density (h^(1/2) g cm^-3)
rho_e= n_e*mass_e;	  				     % electron density (h^(1/2) g cm^-3)

rho_i = n_H*mass_H + 9.8/100*n_H*mass_H*4 + ...
		 n_H*mass_H*Z*2.19/100 + n_H*Z*mass_H*.44/100 + ratio*n_H*mass_e; % density in (h^(1/2) g cm^-3)
% output results
fprintf('\n\nThe results based on the input are: \n \n');
fprintf('You have a plasma with %g h_%g^(-1) hydrogen atoms. \n',N_H,H);
fprintf('For a region with a radius of %g h_%g^(-1) kpc, \n',r,H);
fprintf('this corresponds to a density of %e h_%g^(1/2) cm^-3 \n',n_H,H);
fprintf('or %g h_%g^(1/2) g cm^-3. \n',rho,H);
fprintf('You have  %g h_%g^(-1) electrons. \n',N_e,H);
fprintf('or a density of %e h_%g^(1/2) cm^-3 \n',n_e,H);
fprintf('or %g h_%g^(1/2) g cm^-3. \n',rho_e,H);
fprintf('The other elements contribute to this density,\n');
fprintf('so that the ion density is %e h_%g^(1/2) cm^-3, or \n',n_H*(100+Z*9.96)/100,H);
fprintf('%g h_%g^(1/2) g cm^-3 \n \n',rho_i,H);
