% FindGTI: Find the GTI (good time interval) for a time series of events.
%
% [t_start,t_end]=FindGTI(times)
%
% ARGUMENTS
%  times	The event times.
%
% OUTPUT
%  t_start	A vector of N start times
%  t_end	A vector of N end times
%
% NOTES
%  - The input times should be extracted to come from a source-free region/chip,
%    if at all possible.
%  - There are N intervals, t_start(i) to t_end(i)
%
% SEE ALSO
%  GTI_write

% AUTHOR: Eric Tittley
%
% HISTORY
%  03 06 06 First version.
%  03 06 08 Fixed indexing.
%  08 01 16 Verified Octave compatibility
%
% COMPATIBILITY: Matlab, Octave

function [t_start,t_end]=FindGTI(times)

%% Create a light curve
Ntimes=length(times);
Nbins=ceil(Ntimes/1000); % 1000 events per time bin
[N,t]=hist(times,Nbins);

%% Find the most common rate (Number in eash time bin)
[Nrate,rate]=hist(N,ceil(length(N)/10));
MostFrequentN=mean(rate(Nrate==max(Nrate)));

%% Find all the times in which the rate in within sigma_clip of the
%% most common rate
sigma_clip=3;
sigma=sqrt(MostFrequentN); % Assume Poissonian Statistics
Index=find(N>MostFrequentN-sigma_clip*sigma & N<MostFrequentN+sigma_clip*sigma);

%bar(t,N);
%hold on
%bar(t(Index),N(Index),'r')
%hold off

%% Create a time interval structure.
% We want breakpoints found at the start and end, too.
Index=[-1 Index length(Index)+2];
% dIndex will be mostly 1, but will be >1 at breaks 
% ( eg Index=[-1 1 2 3 4 7 8 9 22] => dIndex = [2 1 1 1 3 1 1 13]
%  =>  breaks = [1 5 8] )
dIndex=diff(Index);
breaks=find(dIndex>1);
i=[1:length(breaks)-1];
t_start=t(Index(breaks(i)+1));
t_end=t(Index(breaks(i+1)));

%%% END
