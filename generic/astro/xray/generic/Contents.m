% astro/xray/generic: Generic routines for processing x-ray data.
%
% ---- RMFs ----
%
% addRMF		Add RMFs, weighting if necessary
%
% CreateRMFStruct	Create an OGIP RMF structure from an RMF array.
%
% ExtractRMF		Extract an RMF from a OGIP RMF structure.
%
%
% ---- GTIs ----
%
% FindGTI	Find the GTI (good time interval) for a time series of events.
%
% GTI_write	Write a GTI to FITS file
%
%
% ---- Coordinates ----
%
% get_RA_DEC_coords	Gets the RA and Dec of positions given
%			in detector coordinates.
%
% asca2rosat	ROSAT coordinates and radius for an ASCA (SKY) extract region
%
% sax2asca	ASCA coordinates and radius for a BeppoSAX (SKY) extract region
%
% sax2rosat	ROSAT coordinates and radius for a BeppoSAX (SKY) extract region
%
%
% ---- Cluster density estimates ----
%
% region_size		Estimates radius of a sphere with same volume as a
%			region
%
% est_Density_cosmo	Galaxy cluster density estimate given a normalization
%	       		from a Mekal or Raymond-Smith model
%
% est_Density		Galaxy cluster density estimate given a normalization
%	       		from a Mekal or Raymond-Smith model
%
% ---- FITS data ----
%
% filter_fields		From a data structure of x-ray events, remove all but
%                	the useful.
