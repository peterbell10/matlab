function rmf_struct=CreateRMFStruct(rmf,Energies)
% CreateRMFStruct: Create an OGIP RMF structure from an RMF array.
%
% rmf_struct=CreateRMFStruct(rmf,Energies);
%
% Given an RMF (or Responose) matrix, return a structure for writing to an
% RMF (or Response) fits file.
%
% rmf must have dimensions [N_energies x N_Channels];
%
% See also:
%  fitsread ExtractRMF

% AUTHOR: Eric Tittley
%
% HISTORY
%  01-12-06 First version.
%
% COMPATIBILITY: Matlab, Octave
%
% BUGS
%  ENERG_LO and ENERG_HI are approximations that are exact in most
%  cases (Energies equally spaced) but will have an error where
%  dEnergies changes.  The user is advised to keep the ENERG_LO and
%  ENERG_HI originally read from the file, if it is available.

%%% Input checks
% Number of arguments
if(nargin~=2)
 error('Takes 2 arguments')
end
% Size of arguments
[N_E,N_Chan]=size(rmf);
if(length(Energies)~=N_E)
 error('Energies must have the same number of elements as rmf has rows')
end

% The OGIP standard for RMFs provides for compression of the data via
% ignoring values in the matrix that are zeros.
% It does this by only saving the non-zero elements in subsets called
% groups, while saving the starting element of each group and the length
% of each group.
% For each energy, there can be any number of groups, but typically there
% are <5.
% Here, we are going to cheat a bit and save only 1 group per energy.
% This will provide an OGIP-compatible structure that will be bigger in size.

%%% Initialise parts of the rmf_struct structure
%% ENERG_LO and ENERG_HI
% This will be a bit of a hack.  The user is best to just
% use the values that were read in originally, if they
% are still available.
% The following is exactly correct if Energies is evenly spaced.
% If there is a break, then there will be an error.
dE=diff(Energies);
dE=[dE;dE(end)];
rmf_struct.ENERG_LO=Energies-dE/2;
rmf_struct.ENERG_HI=Energies+dE/2;
%% N_GRP.  There will only be one group per energy.
rmf_struct.N_GRP =ones(N_E,1);
%% F_CHAN and N_CHAN will be modified in the next step
rmf_struct.F_CHAN=zeros(N_E,1);
rmf_struct.N_CHAN=zeros(N_E,1);

%%% First, find out what the longest group will be.
%%% While we are doing this, also find F_CHAN and N_CHAN.
max_length=0;
for i=1:N_E
 mask=find(rmf(i,:));
 len=0;
 if(~ isempty(mask))
  len=mask(end)-mask(1)+1;
  if(len>max_length)
   max_length=len;
  end
  rmf_struct.F_CHAN(i)=mask(1);
 end
 rmf_struct.N_CHAN(i)=len;
end
% max_length is the length of the longest group.

%%% Initialise the rmf_struct.MATRIX array
rmf_struct.MATRIX=zeros(N_E,max_length);

%%% Fill in the structure
for i=1:N_E
 if(rmf_struct.N_CHAN(i)>0)
  span=rmf_struct.F_CHAN(i)+[0:rmf_struct.N_CHAN(i)-1];
  rmf_struct.MATRIX(i,1:rmf_struct.N_CHAN(i))=rmf(i,span);
 end
end

%%% The last part of the structure, ENERG_LO and ENERG_HI

%%% DONE CreateRMFStruct %%%
