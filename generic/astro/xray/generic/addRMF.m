function [hdr,rmf]=addRMF(files,weights)
% addRMF: Add RMFs, weighting if necessary
%
% [hdr,rmf]=addRMF(files,weights)
%
% INPUT
%  files		A list of M files containing	M-element cell array
%			RMFs. (one RMF per file)
%  weights (optional)	The list of weights for the	M-element Double
%			RMF in each file.
%
% OUTPUT
%  hdr			The FITS header for the new	[Mx80] string array
%			RMF.
%  rmf			The RMF structure.
%
% NOTES
%  The weights will be normalised.  If that is not wanted, just multiply the
%  resulting rmf by the mean of weights.
%
% SEE ALSO
%  ExtractRMF  fits

% AUTHOR: Eric Tittley
%
% HISTORY
%  01-12-05	First version
%
% COMPATIBILITY: Matlab, Octave

%%% Standard argument checks

% Number of arguments
if(nargin~=1 & nargin~=2)
 error('Requires 1 or 2 arguments')
end

% If no weights 
if(nargin==1)
 [M,N]=size(files);
 weights=ones(M,1);
end

% Check to see if there are as many weights as files
M=size(files,2);
if(length(weights)~=M)
 error('There must be a weight for each file')
end

%%% Normalise the weights
weights=weights/sum(weights);

%%% Read in the RMF structures.  The structures contain a `compressed'
%%% form of the RMF.
for i=1:M
 % We need the char() function since files is a cell array.
 [hdrs(i).hdr,rmfs(i)]=fitsload(char(files{i}),1);
end

%%% Convert the RMF structures to RMFs
for i=1:M
 [rmf_in(:,:,i),Energies(:,i),Channels(i,:)]=ExtractRMF(rmfs(i));
end

%%% Check to see that Energies and Channels are the same for each.
% Energies
mask=diff(Energies,1,2);
if(sum(sum(mask))~=0)
 error('The energies in input RMFs do not match up')
end
% Channels
mask=diff(Channels,1,1);
if(sum(sum(mask))~=0)
 error('The Channels in input RMFs do not match up')
end

%%% If the Energies and Channels match up, then we don't need the
%%% repetition.
Energies=Energies(:,1);
Channels=Channels(1,:);

%%% Multiply by the weights
for i=1:M
 rmf_in(:,:,i)=rmf_in(:,:,i)*weights(i);
end

%%% Find the mean along the 3rd column
rmf_whole=mean(rmf_in,3);

%%% Create the rmf structure
rmf=CreateRMFStruct(rmf_whole,Energies);

%%% Use the input ENERG_LO and ENERG_HI which don't suffer errors
%%% due to the approximation of Energies in ExtractRMF.
rmf.ENERG_LO=rmfs(1).ENERG_LO;
rmf.ENERG_HI=rmfs(1).ENERG_HI;

%%% The header
% Start with the first one as a base
hdr=hdrs(1).hdr;
% remove some keywords which will change.
KeysToRemove={{'NAXIS1'},{'PCOUNT'}};
for i=1:size(KeysToRemove,1)
 hdr=fitsremovekey(hdr,char(KeysToRemove{i}));
end

% We won't use array descriptors, so save the matrix in the table as
% a double.
pos=fitsfindkey(hdr,'TFORM6');
hdr=fitsremovekey(hdr,'TFORM6');
hdr=fitsaddkey(hdr,'TFORM6',[int2str(size(rmf.MATRIX,2)),'E'],pos);

