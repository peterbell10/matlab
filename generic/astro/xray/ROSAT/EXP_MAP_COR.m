% EXP_MAP_COR (ROSAT_tools): 
% syntax factor = EXP_MAP_COR(region,bgd_region,mex_file,bgd_extrct);
%
% ARGUMENTS
%  region	The ascii file with the region of the source
%		assumed to be circle(#,#,#)
%		and/or -circle(#,#,#) etc...
%
% bgd_region	The background region file that has the scale of the mex file
%		512x512 (1/2 the region file size)
%		assumed to be circle(#,#,#)
%		and/or -circle(#,#,#) etc...
%
% mex_file	The exposure map file
%
% bgd_extract	The file that will be written with the background ascii regions
%		in the usual 1024x1024 scaling.
%
% RETURNS
% factor	The factor by which the spectral file backscal should be
%		multiplied (OR the background file divided)
%
% REQUIRES
%  fits tools
%
% SEE ALSO
%  ROSAT_tools, ROSAT_exp_map_correction

% AUTHOR: Danny Hudson
%
% HISTORY
%  02 07 30 First version
%  07 10 29 Modified comments
%
% COMPATIBILITY: Matlab, Octave (?)

function factor = EXP_MAP_COR(region,bgd_region,mex_file,bgd_extrct);

% LOAD IN THE EXPOSURE MAP
[h_e,d_e] = fitsload(mex_file);

% Open and parse the region file
fid = fopen(region,'r');
ch = 0;
t = 1;
u = 1;
while ch ~= -1
 line = fgetl(fid);
 ch = line;
 if line(1) == '-' & ch ~= -1
  on_region_subtract(u,1:3) = str2num(line(9:length(line)-1))/2;
  u = u + 1;
 elseif line(1) == 'c'
  on_region(t,1:3) = str2num(line(8:length(line)-1))/2;
  t = t + 1;
 end
end
fclose(fid);
if exist('on_region_subtract') == 0
 on_region_subtract = zeros(1,3);
end

% Open and parse the bgd_region file
fid = fopen(bgd_region,'r');
ch = 0;
t = 1;
u = 1;
while ch ~= -1
 line = fgetl(fid);
 ch = line;
 if line(1) == '-' & ch ~= -1
  back_region_subtract(u,1:3) = str2num(line(9:length(line)-1));
  u = u + 1;
 elseif line(1) == 'c'
  back_region(t,1:3) = str2num(line(8:length(line)-1));
  t = t + 1;
 end
end

if exist('back_region_subtract') == 0
 back_region_subtract = zeros(1,3);
end


% Determine the area ratios of the source region and background regions
a = ones(512,1);
b = [1:512];
x = a*b;
y = b'*a';

on_area = pi*on_region(3)^2 - sum(pi*on_region_subtract(:,3).^2);
back_area = sum(pi*back_region(:,3).^2) - sum(pi*back_region_subtract(:,3).^2);

ratio = on_area/back_area;

% Determine the exposure "counts" of the source region
source_counts = sum(sum(d_e(sqrt((x-on_region(1,1)).^2 + (y-on_region(1,2)).^2) ...
		<= on_region(1,3))))
		
if max(on_region_subtract > 0)
 for t = 1:length(on_region_subtract(:,1))
  source_counts = source_counts - sum(sum(d_e(sqrt((x-on_region_subtract(t,1)).^2 + ...
  		(y-on_region_subtract (t,2)).^2) <= on_region_subtract(t,3))))
 end
end

% Determine the exposure "counts" of the background region
back_counts = 0;
for t = 1:length(back_region(:,1))
 temp_counts = sum(sum(d_e(sqrt((x-back_region(t,1)).^2 + (y-back_region(t,2)).^2) ...
		<= back_region(t,3))));
 source_counts/(temp_counts*on_region(1,3)^2/back_region(t,3)^2)
 back_counts = back_counts+temp_counts;
end

if max(back_region_subtract > 0)
 for t = 1:length(back_region_subtract(:,1))
  back_counts = back_counts - sum(sum(d_e(sqrt((x-back_region_subtract(t,1)).^2 + ...
  		(y-back_region_subtract (t,2)).^2) <= back_region_subtract(t,3))))
 end
end


% Normalize background area to source area and determine the factor
factor = source_counts/(back_counts*ratio)
fclose(fid);

% Write the new background ascii region file
fid = fopen(bgd_extrct,'w');
for k = 1:length(back_region(:,1))
 fprintf(fid,'circle(%g,%g,%g)\n',2*back_region(k,1),2*back_region(k,2),2*back_region(k,3));
end
if max(back_region_subtract) > 0
 for k = 1:length(back_region_subtract(:,1))
  fprintf(fid,'-circle(%g,%g,%g)\n',2*back_region_subtract(k,1),2*back_region_subtract(k,2),2*back_region_subtract(k,3));
 end
end
fclose(fid);
