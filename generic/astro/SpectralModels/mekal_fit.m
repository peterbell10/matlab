function [T,Z,z,fact]=mekal_fit(obs_Counts,obs_error,To,Z,z,rsp,range,fit,nH)
% mekal_fit: Fit a Mekal spectral model to data
%
% [T,Z,z,fact]=mekal_fit(obs_Counts,obs_error,To,Z,z,rsp,range,fit,nH);
%
% ARGUMENTS
%  obs_Counts	The counts in each channel
%  obs_error	The error in the counts
%  To		Initial guess at T
%  Z		Initial guess at Metalicity
%  z		Initial guess at Redshift
%  rsp		OGIP RMF structure.
%  range	Index into obs_Counts and obs_error to select those to use in
%		the fit.
%  fit		A bit-switch which determines what is free and what is fixed:
%  		 fit(1) sets To free (1) or fixed (0)
%		 fit(2) sets Z
%		 fit(3) sets z
%  nH		The cumulative column density of H  [1e22 cm^-2]
%
%
% RETURNS
%  T		Temperature
%  Z		Metalicity
%  z		Redshift
%  fact		Factor
%
% REQUIRES
%  fminsearch mekal_fit mekal_diff mekal wabs
%
% SEE ALSO
%  mekal

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 11 07 First Version
%  00 11 17 Incorporated into the main body of library functions.
%  00 12 21 Added nH absorbtion (wabs)
%	Modified to minimise Chi^2
%  01 04 27 Added comments
%	RSP is now set by a call to ExtractRMF()
%
% COMPATIBILITY: Matlab, Octave (requires fminsearch)

H=1; % We'll let CEm take care of the normalisation
fact=1; % log10(CEm)
Abund=ones(15,1); % In solar units

% *** Get the Response matrix
[RSP,Energies,Channels]=ExtractRMF(rsp);

obs_E_bins=[rsp.ENERG_LO;rsp.ENERG_HI(end)];

Fit_bits=(1*fit(3) + 2*fit(2) + 4*fit(1));
switch Fit_bits
 case 7
  X=[To,Z,z];
  Suppl=[];
 case 6
  X=[To,Z];
  Suppl=[z];
 case 5
  X=[To,z];
  Suppl=[Z];
 case 4
  X=[To];
  Suppl=[Z,z];
 case 3
  X=[Z,z];
  Suppl=[To];
 case 2
  X=[Z];
  Suppl=[To,z];
 case 1
  X=[z];
  Suppl=[To,Z];
 otherwise
  error('oops')
end

X=[fact,X];
Suppl=[Suppl,nH];
Options=optimset('TolX',1e-3); % Set the tolerance on X (T, Z, z) to 2 decimals
Options=optimset(Options,'TolFun',1e-3); % Set the tolerance on mekal_diff.
X=fminsearch('mekal_diff',X,Options,obs_E_bins,obs_Counts,obs_error,Abund,RSP',range,Fit_bits,Suppl);

switch Fit_bits
 case 7
  T=X(2);
  Z=X(3);
  z=X(4);
 case 6
  T=X(2);
  Z=X(3);
 case 5
  T=X(2);
  z=X(3);
 case 4
  T=X(2);
 case 3
  Z=X(2);
  z=X(3);
 case 2
  Z=X(2);
 case 1
  z=X(2);
 otherwise
  error('oops')
end
fact=X(1);

% End of main

