function diff=mekal_diff(X,obs_E_bins,obs_Counts,obs_Error,Abund,RespMat,range,Fit_bits,Suppl)

fact=X(1);
switch Fit_bits
 case 7
  T=X(2);
  Z=X(3);
  z=X(4);
 case 6
  T=X(2);
  Z=X(3);
  z=Suppl(1);
 case 5
  T=X(2);
  Z=Suppl(1);
  z=X(3);
 case 4
  T=X(2);
  Z=Suppl(1);
  z=Suppl(2);
 case 3
  T=Suppl(1);
  Z=X(2);
  z=X(3);
 case 2
  T=Suppl(1);
  Z=X(2);
  z=Suppl(2);
 case 1
  T=Suppl(1);
  Z=Suppl(2);
  z=X(2);
 otherwise
  error('oops')
end % Switch

nH=Suppl(end);
Abund(3:end)=Abund(3:end)*10.^Z;

%Rho_fixed=1e-28;
%amu=1.6605402e-24;      %g
%H=((3/4)*Rho_fixed)/amu; % This is probably not right
H=1;
obs_E_bins=obs_E_bins*(z+1);
[flux,ed]=mekal(obs_E_bins,10^fact,H,T,Abund);
%flux(1)=flux(3);
%flux(2)=flux(3);

% Redshift the spectrum
%flux=redshift_spec(flux,obs_E_bins,z);
obs_E_bins=obs_E_bins/(z+1);

% The flux is in phot/cm**2/s/keV, but the bin sizes are not all the same
% Correct for this
flux=flux.*(obs_E_bins(2:end)-obs_E_bins(1:end-1));

% Absorbtion in the galactic disk/halo
trans=wabs(obs_E_bins,nH,ones(1,17),0);
flux=flux.*trans;

% Calculate the observed spec
spec=RespMat*flux;
diff=sum(( (obs_Counts(range)-spec(range))./obs_Error(range) ).^2);
%size(obs_Counts(range)), size(spec(range))
%diff=sum(abs( (obs_Counts(range)-spec(range)) ) );
