function [spec,bins]=CreateMekalSpec(TvsRho,Ts,Rhos,rsp,Vol,Dist,Abund,z,nH)
%
% [spec,bins]=CreateMekalSpec(TvsRho,Ts,Rhos,rsp,Vol,Dist,Abund,z,nH);
%
% TvsRho is an (NxM) array.
%
% Ts is a vector of length N. It contains the temperatures in units of K.
%
% Rhos is a vector of length M. It contains the densities in units of g/cm^3.
%
% rsp is the Response Matrix structure, as returned by reading
%     in the response file using FITSREAD.
%
% Vol is a scalar, units kpc^3, which is the volume of the
%      emitting region.
%
% Dist is a scaler, units Mpc, which is the distance to the
%      emitting region.
%
% Abund is a vector of length 15, containing the relative abundances of
%  H, He, C, N, O, Ne, Na, Mg, Al, Si, S, Ar, Ca, Fe, Ni
%
% z is the redshift of.
%
% nH is the galactic Hydrogen column density, in 10^22 cm^{-2}
%
% For each combination of Ts(i) and Rhos(j), mekal will be
% called to produce a spectrum which will be weighted by TvsRho(i,j) and
% added to a cumulative spectrum.
%
% spec will be a vector of length, NChannels, which is derived from rsp.
%      It has units cts/s/bin.
%
% bins is a vector of length, NEnergies + 1, giving the edges of the 
%      NEnergies energy bins used in the computation bins.
%
% See also: FITSREAD, MEKAL

% LICENSE
%  Copyright Eric Tittley 2000
%  See the Gnu GPL license.
%  Essentially, free to use.  Free to modify.  But cannot be re-created in
%  whole or in part in anything sold or traded for something of worth.

% HISTORY
% 00 11 16 First version, Eric Tittley. Based on a script by Eric Tittley.
%
% 00 11 17 Minor modifications to the comments.
%
% 00 11 17 Made a correction to multiply the output from mekal by the bin
% 	sizes.  This is required since the output of mekal has units
% 	photons/cm^2/s/keV
%
% 00 11 22 Changed the inputs from log10_T and log10_Rho to T and Rho.
%
% 00 12 21 Added nH absorbtion.
%
% 01 04 18 Various optimizations which speed things up by about %20.
%	Final optimization (using scaled mekal specs) dropped the time
%	to execute by %60. 


% *** Get the Response matrix
[RSP,Energies,Channels]=ExtractRMF(rsp);

%Units
hdr.h100=0.6;
Units

% The volume/distance scale factor
CEm=Vol*kpc^3 / ((Dist*1e6)^2) /1e50; % 1e50 cm**3 / pc**2

bins=[rsp.ENERG_LO;rsp.ENERG_HI(end)];

%blueshift the bins up
bins=bins*(z+1);

Nenergy_bins=length(bins);
flux=zeros(length(Energies),1);
[NT,Nrho]=size(TvsRho);

Ts=Ts/keV; % keV

% This is the time-consuming loop, going as Nrho x NT
% with the call to mekal sucking up the majority of the time.
% A POSSIBLE OPTIMIZATION:  If the spectrum for a given T is
% a simple scaling with H, then we could calculate the spectrum
% for a single density and temperature and then scale it for
% all other densities at that temperature.
% As it turns out, mekal scales simply with Rho^2
%%%% --- EXACT --- 
%if(Nrho>5), disp('Accumulating flux'), end
%for i=1:Nrho
%% if(Nrho>5), disp(i), end
% H=((3/4)*Rhos(i))/amu; % This is probably not right
% good=find(TvsRho(:,i)>0);
% T=Ts(good); % keV
% TvsRho_tmp=TvsRho(good,i);
% for j=1:length(good)
%  [flux_dummy,ed]=mekal(bins,CEm,H,T(j),Abund);
%  flux=flux+TvsRho_tmp(j)*flux_dummy;
% end
%end
%%%% --- Approximate (but pretty much exact and faster FOR ARRAYS) ---
if(Nrho>5), disp('Accumulating flux'), end
% for each temperature, determine one mekal spec at one density and
% scale the rest
Rho_fixed=1e-28;
H=((3/4)*Rho_fixed)/amu; % This is probably not right
for j=1:NT
 good=find(TvsRho(j,:)>0);
 if(length(good)>0)
%  disp([CEm,H,Ts(j),Abund])
  [flux_scalable,ed]=mekal(bins,CEm,H,Ts(j),Abund);
  scale=TvsRho(j,good).*(Rhos(good)/Rho_fixed).^2;
  flux=flux+flux_scalable*sum(scale);
  %%% End Loop Method
 end
end

% Redshift the spectrum
%flux=redshift_spec(flux,bins,z);

% Redshift the bins
bins=bins/(z+1);

% The flux is in phot/cm**2/s/keV, but the bin sizes are not all the same
% Correct for this
flux=flux.*(rsp.ENERG_HI-rsp.ENERG_LO);  % Flux is now phot/cm^2/s/bin

% Absorbtion in the galactic disk/halo
trans=wabs(bins,nH,ones(1,17),0);
flux=flux.*trans;

% Convert flux in energy bins to a channel response
spec=RSP'*flux; % flux is now phot/s/bin
