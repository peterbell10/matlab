% Astronomy Specific
%
% SUBTOPICS
%
% DSS		   Deep Sky Survey input and maipulation.
% Lya              Tools for generating fake Lya forest transmission spectra
% Photometry	   Quick and dirty photometry tools.
% Regions	   DS9-style regions manipulation.
% SBIG		   Input of SBIG-format CCD files (not FITS).
% spectra	   Tools for analysing spectra.
% SpectralModels   Create artificial x-ray spectra.
% xray             Routines for processing x-ray data
%
% FUNCTIONS
%
% determine_B          The magnetic field of a cluster from the radio and x-ray fluxes
% est_V_Alfven         Alven velocity in a plasma
% flux2lumin           Converts flux to luminosity for a given z
% Get_Limits_Manually  Manually get the limits of an RA-Dec square on an image
% IMF                  Initial Mass Function for stars
% LT_relation          The Luminosity-Temperature relation for galaxy clusters
