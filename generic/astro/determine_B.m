function B = determine_B(F_x,alpha_x,low_e,high_e,S,nu);
% determine_B: The magnetic field of a cluster from the radio and x-ray fluxes.
%
% B = determine_B(F_x,alpha_x,low_e,high_e,S,nu);
%
% ARGUMENTS
%  F_x		X-ray flux (ergs/cm^2/s)
%  alpha_x	Photon index
%  low_e	Low energy of the X-ray flux (keV)
%  high_e	High energy of the X-ray flux (keV)
%  S		Radio output (Jy)
%  nu		Frequency of the radio output (Hz)
%
% RETURNS
%  B		The magnetic field (gauss)
%
% SEE ALSO

% AUTHOR: Danny Hudson
%
% HISTORY
%  020730 First version
%  071029 Modified comments
%
% COMPATIBILITY: Matlab, Octave

q = 4.803206815e-10; %electron charge in esu
m = 9.109389754e-28; %electron mass in grams
c = 2.99792458e10; %speed of light in cm/s
ro = 2.8179409238e-13; % classical electron radius in cm
h = 6.26075540e-27; %planck's constant in ergs*s
k = 1.38065812e-16; %boltzmann's constant in ergs/K
keV2ergs = 1.6021773349e-09; % keV/erg

% alpha_r, the spectral index, is equal to the alpha_x (or Gamma_x) -1
% alpha_r = (p-1)/2, where p is the distribution index, therefore
% p = 2*alpha_r+1 = 2*(alpha_x-1)+1 = 2*alpha_x-1
p = 2*(alpha_x-1) + 1; % the electron distribution index 

% From Henriksen 1998 (PASJ 50, 389-398)
%
% F_c = K_2/K_1*(kT)^((p+5)/2)*<B_p>^(-(p+1)/2)*A*int(E^(-(p-1)/2)dE)
% so
% <B> = [F_c*K_1/K_2*(kT)^(-(p+5)/2)/A/int(E^(-(p-1)/2)dE)]^((p+1)/2)

% where K_1 = [(sqrt(3)*q^3)/(m*c^2*(p+1))]* ERF(p/4 + 19/12) * ERF(p/4 - 1/12) * [(2*pi*m*c)/3q]^(-(p-1)/2)

k_1 = sqrt(3)*q^3/(m*c^2*(p+1))*gamma(p/4+19/12)*gamma(p/4-1/12)*(2*pi*m*c/(3*q))^(-(p-1)/2);

% and K_2 = (8*pi*r_o^2*D(p))/(h^3*c^2)
% where D(p) = J(p) * Erf((p+5)/2) * PSI((p+5)/2)
% where PSI(p) = sum(n=1..infinity) n^(-p) 
% where J(p) = 2^(p+3)*[(p^2 + 4*p + 11)/((p+3)^2*(p+5)*(p+1))[

J = 2^(p+3)*((p^2+4*p+11)/((p+3)^2*(p+5)*(p+1)));
psi = sum([1:10000].^(-((p+5)/2)));
D = J*gamma((p+5)/2)*psi;
k_2 = 8*pi^2*ro^2*D/(h^3*c^2);

% The integral of E^(-(p-1)/2) dE = [E_HI^(-(p-1)/2+1) - E_LO^(-(p-1)/2+1)]/(-(p-1)/2 + 1) 
I = ((high_e*keV2ergs)^(((-(p-1))/2)+1) - ...
			(low_e*keV2ergs)^(((-(p-1))/2)+1))/(((-(p-1))/2)+1);  % the keV2ergs factor is to convert from keV to ergs

kT = (k*2.7)^((p+5)/2);	% The Temperature of the CMBR is 2.7 K

A = (S*1e-23)/(nu^(-(alpha_x-1))); % S = A*nu^(-alpha_r), so A = S/nu^(-alpha_r)

B = (k_2/k_1*kT*A*I/F_x)^(2/(p+1));

%fprintf('\nThe magnetic field is estimated as %g uG\n',B*1e6);
