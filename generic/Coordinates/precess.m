% precess: Precess coordinates from one epoch to another.
%
% [RA_new,DEC_new]=precess(RA,DEC,epoch,epoch_new)
%
% ARGUMENTS
%  RA		RA to precess [hours]    (vector)
%  DEC		DEC to precess [degrees] (vector)
%  epoch	Epoch of (RA,DEC)        (scalar)
%  epoch_new	Epoch to precess to      (scalar)
%
% RETURNS
%  RA_new	RA precessed to epoch_new [hours]
%  DEC_new	DEC precessed to epoch_new [degrees]
%
% NOTES
%  Uses an approximation that is good for approximately 50 years
%  around the epoch 2000
%
%  R. M. Green, Spherical Astronomy, Cambridge University Press, 1985
%
% REQUIRES
%
% SEE ALSO
%  dms2deg

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 03 28 First version
%
% COMPATIBILITY: Matlab, Octave

function [RA_new,DEC_new]=precess(RA,DEC,epoch,epoch_new)

% The tolerance on comparisons
tol=1e-5;

% The epochs must be scalars
epoch=epoch(1);
epoch_new=epoch_new(1);

% Convert the input values to radians
a=RA/12*pi;
d=DEC/180*pi;

%First, precess the coordinates to J2000.0
% T is in units of Julian Centuries, which are 36 525 days.
% The first incorrect assumption of the day: 1 year = 365.35 days
T=(epoch/100 - 20);
if(abs(T)>tol) % i.e., if the input was not already in J2000
 %Note: these coefficients are in radians
 M = 2.236172e-2*T + 6.772e-6*T^2;
 N = 9.717173e-3*T - 2.077e-6*T^2;
 a_m = a - 0.5*(M + N*(sin(a).*tan(d)));
 d_m = d - 0.5*N*cos(a_m);
 a_o = a - M - N*(sin(a_m).*tan(d_m));
 d_o = d - N*cos(a_m);

 a = a_o;
 d = d_o;
end

% Now, precess from J2000 to the epoch requested
T=(epoch_new/100 - 20);
if(abs(T)>tol) % i.e., if the requested epoch was not already J2000
 %Note: these coefficients are in radians
 M = 2.236172e-2*T + 6.772e-6*T^2;
 N = 9.717173e-3*T - 2.077e-6*T^2;
 a_m = a - 0.5*(M + N*(sin(a).*tan(d)));
 d_m = d - 0.5*N*cos(a_m);
 a_o = a - M - N*(sin(a_m).*tan(d_m));
 d_o = d - N*cos(a_m);
end

%convert back to hours and degrees from radians
RA_new=a_o/pi*12;
DEC_new=d_o/pi*180;
