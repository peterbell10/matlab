% Coordinate conversion, manipulation, and pretty-printing
%
% deg2dms	Converts degrees (or hours) into the vector
%		[deg, mn, sec] (or [H,M,S]).
%
% dms2deg	Converts the 3 components, deg,min,sec, into
%		the scalar, degrees. Also works for H,M,S.
%
% DEC2str	Pretty-print a declination.
%
% RA2str	Pretty-print a right ascension.
%
% RA_Dec_to_Gal	Convert RA and Dec to galactic coordinates.
%
% DistanceBetweenPoints Find the angular distance between two points on a sphere
%
% precess	Precess coordinates from one epoch to another.
