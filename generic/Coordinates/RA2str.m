% RA2str: Pretty-print a right ascension.
%
% string=RA2str(ra,[depth])
%
% ARGUMENTS
%  ra	Right Ascension [hours]
%
%  depth is the default precision.
%	1 => Just the hours
%	2 => Hours and minutes
%	3 => Hours, minutes, seconds
%	The default is 3
%
% REQUIRES
%
% SEE ALSO
%  deg2dms, RA2str, DEC2str

% AUTHOR: Eric Tittley
%
% HISTORY
%  01 03 01 Mature code
%  01 03 01 Changed 'sec' to 'second' since 'sec' is a
% 	Matlab intrinsic.
%  07 11 05 Modified comments
%
% COMPATIBILITY: Matlab, Octave

function string=RA2str(ra,depth)

if(nargin==1), depth=3; end
tol=1e-6;
string=char(zeros(length(ra),12));
rasign=sign(ra);
raabs=abs(ra);
rafrac=rem(raabs,1);
raint =floor(raabs);
for i=1:length(ra)
 hr=raint(i)*rasign(i);
 if(hr<0.01 & hr>-0.01), hr=0; end
 minute=floor(rafrac(i)*60);
 if(minute<0.01), minute=0; end
 if(abs(minute-60)<tol), hr=hr+rasign(i); minute=0; end
 second=round(rem(rafrac(i)*60,1)*60);
 if(second<0.01), second=0; end
 if(abs(second-60)<tol), minute=minute+1; second=0; end
 dummy=[int2str(hr)];
 if(round(depth)>1), dummy=[dummy,':',int2str_zeropad(minute,2)]; end
 if(round(depth)>2), dummy=[dummy,':',int2str_zeropad(second,2)]; end
 string(i,1:length(dummy))=dummy;
end
