function [A_mean,A_std]=bindata_2d(x,y,v,bins_x,bins_y)
% bindata_2d: Bin data, returning the mean and standard deviation within bins
%
% [A_mean,A_std]=bindata_2d(x,y,v,bins_x,bins_y)
%
% ARGUMENTS
%  x	The x-position of the data
%  y	The y-position of the data
%  v	The values at (x,y)
%  bins_x	The bin edges in the x-dir (M+1 elements)
%  bins_y	The bin edges in the y-dir (N+1 elements)
%
% RETURNS
%  A_mean	The mean values of the v's in the bins (NxM)
%  A_std	The standard deviations of the v's in the bins (NxM)
%
% REQUIRES
%
% SEE ALSO
%  bindata_1d, hist2d

% AUTHOR: Eric Tittley
%
% HISTORY
%  080710 First version
%
% COMPATIBILITY: Matlab, Octave

NumBins_x=length(bins_x)-1;
NumBins_y=length(bins_y)-1;

% Assign space for the arrays
A_mean=zeros(NumBins_y,NumBins_x);
A_std=A_mean;

% Get the histogram, with indices
[N,X,Y,I]=hist2d(x,bins_x,y,bins_y);

% Now get the statistics for each array cell
for j=1:NumBins_y
 for i=1:NumBins_x
  if(~isempty(I{i,j}))
   A_mean(i,j)=mean(v(I{i,j}));
   A_std(i,j) = std(v(I{i,j}));
  end
 end
end
