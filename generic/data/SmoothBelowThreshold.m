% SmoothBelowThreshold: For 1, 2, or 3-D data, smooth values below a threshold
%
% img=SmoothBelowThreshold(img_o,radius,method,threshold)
%
% ARGUMENTS
%  img_o	The input vector/image/array to be smoothed
%  radius	The radius, in pixels, over which to smooth
%  method	A string indicating which method to use which may be one of:
%   Method:			 Operates in:	 Radius:
%   'blackman'  		 Frequency space tau parameter
%   'gaussian'  		 Real space	 HWHM
%   'hamming'			 Frequency space tau parameter
%   'hann'			 Frequency space tau parameter
%   'high-frequency cut-off'	 Frequency space cut-off in pixels
%   'lanczos'			 Frequency space tau parameter
%   'low-frequency cut-off'	 Frequency space cut-off in pixels
%   'tophat'			 real space	 radius
%   The method may be selected by the first unique string that starts the label
%  threshold    The values of img_o below which to smooth
%
% RETURNS
%  The smoothed data
%
% NOTES
%  Smoothing works for 1, 2, or 3-D arrays.
%  For 3-D arrays, the dimensions must be square.
%
%  3-D smoothing doesn't work right if the array isn't square
%
% SEE ALSO:
%  smooth (From Matlab's curvefit toolbox)
%  smooth3 (Matlab's specgraph function)
%  Smooth
%  fft2, fft3

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave
%
% LICENCE
%  Copyright (C) Eric Tittley
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
%
% BUGS
%  3-D smoothing doesn't work right if the array isn't square

function img=SmoothBelowThreshold(img_o,radius,method,threshold)

if(nargin~=4)
 error('syntax: img=SmoothBelowThreshold(img_o,radius,method,threshold)')
end

NDim=ndims(img_o);

DataToSmooth    = img_o;
DataToNotSmooth = 0*img_o;
mask=find(img_o>=threshold); % should this be >= ?
DataToSmooth(mask)    = threshold;
DataToNotSmooth(mask) = img_o(mask)-threshold;
clear mask

% Smooth the data below the threshold
img=Smooth(DataToSmooth,radius,method);
clear DataToSmooth

% Recombine
img = img+DataToNotSmooth;
