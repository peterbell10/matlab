function ChiSqr=chisqr(obs,fit,sigma)
% chisqr: The Chi^2 error of a fit to data with known errors.
%
% ChiSqr=chisqr(obs,fit,sigma);
%
% ARGUMENTS
%  obs		The set of observations,
%  fit		The model
%  sigma	Standard deviations of the obs
%
% RETURNS
%  sum( ((obs-fit)./sigma).^2 )
%
% REQUIRES
%
% SEE ALSO

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

ChiSqr=sum( ((obs-fit)./sigma).^2 );
