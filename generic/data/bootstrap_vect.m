function [mean_coef,std_coef]=bootstrap_vect(x,y,vector)
% boostrap_vect: Bootstrap a fit to linear combinations of vectors.
%
% [mean_coef,std_coef]=bootstrap_vect(x,y,vector)
%
% ARGUMENTS
%  x,y	The data to fit to
%  vector	A set of vectors
%
% RETURNS
%  The coefficients mean_coef+-std_coef
%
% NOTES
%  BOOTSTRAP(x,y,[F1(x) F2(x) ...]) fits Y to the closest linear combination
%  of the vectors F1(x), F2(x), etc.
%  This is not yet fully implemented.
%  Indeed, lots of other things can be done, which haven't been implemented)
%
% REQUIRES
%
% SEE ALSO
%  bootstrap

% AUTHOR: Eric Tittley
%
% HISTORY
%  071217 Regularised comments.
%
% COMPATIBILITY: Matlab, Octave

len=length(x);
%num=ceil(len/2);
num=len;
if(nargin==3)
 Ntrials=1000;
else
 Ntrials=len;
 if(Ntrials<100), Ntrials=100; end
end 

disp(['Bootstrapping with ',int2str(num),' of ',int2str(len),', ',int2str(Ntrials),' times.'])
if(nargin==2)
 for i=1:Ntrials
  mask=ceil(rand([1,num])*len);
  [pout(:,i),yfit]=curvefit2(x(mask),y(mask));
 end
elseif(nargin==3)
 for i=1:Ntrials
  mask=ceil(rand([1,num])*len);
  [pout(:,i),yfit]=curvefit2(x(mask),y(mask),vector(mask,:));
 end
end

mean_coef=mean(pout');
std_coef=std(pout');
