% bootstrap: Bootstraps the fit to y(x) of a non-linear function 'F'
%
% [mean_coef,std_coef]=bootstrap(F,coef0,x,y,sigmas,iters,[MaxFunEvals])
%
% ARGUMENTS
%  F		The function to be minimised. 	        [handle]
%  coef0	Initial guess at the fit parameters. 	[vector length M]
%  x		Independent variable.			[vector length N]
%  y		Dependent variable.			[vector length N]
%  sigma	The errors on y, if any.		[vector length N]
%  iters	Number of bootstrap iterations.		[scalar]
%  MaxFunEvals	(See fminsearch.)			[scalar] (optional)
%
% RETURNS
%  coef		The fit parameters.			[vector length M]
%  std_coef	The standard deviation of the fit parameters.	[vector length M]
%
% Bootstraps the fit to y(x) of a non-linear function 'F'
% which is passed as a string.
% Required input is the initial guess to the coefficients, coef0.
% The vectors of x and y(x) observed are required and passed to the
% function of the observed and calculated, which is to be minimized.
% Iters is the number of times to 'bootstrap'. 
% Returned is the coefficients mean_coef+-std_coef.
% std_coef is the raison d'etre for the bootstrap.
%
% MaxFunEvals is an optional parameter which is passed to FMINS as OPTION(14)
%
% Generally, suppose we have some data y over a span x which we want
% to fit to a function F(x) where F(x) has a set of parameters, P
% so it is called by F(P,x).
% There may or may not be errors associated y.  If so, they can be
% used in the minimization process, (e.g. Chi-square).
% The difference between values of y calculated using the estimate for
% the parameters, P, and the actual observed y values is quantified by a
% function Fdiff(P,x,y,sigma).  This is the function to be minimized.
% Fdiff may return the sum of the differences, the sum of the squares,
% the sum of the absolute differences, the Chi-squared, whatever you like.
% You would call bootstrap with BOOTSTRAP(@Fdiff,P,x,y,sigma,200)
%
% SEE ALSO
%  bootstrap_vect

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 06 21 Changed to use fminsearch instead of fmins
%  05 12 01 Tidied up comments.
%
% COMPATIBILITY: Matlab

function [mean_coef,std_coef]=bootstrap(F,coef0,x,y,sigma,iters,MaxFunEvals)

if nargin<7
 MaxFunEvals=1000;
end
tol=1e-10;

if(~isa(F,'function_handle'))
 error('F must be a function handle')
end

len=length(x);

options=optimset('Display','off',...
                 'TolFun',tol,...
                 'TolX',tol,...
                 'MaxFunEvals',MaxFunEvals);

coef=coef0;
for i=1:iters
 mask=ceil(rand([1,len])*len);
 coef=fminsearch(F,coef,options,x(mask),y(mask),sigma(mask));
 pout(i,:)=coef;
end

mean_coef=fminsearch(F,coef0,options,x,y,sigma);
std_coef=std(pout);
