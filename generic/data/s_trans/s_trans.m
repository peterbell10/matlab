function s_mat=s_trans(vector)
%S_MAT=S_TRANS(VECTOR)
% Returns the Stockwell Transform, S_MAT, of the time-series, VECTOR.

vector_fft=fft(vector);
vector_fft=[vector_fft,vector_fft];
n=length(vector);
s_mat(1,:)=mean(vector)*(1&[1:n]);
for k=1:ceil(n/2+1)-1
 s_mat(k+1,:)=ifft(vector_fft(n+1-k:2*n-k).*g_window(n,k));
end
