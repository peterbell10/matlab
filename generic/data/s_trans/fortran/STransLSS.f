      SUBROUTINE STransLSS(x_vector,length,k_vector)
c This routine stores into K_VECTOR of length LENGTH the S_transform
c of X_VECTOR of length LENGTH centred symetrically about X_VECTOR(0)

cllll+ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

      parameter(MAX_VECT=1024)
      parameter(TwoPiSqrd=19.739208802)

      integer length
      real xvector(0:length-1),xdouble(0:MAX_VECT-1),kvector(0:length-1)
      real sum

      sum=0.
      do 1 n=0,length-1
1      sum=sum+xvector(n)
      kvector(0)=sum/length

      do 2 n=0,length-1
       xdouble(n)=xvector(n)
2      xdouble(2*length-1-n)=xvector(n)

      CALL fft(xdouble,length,1)

      do 3 n=1,length-1
       kvector(n)=0.
       do 3 m=0,n
3       kvector(n)=kvector(n)+xdouble(m+n)
     &             *exp(-TwoPiSqrd*(real(m)/real(n))**2)   

      RETURN
      end
