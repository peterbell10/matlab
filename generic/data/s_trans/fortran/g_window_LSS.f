      SUBROUTINE g_window_LSS(length,freq,
     &                        vector)
c This routine stores into VECTOR of length LENGTH the contents
c exp(-2*(pi*x/FREQ)^2) 
c where x=[0,1,2,..,LENGTH-1]

cllll+ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

      integer length
      real vector(0:length-1)
      real freq
      parameter(PI=3.141592654)

      freq=-2*(PI/freq)**2

      do 1 i=0,length-1
1      vector(i)=exp(freq*real(i**2))

      RETURN
      end
