function period=periodogram2(data)
% periodogram2: Find periods between the events in a series
%
% period=periodogram2(data);
%
% ARGUMENTS
%  data		The time/position of the events
%
% RETURNS
%  The N*(N-1)/2 periods between events
%
% USAGE
%  X=rand(100,1);
%  P=periodogram2(X);
%  hist(P,100);
%
% REQUIRES
%
% SEE ALSO
%  fft, periodogram

% AUTHOR: Eric Tittley
%
% HISTORY
%  00 06 12 First version
%  00 07 17 Renamed periodogram2 since there is a periodogram in the MATLAB
%           signal processing toolbox
%  07 11 17 Regularised comments.
%
% COMPATIBILITY: Matlab, Octave

N=length(data);

% There are N*(N-1)/2 periods in a dataset with N events
period=zeros(N*(N-1)/2,1); 

count=0;
for i=1:N-1
 for j=i+1:N
  count=count+1;
  period(count)=abs(data(j)-data(i));
 end
end
