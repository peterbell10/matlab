% randomdraw: Draw N non-repeating elements from a sample of M elements (M>N).
%
% list=randomdraw(NtoDrawFrom,NtoDraw)
%
% ARGUMENTS
%  NtoDrawFrom	The number of elements in the sample from which to draw.
%  NtoDraw	The number of elements to draw.
%
% RETURNS
%  list		The elements drawn (vector of length NtoDraw).
%
% REQUIRES
%
% SEE ALSO
%  randdist, rand, randn, random (Matlab), rande (Octave), randg, randp (Octave)

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab, Octave

function list=randomdraw(NtoDrawFrom,NtoDraw)

% WAS
% list=0*[1:NtoDraw];
% NDrawn=0;
% while NDrawn<NtoDraw
%  R=ceil(rand(1)*NtoDrawFrom);
%  if(sum(list==R)==0)
%   NDrawn=NDrawn+1;
%   list(NDrawn)=R;
%  end
% end
% NOW. Scales as N, instead of N^2, and takes less than a year for N=2^26.
seed=rand(NtoDrawFrom,1);
[Y,index]=sort(seed);
clear seed Y
index=index(1:NtoDraw);
UnshuffledList=[1:NtoDrawFrom];
list=UnshuffledList(index);
