function skew=skewness(x)
% skewness: The skewness of a distribution.
%
% [skew,err]=skewness(x);
%
% ARGUMENTS
%  x	The distribution
%
% RETURNS
%  skew	The skewness
%
% NOTES
%  If x is an (m x n) array, then skew will be a row vector of length n.
%
% REQUIRES
%
% SEE ALSO
%  mean, std, kurtosis

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Added comments
%	Removed return element 'err' which was non-sensical
%
% COMPATIBILITY: Matlab, Octave

x_mean=mean(x);
x_std =std(x);

[M,N]=size(x);

if(N>1)
 skew=zeros(1,N);
 for i=1:N
  skew(i)=mean( ( (x(:,i)-x_mean(i))/x_std(i) ).^3 );
 end
else
 skew=mean( ( (x-x_mean)/x_std ).^3 );
end
