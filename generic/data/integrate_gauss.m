function [val,bp,wf]=integrate_gauss(fun,a,b,n)
% integrate_gauss: Integrate a function using Gauss's rule
%
% [val,bp,wf]=integrate_gauss(fun,a,b,n)
%
% ARGUMENTS
%  fun		Function to be integrated
%  a,b		Integration limits
%  n		Order of formula (default is 20) [optional]
%
% RETURNS
%  val		The integral
%  bp,wf	Gauss base points and weight factors
%
% NOTES
%  Integration of a function using an n-point Gauss rule is exact for a
%  polynomial of degree 2*n-1.
%  Concepts on page 93 of 'Methods of Numerical Integration' by
%  Philip Davis and Philip Rabinowitz yield the base points and weight factors.
%
% REQUIRES
%
% SEE ALSO
%  integrate_trap

% AUTHOR: Dave Holmgren (?) & Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Regularised comments
%
% COMPATIBILITY: Matlab, Octave

if (nargin < 4)
 n=20;
end

u=(1:n-1)./sqrt((2*(1:n-1)).^2-1);

[vc,bp]=eig(diag(u,-1)+diag(u,1));

[bp,k]=sort(diag(bp));

wf=2*vc(1,k)'.^2;
x=(a+b)/2+(b-a)/2*bp;

f=feval(fun,x)*(b-a)/2;
val=wf(:)'*f(:);
