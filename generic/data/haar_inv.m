function vector=haar_inv(coef)
% haar_inv: Inverse Haar transform
%
% vector=harr_inv(coef)
%
% ARGUMENTS
%  coef		The haar transform coefficients
%
% RETURNS
%  Inverse transformed data
%
% NOTES
%
% REQUIRES
%
% SEE ALSO
%  haar

% AUTHOR: Eric Tittley
%
% HISTORY
%  94 09 01 First version
%  07 12 17 Regularised comments
%
% COMPATIBILITY: Matlab, Octave
%
% LICENSE
%  Copyright Eric Tittley
%  The GPL applies to the contents of this file.

if nargin<1 error('Too few arguments passed to haar'); end
if nargin>1 error('Too many arguments passed to haar'); end
n=length(coef);

k=0;
while(n>2^k) k=k+1; end
pow=k;
if(n~=2^k) error('Length of COEF must be a power of 2 in call to HAAR_INV'); end

coef=coef(:);

vector=coef;
for k=1:pow
 avg=vector(1:2^(k-1));
 diff=vector(2^(k-1)+1:2^k);
 for l=1:2^(k-1)
  vector(2*l-1)=avg(l)+diff(l);
  vector(2*l)=avg(l)-diff(l);
 end
end
 
