function w = wavmat

%
%       Written by Colm Mulcahy (colm@spelman.edu), last 
%       updated 31st Dec 1996. 
% 
%       constructs the overall wavelet transform matrix
% 
 
 
s=256;
num=8;

m = sparse(eye(s));
a = sparse(eye(s));

i = s / 2^num;
while i <= s/2;
        a(1:2*i,1:2*i) = sparse(indmat(2*i));
        m=a*m;
        i=2*i;
end

w=sparse(m);


