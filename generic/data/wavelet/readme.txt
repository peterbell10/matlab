 
Written by Colm Mulcahy (colm@spelman.edu), 31st December 1996.

You need these Matlab m-files to do basic (normalized) Haar
wavelet image compression to a 256 by 256 greyscale image Y:


compresn.m  (does the compression, displays before and after)
wavmat.m    (constructs the product transform matrix W = A_1...A_8)
indmat.m    (constructs the individual transform matrices A_i)

compresn calls wavmat, which in turn calls indmat.


all you need to do is

1       get an image  Y  of the right size
        (e.g., use a built-in image by typing:
        >>  load gatlin
        >>  Y = X(1:256,1:256);
        ) 
 
2       type:
        >>  compresn(Y,e);
        where e is any number of your choosing.
        (If e = 2, and Y is the gatlin subimage above,
        we get a "compression ratio" of 124 to 1)
         
 
 
(all available from http://www.spelman.edu/~colm/papers.html)


