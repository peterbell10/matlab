function a = indmat(n)

% 
%       Written by Colm Mulcahy (colm@spelman.edu), last
%       updated 31st Dec 1996. 
%
%       constructs individual wavelet transform matrices
%
 
b = [1; 1]/sqrt(2);
c = [1;-1]/sqrt(2);

while min(size(b)) < n/2
   b=[b, zeros(max(size(b)),min(size(b)));...
      zeros(max(size(b)),min(size(b))), b];
end

while min(size(c)) < n/2
   c=[c, zeros(max(size(c)),min(size(c)));...
      zeros(max(size(c)),min(size(c))), c];
end

a=[b,c];


