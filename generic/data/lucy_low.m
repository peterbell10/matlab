function reconstructed=lucy_low(observation,profile,niterations,low_filter_flag,damping,sigma,params)
% lucy_low: Spectral deconvolution using a damped Lucy-Richardson method with
%           suppressed high-frequency modification.
%
% No damping or filtering
%  reconstructed=lucy_low(observation,profile,niterations)
%  reconstructed=lucy_low(observation,profile,niterations,0)
% filtering, no damping
%  reconstructed=lucy_low(observation,profile,niterations,1)
% Damping, no filtering
%  reconstructed=lucy_low(observation,profile,niterations,0,damping,sigma)
% Damping and filtering, interactive
%  reconstructed=lucy_low(observation,profile,niterations,1,damping,sigma)
% Damping and filtering, non-interactive
%  reconstructed=lucy_low(observation,profile,niterations,1,damping,sigma,params)
% Filtering, no damping, non-interactive
%  reconstructed=lucy_low(observation,profile,niterations,1,0,0,params)
%
% ARGUMENTS
%  observation		The data to deconvolve
%  profile		The kernel by which to deconvolve
%  niterations		The number of deconvolution iterations to perform
%  low_filter_flag	Low-frequency filter: 0==no or 1==yes [optional]
%  damping		The damping parameter [optional]
%			Typically, it should be an EVEN integer 4 or greater.
%  sigma		A vector of the same length as OBSERVATION giving the
%			noise levels of the observed values at the various pixels
%  params		The low-frequency filtering parameters. [optional]
%			[amplitude, width, slope]
%
% RETURNS
%  The deconvolution of OBSERVATION using a DAMPED RICHARDSON-LUCY method.
%
% NOTES:
%  Solves the integral (sum, in discrete mathematics):
%  observation(x)=sum_over_y(reconstructed(y)*profile(x-y))
%  for the distribution, reconstructed.
%
% See L.B.Lucy, Astronomical Journal, V 79, p. 745
%
% REQUIRES
%  opt_filt
%
% SEE ALSO
%  lucy_damp, opt_filt

% AUTHOR: Eric Tittley
%
% HISTORY
%  010301 Mature version
%  071217 Regularised comments.

if (nargin<3 | nargin>7) error('Incorrect number of parameters passed to LUCY_LOW'), end
if nargin==3 low_filter_flag=0; end
dampflag=0;
if nargin>4
 if damping~=0 
  dampflag=1;
  sigma=sigma/2; % Empirically, this helps a great deal
 end
end


param_flag=0;
if nargin==7
 if length(params)==3
  param_flag=1;
  amp=params(1);
  width=params(2);
  slope=params(3);
 else error('Parameter PARAMS not a vector of length 3 in call to LUCY_LOW')
 end
end

profile=profile(:);

% Check to see that observation and profile are the same length
n=length(observation);
m=length(profile);
if m~=n error('Vectors observation and profile passed to lucy must be the same length'), end

% Find the maximum of the profile.  Note, this assumes that the
% maximum actually falls in the center of a pixel!  If it doesn't,
% a shift equal to the deviation will occur.  However, the
% maximum can occur at _any_ pixel.
profile=[profile;profile;profile];
mid=find(max(profile)==profile);
midm=mid(2);
profile_shift=profile(midm:midm+n-1);
profile_fft=fft(profile_shift);
Q=profile(midm-[0:n-1]);
Q_fft=fft(Q);

% A vector the same length as the input vectors, to be used in
% summations.
% template=[1:n];

if low_filter_flag
% Get the filter to be used to suppress high-frequency modifications
 if param_flag [clean,filter]=opt_filt(observation,amp,width,slope);
 else [clean,filter]=opt_filt(observation);
 end
end

reconstructed=observation;	% Initial guess

for iteration=1:niterations	% START OF MAIN LOOP

% Calculate what would be observed if the calculated distribution
% were convolved with the profile. (eq. 14 in the ref.)
% for k=1:n
%  implied_observation(k)=sum(reconstructed.*profile(midm+k-template));
% end
 implied_observation=real(ifft(fft(reconstructed).*profile_fft)); %Fourier Convolution for the above

% Calculate the next guess to the distribution
% (eq. 15 in the ref.)  Note: if implied_observation==observation,
% then the sum is only of the profile, which is unity, so no
% correction is made.
 if ~dampflag factor=observation./implied_observation;  %Undamped
 else factor=1+(observation-implied_observation)./implied_observation.*exp(-(sigma./(observation-implied_observation)).^damping); %Damped
 end

% for k=1:n
%  correction(k)=sum(profile(midm+template-k).*factor);
% end
 correction=real(ifft(fft(factor).*Q_fft)); %Fourier Convolution for above

 if low_filter_flag
% Filter the correction factor
  correction_tmp=real(ifft(fft(correction).*filter'));
  correction=correction_tmp+mean(correction-correction_tmp);
 end

 reconstructed=reconstructed.*correction;

end				%END OF MAIN LOOP
