% Very basic FoF method which can be used for clustering similar colours

function list=fof(points,linklength)

[M,N]=size(points);

list=zeros(M,M);

dummy=ones(M,1);

for i=1:M
 dist3=points-dummy*points(i,:);
 dist=sqrt(sum(dist3'.^2));
 list(i,:)=dist<linklength;
end

for i=1:M
 if(sum(list(i,:))>1)
  not_done_flag=1;
  while(not_done_flag==1)
   neighbours=find(list(i,:));
   all_neighbours=1&( sum(list(neighbours,:)) );
   if(length(find(all_neighbours))==length(neighbours))
    not_done_flag=0;
   end
   list(neighbours,:)=list(neighbours,:)*0;
   list(i,:)=all_neighbours;
  end
 end
end

%find non-empty lists
groups=find(sum(list'));
list=list(groups,:);
for i=1:length(groups)
 list(i,:)=list(i,:)*i;
end
list=sum(list);
