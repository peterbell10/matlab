N=10000;

load r_calcdens

%load A_calcdens

disp('double...')
[h_d,dn_d]=calcdens_d(r,32,0.1);
disp([' mean error in h : ',num2str(mean(abs(h_d-h_d_o)))]);
disp([' mean error in dn: ',num2str(mean(abs(dn_d-dn_d_o)))]);

disp('single...')
r_s=single(r);
[h_s,dn_s]=calcdens(r_s,32,0.1);
disp([' mean error in h : ',num2str(mean(abs(h_s-h_s_o)))]);
disp([' mean error in dn: ',num2str(mean(abs(dn_s-dn_s_o)))]);
