SI='MKS';
hdr.h100=0.70;
Units

% The dimensions of the data
NLos  = 256;
NR    = 256;

ibound = [0, NR-1];
jbound = [0, NLos-1];

% Lx and Ly are the x and y grid spacings.  I might have called them
% dRx, dRy.  x is along a LOS, y separates different LOSs
Lx  =   8.5997633e+20; % m
Rmax=   2.2015394e+23; % m

Ly = Rmax/NLos;

x = 0.;
y = 0.;
ix = 1;

phi = pi/2. - 0.3655;

% Redshift of the source
zred = 3.81;

Omega_v = 0.732;
Omega_m = 0.268;
Ho = 71.; %km s^-1 Mpc^-1
H = Ho * sqrt(Omega_m*(1. + zred)^3 + Omega_v); % km s^-1 Mpc^-1
disp(['H=',num2str(H),' km s^-1 Mpc^-1'])

% smax is the length of the skewer in Mpc
dz = 3.8964;
smax = dz*c/ (1.e3*H*(1. + zred)); % Mpc
disp(['smax = ', num2str(smax), 'Mpc'])
smax = smax*Mpc; % m

%create a long skewer structure
SK=draw_skewer(ibound, jbound, Lx, Ly, x, y, ix, phi, smax);

plot(SK.is, SK.js, 'k.')
axis square
print -deps SkewerPath.eps
