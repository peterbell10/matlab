more off
clear

nClusters = 1;
nParticlesPerCluster = 1000;
rSizeCluster = 0.01;

Centres=zeros(3,nClusters);
r=zeros(3,nClusters*nParticlesPerCluster);

% Make a reproducible set of initial data.
if(isoctave())
 rand( 'state',[-1969,2015])
 randn('state',[-1969,2012])
else
 load State.mat
 PrevStream = RandStream.setGlobalStream(Stream);
end

% Generate Clusters
Centres=rand(3,nClusters);
for i=1:nClusters
 clusterStartIndex = (i-1)*nParticlesPerCluster+1;
 clusterEndIndex   = i*nParticlesPerCluster;
 % centre-relative position
 rCluster = randn(3,nParticlesPerCluster)*rSizeCluster;
 % centre on Centre
 rCluster = rCluster + meshgrid(Centres(:,i),[1:nParticlesPerCluster])';
 % Fix for repeating BCs
 rCluster = rem(rCluster+1,1);
 r(:,clusterStartIndex:clusterEndIndex) = rCluster;
end

rm=ones(1,nParticlesPerCluster); % Code Units

SI='CGS'; hdr.h100=0.7; Units

hdr.lunit=10*Mpc/rSizeCluster; % cm
hdr.munit=1e15/nParticlesPerCluster; % Mo (Cluster mass is 1e15Mo)
hdr.sft0=0.0001;
hdr.time=1;

hdr.Nsph=32;
hdr.h_guess=0.01;
hdr.GridDimension=512;

% Calculate the gravitational potential
%  Particle-Particle
PotentialPP=GravitationalPotential(r,rm,hdr,'PP');
%  Particle-Mesh
PotentialPM=GravitationalPotential(r,rm,hdr,'PM');

disp(['Mean potential, PP: ',num2str(mean(PotentialPP(:)),'%e')])
disp(['Mean potential, PM: ',num2str(mean(PotentialPM(:)),'%e')])
