bins=10.^[-1:0.01:1];
nH=1;
Abund=ones(17,1);
z=0;

trans=wabs(bins,nH,Abund,z);

E=(bins(1:end-1)+bins(2:end))/2;
loglog(E,trans)
