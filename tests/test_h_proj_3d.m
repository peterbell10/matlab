Limits=[0 1 0 1 0 1];
L=64;
N=128^3;

% savedState was created via:
%  Stream=RandStream.getGlobalStream;
%  save State Stream
load State
PrevStream = RandStream.setGlobalStream(Stream);

r=rand(3,N);
z=ones(N,1);
h=0.02*z;

tic
A=h_proj_3d(r,z,h,L,Limits);
toc

load Ao_h_proj_3d
dA = A-Ao;

disp([' <A-Ao>=',num2str(mean(dA(:))),' std(A-Ao)=',num2str(std(dA(:)))])
