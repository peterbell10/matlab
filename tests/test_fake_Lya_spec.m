load fake_Lya_spec_input.mat

tic
Amp=0.1;
flux_HI_test  =fake_Lya_spec(v, nH1,  T, kms, 1, 0, 0.48, Amp);
Amp=0.001;
flux_HeII_test=fake_Lya_spec(v, nHe2, T, kms, 1, 2, 0.30, Amp);
toc

load fake_Lya_spec_fluxes.mat

error_HI  =std(flux_HI_test-flux_HI);
error_HeII=std(flux_HeII_test-flux_HeII);

disp(['std(flux_HI_test  -flux_HI  )=',num2str(error_HI)])
disp(['std(flux_HeII_test-flux_HeII)=',num2str(error_HeII)])
