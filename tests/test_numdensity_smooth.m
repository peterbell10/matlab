Res=300;
Limits=[0 1 0 1];


N=100000;
x=rand(N,1);
y=rand(N,1);
z=rand(N,1);
Nsrch=100;
KernelFlag=1;
MeanZ=numdensity_smooth(x,y,z,Nsrch,Res,Limits,KernelFlag);

figure(1)
title('With Kernel')
imagesc(MeanZ), colorbar

% This is a pathalogical case for the Kernel, where all the particles will be at 2h so will contribute zero
% Hence you get big zones of zeros in circles around the points.
N=500;
x=[ones(N/5,1)*(1/3);ones(N/5,1)*(1/3);ones(N/5,1)*(2/3);ones(N/5,1)*(2/3);ones(N/5,1)*(1/2)];
y=[ones(N/5,1)*(1/3);ones(N/5,1)*(2/3);ones(N/5,1)*(1/3);ones(N/5,1)*(2/3);ones(N/5,1)*(1/2)];
z=[ones(N/5,1)*1;ones(N/5,1)*2;ones(N/5,1)*3;ones(N/5,1)*4;ones(N/5,1)*5];
Nsrch=10;
KernelFlag=0;
MeanZ=numdensity_smooth(x,y,z,Nsrch,Res,Limits,KernelFlag);

figure(2)
title('Without Kernel')
imagesc(MeanZ), colorbar


