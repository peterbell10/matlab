% USER-MODIFIABLE parameters.

% Grid size per dimension
Res=128;

% Set the Simulation parameters
Simulation.Boxsize=25; % Mpc / h
Simulation.z_i=100; % Initial redshift

% Set the Cosmology
Cosmology.Omega_m=0.236; % All matter, dark+baryons
Cosmology.Omega_b=0.041; % Baryons only
Cosmology.h=0.73; % 100 km/s / Mpc

% End of USER-MODIFIABLE parameters.
% =================================================

N=Res^3;

% Generate random particle data.  Normally you'd insert your favourite particle
% IC generator here, or read in N-body ICs
r=rand(3,N);      % [0,1)
v=randn(3,N)/100; % km/s

% Grid the data
[rho_gas,v_gas]=GasGridFromDM(r,v,Res,Cosmology);

% At the moment, r has units [0,1), v and v_gas is km/s (but random, really)
% and rho_gas is fraction of Omega. i.e. mean(rho_gas)==Omega_b

% Enzo expects the gas density to be a fraction of the total mass density
rho_gas=rho_gas/Cosmology.Omega_m;

% Enzo's Units
[rhoU,LU,TU,tU,vU]=Enzo_CosmologyGetUnits(Cosmology.Omega_m,Cosmology.h, ...
                                          Simulation.Boxsize, ...
                                          Simulation.z_i, Simulation.z_i,'CGS');

% Convert velocities to Enzo's units.  I'm assuming velocities are already km/s
v=(100/vU)*v;
v_gas=(100/vU)*v_gas;

% Write Enzo-ready ICs
Identifier='test';
Enzo_WriteICs(r,v,rho_gas,v_gas,Identifier);

