If only octave is available, then you will be missing the file fintrf.h
necessary for compiling the Fortran-based gateway functions.

The easiest thing to do in that case is just copy fintrf.h to your octave
include directory:

as root:
install fintrf.h /usr/include/octave/
