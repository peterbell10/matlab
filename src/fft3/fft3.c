/* function transform = fft3(data) */

/* The 3-D Fast Fourier Transform */

/* Author: Eric Tittley */

/* HISTORY
 01-10-12 First version
 12-08-21 Converted to use FFTW3
*/

/* SEE ALSO: ifft3 */
   
#include <math.h>
#include <stdio.h> /* only for debugging messages */
#include <fftw3.h> /* the core routine is from the FFTW library (www.fftw.org) */
#include <mex.h>
#if MX_HAS_INTERLEAVED_COMPLEX
# include <matrix.h>
#endif

/* ************************************************** */
void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 const mwSize *Dims;
#if MX_HAS_INTERLEAVED_COMPLEX
 mxComplexDouble * p;
#else /* octave */
 double *pr;
 double *pi;
 bool cmplx;
#endif
 mwSize i;
 mwSize L,M,N,NDims,NElem;
 fftw_complex *in;
 fftw_plan plan; /* plan is a pointer */

 /* Check for the proper number of arguments */
 if(nrhs!=1 || nlhs!=1 ) {
  mexErrMsgTxt("Syntax: transform = fft3(data)");
 }

 /* The inputs must be of the type double */
 if( !(bool)mxIsDouble(prhs[0]) ) {
  mexErrMsgTxt("data must be double");
 }

 /* Make sure the data array is three-dimensional */
 NDims=mxGetNumberOfDimensions(prhs[0]);
 if(NDims != 3) {
  mexErrMsgTxt("data must be 3-dimensional");
 }

 /* Get the sizes of the dimensions */
 Dims=mxGetDimensions(prhs[0]);
 L=Dims[0];
 M=Dims[1];
 N=Dims[2];
 NElem=L*M*N;

 /* Get the pointer to the input complex (or real) data. */
#if MX_HAS_INTERLEAVED_COMPLEX
 p = mxGetComplexDoubles(prhs[0]);
#else /* octave */
 pr=(double*)mxGetPr(prhs[0]);
 pi=(double*)mxGetPi(prhs[0]);

 /* If the array is only real, then pi will be NULL and cmplx will be 0 */
 cmplx = ((pi == NULL) ? (bool)0 : (bool)1);
#endif

 /* Allocate memory for a complex array */
 in = fftw_alloc_complex(NElem);
 if(in==NULL) {
  mexErrMsgTxt("Failed to allocate memory for a complex array.");
 }

 /* Create the `plan'.
  * The last dimension has the fastest-varying index.
  * You must create the plan before initializing the input,
  * because FFTW_MEASURE overwrites the in/out arrays. 
  */
 plan = fftw_plan_dft_3d(L,M,N, in, in, FFTW_FORWARD, FFTW_ESTIMATE );
 if(plan==NULL) {
  mexErrMsgTxt("Failed to create a plan to pass to fftw.");
 }

 /* Transfer the input to the FFTW data array */
 for(i=0; i<NElem; i++) {
#if MX_HAS_INTERLEAVED_COMPLEX
  in[i][0] = p[i].real;
  in[i][1] = p[i].imag;
#else
  in[i][0] = pr[i];
  if(cmplx) {
   in[i][1] = pi[i];
  } else {
   in[i][1] = (double)0.0;
  }
#endif
 }

 /* Calculate the FFT. `in' is replaced with the transformed data */
 fftw_execute(plan);
 
 /* Create the output array */
 /* Re-get the Dims, since Dims gets corrupted when mxGetPr is called
  * in octave */
 Dims=mxGetDimensions(prhs[0]);
 plhs[0]=mxCreateNumericArray(3, Dims , mxDOUBLE_CLASS, mxCOMPLEX);

 /* Copy the contents of the trasformed data to transform */
#if MX_HAS_INTERLEAVED_COMPLEX
 p = mxGetComplexDoubles(plhs[0]);
#else
 pr=(double*)mxGetPr(plhs[0]);
 pi=(double*)mxGetPi(plhs[0]);
#endif
 for(i=0;i<NElem;i++) {
#if MX_HAS_INTERLEAVED_COMPLEX
  p[i].real=in[i][0]; /* Real */
  p[i].imag=in[i][1]; /* Imaginary */
#else
  pr[i]=in[i][0]; /* Real */
  pi[i]=in[i][1]; /* Imaginary */
#endif
 }  

 /* Clean up */
 /* Clear the `plan' */
 fftw_destroy_plan(plan);
 
 /* This step causes a SegFault in octave, but not matlab
  * when the input is complex */
 fftw_free(in);

}
