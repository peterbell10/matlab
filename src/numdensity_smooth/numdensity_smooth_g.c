/* Gateway function for numdensity_smooth:
 * MATLAB sytax: NumDens=numdensity_smooth(x,y,z,Nsrch,Res,Limits)
 */

#include <mex.h>

#include <stdio.h>


/* There is a problem with mexErrMsgTxt and some glibc */
#define mexErrMsgTxt(string) printf("%s\n",string); exit(-1);

void numdensity_smooth_(int *NPoints, double *x_orig, double *y_orig,
                        double *z, int *Nsrch, int *Res, double *Limits,
                        int *KernelFlag,
                        double *NumDens,
                        int *Converged, double *dist, double *x_local,
                        double *y_local, double *z_neighbour,
                        int *dist_indx, int *indx,
                        int *ll_start, int *ll, int *ll_place);

#define NPoints_MAX 262144
#define Res_MAX 1024

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 /* Pointers */
 double *NsrchP;
 double *ResP;
 double *KernelFlagP;

 /* Local variables */
 mwSize xM,xN,yM,yN,zM,zN,NsrchM,NsrchN,ResM,ResN;
 mwSize LimitsM,LimitsN,KernelFlagM,KernelFlagN;
 int Res,Nsrch;
 double *Limits;
 double *x;
 double *y;
 double *z;
 double *NumDens;

 int NPoints;

 /* Storage areas to be pre-allocated for the subroutine */
 int *Converged;
 double *dist;
 double *x_local;
 double *y_local;
 double *z_neighbour;
 int *dist_indx;
 int *indx;
 int *ll_start;
 int *ll;
 int *ll_place;

 int KernelFlag=0; /* Default no kernel smoothing */

 /* Check for the proper number of arguments */
 if( (nlhs!=1) || ( (nrhs!=6) && (nrhs!=7) ) ) {
  mexErrMsgTxt("Syntax: MeanZ=numdensity_smooth(x,y,z,Nsrch,Res,Limits,[KernelFlag])");
 }

 /* Get the sizes of the input arguments */
 xM     =mxGetM(prhs[0]);
 xN     =mxGetN(prhs[0]);
 yM     =mxGetM(prhs[1]);
 yN     =mxGetN(prhs[1]);
 zM     =mxGetM(prhs[2]);
 zN     =mxGetN(prhs[2]);
 NsrchM =mxGetM(prhs[3]);
 NsrchN =mxGetN(prhs[3]);
 ResM   =mxGetM(prhs[4]);
 ResN   =mxGetN(prhs[4]);
 LimitsM=mxGetM(prhs[5]);
 LimitsN=mxGetN(prhs[5]);
 if(nrhs==7) {
  KernelFlagM=mxGetM(prhs[6]);
  KernelFlagN=mxGetN(prhs[6]);
 } else {
  /* Set to supress compiler warnings */
  KernelFlagM=0;
  KernelFlagN=0;
 }

 /* Check sizes */
 if( (xM != 1) && (xN != 1) ) {
  mexErrMsgTxt("x must be a vector");
 }
 if(xM > xN) {
  xN=xM;
 }
 if( (yM != 1) && (yN != 1) ) {
  mexErrMsgTxt("y must be a vector");
 }
 if(yM > yN) {
  yN=yM;
 }
 if( (zM != 1) && (zN != 1) ) {
  mexErrMsgTxt("z must be a vector");
 }
 if(zM > zN) {
  zN=zM;
 }
 if( (xN != yN) || (xN != zN) ) {
  mexErrMsgTxt("x, y, and z must be the same length");
 }
 if( (NsrchM != 1) || (NsrchN != 1) ) {
  mexErrMsgTxt("Nsrch must be a scalar");
 }
 if( (ResM != 1) || (ResN != 1) ) {
  mexErrMsgTxt("Res must be a scalar");
 }
 if(    ( (LimitsM != 1) || (LimitsN != 4) )
     && ( (LimitsM != 4) || (LimitsN != 1) ) ) {
  mexErrMsgTxt("Limits must be a vector of length 4");
 }
 if(nrhs==7) {
  if( (KernelFlagM != 1) || (KernelFlagN != 1) ) {
   mexErrMsgTxt("KernelFlag must be a scalar");
  }
 }

 /* The number of points */
 NPoints=xN;

 

 /* Assign pointers to the input arguments */
 x      = mxGetPr(prhs[0]);
 y      = mxGetPr(prhs[1]);
 z      = mxGetPr(prhs[2]);
 NsrchP = mxGetPr(prhs[3]);
 ResP   = mxGetPr(prhs[4]);
 Limits = mxGetPr(prhs[5]);
 if(nrhs==7) {
  KernelFlagP = mxGetPr(prhs[6]);
 }

 /* Convert to integers */
 Nsrch = (int)*NsrchP;
 Res   = (int)*ResP;
 if(nrhs==7) {
  KernelFlag = (int)*KernelFlagP;
 }
 
 /* Allocate output array */
 plhs[0]  = mxCreateDoubleMatrix(Res,Res,mxREAL);
 NumDens  = mxGetPr(plhs[0]);

 /* Allocate local storage */
 Converged   = (int *   )malloc(Res*Res*sizeof(int));
 dist        = (double *)malloc(NPoints*sizeof(double));
 x_local     = (double *)malloc(NPoints*sizeof(double));
 y_local     = (double *)malloc(NPoints*sizeof(double));
 z_neighbour = (double *)malloc(NPoints*sizeof(double));
 dist_indx   = (int *   )malloc(NPoints*sizeof(int));
 indx        = (int *   )malloc(NPoints*sizeof(int));
 ll_start    = (int *   )malloc(Res*Res*sizeof(int));
 ll          = (int *   )malloc(NPoints*sizeof(int));
 ll_place    = (int *   )malloc(Res*Res*sizeof(int));
 if(   (Converged==NULL) || (dist==NULL) || (x_local==NULL) || (y_local==NULL)
    || (z_neighbour==NULL) || (dist_indx==NULL) || (indx==NULL)
    || (ll_start==NULL) || (ll==NULL) || (ll_place==NULL) ) {
  mexErrMsgTxt("Unable to allocate memory for local storage");
 }

 /* The actual computation */
 numdensity_smooth_(&NPoints,x,y,z,&Nsrch,&Res,Limits,&KernelFlag,
                    NumDens,
                    Converged,dist,x_local,y_local,z_neighbour,
                    dist_indx,indx,ll_start,ll,ll_place);

 /* Free local storage */
 free(Converged);
 free(dist);
 free(x_local);
 free(y_local);
 free(z_neighbour);
 free(dist_indx);
 free(indx);
 free(ll_start);
 free(ll);
 free(ll_place);

}
