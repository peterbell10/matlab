function MeanZ=numdensity_smooth(x,y,z,Nsrch,Res,Limits)
% syntax: MeanZ=numdensity_smooth(x,y,z,Nsrch,Res,Limits);
%
% Returns a [Res x Res] array, MeanZ, corresponding to the
% mean value of z around the nodes of the array of the data points
% distributed in x and y.
%
% Limits sets the limits to the node positions, as
% Limits = [x_lo x_hi y_lo y_hi]
%
% Nsrch sets the 'smoothing' of the routine.
%
% Nsrch is typically 10, or so.
%
% This calls a MEX routine
