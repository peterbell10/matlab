/* Gateway function for numdensity
 *
 * MATLAB sytax: NumDens=numdensity(x,y,Nsrch,Res,Limits)
 */

#include <mex.h>

#include <stdlib.h>
#include <stdio.h>

/* The Fortran function that does all the work */
void numdensity_2d_(int *Npoints, double *x_orig, double *y_orig, int *Nsrch,
                    int *Res, double *Limits, double *NumDens,
                    int *Converged, double *dist, double *x, double *y,
                    int *dist_indx, int *indx,
                    int *ll_start, int*ll, int *ll_place);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 /* Pointers */
 double *x_orig;
 double *y_orig;
 double *NsrchP;
 double *ResP;
 double *Limits;
 double *NumDens;

 /* The size of arrays. These should all be size_t */
 int Res,Nsrch,Npoints;

 /* The size of inputs */
 mwSize xM,xN,yM,yN,NsrchM,NsrchN,ResM,ResN,LimitsM,LimitsN;

 /* Temporary storage for numdensity_2d_ */
 int *Converged;
 double *dist;
 double *x;
 double *y;
 int *dist_indx;
 int *indx;
 int *ll_start;
 int *ll;
 int *ll_place;

 /* Check for the proper number of arguments */
 if( nlhs!=1 || nrhs!=5 ) {
  mexErrMsgTxt("syntax: NumDens=numdensity(x,y,Nsrch,Res,Limits)");
 }

 /* All arguments must be double */
 if( !mxIsDouble(prhs[0]) ) {
  mexErrMsgTxt("x must be a double.");
 }
 if( !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("y must be a double.");
 }
 if( !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("Nsrch must be a double.");
 }
 if( !mxIsDouble(prhs[3]) ) {
  mexErrMsgTxt("Res must be a double.");
 }
 if( !mxIsDouble(prhs[4]) ) {
  mexErrMsgTxt("Limits must be a double.");
 }

 /*Get the sizes of the input arguments */
 xM     =mxGetM(prhs[0]);
 xN     =mxGetN(prhs[0]);
 yM     =mxGetM(prhs[1]);
 yN     =mxGetN(prhs[1]);
 NsrchM =mxGetM(prhs[2]);
 NsrchN =mxGetN(prhs[2]);
 ResM   =mxGetM(prhs[3]);
 ResN   =mxGetN(prhs[3]);
 LimitsM=mxGetM(prhs[4]);
 LimitsN=mxGetN(prhs[4]);

 /* Check sizes */
 if(xM!=1 && xN!=1) {
  mexErrMsgTxt("ERROR: numdensity: x must be a vector");
 }
 if(xM>xN) xN=xM;
 Npoints = (int)xN;
 if(yM!=1 && yN!=1) {
  mexErrMsgTxt("ERROR: numdensity: y must be a vector");
 }
 if(yM>yN) yN=yM;
 if(xN!=yN) {
  mexErrMsgTxt("x and y must be the same size");
 }

 if(NsrchM!=1 || NsrchN!=1) {
  mexErrMsgTxt("ERROR: numdensity: Nsrch must be a scalar");
 }
 if(ResM!=1 || ResN!=1) {
  mexErrMsgTxt("ERROR: numdensity: Res must be a scalar");
 }
 if(   (LimitsM!=1 || LimitsN!=4 )
    && (LimitsM!=4 || LimitsN!=1 ) ) {
  mexErrMsgTxt("ERROR: numdensity: Limits must be a vector of length 4");
 }

 /* Assign pointers to the input arguments */
 x_orig = mxGetPr(prhs[0]);
 y_orig = mxGetPr(prhs[1]);
 NsrchP = mxGetPr(prhs[2]);
 Nsrch  =(int)(*NsrchP);
 ResP   = mxGetPr(prhs[3]);
 Res    =(int)(*ResP);
 Limits = mxGetPr(prhs[4]);

 /* Check that Nsrch > Npoints */
 if(xM < Nsrch) {
  mexErrMsgTxt("ERROR: numdensity: Input vector has fewer than Nsrch elements");
 }
 
 /* Create a matrix for return argument */
 plhs[0] = mxCreateDoubleMatrix((mwSize)Res,(mwSize)Res,mxREAL);
 NumDens = mxGetPr(plhs[0]);

 /* Allocate temporary space */
 Converged = (int *)malloc((size_t)Res*(size_t)Res*sizeof(int));
 if( Converged == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for Converged");
 }
 dist = (double *)malloc((size_t)Npoints*sizeof(double));
 if( dist == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for dist");
 }
 x = (double *)malloc((size_t)Npoints*sizeof(double));
 if( x == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for x");
 }
 y = (double *)malloc((size_t)Npoints*sizeof(double));
 if( y == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for y");
 }
 dist_indx = (int *)malloc((size_t)Npoints*sizeof(int));
 if( dist_indx == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for dist_indx");
 }
 indx = (int *)malloc((size_t)Npoints*sizeof(int));
 if( indx == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for indx");
 }
 ll_start = (int *)malloc((size_t)Res*(size_t)Res*sizeof(int));
 if( ll_start == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for ll_start");
 }
 ll = (int *)malloc((size_t)Npoints*sizeof(int));
 if( ll == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for ll");
 }
 ll_place = (int *)malloc((size_t)Res*(size_t)Res*sizeof(int));
 if( ll_place == NULL ) {
  mexErrMsgTxt("ERROR: numdensity: Unable to allocate space for ll_place");
 }
 
 /* DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE */
 numdensity_2d_(&Npoints,x_orig,y_orig,&Nsrch,&Res,Limits,NumDens,
                Converged, dist, x, y, dist_indx, indx,
                ll_start, ll, ll_place);

 /* Free temporary space */
 free(Converged);
 free(dist);
 free(x);
 free(y);
 free(dist_indx);
 free(indx);
 free(ll_start);
 free(ll);
 free(ll_place);

}
