/* test_HitBoundingBox
 *
 * test_HitBoundingBox BoxSize X0 Y0 Z0 Theta
 */

#include <stdio.h>
#include <stdlib.h>

unsigned int HitBoundingBox(const double *const minB,
                            const double *const maxB,
                            const double *const origin,
                            const double *const dir,
                       /*@out@ */ double *const coord);

int main(int argc, char *argv[])
{
 double BoxSize;
 double minB[3];
 double maxB[3];
 double origin[3];
 double dir[3];
 double coord[3];
 unsigned int status;
 int nFound;

 if (argc != 8) {
  printf("syntax: test_HitBoundingBox BoxSize X0 Y0 Z0 nx ny nz\n");
  exit(EXIT_FAILURE);
 }

 minB[0] = 0;
 minB[1] = 0;
 minB[2] = 0;

 nFound = sscanf(argv[1], "%lf", &BoxSize);
 if (nFound != 1) {
  printf("ERROR: %s: %i: Unable to parse BoxSize in %s\n", __FILE__, __LINE__,
         argv[1]);
  exit(EXIT_FAILURE);
 }
 maxB[0] = BoxSize;
 maxB[1] = BoxSize;
 maxB[2] = BoxSize;

 nFound = sscanf(argv[2], "%lf", &(origin[0]));
 if (nFound != 1) {
  printf("ERROR: %s: %i: Unable to parse X0 in %s\n", __FILE__, __LINE__,
         argv[2]);
  exit(EXIT_FAILURE);
 }
 nFound = sscanf(argv[3], "%lf", &(origin[1]));
 if (nFound != 1) {
  printf("ERROR: %s: %i: Unable to parse Y0 in %s\n", __FILE__, __LINE__,
         argv[3]);
  exit(EXIT_FAILURE);
 }
 nFound = sscanf(argv[4], "%lf", &(origin[2]));
 if (nFound != 1) {
  printf("ERROR: %s: %i: Unable to parse Z0 in %s\n", __FILE__, __LINE__,
         argv[4]);
  exit(EXIT_FAILURE);
 }

 nFound = sscanf(argv[5], "%lf", &(dir[0]));
 if (nFound != 1) {
  printf("ERROR: %s: %i: Unable to parse nx in %s\n", __FILE__, __LINE__,
         argv[5]);
  exit(EXIT_FAILURE);
 }
 nFound = sscanf(argv[6], "%lf", &(dir[1]));
 if (nFound != 1) {
  printf("ERROR: %s: %i: Unable to parse ny in %s\n", __FILE__, __LINE__,
         argv[6]);
  exit(EXIT_FAILURE);
 }
 nFound = sscanf(argv[7], "%lf", &(dir[2]));
 if (nFound != 1) {
  printf("ERROR: %s: %i: Unable to parse nz in %s\n", __FILE__, __LINE__,
         argv[7]);
  exit(EXIT_FAILURE);
 }

 status = HitBoundingBox(minB, maxB, origin, dir, coord);

 printf("status = %u; coord=(%lf %lf %lf)\n", status, coord[0],
        coord[1], coord[2]);

 exit(EXIT_SUCCESS);
}
