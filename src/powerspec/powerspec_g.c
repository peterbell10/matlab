/* Gateway function for powerspec 
 * MATLAB sytax: [k,p]=powerspec(r,L)
 */

#include <mex.h>

#include <stdio.h>

/* There is a problem with mexErrMsgTxt and some glibc */
#define mexErrMsgTxt(string) printf("%s\n",string); exit(-1);


/* The Fortran function that does all the work */
void powerspec_(double *r, int *nobj, double *k, double *p, int *L,
                double *dtl, double *dcl);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 double *k; /* real*8 k(L*L*L),p(L*L*L),r(3,Nmax) */
 double *p;
 double *r;
 double *LP;
 
 int Nobj,L;
 mwSize nobj;
 mwSize mrows;

 /* Work spaces to be deleted */
 double *dtl;
 double *dcl;

 /* Check for the proper number of arguments */
 if( (nlhs != 2) || (nrhs!=2) ) {
  mexErrMsgTxt("syntax: [k,p]=powerspec(r,L)");
 }

 /* The arguments must be double */
 if(   !mxIsDouble(prhs[0])
    && !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("Arguments must be a double.");
 }

 /* r must be 3 rows by N columns */
 mrows = mxGetM(prhs[0]);
 if( mrows!=3 ) {
  mexErrMsgTxt("r must be a 3xN array with N==N");
 }

 /* Number of objects */
 nobj = mxGetN(prhs[0]);
 Nobj = (int)nobj;
      
 /* Assign pointers to the input */
 r  = mxGetPr(prhs[0]);
 LP = mxGetPr(prhs[1]);

 /* Convert to integers */
 L = (int)(*LP);

 /* Create a matrix for return argument.
  *  If unsuccessful in a MEX-file, the MEX-file terminates and returns
  *  control to the MATLAB prompt. */
 plhs[0]=mxCreateDoubleMatrix((mwSize)L*(mwSize)L*(mwSize)L, (mwSize)1, mxREAL);
 plhs[1]=mxCreateDoubleMatrix((mwSize)L*(mwSize)L*(mwSize)L, (mwSize)1, mxREAL);
 k      = mxGetPr(plhs[0]);
 p      = mxGetPr(plhs[1]);

 /* Allocate space for workspaces */
 dtl = (double *)malloc(2*L*L*L * sizeof(double));
 if(dtl==NULL) {
  printf("ERROR: %s: %i: Unable to allocate memory for dtl\n",
         __FILE__,__LINE__);
  exit(-1);
 }
 dcl = dtl; /* They need to be EQUIVALENT (Fortran lingo) */

 /* DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE */
 powerspec_(r,&Nobj,k,p,&L,dtl,dcl);

 free(dtl);

}
