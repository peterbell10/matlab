      SUBROUTINE powerspec(r,nobj,k,p,L,dtl,dcl)
      IMPLICIT NONE
c  calculates power spectrum of data
c  This subroutine was derived from pispec4.f and pspecplot.f

c Local parameters (these can be changed, but must be meshed with the output functions)
c     Input arguments (not modified)
      integer nobj
      integer L
      double precision r(3,nobj)
c     Output arguments, assigned here
      double precision k(L*L*L)
      double precision p(L*L*L)
      double precision dtl(2*L,L,L)
      complex*16 dcl(L,L,L)
c dtl and dcl NEED TO BE POINTERS TO THE SAME MEMORY SPACE!!!!      
c This connects the two
c      equivalence (dtl,dcl)
c Local variables needed for grid assignment
      double precision rca,rcb
      integer i
      double precision rx,ry,rz
      double precision tx,ty,tz
      integer ixm2,iym2,izm2
      integer ixm1,iym1,izm1
      integer ix  ,iy  ,iz
      integer ixp1,iyp1,izp1
      integer ixp2,iyp2,izp2
      double precision hxm2,hym2,hzm2
      double precision hxm1,hym1,hzm1
      double precision hx  ,hy  ,hz
      double precision hxp1,hyp1,hzp1
      double precision hxp2,hyp2,hzp2
      double precision hx2 ,hy2 ,hz2
      integer nxm2,nym2,nzm2
      integer nxm1,nym1,nzm1
      integer nxp1,nyp1,nzp1
      integer nxp2,nyp2,nzp2
      double precision gxm2,gym2,gzm2
      double precision gxm1,gym1,gzm1
      double precision gx  ,gy  ,gz
      double precision gxp1,gyp1,gzp1
      double precision gxp2,gyp2,gzp2
      double precision gx2,gy2,gz2
c Local variables needed for Fourier transform
      integer ndim
      integer nn(3)
      double precision cf
      integer Lnyq
      double precision tpi,tpiL,piL
      parameter(tpi=6.283185307179d0)
      complex*16 rec,xrec,yrec,zrec
      complex*16 c1,ci,c000,c001,c010,c011,cma,cmb,cmc,cmd
      integer icx,icy,icz
      double precision rkx,rky,rkz
      double precision Wkx,Wky,Wkz
      double precision cfac
c Convert the 3-d power spec into a 1-d powerspec
      integer iky,ikz
      double precision rk
      complex*16 ct
      double precision cmod
      double precision pk
      integer count

      rca=dble(L)
      rcb=1.d0
 
      do iz=1,L
       do iy=1,L
        do ix=1,2*L
         dtl(ix,iy,iz)=0.d0
        enddo
       enddo
      enddo

c Assign the particle masses to a 3-d grid.
      do i=1,nobj
       rx=rca*r(1,i)+rcb
       ry=rca*r(2,i)+rcb
       rz=rca*r(3,i)+rcb
       tx=rx+0.5d0
       ty=ry+0.5d0
       tz=rz+0.5d0
       ixm1=int(rx)
       iym1=int(ry)
       izm1=int(rz)
       ixm2=2*mod(ixm1-2+L,L)+1
       ixp1=2*mod(ixm1,L)+1
       ixp2=2*mod(ixm1+1,L)+1
       hx=rx-ixm1
       ixm1=2*ixm1-1
       hx2=hx*hx
       hxm2=(1.d0-hx)**3
       hxm1=4.d0+(3.d0*hx-6.d0)*hx2
       hxp2=hx2*hx
       hxp1=6.d0-hxm2-hxm1-hxp2

       iym2=mod(iym1-2+L,L)+1
       iyp1=mod(iym1,L)+1
       iyp2=mod(iym1+1,L)+1
       hy=ry-iym1
       hy2=hy*hy
       hym2=(1.d0-hy)**3
       hym1=4.d0+(3.d0*hy-6.d0)*hy2
       hyp2=hy2*hy
       hyp1=6.d0-hym2-hym1-hyp2

       izm2=mod(izm1-2+L,L)+1
       izp1=mod(izm1,L)+1
       izp2=mod(izm1+1,L)+1
       hz=rz-izm1
       hz2=hz*hz
       hzm2=(1.d0-hz)**3
       hzm1=4.d0+(3.d0*hz-6.d0)*hz2
       hzp2=hz2*hz
       hzp1=6.d0-hzm2-hzm1-hzp2

       nxm1=int(tx)
       nym1=int(ty)
       nzm1=int(tz)

       gx=tx-nxm1
       nxm1=mod(nxm1-1,L)+1
       nxm2=2*mod(nxm1-2+L,L)+2
       nxp1=2*mod(nxm1,L)+2
       nxp2=2*mod(nxm1+1,L)+2
       nxm1=2*nxm1
       gx2=gx*gx
       gxm2=(1.d0-gx)**3
       gxm1=4.d0+(3.d0*gx-6.d0)*gx2
       gxp2=gx2*gx
       gxp1=6.d0-gxm2-gxm1-gxp2

       gy=ty-nym1
       nym1=mod(nym1-1,L)+1
       nym2=mod(nym1-2+L,L)+1
       nyp1=mod(nym1,L)+1
       nyp2=mod(nym1+1,L)+1
       gy2=gy*gy
       gym2=(1.d0-gy)**3
       gym1=4.d0+(3.d0*gy-6.d0)*gy2
       gyp2=gy2*gy
       gyp1=6.d0-gym2-gym1-gyp2

       gz=tz-nzm1
       nzm1=mod(nzm1-1,L)+1
       nzm2=mod(nzm1-2+L,L)+1
       nzp1=mod(nzm1,L)+1
       nzp2=mod(nzm1+1,L)+1
       gz2=gz*gz
       gzm2=(1.d0-gz)**3
       gzm1=4.d0+(3.d0*gz-6.d0)*gz2
       gzp2=gz2*gz
       gzp1=6.d0-gzm2-gzm1-gzp2

       dtl(ixm2,iym2,izm2)   = dtl(ixm2,iym2,izm2)+ hxm2*hym2 *hzm2
       dtl(ixm1,iym2,izm2)   = dtl(ixm1,iym2,izm2)+ hxm1*hym2 *hzm2
       dtl(ixp1,iym2,izm2)   = dtl(ixp1,iym2,izm2)+ hxp1*hym2 *hzm2
       dtl(ixp2,iym2,izm2)   = dtl(ixp2,iym2,izm2)+ hxp2*hym2 *hzm2
       dtl(ixm2,iym1,izm2)   = dtl(ixm2,iym1,izm2)+ hxm2*hym1 *hzm2
       dtl(ixm1,iym1,izm2)   = dtl(ixm1,iym1,izm2)+ hxm1*hym1 *hzm2
       dtl(ixp1,iym1,izm2)   = dtl(ixp1,iym1,izm2)+ hxp1*hym1 *hzm2
       dtl(ixp2,iym1,izm2)   = dtl(ixp2,iym1,izm2)+ hxp2*hym1 *hzm2
       dtl(ixm2,iyp1,izm2)   = dtl(ixm2,iyp1,izm2)+ hxm2*hyp1 *hzm2
       dtl(ixm1,iyp1,izm2)   = dtl(ixm1,iyp1,izm2)+ hxm1*hyp1 *hzm2
       dtl(ixp1,iyp1,izm2)   = dtl(ixp1,iyp1,izm2)+ hxp1*hyp1 *hzm2
       dtl(ixp2,iyp1,izm2)   = dtl(ixp2,iyp1,izm2)+ hxp2*hyp1 *hzm2
       dtl(ixm2,iyp2,izm2)   = dtl(ixm2,iyp2,izm2)+ hxm2*hyp2 *hzm2
       dtl(ixm1,iyp2,izm2)   = dtl(ixm1,iyp2,izm2)+ hxm1*hyp2 *hzm2
       dtl(ixp1,iyp2,izm2)   = dtl(ixp1,iyp2,izm2)+ hxp1*hyp2 *hzm2
       dtl(ixp2,iyp2,izm2)   = dtl(ixp2,iyp2,izm2)+ hxp2*hyp2 *hzm2
       dtl(ixm2,iym2,izm1)   = dtl(ixm2,iym2,izm1)+ hxm2*hym2 *hzm1
       dtl(ixm1,iym2,izm1)   = dtl(ixm1,iym2,izm1)+ hxm1*hym2 *hzm1
       dtl(ixp1,iym2,izm1)   = dtl(ixp1,iym2,izm1)+ hxp1*hym2 *hzm1
       dtl(ixp2,iym2,izm1)   = dtl(ixp2,iym2,izm1)+ hxp2*hym2 *hzm1
       dtl(ixm2,iym1,izm1)   = dtl(ixm2,iym1,izm1)+ hxm2*hym1 *hzm1
       dtl(ixm1,iym1,izm1)   = dtl(ixm1,iym1,izm1)+ hxm1*hym1 *hzm1
       dtl(ixp1,iym1,izm1)   = dtl(ixp1,iym1,izm1)+ hxp1*hym1 *hzm1
       dtl(ixp2,iym1,izm1)   = dtl(ixp2,iym1,izm1)+ hxp2*hym1 *hzm1
       dtl(ixm2,iyp1,izm1)   = dtl(ixm2,iyp1,izm1)+ hxm2*hyp1 *hzm1
       dtl(ixm1,iyp1,izm1)   = dtl(ixm1,iyp1,izm1)+ hxm1*hyp1 *hzm1
       dtl(ixp1,iyp1,izm1)   = dtl(ixp1,iyp1,izm1)+ hxp1*hyp1 *hzm1
       dtl(ixp2,iyp1,izm1)   = dtl(ixp2,iyp1,izm1)+ hxp2*hyp1 *hzm1
       dtl(ixm2,iyp2,izm1)   = dtl(ixm2,iyp2,izm1)+ hxm2*hyp2 *hzm1
       dtl(ixm1,iyp2,izm1)   = dtl(ixm1,iyp2,izm1)+ hxm1*hyp2 *hzm1
       dtl(ixp1,iyp2,izm1)   = dtl(ixp1,iyp2,izm1)+ hxp1*hyp2 *hzm1
       dtl(ixp2,iyp2,izm1)   = dtl(ixp2,iyp2,izm1)+ hxp2*hyp2 *hzm1
       dtl(ixm2,iym2,izp1)   = dtl(ixm2,iym2,izp1)+ hxm2*hym2 *hzp1
       dtl(ixm1,iym2,izp1)   = dtl(ixm1,iym2,izp1)+ hxm1*hym2 *hzp1
       dtl(ixp1,iym2,izp1)   = dtl(ixp1,iym2,izp1)+ hxp1*hym2 *hzp1
       dtl(ixp2,iym2,izp1)   = dtl(ixp2,iym2,izp1)+ hxp2*hym2 *hzp1
       dtl(ixm2,iym1,izp1)   = dtl(ixm2,iym1,izp1)+ hxm2*hym1 *hzp1
       dtl(ixm1,iym1,izp1)   = dtl(ixm1,iym1,izp1)+ hxm1*hym1 *hzp1
       dtl(ixp1,iym1,izp1)   = dtl(ixp1,iym1,izp1)+ hxp1*hym1 *hzp1
       dtl(ixp2,iym1,izp1)   = dtl(ixp2,iym1,izp1)+ hxp2*hym1 *hzp1
       dtl(ixm2,iyp1,izp1)   = dtl(ixm2,iyp1,izp1)+ hxm2*hyp1 *hzp1
       dtl(ixm1,iyp1,izp1)   = dtl(ixm1,iyp1,izp1)+ hxm1*hyp1 *hzp1
       dtl(ixp1,iyp1,izp1)   = dtl(ixp1,iyp1,izp1)+ hxp1*hyp1 *hzp1
       dtl(ixp2,iyp1,izp1)   = dtl(ixp2,iyp1,izp1)+ hxp2*hyp1 *hzp1
       dtl(ixm2,iyp2,izp1)   = dtl(ixm2,iyp2,izp1)+ hxm2*hyp2 *hzp1
       dtl(ixm1,iyp2,izp1)   = dtl(ixm1,iyp2,izp1)+ hxm1*hyp2 *hzp1
       dtl(ixp1,iyp2,izp1)   = dtl(ixp1,iyp2,izp1)+ hxp1*hyp2 *hzp1
       dtl(ixp2,iyp2,izp1)   = dtl(ixp2,iyp2,izp1)+ hxp2*hyp2 *hzp1
       dtl(ixm2,iym2,izp2)   = dtl(ixm2,iym2,izp2)+ hxm2*hym2 *hzp2
       dtl(ixm1,iym2,izp2)   = dtl(ixm1,iym2,izp2)+ hxm1*hym2 *hzp2
       dtl(ixp1,iym2,izp2)   = dtl(ixp1,iym2,izp2)+ hxp1*hym2 *hzp2
       dtl(ixp2,iym2,izp2)   = dtl(ixp2,iym2,izp2)+ hxp2*hym2 *hzp2
       dtl(ixm2,iym1,izp2)   = dtl(ixm2,iym1,izp2)+ hxm2*hym1 *hzp2
       dtl(ixm1,iym1,izp2)   = dtl(ixm1,iym1,izp2)+ hxm1*hym1 *hzp2
       dtl(ixp1,iym1,izp2)   = dtl(ixp1,iym1,izp2)+ hxp1*hym1 *hzp2
       dtl(ixp2,iym1,izp2)   = dtl(ixp2,iym1,izp2)+ hxp2*hym1 *hzp2
       dtl(ixm2,iyp1,izp2)   = dtl(ixm2,iyp1,izp2)+ hxm2*hyp1 *hzp2
       dtl(ixm1,iyp1,izp2)   = dtl(ixm1,iyp1,izp2)+ hxm1*hyp1 *hzp2
       dtl(ixp1,iyp1,izp2)   = dtl(ixp1,iyp1,izp2)+ hxp1*hyp1 *hzp2
       dtl(ixp2,iyp1,izp2)   = dtl(ixp2,iyp1,izp2)+ hxp2*hyp1 *hzp2
       dtl(ixm2,iyp2,izp2)   = dtl(ixm2,iyp2,izp2)+ hxm2*hyp2 *hzp2
       dtl(ixm1,iyp2,izp2)   = dtl(ixm1,iyp2,izp2)+ hxm1*hyp2 *hzp2
       dtl(ixp1,iyp2,izp2)   = dtl(ixp1,iyp2,izp2)+ hxp1*hyp2 *hzp2
       dtl(ixp2,iyp2,izp2)   = dtl(ixp2,iyp2,izp2)+ hxp2*hyp2 *hzp2

       dtl(nxm2,nym2,nzm2)   = dtl(nxm2,nym2,nzm2)+ gxm2*gym2 *gzm2
       dtl(nxm1,nym2,nzm2)   = dtl(nxm1,nym2,nzm2)+ gxm1*gym2 *gzm2
       dtl(nxp1,nym2,nzm2)   = dtl(nxp1,nym2,nzm2)+ gxp1*gym2 *gzm2
       dtl(nxp2,nym2,nzm2)   = dtl(nxp2,nym2,nzm2)+ gxp2*gym2 *gzm2
       dtl(nxm2,nym1,nzm2)   = dtl(nxm2,nym1,nzm2)+ gxm2*gym1 *gzm2
       dtl(nxm1,nym1,nzm2)   = dtl(nxm1,nym1,nzm2)+ gxm1*gym1 *gzm2
       dtl(nxp1,nym1,nzm2)   = dtl(nxp1,nym1,nzm2)+ gxp1*gym1 *gzm2
       dtl(nxp2,nym1,nzm2)   = dtl(nxp2,nym1,nzm2)+ gxp2*gym1 *gzm2
       dtl(nxm2,nyp1,nzm2)   = dtl(nxm2,nyp1,nzm2)+ gxm2*gyp1 *gzm2
       dtl(nxm1,nyp1,nzm2)   = dtl(nxm1,nyp1,nzm2)+ gxm1*gyp1 *gzm2
       dtl(nxp1,nyp1,nzm2)   = dtl(nxp1,nyp1,nzm2)+ gxp1*gyp1 *gzm2
       dtl(nxp2,nyp1,nzm2)   = dtl(nxp2,nyp1,nzm2)+ gxp2*gyp1 *gzm2
       dtl(nxm2,nyp2,nzm2)   = dtl(nxm2,nyp2,nzm2)+ gxm2*gyp2 *gzm2
       dtl(nxm1,nyp2,nzm2)   = dtl(nxm1,nyp2,nzm2)+ gxm1*gyp2 *gzm2
       dtl(nxp1,nyp2,nzm2)   = dtl(nxp1,nyp2,nzm2)+ gxp1*gyp2 *gzm2
       dtl(nxp2,nyp2,nzm2)   = dtl(nxp2,nyp2,nzm2)+ gxp2*gyp2 *gzm2
       dtl(nxm2,nym2,nzm1)   = dtl(nxm2,nym2,nzm1)+ gxm2*gym2 *gzm1
       dtl(nxm1,nym2,nzm1)   = dtl(nxm1,nym2,nzm1)+ gxm1*gym2 *gzm1
       dtl(nxp1,nym2,nzm1)   = dtl(nxp1,nym2,nzm1)+ gxp1*gym2 *gzm1
       dtl(nxp2,nym2,nzm1)   = dtl(nxp2,nym2,nzm1)+ gxp2*gym2 *gzm1
       dtl(nxm2,nym1,nzm1)   = dtl(nxm2,nym1,nzm1)+ gxm2*gym1 *gzm1
       dtl(nxm1,nym1,nzm1)   = dtl(nxm1,nym1,nzm1)+ gxm1*gym1 *gzm1
       dtl(nxp1,nym1,nzm1)   = dtl(nxp1,nym1,nzm1)+ gxp1*gym1 *gzm1
       dtl(nxp2,nym1,nzm1)   = dtl(nxp2,nym1,nzm1)+ gxp2*gym1 *gzm1
       dtl(nxm2,nyp1,nzm1)   = dtl(nxm2,nyp1,nzm1)+ gxm2*gyp1 *gzm1
       dtl(nxm1,nyp1,nzm1)   = dtl(nxm1,nyp1,nzm1)+ gxm1*gyp1 *gzm1
       dtl(nxp1,nyp1,nzm1)   = dtl(nxp1,nyp1,nzm1)+ gxp1*gyp1 *gzm1
       dtl(nxp2,nyp1,nzm1)   = dtl(nxp2,nyp1,nzm1)+ gxp2*gyp1 *gzm1
       dtl(nxm2,nyp2,nzm1)   = dtl(nxm2,nyp2,nzm1)+ gxm2*gyp2 *gzm1
       dtl(nxm1,nyp2,nzm1)   = dtl(nxm1,nyp2,nzm1)+ gxm1*gyp2 *gzm1
       dtl(nxp1,nyp2,nzm1)   = dtl(nxp1,nyp2,nzm1)+ gxp1*gyp2 *gzm1
       dtl(nxp2,nyp2,nzm1)   = dtl(nxp2,nyp2,nzm1)+ gxp2*gyp2 *gzm1
       dtl(nxm2,nym2,nzp1)   = dtl(nxm2,nym2,nzp1)+ gxm2*gym2 *gzp1
       dtl(nxm1,nym2,nzp1)   = dtl(nxm1,nym2,nzp1)+ gxm1*gym2 *gzp1
       dtl(nxp1,nym2,nzp1)   = dtl(nxp1,nym2,nzp1)+ gxp1*gym2 *gzp1
       dtl(nxp2,nym2,nzp1)   = dtl(nxp2,nym2,nzp1)+ gxp2*gym2 *gzp1
       dtl(nxm2,nym1,nzp1)   = dtl(nxm2,nym1,nzp1)+ gxm2*gym1 *gzp1
       dtl(nxm1,nym1,nzp1)   = dtl(nxm1,nym1,nzp1)+ gxm1*gym1 *gzp1
       dtl(nxp1,nym1,nzp1)   = dtl(nxp1,nym1,nzp1)+ gxp1*gym1 *gzp1
       dtl(nxp2,nym1,nzp1)   = dtl(nxp2,nym1,nzp1)+ gxp2*gym1 *gzp1
       dtl(nxm2,nyp1,nzp1)   = dtl(nxm2,nyp1,nzp1)+ gxm2*gyp1 *gzp1
       dtl(nxm1,nyp1,nzp1)   = dtl(nxm1,nyp1,nzp1)+ gxm1*gyp1 *gzp1
       dtl(nxp1,nyp1,nzp1)   = dtl(nxp1,nyp1,nzp1)+ gxp1*gyp1 *gzp1
       dtl(nxp2,nyp1,nzp1)   = dtl(nxp2,nyp1,nzp1)+ gxp2*gyp1 *gzp1
       dtl(nxm2,nyp2,nzp1)   = dtl(nxm2,nyp2,nzp1)+ gxm2*gyp2 *gzp1
       dtl(nxm1,nyp2,nzp1)   = dtl(nxm1,nyp2,nzp1)+ gxm1*gyp2 *gzp1
       dtl(nxp1,nyp2,nzp1)   = dtl(nxp1,nyp2,nzp1)+ gxp1*gyp2 *gzp1
       dtl(nxp2,nyp2,nzp1)   = dtl(nxp2,nyp2,nzp1)+ gxp2*gyp2 *gzp1
       dtl(nxm2,nym2,nzp2)   = dtl(nxm2,nym2,nzp2)+ gxm2*gym2 *gzp2
       dtl(nxm1,nym2,nzp2)   = dtl(nxm1,nym2,nzp2)+ gxm1*gym2 *gzp2
       dtl(nxp1,nym2,nzp2)   = dtl(nxp1,nym2,nzp2)+ gxp1*gym2 *gzp2
       dtl(nxp2,nym2,nzp2)   = dtl(nxp2,nym2,nzp2)+ gxp2*gym2 *gzp2
       dtl(nxm2,nym1,nzp2)   = dtl(nxm2,nym1,nzp2)+ gxm2*gym1 *gzp2
       dtl(nxm1,nym1,nzp2)   = dtl(nxm1,nym1,nzp2)+ gxm1*gym1 *gzp2
       dtl(nxp1,nym1,nzp2)   = dtl(nxp1,nym1,nzp2)+ gxp1*gym1 *gzp2
       dtl(nxp2,nym1,nzp2)   = dtl(nxp2,nym1,nzp2)+ gxp2*gym1 *gzp2
       dtl(nxm2,nyp1,nzp2)   = dtl(nxm2,nyp1,nzp2)+ gxm2*gyp1 *gzp2
       dtl(nxm1,nyp1,nzp2)   = dtl(nxm1,nyp1,nzp2)+ gxm1*gyp1 *gzp2
       dtl(nxp1,nyp1,nzp2)   = dtl(nxp1,nyp1,nzp2)+ gxp1*gyp1 *gzp2
       dtl(nxp2,nyp1,nzp2)   = dtl(nxp2,nyp1,nzp2)+ gxp2*gyp1 *gzp2
       dtl(nxm2,nyp2,nzp2)   = dtl(nxm2,nyp2,nzp2)+ gxm2*gyp2 *gzp2
       dtl(nxm1,nyp2,nzp2)   = dtl(nxm1,nyp2,nzp2)+ gxm1*gyp2 *gzp2
       dtl(nxp1,nyp2,nzp2)   = dtl(nxp1,nyp2,nzp2)+ gxp1*gyp2 *gzp2
       dtl(nxp2,nyp2,nzp2)   = dtl(nxp2,nyp2,nzp2)+ gxp2*gyp2 *gzp2
      enddo
c
c FT and then convolve with Green
c
      ndim=3
      nn(1)=L
      nn(2)=L
      nn(3)=L

      CALL fourn(dtl,nn,ndim,1)

      cf=1.d0/(6.d0**3*4.d0*nobj)
      Lnyq=L/2+1
      tpiL=tpi/dble(L)
      piL=-tpiL/2.d0
      rec=dcmplx(dcos(piL),dsin(piL))
      c1=dcmplx(1.d0,0.d0)
      ci=dcmplx(0.d0,1.d0)
      zrec=dcmplx(c1)
      do iz=1,Lnyq
       icz=mod(L-iz+1,L)+1
       rkz=tpiL*dble(iz-1)
       Wkz=1.d0
       if(rkz.ne.0.d0)Wkz=(sin(rkz/2.d0)/(rkz/2.d0))**4
       yrec=dcmplx(c1)
       do iy=1,Lnyq
        icy=mod(L-iy+1,L)+1
        rky=tpiL*dble(iy-1)
        Wky=1.d0
        if(rky.ne.0.d0)Wky=(sin(rky/2.d0)/(rky/2.d0))**4
        xrec=dcmplx(c1)
        do ix=1,Lnyq
         icx=mod(L-ix+1,L)+1
         rkx=tpiL*dble(ix-1)
         Wkx=1.d0
         if(rkx.ne.0.d0)Wkx=(sin(rkx/2.d0)/(rkx/2.d0))**4
         cfac=cf/(Wkx*Wky*Wkz)

         cma=ci*xrec*yrec*zrec
         cmb=ci*xrec*yrec*dconjg(zrec)
         cmc=ci*xrec*dconjg(yrec)*zrec
         cmd=ci*xrec*dconjg(yrec*zrec)

         c000=dcl(ix,iy ,iz )*(c1-cma)+dconjg(dcl(icx,icy,icz))*(c1+cma)
         c001=dcl(ix,iy ,icz)*(c1-cmb)+dconjg(dcl(icx,icy,iz ))*(c1+cmb)
         c010=dcl(ix,icy,iz )*(c1-cmc)+dconjg(dcl(icx,iy ,icz))*(c1+cmc)
         c011=dcl(ix,icy,icz)*(c1-cmd)+dconjg(dcl(icx,iy ,iz ))*(c1+cmd)

         dcl(ix,iy ,iz )=c000*cfac
         dcl(ix,iy ,icz)=c001*cfac
         dcl(ix,icy,iz )=c010*cfac
         dcl(ix,icy,icz)=c011*cfac
         dcl(icx,iy ,iz )=dconjg(dcl(ix,icy,icz))
         dcl(icx,iy ,icz)=dconjg(dcl(ix,icy,iz ))
         dcl(icx,icy,iz )=dconjg(dcl(ix,iy ,icz))
         dcl(icx,icy,icz)=dconjg(dcl(ix,iy ,iz ))

         xrec=xrec*rec
        enddo
c       End loop over ix
        yrec=yrec*rec
       enddo
c      End loop over iy
       zrec=zrec*rec
      enddo
c     End loop over iz

c Convert this 3-d powerspec into a useful powerspec.
      count=0
      do iz=1,L
       ikz=mod(iz+L/2-2,L)-L/2+1
       do iy=1,L
        iky=mod(iy+L/2-2,L)-L/2+1
        do ix=1,L/2+1
         rk=sqrt(dble((ix-1)**2+iky**2+ikz**2))
         ct=dcl(ix,iy,iz)
         if(ct.eq.dcmplx(0.d0,0.d0)) ct=dcmplx(1.0d-30,0.d0)
         cmod=cdabs(ct)
         if(cmod.le.0.d0)cmod=1.0d-18
         pk=cmod**2
         count=count+1
         k(count)=rk
         p(count)=pk
        enddo
       enddo
      enddo

c End of powerspec
      RETURN
      END
