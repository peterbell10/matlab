function [A,unresolved,resolved]=h_proj_core(x,y,z,h,L,N,RBCFlag)
% syntax: [A,unresolved,resolved]=h_proj_core(x,y,z,h,L,N,RBCFlag)
%
% Given the particle positions x,y (scaled to L) and the characteristic
% z, using h the particle characteristic is smoothed in the x-y direction
% and the column density if summed in LxL columns and returned in A.
% The number of resolved and unresolved particles are returned, as well.
%
% Note, do not usually call this explicitly.  Use h_proj instead.
% Also note, if the mex file is available (for which there is src
% code) it will be used instead.
%
% See: h_proj

%Some info we'll need for every step
ix_min=floor(x-2*h); iy_min=floor(y-2*h);
ix_min=ix_min.*(ix_min>0); iy_min=iy_min.*(iy_min>0);
ix_max=floor(x+2*h); iy_max=floor(y+2*h);
ix_max=ix_max.*(ix_max<=L-1)+(L-1)*(ix_max>L-1);
iy_max=iy_max.*(iy_max<=L-1)+(L-1)*(iy_max>L-1);
ix=floor(x); iy=floor(y);

zdA=h.^(-2).*z;
pos_x=(1&[1:L]')*[1:L]-0.5;
pos_y=[1:L]'*(1&[1:L])-0.5;

dist=0*pos_x;
A=0*([1:L]'*[1:L]);
unresolved=0; resolved=0;

for i=1:N
 if(0.5>h(i))
  A(iy(i)+1,ix(i)+1)=A(iy(i)+1,ix(i)+1)+z(i); %Put everything in one cell
  unresolved=unresolved+1;
 else
  rangex=[ix_min(i)+1:ix_max(i)+1]; rangey=[iy_min(i)+1:iy_max(i)+1];
  dist(rangey,rangex)=sqrt((pos_x(rangey,rangex)-x(i)).^2+(pos_y(rangey,rangex)-y(i)).^2)/h(i);
  A(rangey,rangex)=A(rangey,rangex)+kernel_2d(dist(rangey,rangex))*zdA(i);
  resolved=resolved+1;
 end
end
