      SUBROUTINE h_proj_core(x,y,z,h,L,N,RBCFlag,A,unresolved,resolved)
C Project the value of z(i) at x(i),y(i) over a range of cells using a smoothing
C kernel.  The smoothing kernel is a 2-D projection of the hydra 3-D kernel.
C
C ARGUMENTS
C  Input, not modified
C   x   N-vector of x positions.
C   y   N-vector of y positions.
C   z   N-vector of z values.  Can be mass, temperature, velocity, ...
C   h   N-vector of smoothing radii.
C   L   The number of cells per side in the final image, A.
C   N   The number of elements in x,y,z, & h
C   RBCFlag     Repeating Boundary Conditions flag (0==off, otherwise on)
C  Output, initialised and modified
C   A           LxL array storing the sum of the projected values of z.
C   unresolved  Number of particles with h small enough that z fits in 1 cell.
C   resolved    Number of particles with h big enough that z is spread over
C               more than 1 cell.
C
C Notes: x and y do not have to be in the range [0:L].  If they are outside
C       the range, the range of cells used is restricted from falling below
C       1 and exceeding L.
C
C AUTHOR: Eric Tittley
C
C HISTORY
C  00 11 12 Added check in the unresolved case to operate only on particles
C           within the image bounds.
C  02 10 31 Added some comments.
C  07 04 10 Added RBCFlag.

      IMPLICIT NONE
c Unmodified
      integer*4 L,N,RBCFlag
      real*8 x(N),y(N),z(N),h(N)
c Modified and returned
      real*8 A(L,L)
      integer*4 unresolved,resolved

c Local variables
      integer i,j,k,ix,iy,jj,kk
      integer ix_min,ix_max,iy_min,iy_max
      real*8 dist,rL
      real*8 rx,ry,rz,rh,dx2
      real*8 x_plus_half,y_plus_half,z_per_h2

c Function
      real*8 kernel_2d

c Debugging
c      real maxz,minz

c     Clear the array
      do i=1,L
       do j=1,L
        A(j,i)=0.d0
       enddo
      enddo

      unresolved=0
      resolved=0
      rL=real(L)
      do i=1,N
       rh=h(i)
       if(0.5d0.gt.rh) then ! if 2h < cell size
        iy=int(y(i))+1
        ix=int(x(i))+1
        if(iy.ge.1 .and. iy.le.L .and. ix.ge.1 .and. ix.le.L) then
         A(iy,ix)=A(iy,ix) + z(i)
         unresolved=unresolved+1
        endif
       else ! 2h > cell size, so spread it out
        rx=x(i)
        ry=y(i)
        rz=z(i)
        x_plus_half=rx+0.5d0
        y_plus_half=ry+0.5d0
        z_per_h2=rz/rh**2
        ix_min=int(rx-2.d0*rh)
        iy_min=int(ry-2.d0*rh)
        ix_max=int(rx+2.d0*rh)
        iy_max=int(ry+2.d0*rh)
        if(RBCFlag .eq. 0) then ! No Repeating Boundary Conditions
         if(ix_min.lt.0) ix_min=0
         if(iy_min.lt.0) iy_min=0
         if(ix_max.gt.L-1) ix_max=L-1
         if(iy_max.gt.L-1) iy_max=L-1
         do j=ix_min+1,ix_max+1
          dx2 = (dble(j)-x_plus_half)**2
          do k=iy_min+1,iy_max+1
           dist=sqrt( dx2 + (dble(k)-y_plus_half)**2 )/rh
           A(k,j)=A(k,j)+kernel_2d(dist)*z_per_h2
          enddo
         enddo
        else ! Repeating Boundary Conditions
         do jj=ix_min+1,ix_max+1
          j = jj ! set default
          if(jj .lt. 1) j = jj + L
          if(jj .gt. L) j = jj - L
          dx2 = (real(jj)-x_plus_half)**2
          do kk=iy_min+1,iy_max+1
           k = kk ! set default
           if(kk .lt. 1) k = kk + L
           if(kk .gt. L) k = kk - L
           dist=sqrt( dx2 + (real(kk)-y_plus_half)**2 )/rh
           A(k,j)=A(k,j)+kernel_2d(dist)*z_per_h2
          enddo
         enddo
        endif
        resolved=resolved+1
       endif
      enddo

      RETURN
      END

C -------------------------------------
      FUNCTION kernel_2d(x)
      real*8 kernel_2d
      real*8 x
      
      if((x.lt.1.d0).and.(x.ge.0.d0)) then
       kernel_2d=0.4767d0+(0.0210d0-0.8009d0*x+0.4146d0*x*x)*x
c      elseif((x.ge.1.).and.(x.lt.2.)) then
      elseif((x.ge.1.d0).and.(x.lt.1.81d0)) then
       kernel_2d=0.9813d0+(-1.5187d0+0.7837d0*x-0.1349d0*x*x)*x
      else
       kernel_2d=0.d0
      endif
      
      RETURN
      END
