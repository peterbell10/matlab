      PROGRAM check
      
      include 'func.inc'
      
      integer ne
      parameter(ne=10)
      double precision ear(0:ne), param(19), photar(ne)
c     dummy is not used, but fills in the photer field in the call to xszvab

      integer i
      
      DATA ear /0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1/

c The column density of H, in 10^22 cm^-2      
      param(1) = 1.0

c The abundances (relative to solar) of the rest of the elements, multiplied
c by the H column density
      do i=2,18
       param(i) = param(1)
      enddo

c The redshift
      param(19) = 0.0

c The solar abundance table.
c Can be one of: feld angr aneb 
c      solar = 'feld'
c The photoelectic cross-sections.
c Can be one of: vern bcmc obcm
c      xsect = 'vern'
c All three should produce the same result, but they don't      
      CALL xszvab(ear, ne, param, photar)
c      CALL xsabsw(ear, ne, param(1), ifl, photar, dummy)
c      CALL xsphab(ear, ne, param(1), ifl, photar, dummy)
      
      do i=1,10
       print *,photar(i)
      enddo
      
      END
