
      SUBROUTINE xszvab(ear, ne, param, photar)

      INTEGER ne
      DOUBLE PRECISION ear(0:ne), param(19), photar(ne)

c  works out the redshifted transmission for material whose abundances 
c  are specified for the following 18 elements :
c    1   hydrogen    (1)
c    2   helium      (2)
c    3   carbon      (6)
c    4   nitrogen    (7)
c    5   oxygen      (8)
c    6   neon        (10)
c    7   sodium      (11)
c    8   magnesium   (12)
c    9   aluminium   (13)
c   10   silicon     (14)
c   11   sulphur     (16)
c   12   chlorine    (17)
c   13   argon       (18)
c   14   calcium     (20)
c   15   chromium    (24)
c   16   iron        (26)
c   17   cobalt      (27)
c   18   nickel      (28)
c  The parameters are the column densities of the 18 elements in units of
c  the column of each element in a solar abundance column of equivalent
c  hydrogen column density of 1e22 /cm/cm. Parameter 19 is the redshift.

c Arguments :
c      ear     r        i: energy ranges
c      ne      i        i: number of energies
c      param   r        i: model parameters
c      photar  r        o: transmitted fraction

      INTEGER NELTS
      PARAMETER (NELTS=18)

      INTEGER atomic(NELTS)
      INTEGER ie, status, i

      DOUBLE PRECISION column(NELTS)
      DOUBLE PRECISION zfac, elow, ehi, xsect

      CHARACTER*2 celts(NELTS)

c External references to cross section function

      DOUBLE PRECISION photo, gphoto, fgabnd
      CHARACTER fgxsct*4
      EXTERNAL photo, gphoto, fgabnd, fgxsct

      DATA atomic /1,    2,    6,    7,    8,    10,   11,   12,
     &             13,   14,   16,   17,   18,   20,   24,   26,
     &             27,   28/

      DATA celts /'H ', 'He', 'C ', 'N ', 'O ', 'Ne', 'Na', 'Mg',
     &            'Al', 'Si', 'S ', 'Cl', 'Ar', 'Ca', 'Cr', 'Fe',
     &            'Co', 'Ni'/


      status = 0

c Set the element columns

      DO i = 1, NELTS
         column(i) = param(i) * 1.e22 * fgabnd(celts(i))
      ENDDO

      zfac = 1.0 + param(19)
      elow = ear(0) * zfac

      IF ( fgxsct() .EQ. 'vern' ) THEN

         DO ie = 1, ne

            ehi = ear(ie) * zfac
            xsect = 0.

            DO i = 1, NELTS

               xsect = xsect + gphoto(elow, ehi, atomic(i), status) 
     &                          * column(i)

            ENDDO

            photar(ie) = EXP(-xsect)

            elow = ehi

         ENDDO

      ELSEIF ( fgxsct() .EQ. 'bcmc' ) THEN

         DO ie = 1, ne

            ehi = ear(ie) * zfac
            xsect = 0.

c Set transmission to unity above 100 keV.

            IF ( elow .LE. 100. ) THEN

               DO i = 1, NELTS

                  xsect = xsect + photo(elow, ehi, atomic(i), 3, status) 
     &                             * column(i)

               ENDDO

               photar(ie) = EXP(-xsect)

            ELSE

               photar(ie) = 1.0

            ENDIF

            elow = ehi

         ENDDO

      ELSEIF ( fgxsct() .EQ. 'obcm' ) THEN

         DO ie = 1, ne

            ehi = ear(ie) * zfac
            xsect = 0.

c Set transmission to unity above 100 keV.

            IF ( elow .LE. 100. ) THEN

               DO i = 1, NELTS

                  xsect = xsect + photo(elow, ehi, atomic(i), 2, status) 
     &                             * column(i)

               ENDDO

               photar(ie) = EXP(-xsect)

            ELSE

               photar(ie) = 1.0

            ENDIF

            elow = ehi

         ENDDO

      ELSE

         DO ie = 1, ne
            photar(ie) = 1.
         ENDDO

      ENDIF

      RETURN
      END



