% trans=wabs(bins,nH,Abund,z)
%
% Computes the transmission function due to a column of gas.
%
% input: bins(nbin+1) lower and upper boundaries of energy bins in keV
%        nH          - hydrogen column density (in 10^22 cm**-2)
%        Abund(17)   - elemental abundances w.r.t. solar values
%        z           - redshift of gas (usually 0, because the bulk is
%		       due to galactic absorbtion
%
% output:trans(nbin) - transmission fraction
%

% HISTORY:
%  00 12 20 First version
%	Based on the core routine xszvab.f taken from FTOOLS v5.02.
