function sigma=v_disp(r,v,h)
% v_disp: Calculates the velocity dispersion of a set of particles
%
% syntax: sigma=v_disp(r,v,h);
%
% ARGUMENTS
%  r   3xN array of particle positions
%  v   3xN array of particle velocities
%  h   N vector of smoothing radii
%
% RETURNS
%  the velocity dispersion for the particles at positions r and velocities, v
%
% The dispersion is simply (| v(i,:) - mean(v(neighbours,:) |)^2 where
% neighbours are the particles within 2*h(i) of the particle.
% The velocities are weighted by the SPH kernel.
%
% SEE ALSO
%  calcdens, calcdens_d

% AUTHOR: Eric Tittley
%
% HISTORY
%  080812 Mature version
%  110627 Regularised comments
