C
C AUTHOR: Eric Tittley
C
C HISTORY
C  1999-06-03 Mature function
C  2002-10-16 Modified for [3xN] format for r & v for new hydra
C  2002-10-24 Speed up by doing multiple passes with different values of L
C       starting at the maximum ( 1/L > 2*h_min ).  At each pass, L is
C       approximately halved.
C       This speedup lets the high-density particles be done first, then they
C       get marked as done.
C  2004-07-27 L is an unused argument to particle_dispersion.  Removed.
C  2008-03-25 Loop over inner indices first.
C
C BUGS
C  The boundaries do not appear to be periodic.  I'm finding stranded particles
C  at the edge of the box (sigma==0, since they are alone).

      SUBROUTINE v_disp(r,v,h,Nparticles,Lmax,sigma,
     &                  box_index,ll,elements,done,ll_start,ll_holder)

      IMPLICIT NONE
      include 'v_disp.inc'
c Unmodified
      integer Nparticles, Lmax
      real*8 r(3,Nparticles),v(3,Nparticles),h(Nparticles)
c Modified and returned
      real*8 sigma(Nparticles)
c Pre-allocated work spaces
      integer box_index(3,Nparticles)
      integer ll(Nparticles)
      integer elements(Nparticles)
c  The Fortran standard requires logicals to have the same size as integers.
      logical done(Nparticles)
c     Variables for the box indices
      integer ll_start(Lmax,Lmax,Lmax)
      integer ll_holder(Lmax,Lmax,Lmax)

c local variables
      integer i,j
      logical Ntodo
c     Cell dimensions
      integer L,L1,L2,L3
      real*8 Linv,Lreal,h_min,hi,sigma_tmp
c     The list of neighbour particles
      integer count
c     The list of particles left to do

c     Find initial box size (4*h_min)
      h_min=h(1)
      do i=2,Nparticles
       h_min=min(h_min,h(i))
      enddo
      if(h_min.le.0.) then
       CALL exit(-1)
      endif

c     Initialse the done list
      do i=1,Nparticles
       done(i)=.false.
      enddo

c     Main loop, over which L is halved each step, until all particles are done
      Ntodo=.true.
      do while(Ntodo)
c      Calculate L, the mesh size.  We will start with small boxes (big L) and
c      work our way to big boxes (small L) to get the remaining particles in
c      low-density regions.
       h_min=h_min*2.0
       L=nint(1./(2.*h_min)-0.51)
       if(L.gt.Lmax) then
        L=Lmax
       endif
       Lreal = dble(L)
       Linv  = 1./Lreal

ccc With our initial value for L
ccc  Index all the particles onto a mesh of length L
ccc  Make a link list for all the particles
ccc  Loop through all mesh cells
ccc   Compile a list of particles in the mesh cell and 26 surrounding cells
ccc   Loop through all particles in the main cell
ccc   If 2h(i) <= Linv
ccc    Calculate the dispersion
ccc    Mark done(i)->0

c      Index the particles into their boxes.  Now each particle 'knows' which
c      box it belongs in.  Passing this to boxlist will gives us lists of all
c      the particles in each box.
       do j=1,Nparticles
        do i=1,3
         box_index(i,j)=int(r(i,j)*Lreal)+1
c        Pick up strays due to rounding errors.
         if(box_index(i,j).lt.1) then
          box_index(i,j)=1
         endif
         if(box_index(i,j).gt.L) then
          box_index(i,j)=L
         endif
        enddo
       enddo
     
c      Compile the box_list into a link list
       CALL boxlist(Nparticles,1,L,Lmax,box_index,ll,ll_start,ll_holder)

c      Try to calculate the dispersion of each particle
       do L3=1,L
        do L2=1,L
         do L1=1,L
c         Find all the particles in this box and its neighbours
          CALL neighbourlist(Nparticles,L,Lmax,L1,L2,L3,ll_start,ll,
     &                       elements,count)
c         For each of the particles in this box, find the dispersion.
          i=ll_start(L1,L2,L3)
          do while( i.ne.0 )
           hi=h(i)
           if( (.not.done(i)) .and. ((2.0*hi).lt.Linv )) then
            CALL particle_dispersion(i,elements,count,hi,
     &                               Nparticles,r,v,sigma_tmp)
            sigma(i)=sigma_tmp
            done(i)=.true.
           endif ! particle is to be done and its h fits in the mesh
           i=ll(i)
          enddo
c        end looping over neighbour particles
         enddo ! end loop over L1
        enddo ! end loop over L2
       enddo ! end loop over L3
c      end looping over all boxes

c      Find the number of particles left to do
       i=1
       do while( done(i) )
        i=i+1
       enddo
       if(i.lt.Nparticles) then
        Ntodo=.true.
       else
        Ntodo=.false.
       endif

      enddo ! end do while Ntodo > 0

      RETURN
      END
