      SUBROUTINE boxlist(n,L_lo,L_hi,Lmax,box_index,ll,
     &                   ll_start,ll_holder)
C Creates a link list (ll) containing a list of all the particles in the
C boxes in L_lo to L_hi. (L_lo is usually 1, and L_hi is usually L)
C ll_start points, for each box, to the first element.
C This is accomplished in one quick step.
C
C AUTHOR: Eric Tittley
C
C HISTORY
C  1999-06-03 Mature version
C  2002-10-16 Changed to presume box_index in (3,Nmax) instead of (Nmax,3)
C  2008-03-25 Loop over inner indices first.
C
      IMPLICIT NONE
      include 'v_disp.inc'
c Input variables
      integer n,L_lo,L_hi,Lmax
      integer box_index(3,n)
c Output variables
      integer ll(n)
      integer ll_start(1:Lmax,1:Lmax,1:Lmax)
c Pre-allocated workspace
      integer ll_holder(1:Lmax,1:Lmax,1:Lmax)

c Local variables
      integer L1,L2,L3,i

c Other variables for the link list
      integer particle

c     Clear variables
      do i=1,n
       ll(i)=0
      enddo
      do L3=L_lo,L_hi
       do L2=L_lo,L_hi
        do L1=L_lo,L_hi
         ll_start(L1,L2,L3)=0
        enddo
       enddo
      enddo
      do L3=L_lo,L_hi
       do L2=L_lo,L_hi
        do L1=L_lo,L_hi
         ll_holder(L1,L2,L3)=0
        enddo
       enddo
      enddo

c     Compile the link list
      do i=1,n
       L1=box_index(1,i)
       L2=box_index(2,i)
       L3=box_index(3,i)
       if(L1.lt.L_lo .or. L1.gt.L_hi) then
        print *,'L1 out of bounds'
        CALL exit(-1);
       endif
       if(L2.lt.L_lo .or. L2.gt.L_hi) then
        print *,'L2 out of bounds'
        CALL exit(-1);
       endif
       if(L3.lt.L_lo .or. L3.gt.L_hi) then
        print *,'L3 out of bounds'
        CALL exit(-1);
       endif
       if(ll_start(L1,L2,L3).gt.0) then
        particle=ll_holder(L1,L2,L3)
        ll(particle)=i
       else
        ll_start(L1,L2,L3)=i
       endif
       ll_holder(L1,L2,L3)=i
      enddo

      RETURN
      end
