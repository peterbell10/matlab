      SUBROUTINE neighbourlist(Nparticles,L,Lmax,L1,L2,L3,ll_start,ll,
     &                         elements,count)
C Finds the list of particles in the surrounding boxes, elements,
C and their distances, dist.
C We will limit the particles found to those within 3 h of the particle
C
C ARGUMENTS
C  Input, not modified
C   Nparticles                  The number of particles
C   L                           The number of cells per dimension in the mesh
C   L1,L2,L3                    The index of the cell in question
C   ll_start(L_max,L_max,L_max) The list of starting elements for each cell
C   ll(Nparticles)              The link-list
C  Output
C   elements(Nparticles)        The list of particles in the cell and           C                               neigbouring cells.
C   count                       The number of particles in the list, elements.
C
C AUTHOR: Eric Tittley
C
C HISTORY
C  02 10 24 Mature version.  Added support for when L<4.
C  08 03 25 Rearranged looping over start to loop through inner index.

      IMPLICIT NONE
      include 'v_disp.inc'
c Arguments input, not modified
      integer Nparticles
      integer L,L1,L2,L3,Lmax
      integer ll_start(Lmax,Lmax,Lmax)
      integer ll(Nparticles)
c Arguments output, modified
      integer elements(Nparticles),count

c local variables
      integer j
      integer dL1,dL2,dL3
      integer bL1,bL2,bL3

      count=0
      if(L.ge.4) then
c      We are looking for a subset of the entire set of particles
c      Loop over surrounding boxes to find neighbours
       do dL3=-1,1
        bL3=L3+dL3
        if(bL3.eq.0) then
         bL3=L
        elseif(bL3.eq.L+1) then
         bL3=1
        endif
        do dL2=-1,1
         bL2=L2+dL2
         if(bL2.eq.0) then
          bL2=L
         elseif(bL2.eq.L+1) then
          bL2=1
         endif
         do dL1=-1,1
          bL1=L1+dL1
          if(bL1.eq.0) then 
           bL1=L
          elseif(bL1.eq.L+1) then
           bL1=1
          endif
c         Loop over particles in a surrounding box
          j=ll_start(bL1,bL2,bL3)
3         if(j.ne.0) then
           count=count+1
           elements(count)=j
           j=ll(j)
           goto 3
          endif
c         Finshed looping over particles in a surrounding box
         enddo
        enddo
       enddo
c      End looping over 27 surrounding boxes
      else ! elements will contain all the particles
       do bL3=1,L
        do bL2=1,L
         do bL1=1,L
c         Loop over particles in a surrounding box
          j=ll_start(bL1,bL2,bL3)
4         if(j.ne.0) then
           count=count+1
           elements(count)=j
           j=ll(j)
           goto 4
          endif
c         Finshed looping over particles in a surrounding box
         enddo
        enddo
       enddo
c      End looping over 27 surrounding boxes
      endif ! if L>=4, else, end

      RETURN
      END
