      SUBROUTINE GetNSmallest(NumNeighbours,Nsph,dist,distance)
C Find index to the Nsph smallest values of dist.
C
C ARGUMENTS
C  Input, not modified
C   NumNeighbours       The number of elements in dist
C   Nsph                The number of smallest elements to index
C
C  Input, modified
C   dist                The elements to sort
C
C  Input and initialised
C   distance            The distances for the Nsph smallest.
C
C AUTHOR: Eric Tittley

      IMPLICIT NONE
      integer NumNeighbours,Nsph
      real dist(NumNeighbours)
      real distance(Nsph)
      
      real min
      integer i,j,minindx
      
c     For each of the Nsph nearest, loop through finding the smallest
c     value.  When found, set the dist to 1e29 so we don't find it
c     again.
      do i=1,Nsph
       min=1.0e29
       minindx=-1
       do j=1,NumNeighbours
        if(dist(j).lt.min) then
         min=dist(j)
         minindx=j
        endif
       enddo
       distance(i)=min
       dist(minindx)=1.0e29
      enddo
      
      END
