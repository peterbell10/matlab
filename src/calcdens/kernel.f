      SUBROUTINE createkernel(ntp,wtab,dwtab)
c
c  creates lookup tables for the smoothing kernel and its derivative
c
      IMPLICIT NONE
      integer ntp
      real wtab(0:ntp+1), dwtab(0:ntp+1)

c     Functions
      real kernel,kegrad

c     Local variables
      integer i
      real rad2,rad

      wtab(0)=kernel(0.)
      dwtab(0)=0.
      do i=1,ntp
       rad2=float(i)*4./float(ntp)
       rad=sqrt(rad2)
       wtab(i)=kernel(rad)
       dwtab(i)=kegrad(rad)
      enddo
      wtab(ntp+1)=0.
      dwtab(ntp+1)=0.

      RETURN
      end
c--------------------------------------------------------------
      real FUNCTION kernel(x)
c  smoothing kernel
c
      real x,pi,wnorm
      parameter(pi=3.141592654)
      wnorm=1./4./pi
      kernel=0.0
      if (x.lt.0.0) then
       kernel=0.0
      else if (x.le.1.0) then
       kernel=wnorm*(4.-6.*x**2+3.*x**3)
      else if (x.le.2.0) then
       kernel=wnorm*(2.-x)**3
      end if
c
      RETURN
      end
c-------------------------------
c
      real FUNCTION kegrad(x)
c  (1/r)(d/dr)smoothing kernel
c
      real x,pi,gnorm
      parameter(pi=3.141592654)
      gnorm=-1./4./pi
      if (x.le.0.0) then
         STOP 'kegrad: invalid range for argument'
      else if (x.le.0.66666) then
         kegrad=gnorm*4./x
      else if (x.le.1.0) then
         kegrad=gnorm*3.*(4.-3.*x)
      else if (x.le.2.0) then
         kegrad=gnorm*3.*(2.-x)**2/x
      else
         STOP 'kegrad: invalid range for argument'
      end if
c
      RETURN
      end
c--------------------------------
      real FUNCTION hsmooth(x)   
c  smoothing kernel
c
      real x,sx,wnorm
      wnorm=0.25
      if (x.lt.0.0) then
         hsmooth=0.
      else if (x.le.1.5) then
         hsmooth=1.
      else if (x.le.1.75.and.x.gt.1.5) then
         sx=4.*(x-1.5)
         hsmooth=wnorm*(4.-6.*sx**2+3.*sx**3)
      else if (x.le.2.0.and.x.gt.1.75) then
         sx=4.*(x-1.5)
         hsmooth=wnorm*(2.-sx)**3
      else
         STOP 'hsmooth: invalid range for argument'
      end if
c
      RETURN
      end
c-------------------------------

