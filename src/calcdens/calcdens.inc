c The number of bins in the kernel table
      integer ntp
      parameter(ntp=3000)
c The factor by which to increase h between iterations, H_FACT^3=2
      real H_FACT
      parameter (H_FACT=1.2599210)
c Maximum number of neighbours. Theoretically this is Nmax.  But is likely
c much smaller.
      integer NumNeighboursMax
      parameter(NumNeighboursMax=524288)
