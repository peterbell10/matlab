      SUBROUTINE SortCellsByNcounts(L,Ncounts,indx,Ncounts_long)
C Given cells with counts, Ncounts, produce an index which lists the
C cells in order of decreasing number of counts.
C
C ARGUMENTS
C  Input, not modified
C   L   The number of mesh cells per dimension
C   NCounts(0:L-1,0:L-1,0:L-1)  The number of particles in each cell
C  Output
C   indx(L*L*L) The sorted index into NCounts, largest first.
C       indx has values in the range of 0 to L^3-1, so you need to reconstruct
C       the ix,iy,iz values.
C
C AUTHOR: Eric Tittley
C
C HISTORY
C  02-11-11
C       Let compiler find psize.inc
C       Add IMPLICIT NONE
C  07 01 23 Remove need for psize.inc.  Use calcdens.inc
C  13 05 20 Removed need for calcdens.inc.  Big arrays must be pre-allocated.

      IMPLICIT NONE
c Input, not modified
      integer*8 L
      integer Ncounts(0:L-1,0:L-1,0:L-1)
c Output, modified
      integer*8 indx(L*L*L)
C Pre-allocated local storage
      integer*8 Ncounts_long(0:L*L*L-1)

c Local variables
      integer*8 L2,L3,L3m1
      integer*8 i,j,k

      L2=L*L
      L3=L2*L
      L3m1=L3-1

c copy the counts in the cells to a vector
c Note, this may be elliminated if Ncounts were stored
c in this format in the first place.
      do k=0,L-1
       do j=0,L-1
        do i=0,L-1
         Ncounts_long(k*L2 + j*L + i)=int(Ncounts(i,j,k),8)
        enddo
       enddo
      enddo
      
c Note: Ncounts_long goes from 0 here, but will be index from 1 in isort
      CALL isort(L*L*L,Ncounts_long,indx)
      
c We don't need Ncounts_long anymore, so we will use it as temporary
c storage for flipping over the sorted index.
c Ideally, we should modifiy isort to sort things largest to smallest.
c 1 will be subtracted from all indx values to bring it back in line
c with the 0:L^3-1 format used elsewhere
      do i=0,L3m1
       Ncounts_long(i)=indx(i+1)-1
      enddo
      do i=0,L3m1
       indx(i+1)=Ncounts_long(L3m1-i)
      enddo

      RETURN
      END
