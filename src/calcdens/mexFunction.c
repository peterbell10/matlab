#include <mex.h>

#include <stdlib.h>
#include <stdio.h>

#ifdef _OPENMP
# include <omp.h>
#endif

/* There is a problem with mexErrMsgTxt and some glibc */
#define mexErrMsgTxt(string) printf("%s\n",string); exit(-1);

void densities_(long int *N, float *r, int *Nsph, float *h, float *dn,
                int *box_index, int *ll, int *particle_is_converged,
		int *elements,
#ifdef MEMSORT
                int *sorted_index, int *unsorted_index, long int *cell_index,
		float *AI_Storage, int *Ncounts,
		long int *Ncounts_long, long int *indx,
#endif
		long int *L_Max, int *ll_start, int *ll_holder,
		int *NumThreads, int *ErrorCode,
                float *dist, float *distance);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 float *r;
 double *dNsph;
 int    Nsph;
 double *h_guess;

 double *h;
 double *dn;
 float *hf;
 float *dnf;

 /* Pre-allocated storage areas */
 int *box_index;
 int *ll;
 int *particle_is_converged;
 int *elements;
 int *ll_start;
 int *ll_holder;
 float *dist;
 float *distance;
#ifdef MEMSORT
 int *sorted_index;
 int *unsorted_index;
 long int *cell_index;
 float *AI_Storage;
 int *Ncounts;
 long int *Ncounts_long;
 long int *indx;
#endif

 size_t mrows, ncols, i, nParticles;
 int NumThreads;
 long int L_MAX=1024; /* This should be an input parameter */
 int *ErrorCode;

 /* Check for the proper number of arguments */
 if( nlhs!=2 || nrhs!=3 ) {
  mexErrMsgTxt("Syntax: [h,dn]=calcdens(r,Nsph,h_guess)");
 }

 /* r must be of the type single */
 if( !mxIsSingle(prhs[0]) ) {
  mexErrMsgTxt("r must be single. Use calcdens_d() instead.");
 }

 /* Nsph must be of the type int */
 if( !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("Nsph must be a double. (It will be converted to int).");
 }

 /* h_guess must be a double */
 if( !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("h_guess must be a double.");
 }

 /* The second argument must be scalar */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("Nsph must be scalar");
 }

 /* The third argument must be scalar */
 mrows = mxGetM(prhs[2]);
 ncols = mxGetN(prhs[2]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("h_guess must be scalar");
 }

 /* The first argument must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows!=3 || ncols<11) {
  mexErrMsgTxt("r must be a 3xN array with N>10");
 }
 nParticles = ncols;

 /* Assign pointers to the input */
 r       = (float*)mxGetPr(prhs[0]);
 dNsph   = mxGetPr(prhs[1]);
 h_guess = mxGetPr(prhs[2]);

 /* Convert to integer */
 Nsph    = (int)(*dNsph);

 hf = (float*)malloc(nParticles*sizeof(float));
 if(hf==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for hf.\n");
  return;
 }
 /* Set h to the guess value */
 for(i=0;i<nParticles;i++) { hf[i]=(float)(*h_guess); }

 dnf = (float*)malloc(nParticles*sizeof(float));
 if(dnf==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for dnf.\n");
  return;
 }

 /* Pre-allocated local storage */
 box_index = (int*)malloc(3*nParticles*sizeof(int));
 if(box_index==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for box_index.\n");
  return;
 }
 ll = (int*)malloc(nParticles*sizeof(int));
 if(ll==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for ll.\n");
  return;
 }
 particle_is_converged = (int*)malloc(nParticles*sizeof(int));
 if(particle_is_converged==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for particle_is_converged.\n");
  return;
 }
 ll_start = (int*)malloc(L_MAX*L_MAX*L_MAX*sizeof(int));
 if(ll_start==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for ll_start.\n");
  return;
 }
 ll_holder = (int*)malloc(L_MAX*L_MAX*L_MAX*sizeof(int));
 if(ll_holder==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for ll_holder.\n");
  return;
 }
#ifdef _OPENMP
#pragma omp parallel
 {
  NumThreads=omp_get_num_threads();
 }
#else
 NumThreads=1;
#endif
 elements = (int*)malloc((size_t)NumThreads*nParticles*sizeof(int));
 if(elements==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for elements.\n");
  return;
 }

 ErrorCode = (int*)calloc((size_t)NumThreads,sizeof(int));
 if(ErrorCode==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for ErrorCode.\n");
  return;
 }

 dist = (float *)malloc((size_t)NumThreads*nParticles*sizeof(double));
 if(dist==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for dist.\n");
  return;
 }

 distance = (float *)malloc((size_t)NumThreads*(size_t)Nsph*sizeof(double));
 if(distance==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for distance.\n");
  return;
 }

#ifdef MEMSORT
 sorted_index = (int*)malloc(nParticles*sizeof(int));
 if(sorted_index==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for sorted_index.\n");
  return;
 }
 unsorted_index = (int*)malloc(nParticles*sizeof(int));
 if(unsorted_index==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for unsorted_index.\n");
  return;
 }
 cell_index = (long int*)malloc(nParticles*sizeof(long int));
 if(cell_index==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for cell_index.\n");
  return;
 }
 AI_Storage = (float*)malloc(3*nParticles*sizeof(float));
 if(AI_Storage==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for AI_Storage.\n");
  return;
 }
 Ncounts = (int*)malloc(((L_MAX/8)*L_MAX*L_MAX)*sizeof(int));
 if(Ncounts==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for Ncounts.\n");
  return;
 }
 Ncounts_long = (long int*)malloc(((L_MAX/8)*L_MAX*L_MAX)*sizeof(long int));
 if(Ncounts_long==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for Ncounts_long.\n");
  return;
 }
 indx = (long int*)malloc(((L_MAX/8)*L_MAX*L_MAX)*sizeof(long int));
 if(indx==NULL) {
  printf("ERROR: calcdens: Unable to allocate memory for indx.\n");
  return;
 }
#endif

 /* Call the subroutine */
 long int n = (long int)nParticles;
 densities_(&n,r,&Nsph,hf,dnf,
            box_index,ll,particle_is_converged,elements,
#ifdef MEMSORT
            sorted_index, unsorted_index, cell_index, AI_Storage,
	    Ncounts, Ncounts_long, indx,
#endif
	    &L_MAX,ll_start,ll_holder,&NumThreads,ErrorCode,dist,distance);
 
 /* Create matrix for the return argument and assign a pointer.  Then
  * copy the contents and release the memory. */

 /* Smoothing length */
 plhs[0]=mxCreateDoubleMatrix(nParticles, 1, mxREAL);
 h = mxGetPr(plhs[0]);
 for(i=0;i<nParticles;i++) {  h[i]=(double)hf[i]; }
 free(hf);

 /* Density */
 plhs[1]=mxCreateDoubleMatrix(nParticles, 1, mxREAL);
 dn = mxGetPr(plhs[1]);
 for(i=0;i<nParticles;i++) { dn[i]=(double)dnf[i]; }
 free(dnf);

 free(box_index);
 free(ll);
 free(particle_is_converged);
 free(elements);
#ifdef MEMSORT
 free(sorted_index);
 free(unsorted_index);
 free(cell_index);
 free(AI_Storage);
 free(Ncounts);
 free(Ncounts_long);
 free(indx);
#endif
 free(ll_start);
 free(ll_holder);
 free(dist);
 free(distance);

 /* Deal with the error codes */
 for(i=0;i<(size_t)NumThreads;i++) {
  switch ( ErrorCode[i] ) {
   case 0:
    break;
   case 2:
    printf("ERROR: max(h)<0\n");
    break;
   case 3:
    printf("ERROR: max(h)<0\n");
    break;
   case 4:
    printf("ERROR: NumThreads != NThreads\n");
    break;
   case 5:
    printf("ERROR: count=0 in a neighbour list that contains at least one particle\n");
    break;
   default:
    printf("ERROR: Unknown Error %i returned from densities\n", ErrorCode[i]);
    break;
  }
 }
 free(ErrorCode);

 return;
}
