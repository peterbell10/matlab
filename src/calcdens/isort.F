      SUBROUTINE isort(n,arr,indx)
C Integer sort
C Given an array of integers, arr(n), find the index to the array
C such that arr(indx(i)) lists the values in INCREASING order. 

C HISTORY
C  Based on the rxsort.f file found with the base hydra distro.
C  Converted to integers by Eric Tittley 01 05 15

      IMPLICIT NONE
c Input, not modified
      integer*8 n
      integer*8 arr(n)
c Input, modified
      integer*8 indx(n)

c Local variables
      INTEGER*8 M,NSTACK
      PARAMETER (M=7,NSTACK=1024)
      INTEGER*8 i,indxt,ir,itemp,j,jstack,k,l,istack(NSTACK)
      integer*8 a

      do j=1,n
       indx(j)=j
      enddo
      do j=1,NSTACK
       istack(j)=-1
      enddo
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M) then
       do j=l+1,ir
        indxt=indx(j)
        a=arr(indxt)
        do i=j-1,1,-1
         if(arr(indx(i)).le.a) goto 2
         indx(i+1)=indx(i)
        enddo
        i=0
2       indx(i+1)=indxt
       enddo
       if(jstack.eq.0) return
       ir=istack(jstack)
       l=istack(jstack-1)
       jstack=jstack-2
      else
       k=(l+ir)/2
       itemp=indx(k)
       indx(k)=indx(l+1)
       indx(l+1)=itemp
       if(arr(indx(l+1)).gt.arr(indx(ir))) then
        itemp=indx(l+1)
        indx(l+1)=indx(ir)
        indx(ir)=itemp
       endif
       if(arr(indx(l)).gt.arr(indx(ir))) then
        itemp=indx(l)
        indx(l)=indx(ir)
        indx(ir)=itemp
       endif
       if(arr(indx(l+1)).gt.arr(indx(l))) then
        itemp=indx(l+1)
        indx(l+1)=indx(l)
        indx(l)=itemp
       endif
       i=l+1
       j=ir
       indxt=indx(l)
       a=arr(indxt)
3      continue
        i=i+1
       if(arr(indx(i)).lt.a) goto 3
4      continue
        j=j-1
       if(arr(indx(j)).gt.a) goto 4
       if(j.lt.i) goto 5
       itemp=indx(i)
       indx(i)=indx(j)
       indx(j)=itemp
       goto 3
5      indx(l)=indx(j)
       indx(j)=indxt
       jstack=jstack+2
c       if(jstack.gt.NSTACK) print *, 'NSTACK too small in isort'
       if(ir-i+1.ge.j-l) then
        istack(jstack)=ir
        istack(jstack-1)=i
        ir=j-1
       else
        istack(jstack)=j-1
        istack(jstack-1)=l
        l=i
       endif
      endif
      goto 1
      END
