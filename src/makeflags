# I've tried using the ICC compilers, but just can't get it to work.
# Too many undefined symbols.

# Only GCC at the moment
SUITE = GCC

# Set the HOST variable if not already set (as not done in a bash shell)
HOST ?= $(shell hostname)

# Comment out (don't set to 0) to turn off debugging
#DEBUG = 1

OPENMP = 1

# Defaults (empty)
OMP_FLAG =
OMP_LIB  =

# Compilers
ifeq ($(SUITE),GCC)
 CC  = gcc
 F77 = gfortran
 CXX = g++
 CFLAGS  = -O2 -funroll-loops -fpeel-loops -frename-registers -ftree-vectorize -fPIC
 CFLAGS += -fno-omit-frame-pointer
 CFLAGS += -march=native
 CFLAGS += -fexceptions
 CFLAGS += -m64
 CFLAGS += -mcmodel=large
 CFLAGS += -Wall
 ifeq ($(OPENMP),1)
  OMP_FLAG = -fopenmp
  OMP_LIB  = -lgomp
 endif
 FFLAGS = $(CFLAGS)
 CXXFLAGS = $(CFLAGS)
 FFLAG_EXTENDED_LINE = -ffixed-line-length-132
 FFLAG_DOUBLE_REAL = -fdefault-real-8
endif

ifdef DEBUG
 FFLAGS = -fPIC -m64 -O0 -g
 CFLAGS = -fPIC -m64 -O0 -g
endif

# Matlab R2019b requires gcc <= 6.3.
# Ubuntu 19.10 dropped the gcc-6 series packages, so you just have
# to live with the Warning messages.
ifeq ($(HOST),cuillin)
 CC  = gcc-6
 F77 = gfortran-6
endif

# Octave-specific compilation flags
OCTAVE_INSTALL_DIR = $(HOME)/matlab/octave/oct64/
OCTEXT = mex
MKOCT = mkoctfile --mex
MKOCTFLAGS =
OCT_INC = -I/usr/include/octave
ifeq ($(HOST),arrakis)
 OCT_INC = -I/usr/include/octave-4.4.1/octave
endif
ifeq ($(HOST),cuillin)
 OCT_INC = -I/usr/include/octave-4.2.2/octave
endif
ifeq ($(HOST),nostromo)
 OCT_INC = -I/usr/include/octave-4.4.1/octave
endif
ifeq ($(HOST),phlebas)
 OCT_INC = -I/usr/include/octave-4.4.1/octave
endif

# Matlab-specific compilation flags
# defaults good for all of IfA
MATLAB_INSTALL_DIR = $(HOME)/matlab/matlab/mex
MEXEXT = mexglx
ifndef DEBUG
 MEXFLAGS = -O
endif
MEXFLAGS += -R2018a GCC=$(CC)
MEX_INC = -I/usr/local/MATLAB/current/extern/include
MEXEXT = mexa64
MEX = mex
ifeq ($(HOST),cuillin)
 MEX_INC = -I$(MATLAB_DIR)/extern/include
endif
ifeq ($(HOST),head.cluster.loc)
 MEX_INC = -I$(MATLAB_DIR)/extern/include
endif

# FFTW needs to be installed
LIB_FFTW = -L$(PWD)/../fftw/lib -lfftw3
INC_FFTW = -I$(PWD)/../fftw/include

INCS_BASE = -I.

INCS_MEX = $(INCS_BASE) $(MEX_INC)
INCS_OCT = $(INCS_BASE) $(OCT_INC)

FLIBS = -lgfortran

.f.o:
	$(F77) $(FFLAGS) -c $<

.F.o:
	$(F77) $(FFLAGS) $(FDEFS) $(INCS) -c $<

.c.o:
	$(CC)  $(CFLAGS) $(DEFS) $(INCS) -c $<

.C.o:
	$(CXX)  $(CXXFLAGS) $(DEFS) $(INCS) -c $<
