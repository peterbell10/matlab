% SPHrayTrace: Integrals along rays through SPH data
%
% Syntax: RayIntegrals=SPHrayTrace(r,z,h,Rays);
%
% ARGUMENTS
%  r      particle positions [3,N]
%  z      Quantity to integrate along rays [N]
%  h      Particle smoothing lengths [N]
%  Rays   Rays, defined by [3,2,Nrays] or, as vector,
%         [x0,y0,z0, xf,yf,zf, x0,y0,z0, xf,yf,zf, ...]
%          |---- RAY 1 -----|  |---- RAY 2 -----|
%
% RETURNS
%  RayIntegrals   The integral of z along each ray in Rays. [Nrays]
