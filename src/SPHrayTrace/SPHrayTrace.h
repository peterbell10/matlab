#ifndef _SPHRAYTRACE_H_
#define _SPHRAYTRACE_H_

#include <stddef.h>

void SPHrayTrace(const double * const r,
                 const double * const z,
		 const double * const h,
		 const double * const Rays,
		 const size_t N,
		 const size_t Nrays,
		 double * const RayIntegrals);

#endif
