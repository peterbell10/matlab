#! /usr/bin/python2

from struct import pack

bout = open('inim.lns', 'wb')

#write header
#unsigned long
bout.write(pack('L', 1000*1000))

#write data
#p1x, p1y, p1z, p2x, p2y, p2z, ... * count
for iy in range(0,1000):
    for ix in range(0,1000):
        bout.write(pack('ddd', 5000.0, 5000.0, 5000.0))
        bout.write(pack('ddd', 10.0*ix, 10.0*iy,4900.0))

bout.close()
