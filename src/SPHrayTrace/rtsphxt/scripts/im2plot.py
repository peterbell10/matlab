#! /usr/bin/python2

import numpy as np
from struct import unpack
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import Axes3D

sah = open("sah.cds","rb")
med = open("med.cds","rb")

snx = unpack("L", sah.read(8))[0]
sny = unpack("L", sah.read(8))[0]

mnx = unpack("L", med.read(8))[0]
mny = unpack("L", med.read(8))[0]

sd = np.fromfile(sah, dtype='double', count=snx*sny)
md = np.fromfile(med, dtype='double', count=mnx*mny)

srd = np.reshape(sd, (sny,snx))
mrd = np.reshape(md, (mny,mnx))
print "total ", np.sum(srd), " ", np.sum(mrd)

sah.close()
med.close()

#ns = np.rot90(ns,3)
#ns = ns[::,::-1]
drd = np.subtract(srd, mrd)

plt.imshow(drd, norm=LogNorm(), cmap='jet')
#plt.imshow(ns, cmap='jet')

#plt.imshow(ns, cmap='jet', interpolation='nearest')
#plt.imshow(ns, cmap='jet')

#plt.contour(xi, yi, zi, 15, linewidths=0.5, colors='k')
#plt.pcolormesh(xi,yi,wi,cmap='jet',norm=LogNorm())

#plt.xlabel("X Axis of simulation (10 KPc)")
#plt.ylabel("Y Axis of simulation (10 KPc)")
#plt.title("Column Density of projection along Z Axis")

#plt.rc('text',usetex=True)
cbar = plt.colorbar()
#cbar.ax.set_ylabel("Column Density (\(cm^{-2}\))")
#plt.scatter(x,y,marker='o',c='b',s=5,zorder=10)
#plt.xlim(xmin,xmax)
#plt.ylim(ymin,ymax)
plt.show()
#plt.savefig("im.png",format="png")
