#! /usr/bin/python2

from struct import pack

bout = open('rayimage.rays', 'wb')

#write header
#unsigned long
bout.write(pack('L', 1))

#write data
#double
#p1x, p1y, p1z, p2x, p2y, p2z, ... * count
bout.write(pack('ddd', 0.0, 0.0, 0.0))
bout.write(pack('ddd', 10000.0, 10000.0, 10000.0))

bout.close()
