#! /usr/bin/python

import numpy as np
from struct import unpack
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import Axes3D

fid = open("rad.cds","rb")
cou = unpack("L",fid.read(8))[0]
print cou

tx = np.fromfile(fid,dtype='double',count=cou)
ty = np.fromfile(fid,dtype='double',count=cou)
tz = np.fromfile(fid,dtype='double',count=cou)
tw = np.fromfile(fid,dtype='double',count=cou)

fid.close()

filt = np.logical_and((tz>4000),(tz<6000))
x = tx[filt]
y = ty[filt]
z = tz[filt]
w = tw[filt]

print "read"

ny, nx = 1000, 1000
xmin, xmax = 0, 10000
ymin, ymax = 0, 10000

xi = np.linspace(xmin,xmax,nx)
yi = np.linspace(ymin,ymax,ny)
wi = ml.griddata(x, y, w, xi, yi)

print "inter"

#plt.contour(xi, yi, zi, 15, linewidths=0.5, colors='k')
plt.pcolormesh(xi,yi,wi,cmap='jet',norm=LogNorm())

plt.colorbar()
#plt.scatter(x,y,marker='o',c='b',s=5,zorder=10)
#plt.xlim(xmin,xmax)
#plt.ylim(ymin,ymax)
#plt.show()
plt.savefig("rad.png",format="png")
