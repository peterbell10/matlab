#include <cassert>
#include <ctime>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <iostream>

#include "RTSPH.h"

#define grange 30.999999
#define gdf 3.225806e-02
#define ginvdf 31.0f
#define length 512

#define warps 1

//shared interpolation data
__shared__ float gsamples[32];
__shared__ float gvalues[32];
__shared__ float gderivatives[32];

//shared sphere data cache
__shared__ float cache_cx[32*warps];
__shared__ float cache_cy[32*warps];
__shared__ float cache_cz[32*warps];
__shared__ float cache_invh2[32*warps];

//shared packet stack
__shared__ volatile int stack[32*warps];

struct GPUSpheres {
    float* cx;
    float* cy;
    float* cz;
    float* invh2;
    int count;
};

struct GPUNodes {
    int count;
    float *minx, *miny, *minz;
    float *maxx, *maxy, *maxz;
    int *right, *offset;
};

struct GPUPackets {
    int count;
    float *ox, *oy, *oz;
    float *dx, *dy, *dz;
    float *vx, *vy, *vz;
    float *tmax;
};

struct GPUKernel {
    float* nb2s;
    float* kvals;
    float* dkdnb2s;
};

__device__ bool intersectAABB(int inode, GPUNodes nodes, 
                              float ox, float oy, float oz, 
                              float vx, float vy, float vz, 
                              float tmax) {
    float txmin, txmax, tymin, tymax, tzmin, tzmax;

    if(vx >= 0) {
        txmin = (nodes.minx[inode] - ox) * vx;
        txmax = (nodes.maxx[inode] - ox) * vx;
    }
    else {
        txmin = (nodes.maxx[inode] - ox) * vx;
        txmax = (nodes.minx[inode] - ox) * vx;
    }

    if(vy >= 0) {
        tymin = (nodes.miny[inode] - oy) * vy;
        tymax = (nodes.maxy[inode] - oy) * vy;
    }
    else {
        tymin = (nodes.maxy[inode] - oy) * vy;
        tymax = (nodes.miny[inode] - oy) * vy;
    }

    if(vz >= 0) {
        tzmin = (nodes.minz[inode] - oz) * vz;
        tzmax = (nodes.maxz[inode] - oz) * vz;
    }
    else {
        tzmin = (nodes.maxz[inode] - oz) * vz;
        tzmax = (nodes.minz[inode] - oz) * vz;
    }

    float t0, t1;
    t0 = fmaxf(fmaxf(txmin, tymin), tzmin);
    t1 = fminf(fminf(txmax, tymax), tzmax);

    //ray limits
    return (t0 <= tmax) && (0.0 <= t1);
}

__device__ float intersectSphere(float cx, float cy, float cz, 
                                 float invh2, 
                                 float ox, float oy, float oz, 
                                 float dx, float dy, float dz, 
                                 float tmax) {
    //local transform
    float lx = cx - ox;
    float ly = cy - oy;
    float lz = cz - oz;
    float l2 = (lx*lx) + (ly*ly) + (lz*lz);

    //project onto direction
    float tb = (dx*lx) + (dy*ly) + (dz*lz);

    //impact parameter, divergent
    float nb2 = 1.0f;
    if((0.0f<tb) && (tb<tmax)) {
        //always (l2 >= tb*tb) => (0.0 <= nb2)
        nb2 = (l2 - (tb*tb)) * invh2;
    }

    //numerical issues (nb2 < 0.0), loss of signifigance
    return fminf(1.0f, fmaxf(0.0, nb2));
}

__device__ float interpolation(const float nb2) {
    //truncation interpolation search
    int ilow, ihigh;
    ilow = (int)(nb2 * grange);
    ihigh = ilow + 1;

    //cubic spline interpolation
    //shrd mem multicast
    float t = (nb2 - gsamples[ilow]) * ginvdf;
    float t2 = t * t;
    float t3 = t2 * t;

    float h00 = (2*t3) - (3*t2) + 1;
    float h10 = (t3 - (2*t2) + t) * gdf;

    float h01 = (-2*t3) + (3*t2);
    float h11 = (t3 - t2) * gdf;

    //shrd mem multicast
    float pl = gvalues[ilow] * h00;
    float ml = gderivatives[ilow] * h10;

    //shrd mem multicast
    float ph = gvalues[ihigh] * h01;
    float mh = gderivatives[ihigh] * h11;

    return (pl + ph) + (ml + mh);
}

/*__global__ void brute(GPUPackets packets,
                      GPUSpheres spheres, GPUKernel kernel,
                      float* weights) {
    if(threadIdx.x < 32) {
        gsamples[threadIdx.x]    = kernel.nb2s[threadIdx.x];
        gvalues[threadIdx.x]   = kernel.kvals[threadIdx.x];
        gderivatives[threadIdx.x] = kernel.dkdnb2s[threadIdx.x];
    }
    __syncthreads();

    int iray = blockIdx.x * blockDim.x + threadIdx.x;
    float ox, oy, oz, dx, dy, dz, tmax;
    ox = packets.ox[iray];
    oy = packets.oy[iray];
    oz = packets.oz[iray];
    dx = packets.dx[iray];
    dy = packets.dy[iray];
    dz = packets.dz[iray];
    tmax = packets.tmax[iray];

    float weight = 0.0f;
    for(int isph = 0; isph < spheres.count; ++isph) {
        float nb2 = intersectSphere(spheres.cx[isph],
                                    spheres.cy[isph],
                                    spheres.cz[isph],
                                    spheres.invh2[isph],
                                    ox, oy, oz,
                                    dx, dy, dz,
                                    tmax);

        if((0.0f <= nb2) && (nb2 < 1.0f)) {
            float kernel = interpolation(nb2);
            weight += kernel * spheres.invh2[isph];
        }
    }

    weights[iray] = weight;
}*/

__global__ void tracePackets(GPUPackets packets, GPUNodes nodes,
                             GPUSpheres spheres, GPUKernel kernel,
                             float* weights) {
    //load kernel function to shared memory
    if(threadIdx.x < 32) {
        gsamples[threadIdx.x]     = kernel.nb2s[threadIdx.x];
        gvalues[threadIdx.x]      = kernel.kvals[threadIdx.x];
        gderivatives[threadIdx.x] = kernel.dkdnb2s[threadIdx.x];
    }
    __syncthreads();

    int warpIdx = threadIdx.x / 32;
    int tix = threadIdx.x % 32;
    int wix = warpIdx * warpSize;

    //ray index
    int iray = blockIdx.x * blockDim.x + threadIdx.x;

    //cache ray
    float ox, oy, oz, dx, dy, dz, vx, vy, vz, tmax;
    ox = packets.ox[iray];
    oy = packets.oy[iray];
    oz = packets.oz[iray];
    dx = packets.dx[iray];
    dy = packets.dy[iray];
    dz = packets.dz[iray];
    vx = packets.vx[iray];
    vy = packets.vy[iray];
    vz = packets.vz[iray];
    tmax = packets.tmax[iray];

    //initialize stack
    int top = 0;
    //push root, divergent
    if(tix == 0) {
        stack[wix+top] = 0;
    }
        
    //stack based BVH traversal
    float weight = 0.0f;
    while(top >= 0) {
        int inode = stack[wix+top];
        top--;
        
        //intersect node
        int intersect = intersectAABB(inode, nodes, ox, oy, oz,
                                                    vx, vy, vz,
                                                    tmax);

        //if any intersect node
        if(__any(intersect)) {
            //internal node
            if(nodes.right[inode] != 0) {
                //push children, divergent
                if(tix == 0) {
                    stack[wix + top+1] = nodes.right[inode];
                    stack[wix + top+2] = inode+1;
                }
                //increment stack pointer
                top += 2;
            }
            //leaf node
            else {
                //coalesced gobl sph to shrd mem
                int off = nodes.offset[inode];
                cache_cx[wix+tix] = spheres.cx[off+tix];
                cache_cy[wix+tix] = spheres.cy[off+tix];
                cache_cz[wix+tix] = spheres.cz[off+tix];
                cache_invh2[wix+tix] = spheres.invh2[off+tix];

                //iterate sphs, broadcast shrd mem
                for(int isph = 0; isph < LEAFSIZE; ++isph) {
                    float nb2 = intersectSphere(cache_cx[wix+isph],
                                                cache_cy[wix+isph],
                                                cache_cz[wix+isph],
                                                cache_invh2[wix+isph],
                                                ox, oy, oz,
                                                dx, dy, dz,
                                                tmax
                                                );

                    //divergent
                    if((0.0f <= nb2) && (nb2 < 1.0f)) {
                        float kernel = interpolation(nb2);
                        
                        //shrd mem broadcast
                        weight += kernel * cache_invh2[wix+isph];
                        //weight += 1.0f;
                    }
                }
            }
        }
    }

    //write final weight out
    weights[iray] = weight;
}

int main(int argc, char** argv) {
    assert(argc == 2);

    //float start, end;
    clock_t start;

    std::cout << "parsing" << std::endl;
    G2Dataset data;
    std::string filename(argv[1]);
    start = std::clock();
    parseG2Dataset(filename, data);
    std::cout << "parsed in " << (std::clock()-start)/(float)CLOCKS_PER_SEC << std::endl;

    //generate tree
    std::cout << "building" << std::endl;
    BVH tree;
    Spheres sphs; float* hs;
    start = std::clock();
    data.getSpheres(sphs, hs);
    mkOBVH(sphs, hs, tree);
    delete[] hs; sphs.deallocate();
    std::cout << "built in " << (std::clock()-start)/(float)CLOCKS_PER_SEC << std::endl;
    std::cout << "bvh size " << tree.nodes.size() << std::endl;

    //generate rays
    std::cout << "generate rays" << std::endl;
    float *ox, *oy, *oz, *dx, *dy, *dz;
    float *vx, *vy, *vz, *tmax;

    int rcount = LEAFSIZE;
    //size_t rcount = 9 * length*length;
    //size_t rcount = 128 * 128 * 128;

    ox = new float[rcount];
    oy = new float[rcount];
    oz = new float[rcount];
    dx = new float[rcount];
    dy = new float[rcount];
    dz = new float[rcount];
    vx = new float[rcount];
    vy = new float[rcount];
    vz = new float[rcount];
    tmax = new float[rcount];
    
    start = std::clock();
    //use first 32 sphere packet from tree
    for(size_t isph = 0; isph < rcount; ++isph) {
        ox[isph] = 5000.0f;
        oy[isph] = 5000.0f;
        oz[isph] = 5000.0f;

        float tx, ty, tz;
        tx = 5000.0f - tree.spheres.c[0][isph];
        ty = 5000.0f - tree.spheres.c[1][isph];
        tz = 5000.0f - tree.spheres.c[2][isph];

        float l = sqrt(tx*tx+ty*ty+tz*tz);
        tmax[isph] = l;
        tx /= l; ty /= l; tz /= l;

        dx[isph] = tx;
        dy[isph] = ty;
        dz[isph] = tz;

        vx[isph] = 1.0f / dx[isph];
        vy[isph] = 1.0f / dy[isph];
        vz[isph] = 1.0f / dz[isph];
    }

    /*for(size_t isph = 0; isph < 128*128*128; ++isph) {
        //ox[isph] = tree.spheres.c[0][isph];
        //oy[isph] = tree.spheres.c[1][isph];
        //oz[isph] = tree.spheres.c[2][isph];
        ox[isph] = 5000.0f;
        oy[isph] = 5000.0f;
        oz[isph] = 5000.0f;

        float tx, ty, tz;
        //tx = tree.spheres.c[0][isph] - 5000.0f;
        //ty = tree.spheres.c[1][isph] - 5000.0f;
        //tz = tree.spheres.c[2][isph] - 5000.0f;
        tx = 5000.0f - tree.spheres.c[0][isph];
        ty = 5000.0f - tree.spheres.c[1][isph];
        tz = 5000.0f - tree.spheres.c[2][isph];

        float l = sqrt(tx*tx+ty*ty+tz*tz);
        tmax[isph] = l;
        tx /= l; ty /= l; tz /= l;

        dx[isph] = tx;
        dy[isph] = ty;
        dz[isph] = tz;

        vx[isph] = 1.0f / dx[isph];
        vy[isph] = 1.0f / dy[isph];
        vz[isph] = 1.0f / dz[isph];
    }*/

    /*size_t idx = 0;
    for(int sy = 0; sy < 3; ++sy) {
        float fy = 10000.0f * (float)(sy-1);

        for(int sx = 0; sx < 3; ++sx) {
            float fx = 10000.0f * (float)(sx-1);

            for(size_t iy = 0; iy < length; ++iy) {
                float py = (iy+0.5f)/length * 10000.0f;

                for(size_t ix = 0; ix < length; ++ix) {
                    float px = (ix+0.5f)/length * 10000.0f;

                    ox[idx] = px + fx;
                    oy[idx] = py + fy;
                    oz[idx] = -10000.0f;

                    dx[idx] = 0.0f;
                    dy[idx] = 0.0f;
                    dz[idx] = 1.0f;

                    vx[idx] = 1.0f/dx[idx];
                    vy[idx] = 1.0f/dy[idx];
                    vz[idx] = 1.0f/dz[idx];

                    tmax[idx] = 30000.0f;

                    idx++;
                }
            }
		}
    }*/
    std::cout << "produced " << rcount << " rays in "<< (std::clock()-start)/(float)CLOCKS_PER_SEC << std::endl;

    //AOS -> SOA, NODES
    std::cout << "copying" << std::endl;
    start = std::clock();

    //GPU data copy
    //device malloc
    size_t scount = tree.spheres.count;
    GPUSpheres gsphs;
    gsphs.count = scount;
    cudaMalloc((void**)&gsphs.cx, sizeof(float)*scount);
    cudaMemcpy(gsphs.cx, tree.spheres.c[0], sizeof(float)*scount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gsphs.cy, sizeof(float)*scount);
    cudaMemcpy(gsphs.cy, tree.spheres.c[1], sizeof(float)*scount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gsphs.cz, sizeof(float)*scount);
    cudaMemcpy(gsphs.cz, tree.spheres.c[2], sizeof(float)*scount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gsphs.invh2, sizeof(float)*scount);
    cudaMemcpy(gsphs.invh2, tree.spheres.invh2, sizeof(float)*scount, cudaMemcpyHostToDevice);

    GPUNodes gnods;
    float *minx, *miny, *minz;
    float *maxx, *maxy, *maxz;
    int *right, *offset;
    minx = new float[tree.nodes.size()];
    miny = new float[tree.nodes.size()];
    minz = new float[tree.nodes.size()];
    maxx = new float[tree.nodes.size()];
    maxy = new float[tree.nodes.size()];
    maxz = new float[tree.nodes.size()];
    right = new int[tree.nodes.size()];
    offset = new int[tree.nodes.size()];
    for(size_t i = 0; i < tree.nodes.size(); ++i) {
        minx[i] = tree.nodes[i].min[0];
        miny[i] = tree.nodes[i].min[1];
        minz[i] = tree.nodes[i].min[2];

        maxx[i] = tree.nodes[i].max[0];
        maxy[i] = tree.nodes[i].max[1];
        maxz[i] = tree.nodes[i].max[2];

        right[i] = (int)tree.nodes[i].right;
        offset[i] = (int)tree.nodes[i].offset;
    }

    size_t ncount = tree.nodes.size();

    cudaMalloc((void**)&gnods.minx, sizeof(float)*ncount);
    cudaMemcpy(gnods.minx, minx, sizeof(float)*ncount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gnods.miny, sizeof(float)*ncount);
    cudaMemcpy(gnods.miny, miny, sizeof(float)*ncount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gnods.minz, sizeof(float)*ncount);
    cudaMemcpy(gnods.minz, minz, sizeof(float)*ncount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gnods.maxx, sizeof(float)*ncount);
    cudaMemcpy(gnods.maxx, maxx, sizeof(float)*ncount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gnods.maxy, sizeof(float)*ncount);
    cudaMemcpy(gnods.maxy, maxy, sizeof(float)*ncount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gnods.maxz, sizeof(float)*ncount);
    cudaMemcpy(gnods.maxz, maxz, sizeof(float)*ncount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gnods.right, sizeof(int)*ncount);
    cudaMemcpy(gnods.right, right, sizeof(int)*ncount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gnods.offset, sizeof(int)*ncount);
    cudaMemcpy(gnods.offset, offset, sizeof(int)*ncount, cudaMemcpyHostToDevice);

    GPUKernel gkern;

    size_t kcount = 32;
    cudaMalloc((void**)&gkern.nb2s, sizeof(float)*kcount);
    cudaMemcpy(gkern.nb2s, nb2s, sizeof(int)*kcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gkern.kvals, sizeof(float)*26);
    cudaMemcpy(gkern.kvals, weights, sizeof(int)*kcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gkern.dkdnb2s, sizeof(float)*26);
    cudaMemcpy(gkern.dkdnb2s, dwdnb, sizeof(int)*kcount, cudaMemcpyHostToDevice);

    GPUPackets gpacs;
    cudaMalloc((void**)&gpacs.ox,sizeof(float)*rcount);
    cudaMemcpy(gpacs.ox, ox, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.oy,sizeof(float)*rcount);
    cudaMemcpy(gpacs.oy, oy, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.oz,sizeof(float)*rcount);
    cudaMemcpy(gpacs.oz, oz, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.dx,sizeof(float)*rcount);
    cudaMemcpy(gpacs.dx, dx, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.dy,sizeof(float)*rcount);
    cudaMemcpy(gpacs.dy, dy, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.dz,sizeof(float)*rcount);
    cudaMemcpy(gpacs.dz, dz, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.vx,sizeof(float)*rcount);
    cudaMemcpy(gpacs.vx, vx, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.vy,sizeof(float)*rcount);
    cudaMemcpy(gpacs.vy, vy, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.vz,sizeof(float)*rcount);
    cudaMemcpy(gpacs.vz, vz, sizeof(float)*rcount, cudaMemcpyHostToDevice);
    cudaMalloc((void**)&gpacs.tmax,sizeof(float)*rcount);
    cudaMemcpy(gpacs.tmax, tmax, sizeof(float)*rcount, cudaMemcpyHostToDevice);

    //output data
    float* out;
    cudaMalloc((void**)&out, sizeof(float)*rcount);

    std::cout << "copied " << (std::clock()-start)/(float)CLOCKS_PER_SEC << std::endl;

    //processing
    //float dL = 10000.0f/length;
    //float dA = dL*dL;

    cudaDeviceSynchronize();
    start = std::clock();
    std::cout << "processing rays " << std::endl;

    tracePackets<<<rcount/(warps*32), warps*32>>>(gpacs, gnods, gsphs, gkern, out);
    //brute<<<rcount/(warps*32), warps*32>>>(gpacs, gsphs, gkern, out);

    cudaDeviceSynchronize();

    cudaError_t err = cudaGetLastError();
    std::cout << err << " " << cudaSuccess << std::endl;
    if(err != cudaSuccess) {
        printf("%s\n", cudaGetErrorString(err));
    }

    cudaDeviceSynchronize();
    std::cout << "processed in " << (std::clock()-start)/(float)CLOCKS_PER_SEC << " s" << std::endl;

    //read out
    start = std::clock();
    float* o = new float[rcount];
    cudaMemcpy(o, out, sizeof(float)*rcount, cudaMemcpyDeviceToHost);

    /*float total = 0.0;
    for(size_t i = 0; i < rcount; ++i) {
        total += o[i];
    }
    total *= dA;*/
    std::cout << "copy out: " << (std::clock()-start)/(float)CLOCKS_PER_SEC<< std::endl;

    std::cout.precision(16);
    //std::cout << "total " << total << std::endl;

    for(size_t iray = 0; iray < rcount; ++iray) {
        std::cout << o[iray] << std::endl;
    }

    //float exact = 128.0*128.0*128.0;
    //float diff = total - exact;
    //std::cout << "diff " << diff << std::endl;
}
