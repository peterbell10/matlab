#include "Weight.h"

void cubicnb2(real nb2, real& weight) {
    //interpolation bounds search
    size_t ilow, ihigh;    
    ilow = (size_t)(nb2*range);
    ihigh = ilow + 1;

    //cubic hermite interpolation in quadratic space
    real t = (nb2 - nb2s[ilow]) * invdf;
    real t2 = t*t;
    real t3 = t*t2;

    real h00 = (2*t3) - (3*t2) + 1;
    real h10 = (t3 - (2*t2) + t) * df;
    real h01 = (-2*t3) + (3*t2);
    real h11 = (t3 - t2) * df;

    real pl = weights[ilow] * h00;
    real ml = dwdnb[ilow] * h10;

    real ph = weights[ihigh] * h01;
    real mh = dwdnb[ihigh] * h11;

    weight = (pl + ph) + (ml + mh);
}

/*void linearnb2(real nb2, real& weight) {
    //indicies
    size_t ilow, ihigh;
    ilow = (size_t)(nb2*1022.999999);
    ihigh = ilow + 1;

    //linear interpolation
    real intplt = (nb2-nb2s[ilow]) * invdf;

}*/

void kernelnb2(const real nbs1, real& kern) {
    //linearnb2(nbs1, kern);
    cubicnb2(nbs1, kern);
}
