#include "G2Dataset.h"

#include <fstream>
#include <iostream>

using namespace std;

void parseG2Dataset(std::string& filename, G2Dataset& data) {
    //LOAD ONLY GAS DATA
    //open file
    ifstream stream;
    stream.open(filename.c_str(), ifstream::in|ifstream::binary);

    //check file
    if(!stream.good()) {
        std::cerr << "Failed to load: " << filename << std::endl;
        return;
    }

    //parse header
    stream.seekg(4,stream.cur);
    stream.read((char*)data.npart,           6*sizeof(uint32_t));
    stream.read((char*)data.hmass,           6*sizeof(double));

    stream.read((char*)&data.time,            sizeof(double));
    stream.read((char*)&data.redshift,        sizeof(double));
    
    stream.read((char*)&data.sft,             sizeof(uint32_t));
    stream.read((char*)&data.feedback,        sizeof(uint32_t));
    stream.read((char*)data.npartTotal,     6*sizeof(uint32_t));
    stream.read((char*)&data.flag_cooling,    sizeof(uint32_t));
    stream.read((char*)&data.num_files,       sizeof(uint32_t));
    
    stream.read((char*)&data.BoxSize,         sizeof(double));
    stream.read((char*)&data.Omega0,          sizeof(double));
    stream.read((char*)&data.OmegaLambda,     sizeof(double));
    stream.read((char*)&data.HubbleParam,     sizeof(double));

    stream.read((char*)&data.flag_stellarage, sizeof(uint32_t));
    stream.read((char*)&data.flag_metals,     sizeof(uint32_t));
    stream.read((char*)&data.hashtabsize,     sizeof(uint32_t));

    stream.seekg(84,stream.cur);
    stream.seekg(4,stream.cur);


    uint32_t ototal = data.npart[1] + data.npart[2] + 
                      data.npart[3] + data.npart[4] + 
                      data.npart[5];

    //parse fields
    data.r = new float[data.npart[0]*3];
    stream.seekg(4,stream.cur);
    stream.read((char*)data.r, data.npart[0]*3*sizeof(float));
    stream.seekg(ototal*3*sizeof(float),stream.cur);
    stream.seekg(4,stream.cur);

    data.v = new float[data.npart[0]*3];
    stream.seekg(4,stream.cur);
    stream.read((char*)data.v, data.npart[0]*3*sizeof(float));
    stream.seekg(ototal*3*sizeof(float),stream.cur);
    stream.seekg(4,stream.cur);

    data.id = new uint32_t[data.npart[0]];
    stream.seekg(4,stream.cur);
    stream.read((char*)data.id, data.npart[0]*sizeof(uint32_t));
    stream.seekg(ototal*sizeof(uint32_t),stream.cur);
    stream.seekg(4,stream.cur);

    //mass section
    uint32_t parts = 0;
    for(uint32_t i = 0; i < 6; ++i) {
        if((data.npart[i] > 0) && (data.hmass[i] == 0.0)) {
            parts += data.npart[i];
        }
    }
    data.mass = 0;
    if(0 < parts) {
        stream.seekg(4,stream.cur);
        data.mass = new float[parts];
        stream.read((char*)data.mass, parts*sizeof(float));
        stream.seekg(4,stream.cur);
    }

    //Gas specific data
    if(0 < data.npart[0]) {
        stream.seekg(4,stream.cur);
        data.u = new float[data.npart[0]];
        stream.read((char*)data.u, data.npart[0]*sizeof(float));
        stream.seekg(4,stream.cur);

        stream.seekg(4,stream.cur);
        data.rho = new float[data.npart[0]];
        stream.read((char*)data.rho, data.npart[0]*sizeof(float));
        stream.seekg(4,stream.cur);

        stream.seekg(4,stream.cur);
        data.h = new float[data.npart[0]];
        stream.read((char*)data.h, data.npart[0]*sizeof(float));
        stream.seekg(4,stream.cur);
    }
    else {
        data.u = 0;
        data.rho = 0;
        data.h = 0;

        std::cerr << "File " << filename << " does not contain ";
        std::cerr << "information about gas." << std::endl;
    }

    stream.close();
}

void G2Dataset::getSpheres(Spheres& spheres, real*& hs) const {
    spheres.allocate(npart[0]);
    hs = new real[npart[0]];

    for(uint32_t idx = 0; idx < npart[0]; ++idx) {
        spheres.c[0][idx] = (real)r[idx*3+0];
        spheres.c[1][idx] = (real)r[idx*3+1];
        spheres.c[2][idx] = (real)r[idx*3+2];
       
        hs[idx] = (real)h[idx];
        spheres.invh2[idx] = (real)(1.0/(h[idx]*h[idx]));
    }
}
