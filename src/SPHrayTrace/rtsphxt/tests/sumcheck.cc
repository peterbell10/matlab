#include <omp.h>
#include <fstream>
#include <iostream>

#include "RTSPH.h"

#define length 512

int main(int argc, char** argv) {
    assert(argc == 2);

    double start, end;
    
    std::cout << "parsing" << std::endl;
    G2Dataset data;
    std::string filename(argv[1]);
    start = omp_get_wtime();
    parseG2Dataset(filename, data);
    end = omp_get_wtime();
    std::cout << "parsed in " << (end-start) << std::endl;

    //generate tree
    std::cout << "building" << std::endl;
    BVH tree;
    Spheres sphs; real* hs;
    data.getSpheres(sphs, hs);
    mkOBVH(sphs, hs, tree);
    delete[] hs; sphs.deallocate();
    end = omp_get_wtime();
    std::cout << "built in " << (end-start) << std::endl;
    std::cout << "bvh size " << tree.nodes.size() << std::endl;

    //generate rays
    std::cout << "generate rays" << std::endl;
    std::vector<Ray> rays;
    start = omp_get_wtime();

    for(int sy = 0; sy < 3; ++sy) {
        real oy = 10000.0f * (real)(sy-1);

        for(int sx = 0; sx < 3; ++sx) {
            real ox = 10000.0f * (real)(sx-1);

            for(size_t iy = 0; iy < length; ++iy) {
                real py = (iy+0.5f)/length * 10000.0f;

                for(size_t ix = 0; ix < length; ++ix) {
                    real px = (ix+0.5f)/length * 10000.0f;

                    Ray ray;

                    ray.o[0] = px + ox;
                    ray.o[1] = py + oy;
                    ray.o[2] = -10000.0f;

                    ray.d[0] = 0.0f;
                    ray.d[1] = 0.0f;
                    ray.d[2] = 1.0f;

                    ray.v[0] = 1.0f/ray.d[0];
                    ray.v[1] = 1.0f/ray.d[1];
                    ray.v[2] = 1.0f/ray.d[2];

                    //ray.l[0] = ray.o[0] / ray.d[0];
                    //ray.l[1] = ray.o[1] / ray.d[1];
                    //ray.l[2] = ray.o[2] / ray.d[2];

                    ray.tmax = 30000.0f;
                    ray.id = 0;

                    rays.push_back(ray);
                }
            }
		}
    }
    end = omp_get_wtime();
    std::cout << "produced " << rays.size() << " rays in "<< (end-start) << " s" << std::endl;

    //processing
    real dL = 10000.0f/length;
    real dA = dL*dL;
    real total = 0.0f;

    start = omp_get_wtime();
    std::cout << "processing rays " << rays.size() << std::endl;
    for(size_t iray = 0; iray < rays.size(); ++iray) {
        //calculation
        real density = 0.0; 
        tree.weightRay(rays[iray], &density);

        total += density;

        if(iray % (rays.size()/100) == 0){
            printf("\r");
            printf("%f %%", iray*100.0/rays.size());
            fflush(stdout);
        }
    }
    total *= dA;
    end = omp_get_wtime();
    std::cout << std::endl;
    std::cout << "processed in " << (end-start) << " s ";
    std::cout << std::endl;

    std::cout.precision(16);
    std::cout << "total - " << total << std::endl;

    real exact = 128.0*128.0*128.0;
    real diff = total - exact;
    std::cout << "diff " << diff << std::endl;
}
