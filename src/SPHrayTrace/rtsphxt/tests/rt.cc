#include <fstream>
#include <iostream>
#include <cmath>
#include <omp.h>

#include "RTSPH.h"

int main(int argc, char** argv) {
    assert(argc == 2);

    std::cout << "parsing" << std::endl;
    G2Dataset data;
    std::string filename(argv[1]);
    parseG2Dataset(filename, data);
    std::cout << "parsed in " << std::endl;

    //generate tree
    std::cout << "building" << std::endl;
    BVH tree;
    Spheres sphs; real* hs;
    data.getSpheres(sphs, hs);
    mkOBVH(sphs, hs, tree);
    delete[] hs; sphs.deallocate();
    std::cout << "built" << std::endl;
    std::cout << "bvh size " << tree.nodes.size() << std::endl;

    //generate rays
    std::cout << "generate rays" << std::endl;
    std::vector<Ray> rays;
    
    for(size_t isph = 0; isph < 128*128*128; ++isph) {
        Ray ray;
        
        ray.o[0] = tree.spheres.c[0][isph];
        ray.o[1] = tree.spheres.c[1][isph];
        ray.o[2] = tree.spheres.c[2][isph];
        //ray.o[0] = 5000.0f; 
        //ray.o[1] = 5000.0f; 
        //ray.o[2] = 5000.0f; 

        float tx, ty, tz;
        tx = tree.spheres.c[0][isph] - 5000.0f;
        ty = tree.spheres.c[1][isph] - 5000.0f;
        tz = tree.spheres.c[2][isph] - 5000.0f;
        
        //tx = 5000.0f - tree.spheres.c[0][isph];
        //ty = 5000.0f - tree.spheres.c[1][isph];
        //tz = 5000.0f - tree.spheres.c[2][isph];

        float l = sqrt(tx*tx+ty*ty+tz*tz);
        ray.tmax = l;

        ray.d[0] = tx / l;
        ray.d[1] = ty / l;
        ray.d[2] = tz / l;

        rays.push_back(ray);
    }

    std::cout << "produced " << rays.size() << " rays" << std::endl;

    //processing
    double total = 0.0f;

    std::cout << "processing rays " << rays.size() << std::endl;
    double start = omp_get_wtime();
   #pragma omp parallel for reduction(+:total)
    for(size_t iray = 0; iray < rays.size(); ++iray) {
        //calculation
        real density = 0.0;
        tree.weightRay(rays[iray], &density);

        total += density;

       // if(iray % (rays.size()/100) == 0){
       //     printf("\r");
       //     printf("%f %%", iray*100.0/rays.size());
       //     fflush(stdout);
       // }
    }
    double end = omp_get_wtime();
    std::cout << std::endl;
    std::cout << "processed " << (end - start) << std::endl;

    std::cout.precision(16);
    std::cout << "rt total - " << total << std::endl;
}
