#include "Weight.h"
#include <iostream>

#define SIZE 5000

int main() {
    real radius = 100.0;
    real radius2 = radius*radius;
    real diameter = radius*2.0;
    real dL = diameter/SIZE;
    real dA = dL*dL;

    //integrate over entire sphere
    double total = 0.0;

    for(size_t iy = 0; iy < SIZE; ++iy) {
        for(size_t ix = 0; ix < SIZE; ++ix) {
            real dy = -radius + diameter * (iy+0.5)/SIZE;
            real dx = -radius + diameter * (ix+0.5)/SIZE;

            real b2 = dx*dx + dy*dy;
            real nb2 = b2 / radius2;

            nb2 = MIN(nb2, 1.0);

            real k2d; kernelnb2(nb2, k2d);
            total += (double)k2d;
        }
    }

    //integral
    total *= dA;
    //divide by h^-2
    total /= radius2;

    printf("Total=%16.14f\n", total);
}
