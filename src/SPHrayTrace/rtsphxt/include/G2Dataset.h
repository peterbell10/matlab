#pragma once

#include "Spheres.h"
#include <string>

struct G2Dataset {
    //sphere header data

    ///
    uint32_t npart[6];
    ///
    double hmass[6];
    ///
    double time;
    ///
    double redshift;
    ///
    uint32_t sft;
    ///
    uint32_t feedback;
    ///
    uint32_t npartTotal[6];
    ///
    uint32_t flag_cooling;
    ///
    uint32_t num_files;
    ///
    double BoxSize;
    ///
    double Omega0;
    ///
    double OmegaLambda;
    ///
    double HubbleParam;
    ///
    uint32_t flag_stellarage;
    ///
    uint32_t flag_metals;
    ///
    uint32_t hashtabsize;

    //sphere raw data

    ///
    float *r;
    ///
    float *v;
    ///
    float *mass;
    ///
    float *u;
    ///
    float *rho;
    ///
    float *h;
    ///
    uint32_t *id;

    /// \brief Extract sphere data from Gadget2 dataset.
    ///
    /// 
    void getSpheres(Spheres&, real*&) const;
};

/// \brief Parse Gadget2 data file.
void parseG2Dataset(std::string&, G2Dataset&);
