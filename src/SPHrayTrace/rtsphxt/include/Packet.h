#pragma once

#include "Common.h"

#ifdef SIMD

#define PACKETSIZE 16
#define PACKETWIDTH PACKETSIZE/SIMDWIDTH

/// \brief Packet is a collection of similar rays.
///
///
struct Packet {
    doublew ox[PACKETWIDTH], oy[PACKETWIDTH], oz[PACKETWIDTH];
    doublew dx[PACKETWIDTH], dy[PACKETWIDTH], dz[PACKETWIDTH];
    doublew vdx[PACKETWIDTH], vdy[PACKETWIDTH], vdz[PACKETWIDTH];
    
    doublew tmax[PACKETWIDTH];
    uint64_t ids[PACKETSIZE];
};

#endif
