#pragma once
/// \file Ray.h
/// \brief Ray Structure

#include "Common.h"

/// \brief Ray data structure.
///
/// The Ray structure contains basic information for representing
/// a ray in 3 dimensional space. Additionally it stores the 
/// inverse direction (1/d) to avoid calculating divisions during
/// tracing.
struct Ray {
    float o[3];
    float d[3];
    float v[3];
    float tmax;
    uint32_t id;
};
