/* Gateway function for SPHrayTrace
 *
 * MATLAB sytax: RayIntegrals=SPHrayTrace(r,z,h,Rays) 
 *
 * ARGUMENTS
 *  r      particle positions [3,N]
 *  z      Quantity to integrate along rays [N]
 *  h      Particle smoothing lengths [N]
 *  Rays   Rays, defined by [3,2,Nrays] or, as vector,
 *         [x0,y0,z0, xf,yf,zf, x0,y0,z0, xf,yf,zf, ...]
 *          |---- RAY 1 -----|  |---- RAY 2 -----|
 * RETURNS
 *  RayIntegrals   The integral of z along each ray in Rays. [Nrays]
 */

#include <mex.h>

#include <stdio.h>

#include "SPHrayTrace.h"

/* There is a problem with mexErrMsgTxt and some glibc */
#define mexErrMsgTxt(string) printf("%s\n",string); exit(-1);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 /* Output */
 double *RayIntegrals;

 /* Input */
 double *r;
 double *z;
 double *h;
 double *Rays;

 size_t N,Nrays;
 mwSize mrows,ncols;

 /* Check for the proper number of arguments */
 if( nlhs!=1 || nrhs!=4 ) {
  mexErrMsgTxt("syntax: RayIntegrals=SPHrayTrace(r,z,h,Rays)");
 }

 /* All arguments must be double */
 if( !mxIsDouble(prhs[0]) ) {
  mexErrMsgTxt("r must be a double.");
 }
 if( !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("z must be a double.");
 }
 if( !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("h must be a double.");
 }
 if( !mxIsDouble(prhs[3]) ) {
  mexErrMsgTxt("L must be a double.");
 }

 /* Assign pointers to the input */
 r    = mxGetPr(prhs[0]);
 z    = mxGetPr(prhs[1]);
 h    = mxGetPr(prhs[2]);
 Rays = mxGetPr(prhs[3]);

 /* r must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows!=3) {
  mexErrMsgTxt("r must be a 3xN array");
 }
 N = (size_t)ncols;

 /* z must be N columns */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if( (mrows!=1 || (size_t)ncols!=N) && ((size_t)mrows!=N || ncols!=1) ) {
  mexErrMsgTxt("z must be a vector of length N");
 }

 /* h must be N columns */
 mrows = mxGetM(prhs[2]);
 ncols = mxGetN(prhs[2]);
 if( (mrows!=1 || (size_t)ncols!=N) && ((size_t)mrows!=N || ncols!=1) ) {
  mexErrMsgTxt("h must be a vector of length N");
 }

 /* Rays must have 6*Nrays */
 mrows = mxGetNumberOfElements(prhs[3]);
 if( mrows % 6 ) {
  mexErrMsgTxt("Rays must have a multiple of 6 elements");
 }
 Nrays = mrows/6;

 /* Create a matrix for return argument.
  *  If unsuccessful in a MEX-file, the MEX-file terminates and returns
  *  control to the MATLAB prompt. */
 plhs[0]=mxCreateDoubleMatrix((mwSize)Nrays, (mwSize)1, mxREAL);
 RayIntegrals = mxGetPr(plhs[0]);

 /* DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE */
 SPHrayTrace(r,z,h,Rays,N,Nrays,RayIntegrals);
}
