% syntax: smooth=smooth_kernel(Image,level,mask);
%
% Given an Image (MxN array), the image is smoothed over an
% adaptive radius such that the sum within the radius is equal
% to level.  A mask is required to highlight those regions for which
% we would like the smoothing done since voids have no information
% and take up lots of cpu time.
%
% Just set mask=Image*0+1; to do the whole image.
