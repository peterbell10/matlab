#include <math.h>
#include <stdlib.h> /* for the memory allocation */

int anisotropy(
 double *r, /* r is 3xN data stored column after column */
 int N,
 double length,
 double *aniso /* empty matrix 12xN */
) ; 

int aniso_from_list(
 double *r, /* the particle positions: r is 3xN data stored column after column */
 int N,     /* the number of particles */
 double *rs, /* the sample positions: rs is 3xM data stored column after column */
 int M,      /* the number of sample positions */
 double length,
 double *aniso /* empty matrix 12xM */
);

int boxlist(
 /* Input */
 double *r,	/* The particle positions */
 int n,		/* the number of particles */
 double *min,	/* the minimum in each dimension */
 int Nnodes,	/* the number of nodes per side */
 double span,	/* the length of each side of the mesh (in particle units) */
 int L_lo,
 int L_hi,
 /* Output */
 int *ll,	/* the length N vector containing the link list */
 int *ll_start	/* the starting element for each box in the list (Nnodex x Nnodex) */
);

int Cart_to_AltAz(
/* input */
 double x,
 double y,
 double z,
/* output */
 double *Alt,
 double *Az
) ;

int neighbourlist(
/* Arguments input, not modified */
 int L,  /* the number of nodes per side */
 int L1, /* the coordinates of teh box for which to for(the search */
 int L2,
 int L3,
 int N,		/* the number of particles */
 int *ll_start, /* the starting particle for the link list for each particle (size  LxL )  */
 int *ll, /* the link list of all particles ( size N )  */
/* Arguments output, modified */
 int *elements, /* memory must have been allocated! */
 int *count	/* the number of particles found */
);

double SphericalDistance(
 double alpha1,
 double dec1,
 double alpha2,
 double dec2
);
