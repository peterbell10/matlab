/*
* Finds the list of particles in the box (L1,L2,L3) as well as the
* particles in the surrounding 26 boxes and stores them in the list, elements.
*/
#include "anisotropy.h"
int neighbourlist(
/* Arguments input, not modified */
 int L,  /* the number of nodes per side */
 int L1, /* the coordinates of the box for which to search */
 int L2,
 int L3,
 int N,		/* the number of particles */
 int *ll_start, /* the starting particle for the link list for each particle (size  LxL )  */
 int *ll, /* the link list of all particles ( size N )  */
/* Arguments output, modified */
 int *elements, /* memory must have been allocated! */
 int *count	/* the number of particles found */
){

/* local variables */
 int j;
 int dL1,dL2,dL3;
 int bL1,bL2,bL3;
 int Lsqr;

 Lsqr=L*L;
 *count=0;

/* Loop over surrounding boxes to find neighbours */
 for(dL1=-1;dL1<=1;dL1++) {
  bL1=L1+dL1;
  if(bL1==-1) { bL1=L-1; }
  else{ if(bL1==L) { bL1=0; } }
  for(dL2=-1;dL2<=1;dL2++){
   bL2=L2+dL2;
   if(bL2==-1) { bL2=L-1; }
   else{ if(bL2==L) { bL2=0;} }
   for(dL3=-1;dL3<=1;dL3++) {
    bL3=L3+dL3;
    if(bL3==-1) { bL3=L-1; }
    else{ if(bL3==L) { bL3=0; } }
/* Loop over particles in a surrounding box */
    j=ll_start[bL1*Lsqr+bL2*L+bL3];
    while(j!=-1) {
     elements[*count]=j;
     (*count)++;
     j=ll[j];
    }
/*  Finshed looping over particles in a surrounding box */
   }
  }
 }
/* End looping over all 27 boxes */
 return(0);
}
