/* 
 Creates a link list (ll) containing a list of all the particles in the
 boxes in L_lo to L_hi. (L_lo is usually 0, and L_hi is usually Nnodes)
 ll_start points, for each box, to the first element.
 This is accomplished in one quick step.
*/
#include <stdio.h>

#include "mex.h"

#include "anisotropy.h"

int boxlist(
 /* Input */
 double *r,	/* The particle positions */
 int n,		/* the number of particles */
 double *min,	/* the minimum in each dimension */
 int Nnodes,	/* the number of nodes per side */
 double span,	/* the length of each side of the mesh (in particle units) */
 int L_lo,
 int L_hi,
 /* Output */
 int *ll,	/* the length N vector containing the link list */
 int *ll_start	/* the starting element for each box in the list (Nnodex x Nnodex) */
) {

/* Local variables */
 int L1,L2,L3,i,part,dim;
 int Nnodes2; /* The number of nodes, squared*/
 int *box_index; /* The list of box positions for each particle */
/* Other variables for the link list */
 int *ll_holder;
 int particle;
 int cell;
 int index;

 Nnodes2=Nnodes*Nnodes;

 if(L_lo>L_hi){ mexErrMsgTxt("L_lo>L_hi in boxlist"); }
 
 if(L_hi>Nnodes){ mexErrMsgTxt("L_hi>Nnodes in boxlist"); }
 
/* Allocate memory for the box_index */
 box_index = (int*) mxMalloc(sizeof(int)*n*3);
 if(box_index==NULL){ mexErrMsgTxt("Unable to allocate memory for box_index"); }

/* Allocate memory for the link list place holder */
 ll_holder=(int *) mxMalloc(sizeof(int)*Nnodes*Nnodes*Nnodes);
 if(ll_holder==NULL){
  mxFree(box_index);
  mexErrMsgTxt("Unable to allocate memory for ll_holder");
 }

/* Index the particles */
 for(part=0;part<n;part++){
  for(dim=0;dim<3;dim++){
   index=(int)floor( (r[3*part+dim]-min[dim])/span*(double)Nnodes );
   if( (index<0) || (index>=Nnodes) ) {
    mxFree(box_index);
    printf("\n particle=%d, dim=%d\n",part,dim);
    printf("x=%f, min x=%f, span=%f, Nnodes=%d\n",r[3*part+dim],min[dim],span,Nnodes);
    mexErrMsgTxt("Sample point is outside the range of the particles");
   }
   box_index[3*part+dim]=index;
  }
 }

/* Clear variables */
 for(i=0;i<n;i++){
  ll[i]=-1;
 }
 for(L1=L_lo;L1<L_hi;L1++){
  for(L2=L_lo;L2<L_hi;L2++){
   for(L3=L_lo;L3<L_hi;L3++){
    cell=L1*Nnodes2+L2*Nnodes+L3;
    ll_start[cell]=-1;
    ll_holder[cell]=-1;
   }
  }
 }

/* Compile the link list */
 for(i=0;i<n;i++){
  L1=box_index[3*i+0];
  L2=box_index[3*i+1];
  L3=box_index[3*i+2];
  cell=L1*Nnodes2+L2*Nnodes+L3;
  if(ll_start[cell]>-1) {
   particle=ll_holder[cell];
   ll[particle]=i;
  }
  else{
   ll_start[cell]=i;
  }
  ll_holder[cell]=i;
 }

 mxFree(box_index);
 mxFree(ll_holder);
 return(0);
}
