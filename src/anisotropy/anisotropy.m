function aniso=anisotropy(r,scale,rs)

% aniso=anisotropy(r,scale,[sample_positions]);
%
% r are the particle positions;
% scale is the distance over which to find the anisotropy
% sample_positions is an optional list which, if included, finds the 
%                  anisotropies at the positions, instead of the porticle
%                  positions

