/* function aniso=anisotropy(r,length) */

/* given the particle positions, r, find the anisotropy index, aniso, */
/* for each particle as well as the  */
/* The anisotropy is the deviation from spherical symmetry of the local density */ 

#include "mex.h"

#include "anisotropy.h"

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 double *r, *rs;
 double *length;
 double *aniso;

 size_t mrows,ncols, ncols_rs;
 int status;
 
 /* Check for the proper number of arguments */
 if( ( (nrhs!=2) && (nrhs!=3) ) || (nlhs!=1) ) {
  mexErrMsgTxt("Syntax: aniso=anisotropy(r,scale,[sample_positions])");
 }

 /* The inputs must be of the type double */
 if( !mxIsDouble(prhs[0]) || !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("r and length must be double");
 }
 if( nrhs==3 ) {
  if( !mxIsDouble(prhs[2]) ) {
   mexErrMsgTxt("sample_positions must be double");
  }
 }

 /* The second argument must be scalar */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("length must be scalar");
 }

 /* The first argument must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows!=3 || ncols<11) {
  mexErrMsgTxt("r must be a 3xN array with N>10");
 }

 /* The third argument, if it exists, must be 3 rows by M columns, M >= 1 */
 ncols_rs=0;
 if( nrhs==3 ) {
  mrows = mxGetM(prhs[2]);
  ncols_rs = mxGetN(prhs[2]);
  if(mrows!=3 || ncols_rs<1) {
   mexErrMsgTxt("samples_positions must be a 3xM array with M>=1");
  }
 }

 /* Create matrix for the return argument and assign a pointer */
 if( nrhs==2) {
  plhs[0]=mxCreateDoubleMatrix(12,ncols, mxREAL);
 }
 else {
  plhs[0]=mxCreateDoubleMatrix(12,ncols_rs, mxREAL);
 }
 aniso = mxGetPr(plhs[0]);

 /* Assign pointers to the input */
 r      = mxGetPr(prhs[0]);
 length = mxGetPr(prhs[1]);
 rs=NULL;
 if( nrhs==3 ) {
  rs    = mxGetPr(prhs[2]);
 }
 
 /* Call the subroutine */
 status=0;
 if( nrhs==2 ) {
  status = anisotropy(r,ncols,*length,aniso);
 }
 else {
  if( (ncols_rs==0) || (rs==NULL) ) {
   mexErrMsgTxt("Something smells rotten.  Exiting...");
  }
  else {
   status = aniso_from_list(r,ncols,rs,ncols_rs,*length,aniso);
  }
 }
 if(status!=0) { mexErrMsgTxt("anisotropy returned a non-zero exit code"); }
}
