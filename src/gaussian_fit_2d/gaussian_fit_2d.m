% gaussian_fit_2d :  Fit a gaussian to a 2D distribution.
%
% [bias,sigma,ampl,x,y]=gaussian_fit_2d(frame);
%
% ARGUMENTS
%  frame	The x-y distribution of P, to which the Gaussian will be fit.
%
% OUTPUT
%  bias		The zero-point P(r -> inf)
%  sigma	The width of the Gaussian
%  ampl		The amplitude of the Gaussian
%  x,y		The centre of the Guassian
%
% SEE ALSO
%  gaussian_fit (1D), fit_gaussian (taylored for stellar images)
