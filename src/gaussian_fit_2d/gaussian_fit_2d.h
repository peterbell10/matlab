#include <math.h>
#include <stdlib.h> /* for the memory allocation */
#include <stdio.h>
#include "mex.h"


int gaussian_fit_2d(
/* Input arguments */
double *frame, int *N, int *M,
/* output arguments */
double *bias, double *sigma, double *ampl,
double *x, double *y );
