/*  HISTORY  */
/*
* 00 12 13  I don't know how it ever worked before 
*      Fixed numerous bugs.                    
*      Added boxcar averaging of the frame to permit
*      a discriminant for cosmic arrays        
*      when finding the guess at the centre of the
*      star.                                   
*
* 00 12 14 Fixed a bug in boxcar, where I was multi ii by *N
*	instead of *M.
*/


#include <math.h>
#include <stdlib.h>
#include <stdio.h>



/* ********************************************************************* */
/* BOXCAR AVERAGE AN ARRAY */
void boxcar( const int * const Array,
                   int * const Array_out,
             const int * const N,
             const int * const M ) {
/* Memory for Array_out must already have been allocated */

/* M is the inner array dimension, N the outer i.e. (0->M-1) loops N-1 times */
/* This is because Matlab stores arrays one column at a time */
/* A byproduct of this is that x corresponds to the N and y corresponds to M */
/* If you like, the data is stored one RA at a time, cylcing through */
/* declination */


 int i,j;
 int di,dj;
 int ii,jj;
 int sum;

 for(i=0;i<*N;i++){
  for(j=0;j<*M;j++){
   sum=0;
   for(di=-1;di<=1;di++){
    ii=i+di;
    if(ii==-1) {ii=*N-1;}
    if(ii==*N) {ii=0;}
    for(dj=-1;dj<=1;dj++){
     jj=j+dj;
     if(jj==-1) {jj=*M-1;}
     if(jj==*M) {jj=0;}
     sum+=Array[ii*(*M)+jj];
    }
   }
   Array_out[i*(*M)+j]=sum/9;
  }
 }

}
/* END OF BOXCAR AVERAGE   */
/* ********************************************************************* */

float gaussian( const float * const x,
                const float * const sigma,
                const float * const ampl  ){
 return( (*ampl)/(*sigma)*exp(-1.0*((*x)/(*sigma))*((*x)/(*sigma))/2.0) ); 
}

/* ********************************************************************* */

double frame_error( const int * const frame_int,
                    const float * const frame_float,
                    const int * const ArrLen ) {
 double err;
 int i;
 
 err=0.0;
 for( i=0; i < *ArrLen; i++ ) {
/*  printf("%f ",pow( (double)((float)(*(frame_int+i)) - *(frame_float+i)), 2.0 )); */
  err += pow( (double)(((float)frame_int[i])-frame_float[i]), 2.0 );
 }
 err = sqrt(err)/(*ArrLen);
 return(err);
}

/* ********************************************************************* */

void make_gaussian(
/* Input arguments, not modified */
/* M is the inner array dimension, N the outer i.e. (0->M-1) loops N-1 times */
/* This is because Matlab stores arrays one column at a time */
/* A byproduct of this is that x corresponds to the N and y corresponds to M */
/* If you like, the data is stored one RA at a time, cylcing through */
/* declination */
const int * const M, const int * const N,
const float * const bias, const float * const sigma, const float * const ampl,
const float * const x, const float * const y,
/* Output argument (space must already must have been allocated! */
/* ( a "int const *" is a pointer whose value cannot change, but the contents
     at the address to which it points may change) */
float * const frame ) { 

 int i,j;
 float rx,ry,rx2;
 float dist;
 
/*
 printf("bias=%f, sigma=%f, ampl=%f, x=%f, y=%f\n", *bias, *sigma, *ampl, *x, *y );
*/

/* x corresponds to N corresponds to i */
/* y corresponds to M corresponds to j */

 for(i = 0; i < *N ; i++) {
  rx = (float)i - *x;
  rx2 = rx*rx;
  for(j = 0; j < *M ; j++) {
   ry = (float)j - *y;
   dist = sqrt( rx2 + ry*ry );
   frame[i*(*M) + j] = gaussian( &dist, sigma, ampl) + (*bias) ;
  }
 }
} /* end of make_gaussian */

/* ********************************************************************* */

int fit_gaussian(
/* Input arguments */
const int * const frame, const int * const N, const int * const M,
/* output arguments */
float * const bias, float * const sigma, float * const ampl,
float * const x, float * const y ) {

/* given an image, frame, of a star,
   fit a gaussian to it and return the fit */

/* History */
/* First Version: Eric Tittley 00 09 23 */

 int i;
 int sum, max, index_max;
 const int ITER_MAX=2000;
 int iters1, iters2; 
 float *frame_approx;
 const int ArrLen = *N * *M ;
 double err, err_o, err_oo;
 double TOL = 0.00001;
 double FACT = TOL*10.0;
 float DeltaPlusOne = 1.0+(float)FACT;
 float Delta = (float)FACT;
 float test_bias, test_sigma, test_ampl, test_x, test_y;
 double df_dbias, df_dsigma, df_dampl, df_dx, df_dy;
 int *frame_smooth;
 double Sum;

 /* Allocate Memory for the box-car-smoothed image */
 frame_smooth=(int *)malloc( ArrLen * sizeof(int) );
 if(frame_smooth==NULL) {
  printf("fit_gaussian: failed to allocate memory for frame_smooth\n");
  return(-1);
 }

 /* Smooth the image */
 boxcar(frame,frame_smooth,N,M);

 /* make a guess at the bias using the first 10 points */
 sum=0;
/* for( i=0;i<ArrLen;i++ ) {*/
 for( i=0;i<10;i++ ) {
  sum=sum+frame_smooth[i];
 }
/* *bias = ((float)sum)/((float)(ArrLen)); */
 *bias = ((float)sum)/10.0;
/* printf("bias=%f sum=%i\n",*bias,sum); */

 /* find the approximate centre */
 /* The criterion for Cosmic ray rejection is if the pixel value less the */
 /* bias is greater than twice the smoothed pixel value less the bias */
 /* i.e. frame-bias > 2*(frame_smooth-bias) */
 /* which is the same thing as: frame > 2*frame_smooth-bias */
 max=0;
 index_max=-1;
 for( i=0;i<ArrLen;i++ ) {
  if(frame[i]>max) {
   if( frame[i] < (2*frame_smooth[i]-((int) *bias)) ) {
    max=frame[i];
    index_max=i;
   }
  }  
 }
 if(index_max==-1) {
  printf("error in fit_gaussian: No maximum found\n");
  return(-1);
 }
 
 /* guess at x and y */
 /* this is very important, since convergence is worse for this parameter */
 /* Convergence to a valid set of parameters is fairly sensitive to
    getting x and y centred on the star  (within +- 10 pixels) */
 *x = (float)(floor( ((double)index_max)/((double)(*M)) ));
 *y = (float)(index_max-(int)(*x)*(*M)) ;

/* This part of the routine still needs work */
/* The x-y position found here is usually a few pixels off from */
/* the final position */
/* *x = *x -3.0; */ /* Why 3? */ 
/* *y = *y -1.0; */
/* printf("x=%f y=%f\n",*x,*y); */


 /* guess at sigma */
 *sigma = 5.0;

 /* guess at ampl */
 *ampl = 5 * ( max - *bias );

 printf("bias=%f, sigma=%f, ampl=%f, x=%f, y=%f\n",*bias,*sigma,*ampl,*x,*y);
 /* allocated space for the calculated field */
 frame_approx = (float *)malloc( ArrLen*sizeof(float) );
 if(frame_approx==NULL) {
  printf("fit_gaussian: failed to allocate memory for frame_approx\n");
  return(-1);
 }

 /* initial guess at frame */
 make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );

 /* initial error */
 err = frame_error( frame, frame_approx, &ArrLen );

 /* The sum of the frame */
 Sum=0.0;
 for(i=0;i<ArrLen;i++) {
  Sum += (float)frame[i];
 }

 /* Now loop through, trying to minimise the err */
 /* The variables that can change are bias, ampl, sigma, x, and y */
 iters1=0;
 do {
  iters1++;
/*  if(iters1>ITER_MAX) { */
  if(iters1>10) {
   printf("warning fit_gaussian: failed to converge (iters1)\n");
   return(-2);
  }
  err_oo=err;
  
  iters2=0;
  do {
   iters2++;
   err_o=err;
   test_bias  = DeltaPlusOne * (*bias);
   make_gaussian( M, N, &test_bias, sigma, ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
/*   printf("test_bias=%f bias=%f err=%f err_o=%f\n",test_bias,*bias,err,err_o); */
   if(fabs((err_o-err)/err)>TOL) {
    df_dbias=(err-err_o)/((double)( Delta*(*bias) ));
    *bias  -= (float)(FACT*err/df_dbias);
    if(*bias<1.0){*bias=1.0;}
   }
   make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
   if(iters2>ITER_MAX) {
    printf("warning fit_gaussian: failed to converge (iters2.1)\n");
/*    return(-2); */
    err=0.0;
   }
/*  } while(fabs((err_o-err)/err)>TOL); */ /* the percentage change is > TOL */
  } while(fabs(err)/Sum>TOL); /* the percentage error is > TOL */

  err = frame_error( frame, frame_approx, &ArrLen );
  iters2=0;
  do {
   iters2++;
   err_o=err;
   test_ampl  = DeltaPlusOne*(*ampl);
   make_gaussian( M, N, bias, sigma, &test_ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
/*   printf("test_amp=%f ampl=%f err=%f err_o=%f\n",test_ampl,*ampl,err,err_o); */
   if(fabs((err_o-err)/err)>TOL) {
    df_dampl=(err-err_o)/(double)(Delta*(*ampl));
    *ampl  -= (float)(FACT*err/df_dampl);
    if(*ampl<1.0){ *ampl=1.0; }
   }
   make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
   if(iters2>ITER_MAX) {
    printf("warning fit_gaussian: failed to converge (iters2.2)\n");
   /* return(-2); */
    err=0.0;
   }
/*  } while(fabs((err_o-err)/err)>TOL); */ /* the percentage change is > TOL */
  } while(fabs(err)/Sum>TOL); /* the percentage error is > TOL */

  err = frame_error( frame, frame_approx, &ArrLen );
  /* the error is not very sensitive to changes in sigma, so we will decrease
     the tolerance by a factor of 10 */
  iters2=0;
  do {
   iters2++;
   err_o=err;
   /* test_sigma = DeltaPlusOne*(*sigma); */
   test_sigma = 1.01*(*sigma);
   make_gaussian( M, N, bias, &test_sigma, ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
/*   printf("sigma=%f, test_sigma=%f, err_o=%f, err=%f\n",*sigma,test_sigma,err_o,err); */
/*   printf("test_sigma=%f sigma=%f err=%f err_o=%f\n",test_sigma,*sigma,err,err_o); */
   if(fabs((err_o-err)/err)>TOL*0.1) {
    df_dsigma=(err-err_o)/(double)(Delta*(*sigma));
/*    printf("df_dsigma=%f\n",df_dsigma); */
/*    *sigma -= (float)(FACT*err/df_dsigma); */
    *sigma -= (float)(err/df_dsigma);
/*    printf("new sigma=%f\n",*sigma); */
    if(*sigma<1.0){ *sigma=1.0; }
    if(*sigma>(*N/2)) *sigma=(*N/2);
   }
   make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
   if(iters2>ITER_MAX) {
    printf("warning fit_gaussian: failed to converge (iters2.3)\n");
   /* return(-2); */
    err=0.0;
   }
/*  } while(fabs((err_o-err)/err)>TOL*0.1); */ /* the percentage change is > TOL */
  } while(fabs(err)/Sum>TOL); /* the percentage error is > TOL */

/*
  err_min=err*100.0;
  good_x=-1.0;
  good_y=-1.0;
  for(test_x= *x - 10.0; test_x < *x + 10.0; test_x++) {
   for(test_y= *y - 10.0; test_y < *y + 10.0; test_y++) {
    make_gaussian( M, N, bias, sigma, ampl, &test_x, &test_y, frame_approx );
    err = frame_error( frame, frame_approx, &ArrLen );
    if(err<err_min) {
     err_min=err;
     good_x=test_x;
     good_y=test_y;
    }
   }
  }
  if(good_x==-1) {
   printf("error in fit_gaussian: failed to find the centre of the star\n");
   return(-1);
  }
  printf("\nx=%f, y=%f\n",good_x,good_y);
*/

  err = frame_error( frame, frame_approx, &ArrLen );
  iters2=0;
  do {
   iters2++;
   err_o=err;
/*   test_x     = DeltaPlusOne*(*x); */
   test_x     = 1.01*(*x);
   make_gaussian( M, N, bias, sigma, ampl, &test_x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
/*   printf("test_x=%f x=%f err=%f err_o=%f\n",test_x,*x,err,err_o); */
   if(fabs((err_o-err)/err)>TOL) {
/*    df_dx=(err-err_o)/(double)(Delta*(*x)); */
    df_dx=(err-err_o)/(double)(0.01*(*x));
/*    *x     -= (float)(FACT*err/df_dx); */
    *x     -= (float)(0.1*err/df_dx);
    if(*x < 0) *x=0;
    if(*x >= *N) *x=(*N)-1;
   }

   printf("err=%f, err_o=%f, x=%f, test_x=%f, df_dx=%f\n",
          err, err_o, *x, test_x, df_dx);

   make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
   if(iters2>ITER_MAX) {
    printf("warning fit_gaussian: failed to converge (iters2.4)\n");
   /* return(-2); */
    err=0.0;
   }
/*  } while(fabs((err_o-err)/err)>TOL); */ /* the percentage change is > TOL */
  } while(fabs(err)/Sum>TOL); /* the percentage error is > TOL */
 
  err = frame_error( frame, frame_approx, &ArrLen );
  iters2=0;
  do {
   iters2++;
   err_o=err;
/*   test_y     = DeltaPlusOne*(*y); */
   test_y     = 1.01*(*y);
   make_gaussian( M, N, bias, sigma, ampl, x, &test_y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
/*   printf("test_y=%f y=%f err=%f err_o=%f\n",test_y,*y,err,err_o); */
   if(fabs((err_o-err)/err)>TOL) {
    df_dy=(err-err_o)/(double)(Delta*(*y));
/*    *y     -= (float)(FACT*err/df_dy); */
    *y     -= (float)(0.1*err/df_dy);
    if(*y < 0) *y=0;
    if(*y >= *M) *y=(*M)-1;
   }
   make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
   err = frame_error( frame, frame_approx, &ArrLen );
   if(iters2>ITER_MAX) {
    printf("warning fit_gaussian: failed to converge (iters2.5)\n");
    /* return(-2); */
    err=0.0;
   }
/*  } while(fabs((err_o-err)/err)>TOL); */ /* the percentage change is > TOL */
  } while(fabs(err)/Sum>TOL); /* the percentage error is > TOL */

/*  printf("%f %f %f %f %f\n",*bias,*sigma,*ampl,*x,*y); */
  make_gaussian( M, N, bias, sigma, ampl, x, y, frame_approx );
  err = frame_error( frame, frame_approx, &ArrLen );

  TOL=TOL*0.5;
  FACT=TOL*10;
  DeltaPlusOne=1.0+TOL;

/* } while(fabs((err_oo-err)/err)>TOL); */ /* the percentage change is > TOL */
 } while(fabs(err)/Sum>TOL); /* the percentage error is > TOL */

 /* free up allocated space */
 free( (void *) frame_approx);
 free( (void *) frame_smooth);
 
 /* increment x and y by 1 to translate from the [0..N-1] indexing of
  * C to the [1..N] indexing of Matlab */
 *x = *x +1;
 *y = *y +1;
 return(0);
} /* end of fit_gaussian */
