% [bias,sigma,ampl,x,y]=fit_gaussian(frame);
%
% Given the image of a single star in frame,
% return the fit of a Gaussian to the star's image.
%
% The image frame must be integer values. If you have a real
% array with values <1, then multiply by a large integer
% (1e5 for eg.) before passing in.  Then divide bias and ampl
% by this factor. 
%
% Note, the  mexFunction assumes a double and converts to an integer.
