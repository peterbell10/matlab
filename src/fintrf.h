#ifndef _FINTRF_H_
#define _FINTRF_H_
/*
 * fintrf.h	- MATLAB/FORTRAN interface header file. This file
 *		  contains the declaration of the pointer type needed
 *		  by the MATLAB/FORTRAN interface.
 *
 * Copyright 1984-2008 The MathWorks, Inc.
 * All Rights Reserved.
 *
 * Blatantly stolen from Matlab's fintrf.h file.  Though, to be fair, I could
 * have written this from scratch.  Just wouldn't have had the PPC and SPARC
 * tests. --Eric Tittley 2010 06 08
 */
#if defined(__x86_64__) || defined(_M_AMD64) || defined(__amd64) || \
    defined(__sparcv9)  || defined(__ppc64__)
# define mwpointer integer*8
# define mwPointer integer*8
# define MWPOINTER INTEGER*8
#else
# define mwpointer integer*4
# define mwPointer integer*4
# define MWPOINTER INTEGER*4
#endif

# define mwsize  mwpointer
# define mwSize  mwpointer
# define MWSIZE  MWPOINTER
# define mwindex mwpointer
# define mwIndex mwpointer
# define MWINDEX MWPOINTER
# define mwsignedindex mwpointer
# define mwSignedIndex mwpointer
# define MWSIGNEDINDEX MWPOINTER

#endif
