      SUBROUTINE shcorr(N,r,ll,Ls,ihc,nbin,rllw,rldiv,ncorr)
C Bins the particle separation into ncorr, which can then be used to
C find the 2-point correlation function.
C
C NOTE: THIS VERSION DOES NOT ASSUME REPEATING BOUNDARY CONDITIONS!!!
C This way we can use it on isolated objects extracted from a simulation.
      IMPLICIT NONE
c Arguments input, not modified
      integer N,Ls,nbin,ihc(Ls**3),ll(N)
      real*8 r(3,N),rllw,rldiv
c Arguments output, modified
c    Note, this should be an integer, but MATLAB likes it's arguments
c     to be Real*8
      real*8 ncorr(nbin)

c Local variables
      integer i,j
      integer Ls2
      integer ibx,iby,ibz,nbx,nby,nbz,inbx,inby,inbz,nbox
      integer ibin,ibox
      real*8 rL,re,re2,rlw2
      real*8 rx,ry,rz,rad2

      rL=1.d0
      re=rL/dble(Ls)
      re2=re**2

      rlw2=10.d0**(2.d0*rllw)

      do i=1,nbin
       ncorr(i)=0.d0
      enddo

      Ls2=Ls**2

      do ibz=1,Ls
       do iby=1,Ls
        do ibx=1,Ls
         ibox=ibx+Ls*(iby-1)+Ls2*(ibz-1)
c Start of loop through link list for this box
         i=ihc(ibox)
         do while (i.gt.0)
          do nbz=-1,1
           inbz=ibz+nbz
           if(inbz.ge.1 .and. inbz.le.Ls) then
           do nby=-1,1
            inby=iby+nby
            if(inby.ge.1 .and. inby.le.Ls) then
            do nbx=-1,1
             inbx=ibx+nbx
             if(inbx.ge.1 .and. inbx.le.Ls) then
              nbox=inbx+Ls*(inby-1)+Ls2*(inbz-1)
c             inner link-list loop over particles in a neighbour box
              j=ihc(nbox)
              do while (j.gt.0)
c              This should be j != i, but using j < i since we would otherwise
c              double-count.  There is a factor of 1/2 in cav in pscorr.f to
c              account for the removal of the double-counting
               if(j.lt.i)then
c
c ** Pair operation **
c
                rz=r(3,i)-r(3,j)
                ry=r(2,i)-r(2,j)
                rx=r(1,i)-r(1,j)
                rad2=rx**2+ry**2+rz**2
                if(rad2.lt.re2) then
                 if(rad2.ge.rlw2) then
                  ibin=int(rldiv*(0.5d0*log10(rad2)-rllw))+1
                 else
                  ibin=nbin
                 endif
                 ncorr(ibin)=ncorr(ibin)+1.0d0
                endif
               endif
               j=ll(j)
              enddo
c            end of looping over inner link-list
            endif
            enddo
c          end loop over nbx and test that we are still in the volume   
           endif
           enddo
c         end loop over nby and test that we are still in the volume
          endif
          enddo
c         end loop over nbz and test that we are still in the volume
c         end looping over neighbour boxes   
          i=ll(i)
         enddo
c       end loop over link list for box 
        enddo
c      end loop over ibx
       enddo
c     end loop over iby
      enddo
c    end loop over ibz
c    end looping over boxes     

      RETURN
      END
