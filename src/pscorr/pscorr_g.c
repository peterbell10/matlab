/* Gateway function for pscorr
 * MATLAB syntax: [rad,ncorr,ntl,corr,J3]=pscorr(r,L,nbins,rllw)
 */

#include <mex.h>

#include <stdio.h>

/* There is a problem with mexErrMsgTxt and some glibc */
#define mexErrMsgTxt(string) printf("%s\n",string); exit(-1);


/* The Fortran function that does all the work */
void pscorr_(int *nobj,
             double *r,
             int *L,
             int *nbins,
             double *rllw,
             double *rad,
             double *ncorr,
             double *nlt,
             double *corr,
             double *J3,
             int *Ls,
             int *ihc);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 /* Local variables */
 int nobj;
 int L;
 int nbins;
 int nrows;
 double *LP;
 double *nbinsP;
 double *r;
 double *rllw;
 double *rad;
 double *ncorr;
 double *nlt;
 double *corr;
 double *J3;

 /* Temporary storage for the Fortran subroutine */
 int *ll;
 int *ihc;

 /* Check for the proper number of arguments */
 if( (nlhs != 5) || (nrhs!=4) ) {
  mexErrMsgTxt("syntax: [rad,ncorr,ntl,corr,J3]=pscorr(r,L,nbins,rllw)");
 }
 
 /* The arguments must be double */
 if(   !mxIsDouble(prhs[0])
    && !mxIsDouble(prhs[1])
    && !mxIsDouble(prhs[2])
    && !mxIsDouble(prhs[3]) ) {
  mexErrMsgTxt("Arguments must be a double.");
 }

 /* Verify that r is 3 rows x nobj cols */
 nrows = mxGetM(prhs[0]);
 if(nrows != 3) {
  mexErrMsgTxt("r must be 3 x N");
 }

 /* Get the number of objects in r */
 nobj = mxGetN(prhs[0]);

 /* Assign pointer to the argument */
 r           = mxGetPr(prhs[0]);
 LP          = mxGetPr(prhs[1]);
 nbinsP      = mxGetPr(prhs[2]);
 rllw        = mxGetPr(prhs[3]);

 /* Convert Matlab's default doubles to integers */
 L = (int)(*LP);
 nbins = (int)(*nbinsP);

 /* Create a matrix for return arguments */
 plhs[0] = mxCreateDoubleMatrix((mwSize)nbins,(mwSize)1,mxREAL);
 plhs[1] = mxCreateDoubleMatrix((mwSize)nbins,(mwSize)1,mxREAL);
 plhs[2] = mxCreateDoubleMatrix((mwSize)nbins,(mwSize)1,mxREAL);
 plhs[3] = mxCreateDoubleMatrix((mwSize)nbins,(mwSize)1,mxREAL);
 plhs[4] = mxCreateDoubleMatrix((mwSize)nbins,(mwSize)1,mxREAL);
 rad     = mxGetPr(plhs[0]);
 ncorr   = mxGetPr(plhs[1]);
 nlt     = mxGetPr(plhs[2]);
 corr    = mxGetPr(plhs[3]);
 J3      = mxGetPr(plhs[4]);

 /* Pre-allocate local storage */
 ll = (int *)malloc(nobj*sizeof(int));
 ihc = (int *)malloc(L*L*L*sizeof(int));
 if( (ll==NULL) || (ihc==NULL) ) {
  mexErrMsgTxt("pscorr: Unable to allocate memory for temporary storage");
 }

 /* Do the actual computations in a subroutine */
 pscorr_(&nobj, r, &L, &nbins, rllw, rad, ncorr, nlt, corr, J3, ll, ihc);

 free(ll);
 free(ihc);

}
