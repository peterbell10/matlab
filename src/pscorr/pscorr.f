      SUBROUTINE pscorr(nobj,r,Ls,nbin,rllw, rs,ncorr,nlt,corr,rJ3,
     &           ll,ihc)
C Routine to find the 2-point correlation function for the particles in r
      IMPLICIT NONE
c Arguments input, not modified
      integer nobj,Ls,nbin
      real*8 r(3,nobj),rllw
c Arguments output, modified
c ncorr and ntl should be integers, but matlab likes real*8's
      real*8 rs(nbin),ncorr(nbin),nlt(nbin),corr(nbin),rJ3(nbin)
c Pre-allocated local storage
      integer ll(nobj),ihc(Ls*Ls*Ls)

c Local Variables
      integer i
      real*8 rca,rcb
      real*8 rldiv,cav,vSphereLower,vSphereUpper,VolShell
      
      real*8 pi
      parameter(pi=3.14159265358979323846d0)

c Functions
c      real*8 vol

      rldiv=dble(nbin-1)/(-1.d0*log10(dble(Ls))-rllw)

      do i=1,nbin
       rs(i)=10.0d0**(dble(i-1)/rldiv+rllw)
      enddo
      
      rca=dble(Ls)
      rcb=1.d0
c    List the particles into the mesh boxes     
      call list(nobj,r,ll,Ls,ihc,rca,rcb)
c    Find the short-range correlation function?
      call shcorr(nobj,r,ll,Ls,ihc,nbin,rllw,rldiv,ncorr)

c    The number of pair separations
      cav=0.5d0*dble(nobj)*(dble(nobj-1))
c      cav=dble(nobj)*(dble(nobj-1))

C vol() is a stunningly complicated function that does nothing more AFAICT then
C what I've put inline.

      nlt(1)=ncorr(nbin)
c      vshells=vol(rs(1),1.0d0)
      vSphereLower=4.0d0/3.0d0*pi*rs(1)*rs(1)*rs(1)
      do i=1,nbin-1
c       vSphereUpper=vol(rs(i+1),1.0d0)
       vSphereUpper=4.0d0/3.0d0*pi*rs(i+1)*rs(i+1)*rs(i+1)
       VolShell=vSphereUpper-vSphereLower
       corr(i)=0.0d0
       if(VolShell.gt.0.0d0) then
        corr(i)=ncorr(i)/cav/VolShell-1.0d0
       endif
       rJ3(i)=nlt(i)/cav/vSphereLower-1.0d0
       nlt(i+1)=nlt(i)+ncorr(i)
       vSphereLower=vSphereUpper
      enddo

c rs is the radius (lower end of bin?)
c ncorr is the number in the bin
c nlt is the cumulative number in the bins
c corr is the correlation
c rJ3 is the J3 which is cumulative number / volume
      RETURN
      END
