% pscorr: the 2-point correlation function for the particles in r
%
% Syntax: [rad,ncorr,nlt,corr,J3]=pscorr(r,L,nbins,rllw)
%
% ARGUMENTS
%  r is a collection of particle positions (3,N) double [0,1)
%  L is the mesh size which sets the upper limit on the radii sampled:
%    max(rad)=1/L (less a bin width)
%  nbins is the number of bins
%  rllw is the log of the minimum radius over which to sample (must be negative)
%
% RETURNS
%  rad         Radius bin edges (lower end of bin?)
%  ncorr       Number in the bin
%              (last element contains the # of particles < 10^rllw)
%  nlt         Cumulative number in the bins
%  corr        Correlation (last element contains the # of particles < 10^rllw)
%  J3          Cumulative number / volume
%
%  Note: no checks are done to make sure a valid range is given for L and rllw.
%  rllw < log10(1/L) should be assumed.
%
% Execution time is governed primarily by the number of objects and L.
%  t_exec propto Nobj * (1/L)^3
%
% Repeating Boundary Conditions are NOT assumed.  This will lead to a loss of
% correlation at the larger radius.
%
% SEE ALSO
%
% RBC_CorrelationCorrectionFactor
