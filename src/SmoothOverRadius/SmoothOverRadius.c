/* function smooth=smooth_kernel(Image,level) */

/* HISTORY
   01-05-31 Mature code
   01-05-31
    Implemented kernel smoothing.
    Increased speed by having distance found by mostly integer operations.
    Fix the correction factor.
*/
   

#include <math.h>
#include <stdio.h> /* only for debugging messages */
#include <mex.h>

int SmoothOverRadius(const double *, const int, const int, const double*,
                     double *); 
double sum_over_radius(const double *, const int, const int, const int, const int,
                       double);
double kernel_2d__(double*);

/* ************************************************** */
void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

/* mxArray *smooth,*Image; */
 double *smooth,*Image;
 double *radius;
 size_t mrows,ncols;
 size_t mrows_dum,ncols_dum;
 int status;

/* Check for the proper number of arguments */
 if( (nrhs!=2) || (nlhs!=1) ) {
  mexErrMsgTxt("Syntax: smooth=SmoothOverRadius(Image,radius)");
 }

/* The inputs must be of the type double */
 if( !mxIsDouble(prhs[0]) || !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("Image and radius must be double");
 }

/* The first and third arguments must be the same size */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 mrows_dum = mxGetM(prhs[1]);
 ncols_dum = mxGetN(prhs[1]);
 if(mrows != mrows_dum || ncols != ncols_dum) {
  mexErrMsgTxt("Image and radius must be the same size");
 }

/* Create matrix for the return argument and assign a pointer */
 plhs[0]=mxCreateDoubleMatrix(mrows,ncols, mxREAL);
 smooth = mxGetPr(plhs[0]);

/* Assign pointers to the input */
 Image  = mxGetPr(prhs[0]);
 radius = mxGetPr(prhs[1]);

 /* Call the subroutine */
 status = SmoothOverRadius(Image,(int)mrows,(int)ncols,radius,smooth);
}

/* ************************************************** */

int SmoothOverRadius(
 const double *Image, /* Image is 2-d data stored column after column */
 const int mrows,
 const int ncols,
 const double *radius,  /* The radii for each pixel */
 double *smooth /* empty matrix size of Image */
) {
 
 int ix,iy;
 double rad;

 for(ix=0; ix<ncols; ix++) {
  for(iy=0; iy<mrows; iy++) {
   rad=radius[iy+mrows*ix];
   if(rad>1.0) {
    smooth[iy + mrows*ix]=sum_over_radius(Image,ncols,mrows,ix,iy,rad);
/*
    sum=sum_over_radius(Image,ncols,mrows,ix,iy,rad);
    smooth[iy + mrows*ix]=sum/(M_PI*rad*rad);
*/
   }
   else {
    smooth[iy + mrows*ix]=Image[iy + mrows*ix];
   }
  } /* end for iy */
 } /* end for ix */
 return(0);
}

/* ************************************************** */

double sum_over_radius(
 const double *Image,        /* The input array */
 const int ncols, const int mrows, /* The size of the array */
 const int ix, const int iy,       /* The point in the array */
 const double radius         /* The search radius */
) {
 double sum,dist;
#ifdef KERNEL
 double x;
#endif
 int jx, jy;
 double outside, npixels, area;
 int integer_radius;
 
#ifdef KERNEL
 integer_radius=2*(int)ceil(radius);
#else
 integer_radius=(int)ceil(radius);
#endif
 
 sum=0.;

 npixels=0.0;
 outside=0.0;
 for(jx=ix-integer_radius; jx<ix+integer_radius; jx++) {
  for(jy=iy-integer_radius; jy<iy+integer_radius; jy++) {
/*   dist=sqrt( pow((double)(jx-ix),2.) + pow((double)(jy-iy),2.) ); */
/* The following does the previous much faster ( total time > 3x faster) */
   dist=sqrt( (double)( (jx-ix)*(jx-ix) + (jy-iy)*(jy-iy) ) );
#ifdef KERNEL
   if(dist<=2.0*radius) {
    x=dist/(2.0*radius);
    npixels+=kernel_2d__(&x);
    if( (jx<0) || (jx>=ncols) || (jy<0) || (jy>=mrows) ) {
     outside+=kernel_2d__(&x);
    } else {
     sum=sum+Image[jy + mrows*jx]*kernel_2d__(&x);
    }
   }
#else
   if(dist<=radius) {
    npixels++;
    if( (jx<0) || (jx>=ncols) || (jy<0) || (jy>=mrows) ) {
     outside++;
    } else {
     sum=sum+Image[jy + mrows*jx];
    }
   }
#endif
  }
 }
 area=npixels-outside;
 if(area<=0.0) {
  sum=0.0;
  area=0.01;
 }
 return(sum/area);
}
