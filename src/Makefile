include makeflags

DIRS = anisotropy calcdens calcdens_d deproj fft3 fit_gaussian fof \
       gaussian_fit_2d h_proj h_proj_3d  ifft3 mekal numdensity  \
       numdensity_smooth powerspec pscorr \
       rho_grid skid smoothkernel SmoothOverRadius v_disp wabs \
       fake_Lya_spec trans_insitu DistributedGridDimensions SPHrayTrace 

# The "all: all-matlab all-octave" dependency is not compatible with paralle builds (make -j n).
# Either all-matlab or all-octave can be built in parallel, but not both.
# This form enforces that, while allowing parallel builds.
all:
	$(MAKE) all-matlab
	$(MAKE) all-octave

all-matlab: lib/libcfitsio.a fftw/lib/libfftw.a $(MATLAB_INSTALL_DIR)
	for i in $(DIRS); do (cd $$i && $(MAKE) clean ); done
	for i in $(DIRS); do (cd $$i && $(MAKE) INCS='$(INCS_MEX)' DEFS='-DMEX' matlab ); done
	for i in $(DIRS); do (cd $$i && $(MAKE) install-matlab ); done

all-octave: lib/libcfitsio.a fftw/lib/libfftw.a $(OCTAVE_INSTALL_DIR)
	for i in $(DIRS); do (cd $$i && $(MAKE) clean ); done
	for i in $(DIRS); do (cd $$i && $(MAKE) INCS='$(INCS_OCT)' DEFS='-DOCTAVE' octave ); done
	for i in $(DIRS); do (cd $$i && $(MAKE) install-octave ); done

lib:
	mkdir lib

lib/libcfitsio.a: lib libcfitsio/cfitsio_latest.tar.gz
	cd libcfitsio; export CC="$(CC)"; export CFLAGS="$(CFLAGS)"; ./INSTALL _latest

libcfitsio/cfitsio_latest.tar.gz:
	cd libcfitsio; wget http://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio_latest.tar.gz

fftw/lib/libfftw.a:
	cd fftw; export CC="$(CC)"; export CFLAGS="$(CFLAGS)"; ./INSTALL 3.3.3

install: all

clean:
	for i in $(DIRS); do (cd $$i && $(MAKE) clean ); done
	cd libcfitsio; rm -fr `find . -maxdepth 1 -name "cfitsio*" -type d`
	cd fftw;  rm -fr fftw-3.3.3

distclean: clean
	-rm Make.log
	-rm Install.log
	-rm lib/libcfitsio.a
	-rm -fr fftw/include fftw/lib fftw/info fftw/bin fftw/share
	-rm -fr fftw/fftw-3.3.3

$(MATLAB_INSTALL_DIR):
	mkdir $(MATLAB_INSTALL_DIR)

$(OCTAVE_INSTALL_DIR):
	mkdir $(OCTAVE_INSTALL_DIR)
