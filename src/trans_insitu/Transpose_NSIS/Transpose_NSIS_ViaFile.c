#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Calculate the position in an [MxN] array that the element in position index
 * in an [NxM] array shoule be tranposed to. */
unsigned int SwapTo(const unsigned int index, const unsigned int N,
                    const unsigned int M) {
 unsigned int i,j,swapto;
 j = index % N;
 i = (index - j)/N;
 swapto = j*M + i;
 return swapto;
}

void Transpose_NSIS_ViaFile_d(double * A, const int N, const int M) {
 const unsigned int Nelem = N*M;
 unsigned int i, j;
 double swpbuf;
 char tempfilename[] = "/Transpose_NSIS_ViaFile-XXXXXX";
 char *tempfile;
 double * const A_start = A;
 int fid;
 size_t nbytes;
 ssize_t bytes;
 off_t offset;
 
 /* ** Open the file to which we will write** */
 /* Allocate memory for the filename */
 tempfile = malloc(sizeof(char)*FILENAME_MAX);
 /* Get the current working directory */
 tempfile = getcwd(tempfile,FILENAME_MAX);
 if( tempfile == NULL ) {
  printf("ERROR: Transpose_NSIS_ViaFile: %i: Unable to get current directory\n",
         __LINE__-3);
  exit(-1);
 }
 /* Concatenate the temporary file to the end */
 tempfile = strncat(tempfile,tempfilename,FILENAME_MAX);
 /* Create the temporary file */
 fid=mkstemp(tempfile);
 if(fid==-1) {
  printf("ERROR: Transpose_NSIS_ViaFile: %i: Unable to create temp file %s\n",
         __LINE__-3,tempfile);
  exit(-1);
 }
 
 /* Unlink the file immediately so that it will be deleted when fid is closed */
 unlink(tempfile);

 /* Write the array to the file */
 nbytes=sizeof(double)*(size_t)Nelem;
 while(nbytes>0) {
  bytes   = write(fid, A, nbytes);
  nbytes -= bytes;
  A      += bytes/sizeof(double);
 }
 A = A_start; /* Reset to the beginning */

 /* Rewind the file */
 offset = lseek(fid, 0, SEEK_SET);

 /* Now do the transpose */
 for(i=0;i<Nelem;i++) {
  /* Read in one element */
  bytes=read(fid,&swpbuf,sizeof(double));
  if(bytes!=sizeof(double)) {
   printf("ERROR: Transpose_NSIS_ViaFile: %i: Unable to read from temp file %s\n",
          __LINE__-3,tempfile);
   exit(-1);
  }
  /* Get the index of the object that should be in this cell */
  j=SwapTo(i,N,M);
  A[j]=swpbuf;
 } /* End of main loop over all cells */

 /* Close the temp file.  Closing automatically deletes it. */
 close(fid);
 /* Free the memory */
 free(tempfile);

}

/* Since transposing insitu in memory has terrible scaling (see Timings)
 * for large arrays, use a temporary file. */
void Transpose_NSIS_ViaFile_f(float * A, const int N, const int M) {
 const int Nelem = N*M;
 unsigned int i, j;
 float swpbuf;
 char tempfile[] = "Transpose_NSIS_ViaFile-XXXXXX";
 int fid;
 size_t nbytes;
 ssize_t bytes;
 off_t offset;
 
 /* Open the file to which we will write */
 fid=mkstemp(tempfile);
 if(fid==-1) {
  printf("ERROR: Transpose_NSIS_ViaFile: %i: Unable to create temp file %s\n",
         __LINE__-3,tempfile);
  exit(-1);
 }
 
 /* Unlink the file immediately so that it will be deleted when fid is closed */
 unlink(tempfile);

 /* Write the array to the file */
 nbytes=sizeof(float)*N*M;
 bytes = write(fid, A, nbytes);
 if(nbytes != bytes) {
 #if __SIZEOF_SIZE_T__ == 4
  printf("ERROR: Transpose_NSIS_ViaFile: %i: Only wrote %i of %i bytes to temp file %s\n",
         __LINE__-4,bytes,nbytes,tempfile);
 #else
  printf("ERROR: Transpose_NSIS_ViaFile: %i: Only wrote %li of %li bytes to temp file %s\n",
         __LINE__-7,(long int)bytes,(long int)nbytes,tempfile);
 #endif
  exit(-1);
 }

 /* Rewind the file */
 offset = lseek(fid, 0, SEEK_SET);
 
 /* Now do the transpose */
 for(i=0;i<Nelem;i++) {
  /* Read in one element */
  bytes=read(fid,&swpbuf,sizeof(float));
  if(bytes!=sizeof(float)) {
   printf("ERROR: Transpose_NSIS_ViaFile: %i: Unable to read from temp file %s\n",
          __LINE__-3,tempfile);
   exit(-1);
  }
  /* Get the index of the object that should be in this cell */
  j=SwapTo(i,N,M);
  A[j]=swpbuf;
 } /* End of main loop over all cells */

 /* Close the temp file.  Closing automatically deletes it. */
 close(fid);

}
