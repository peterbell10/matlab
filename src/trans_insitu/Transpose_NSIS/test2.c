#include <stdlib.h>
#include <stdio.h>

#include "Transpose_NSIS.h"

int main(void) {
 const unsigned int N=2000;
 double A[3*N];
 unsigned int i, j;
 
 for(i=0;i<N;i++) {
  for(j=0;j<3;j++) {
   A[i*3+j] = (double)rand()/((double)RAND_MAX);
  }
 }
 
 /*
 printf("Before: ");
 for(i=0;i<N;i++) {
  printf("%2.0f ",A[i]);
 }
 printf("\n");
 */
 
 Transpose_NSIS_d(A, 3, N);
 
 /*
 printf("After : ");
 for(i=0;i<N;i++) {
  printf("%2.0f ",A[i]);
 }
 printf("\n");
 printf("Expect: ");
 for(i=0;i<N;i++) {
  printf("%2.0f ",B[i]);
 }
 printf("\n");
 */
 
 return EXIT_SUCCESS;
}
