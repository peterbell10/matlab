#include <stdlib.h>
#include <stdio.h>

#include "TNSIS_LinkList.h"
#include "Transpose_NSIS.h"

/* Calculate the position in an [NxM] array of the element that should be
 * in the index'th position in the transposed [MxN] array. */
unsigned int SwapWith(const unsigned int index, const unsigned int N,
                      const unsigned int M) {
 unsigned int i,j,swapwith;
 j = index % M;
 i = (index - j)/M;
 swapwith = j*N + i;
 return swapwith;
}

/* Search the list for a == j.  If found, return the node.  Else return NULL */
LL_node * LL_Search_a(unsigned int j, LL_node * node) {
 while ( node != NULL ) {
  if (node->a == j) {
   break;
  }
  node = node->next;
 }
 return node;
}

/* Search the list for b == j.  If found, return the node.  Else return NULL */
LL_node * LL_Search_b(unsigned int j, LL_node * node) {
 while ( node != NULL ) {
  if (node->b == j) break;
  node = node->next;
 }
 return node;
}

LL_node * CheckIfMoved( unsigned int *j, LL_node * node) {
 LL_node * const recall_node = node;
 if( node != NULL ) {
  /* Move to the first node */
  node = LL_Start(node);
  /* Search for a pairing with j in the list */
  node = LL_Search_a(*j, node);
  if(node != NULL) {
   /* Found a match, so get the new destination then delete the node. */
   *j = node->b;
   node = LL_Remove(node);
  } else {
   /* Return where we started from */
   node = recall_node;
  }
 }
 return node;
}

/* Run through a list, checking for pairings: (4,2),(2,8) => (4,8) */
LL_node * LL_Compact(LL_node * node) {
 LL_node * pair;
 LL_node * start;
 node = LL_Start(node);
 start = node;
 while(node != NULL) {
  pair = LL_Search_a(node->b,start);
  if( pair != NULL ) {
   node->b = pair->b;
   LL_Remove(pair);
   /* Restart the search */
   start = LL_Start(node);
   /* break; */
  }
  node = node->next;
 }
 return start;
}

void Transpose_NSIS_d(double * A, const int N, const int M) {
 const int Nelem = N*M;
 unsigned int i, j;
 double swpbuf;
 /* A pointer to the list.
  * The list can always be accessed by any node in the list, so list is just
  * a pointer to a node in the list, currently empty */
 LL_node * list = NULL;
 for(i=0;i<Nelem;i++) {
  /* Get the index of the object that should be in this cell */
  j=SwapWith(i,N,M);
  /* Check to see if that object has moved. j will be updated if so. */
  list=CheckIfMoved(&j,list);
  if( j != i ) {
   /* Swap values */
   swpbuf=A[i];
   A[i]=A[j];
   A[j]=swpbuf;
   /* Add pair to list */
   list = LL_Insert(list, i, j);
   /* Compact list */
   list = LL_Compact(list);
  } /* End of if I need to swap */
  /* LL_Print(LL_Start(list)); printf("\n"); */
 } /* End of main loop over all cells */
}

void Transpose_NSIS_f(float * A, const int N, const int M) {
 const int Nelem = N*M;
 unsigned int i, j;
 float swpbuf;
 /* A pointer to the list.
  * The list can always be accessed by any node in the list, so list is just
  * a pointer to a node in the list, currently empty */
 LL_node * list = NULL;
 for(i=0;i<Nelem;i++) {
  /* Get the index of the object that should be in this cell */
  j=SwapWith(i,N,M);
  /* Check to see if that object has moved. j will be updated if so. */
  list=CheckIfMoved(&j,list);
  if( j != i ) {
   /* Swap values */
   swpbuf=A[i];
   A[i]=A[j];
   A[j]=swpbuf;
   /* Add pair to list */
   list = LL_Insert(list, i, j);
   /* Compact list */
   list = LL_Compact(list);
  } /* End of if I need to swap */
  /* LL_Print(LL_Start(list)); printf("\n"); */
 } /* End of main loop over all cells */
}
