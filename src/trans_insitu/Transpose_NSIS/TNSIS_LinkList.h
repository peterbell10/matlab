#ifndef _TNSIS_LINK_LIST_H_
#define _TNSIS_LINK_LIST_H_

typedef struct LL_node_struct {
 struct LL_node_struct * prev;
 struct LL_node_struct * next;
 unsigned int a;
 unsigned int b;
} LL_node;

LL_node * LL_Insert( LL_node * const node, const int a, const int b );
LL_node * LL_Append( LL_node * const node, const int a, const int b );
LL_node * LL_Prepend( LL_node * node, const int a, const int b );
LL_node * LL_Remove( LL_node * node );

LL_node * LL_Start( LL_node * const node );
LL_node * LL_End( LL_node * const node );
void LL_Print( LL_node * node );

#endif
