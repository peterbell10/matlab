anisotropy	Calculate anisotropy vectors for a set of particles.
calcdens	Calculate the SPH smoothing length & density for a particle set.
calcdens_d	Same as calcdens, but takes doubles.
deproj		Tomographic reconstruction of halo centres.
fft3		3-D FFT. Powered by FFTW.
fit_gaussian	Fit a 2-d gaussian (taylored for stellar images).
fof		A group finder for N-body simulations (FOF v1.1).
gaussian_fit_2d	Fit a 2-d gaussian.
h_proj		Projects particles with smoothing lengths onto a 2-d plane.
h_proj_3d	Grids particles with smoothing legths onto a 3-d grid.
ifft3		Inverse 3-D FFT. Powered by FFTW.
mekal		The flux from a hot plasma using the mekal code.
numdensity	2-D number density for a set of points in a plane.
numdensity_smooth Local means of a value for positions distributed in a plane.
powerspec	Calculates the power spectrum of a particle distribution.
pscorr		Finds the 2-point correlation function for a set of particles.
pspcpsf		The ROSAT PSPC PSF.
pspcpsf_map	Create a ROSAT image. At a given energy and using an input
		intensity map, Imap, convolve on a pixel-by-pixel basis the
		intensity by the appropriate PSF for the off-axis angle and
		energy.
pspcpsf_map2	Same as pspcpsf_map, but takes multiple energies.
rho_grid	Grids particles with masses onto a grid.
skid		A group finder for N-body simulations. (SKID v1.4.1)
smoothkernel	Smooth an image over and adaptive radius.
SmoothOverRadius Smooth an image over local radii supplied.
v_disp		Calculates the velocity dispersions for particles within their
		smoothing radii.
wabs		The x-ray transmission function due to a column of gas.


TODO:
apec
fscanbin

PREREQUISITES:

If Matlab is not installed fintrf.h should be installed in a system directory,
like /usr/include/octave/
It provides macro definitions for the types for pointers and array sizes, which
differ between 32bit and 64bit machines.

INSTALLATION:

The following will build and install into $HOME/matlab/matlab/mex/
and $HOME/matlab/octave/oct/ , depending on the availability of Matlab or
Octave.

make >& Make.log  # Builds and installs

To build and install just the Matlab parts:
 make all-matlab >& Make.log

To install just the Octave parts:
 make all-octave >& Make.log

After building and installing, clean up:
 make distclean >& /dev/null
