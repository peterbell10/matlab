#include <mex.h>

#include <stdio.h>


/* There is a problem with mexErrMsgTxt and some glibc */
#define mexErrMsgTxt(string) printf("%s\n",string); exit(-1);

void fmekal_(float *E1, float *E2, float *flux, int *Nbin, float *CEm,
             float *H, float *T, float *Abund, float *ed, int *Ierr);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 mwSize binsM,binsN, CEmM,CEmN, HM,HN, TM,TN, AbundM,AbundN;
 
 int Ierr;
 int i;
 int NFlux;

 /* Temporary local variables */
 double *bins_dbl;
 double *CEm_dbl;
 double *H_dbl;
 double *T_dbl;
 double *Abund_dbl;
 double *flux_dbl;
 double *ed_dbl;

 float CEm;
 float H;
 float T;
 float Abund[15];
 float *E1;
 float *E2;
 float *flux;
 float ed;

 /* Check for the proper number of arguments */
 if( nlhs!=2 || nrhs!=5 ) {
  mexErrMsgTxt("Syntax: [flux,ed]=mekal(bins,CEm,H,T,Abund)");
 }

 /* Get the sizes of the input arguments */
 binsM  = mxGetM(prhs[0]);
 binsN  = mxGetN(prhs[0]);
 CEmM   = mxGetM(prhs[1]);
 CEmN   = mxGetN(prhs[1]);
 HM     = mxGetM(prhs[2]);
 HN     = mxGetN(prhs[2]);
 TM     = mxGetM(prhs[3]);
 TN     = mxGetN(prhs[3]);
 AbundM = mxGetM(prhs[4]);
 AbundN = mxGetN(prhs[4]);

 /* Check sizes */
 if( (binsM != 1) && (binsN != 1) ) {
  mexErrMsgTxt("bins must be a vector");
 }
 if(binsM > binsN) {
  binsN=binsM;
 }
 if( (CEmM != 1) || (CEmN != 1) ) {
  mexErrMsgTxt("CEm must be a scalar");
 }
 if( (HM != 1) || (HN != 1) ) {
  mexErrMsgTxt("H must be a scalar");
 }
 if( (TM != 1) || (TN != 1) ) {
  mexErrMsgTxt("T must be a scalar");
 }
 if( (AbundM != 1) && (AbundN != 1) ) {
  mexErrMsgTxt("Abund must be a vector with 15 elements");
 }
 if(AbundM > AbundN) {
  AbundN=AbundM;
 }
 if(AbundN != 15) {
  mexErrMsgTxt("Abund must be a vector with 15 elements");
 }

 /* Assign pointers to the input */
 bins_dbl = mxGetPr(prhs[0]);
 CEm_dbl  = mxGetPr(prhs[1]);
 H_dbl    = mxGetPr(prhs[2]);
 T_dbl    = mxGetPr(prhs[3]);
 Abund_dbl= mxGetPr(prhs[4]);

 /* Storage for the real*4 input and output arrays */
 E1 = (float *)malloc(binsN*sizeof(float));
 if(E1==NULL) {
  printf("ERROR: mekal: Unable to allocate memory for E1.\n");
  return;
 }
 E2 = (float *)malloc(binsN*sizeof(float));
 if(E2==NULL) {
  printf("ERROR: mekal: Unable to allocate memory for E2.\n");
  return;
 }
 flux = (float *)malloc(binsN*sizeof(float));
 if(flux==NULL) {
  printf("ERROR: mekal: Unable to allocate memory for flux.\n");
  return;
 }

 /* Copt input arguments to local real*4 arrays and variables. */
 if(binsN >= 2) {
  for(i=0;i<(binsN-1);i++) {
   E1[i]=(float)bins_dbl[i];
   E2[i]=(float)bins_dbl[i+1];
  }
 } else {
  mexErrMsgTxt("Must be more than 2 values in vector, bins");
 }
 
 for(i=0;i<15;i++) {
  Abund[i]=(float)Abund_dbl[i];
 }
 
 CEm=(float)*CEm_dbl;
 H=(float)*H_dbl;
 T=(float)*T_dbl;

 /* The actual computation */
 NFlux=(int)binsN-1;
 fmekal_(E1,E2,flux,&NFlux,&CEm,&H,&T,Abund,&ed,&Ierr);

 free(E1);
 free(E2);

 /* Copy to double precision output arrays */
 /*  Flux */
 plhs[0]=mxCreateDoubleMatrix(NFlux, 1, mxREAL);
 flux_dbl = mxGetPr(plhs[0]);
 for(i=0;i<NFlux;i++) {
  flux_dbl[i]=(double)flux[i];
 }
 free(flux);

 /*  Electron density */
 plhs[1]=mxCreateDoubleMatrix(1,1, mxREAL);
 ed_dbl = mxGetPr(plhs[1]);
 *ed_dbl = (double)ed;

}
