function [X,Y,Z,tolerance]=deproj(X1,Y1,X2,Z2,Y3,Z3,max_tol)
%
% syntax: [X,Y,Z,tolerance]=deproj(X1,Y1,X2,Z2,Y3,Z3,max_tol)
%
% Using the three sets of peak positions (X1,Y1), (X2,Z2), (Y3,Z3)
% Returns the deprojected positions [X,Y,Z] and the tolerances for each
% triplet (the tolerance is how many pixels away it was from being a perfect
% match).  The input max_tol sets, well, the maximum tolerance.
%
% This is a mex function

%count=0;
%X=[]; Y=X; Z=X;
%
%for tol=0:max_tol
% disp(['In deproj: searching over a radius of ',num2str(tol)])
% for i=1:length(X1);
%  Xtemp=X1(i);
%  Ytemp=Y1(i);
%  % Find all the Z's from the XZ plane that match the X from the XY plane
%  Z1temp=Z2(Xtemp>=(X2-tol) & Xtemp<=(X2+tol));
%  if(~isempty(Z1temp))
%   for k=1:length(Z1temp)
%    % Find all the Z's that match the above test and have the Y from
%    % the YZ plane match the Y from the XY plane
%    Z2temp=Z3(Z1temp(k)>=(Z3-tol) & Z1temp(k)<=(Z3+tol) & Ytemp>=(Y3-tol) & Ytemp<=(Y3+tol) );
%    for j=1:length(Z2temp)
%     if(isempty(X))
%      % we should only get here once
%      count=count+1;
%      X(count)=Xtemp;
%      Y(count)=Ytemp;
%      Z(count)=Z2temp(j);
%      tolerance(count)=tol;
%     else
%      % check to see if we haven't already found this match.
%      if(sum(X>=(Xtemp-tol) & X<=(Xtemp+tol) & Y>=(Ytemp-tol) & Y<=(Ytemp+tol) & Z>=(Z2temp(j)-tol) & Z<=(Z2temp(j)+tol) )==0)
%       count=count+1;
%       X(count)=Xtemp;
%       Y(count)=Ytemp;
%       Z(count)=Z2temp(j);
%       tolerance(count)=tol;
%      end %if unique
%     end %if we've found something already
%    end %j
%   end %k
%  end %if Z1 is not empty
% end %i
%end %tol
