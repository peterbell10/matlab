/* deproj: Tomographically deproject 3 images to find peaks.
 *
 * ARGUMENTS
 *  Input, not modified
 *   X1
 *   Y1
 *   X2
 *   Z2
 *   Y3
 *   Z3
 *   max_tol
 *   length
 *
 *  Output, modified
 *   X
 *   Y
 *   Z
 *   tolerance
 *   c
 */

#include "deproj.h"

#include <stdlib.h>

#include <mex.h>

void deproj(
 /* Ouput */
 int *X, int *Y, int *Z, int *tolerance, int *c,
 /* Input */
 int *X1, int *Y1, int *X2, int *Z2, int *Y3, int *Z3, int max_tol,
 const int length1, const int length2, const int length3 
)
{
 int i,j,k,m,Z1c,Z2c,tol;
 int found_flag;
 int Xtemp,Ytemp;
 int count;
 /* These will be malloc'ed */
 int *Z1temp, *Z2temp, *X1temp, *Y2temp;
 
 Z1temp = (int *) malloc( length2 *sizeof(int));
 if(Z1temp==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Z2temp = (int *) malloc( length3 *sizeof(int));
 if(Z2temp==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 X1temp = (int *) malloc( length2 *sizeof(int));
 if(X1temp==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 Y2temp = (int *) malloc( length3 *sizeof(int));
 if(Y2temp==NULL) {
  mexErrMsgTxt("deproj: Unable to allocate sufficient memory");
 }
 
 count=-1;
 for(tol=0;tol<=max_tol;tol++) {
  for(i=0;i<length1;i++) {
   Xtemp=X1[i];
   Ytemp=Y1[i];
   /* Find all the Z's from the XZ plane that match the X from the XY plane */
   Z1c=-1;
   for(j=0;j<length2;j++) {
    if((Xtemp>=X2[j]-tol) && (Xtemp<=X2[j]+tol)) {
     Z1c++;
     Z1temp[Z1c]=Z2[j];
     X1temp[Z1c]=X2[j];
    }
   }
   for(k=0;k<=Z1c;k++) {
    /* Find all the Z's that match the above test and have the Y from */
    /* the YZ plane match the Y from the XY plane. */
    Z2c=-1;
    for(j=0;j<length3;j++) {
     if(   (Z1temp[k]>=Z3[j]-tol)
        && (Z1temp[k]<=Z3[j]+tol)
        && (Ytemp>=Y3[j]-tol)
        && (Ytemp<=Y3[j]+tol)
        ) {
      Z2c++;
      Z2temp[Z2c]=Z3[j];
      Y2temp[Z2c]=Y3[j];
     }
    }
    for(j=0;j<=Z2c;j++) {
     if(count==-1) {
      /* we should only get here once */
      count++;
      X[count]=(Xtemp+X1temp[k])/2;
      Y[count]=(Ytemp+Y2temp[j])/2;
      Z[count]=(Z2temp[j]+Z1temp[k])/2;
      tolerance[count]=tol;
     } /* end if (case first position) */
     else {
      /* check to see if we haven't already found this match. */
      found_flag=0;
      m=-1;
      while((m<=count) && (found_flag==0)) {
       m++;
       if(   (X[m]>=Xtemp-tol)
          && (X[m]<=Xtemp+tol)
          && (Y[m]>=Ytemp-tol)
          && (Y[m]<=Ytemp+tol)
          && (Z[m]>=Z2temp[j]-tol)
          && (Z[m]<=Z2temp[j]+tol)
         ) {
        found_flag=1;
       }
      }
      if(found_flag==0) { /* We haven't found this one, yet */
       count++;;
       if(count>MAX_CLUSTERS) {
        mexPrintf("N clusters = %i > MAX_CLUSTERS: ",count);
        mexErrMsgTxt("MAX_CLUSTERS exceeded");
       }
       X[count]=Xtemp;
       Y[count]=Ytemp;
       Z[count]=(Z2temp[j]+Z1temp[k])/2;
       tolerance[count]=tol;
      }
     } /* end else (case not first position) */
    } /* end for j over all Z2's */
   } /* end for k over all Z1's */
  } /* end for i over all X1,Y1 positions */
 } /* end for tol over span of tolerances */

 /* Free malloc'ed memory */
 free(Z1temp);
 free(Z2temp);
 free(X1temp);
 free(Y2temp);

 /* Return to the calling function the number of matches found */
 *c = count;

} /* end function deproj() */
