function [h,dn]=calcdens_d(r,Nsph,h_guess)
% calcdens_d:  Calculate the SPH smoothing length and density in a particle set.
%              Input is double.
%
% [h,dn]=calcdens_d(r,Nsph,h_guess)
%
% ARGUMENTS
%  r	The particle positions. ([3xN] double, on the range [0,1) )
%  Nsph	The number of neighbour particles over which to smooth.
%
% RETURNS
%  h	The smoothing radii. (N double)
%  dn	The local density at each particle's position. (N double)
%
% NOTES
%  While the Octave version is parallelised, the matlab version of the code
%  is not.  But Matlab can handle larger datasets.
%  Try the octave version first, then resort to the matlab

% AUTHOR: Eric Tittley
%
% HISTORY
%  070123 First version
