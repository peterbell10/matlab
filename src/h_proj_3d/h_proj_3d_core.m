% syntax: A=h_proj_3d_core(r,z,h,L,N)
%
% Given the particle positions r (scaled to L) and the characteristic
% z, using h the particle characteristic is smoothed into a mesh, A.
% The number of resolved and unresolved particles are returned, as well.
%
% Note, do not usually call this explicitly.  Use h_proj_3d instead.
% Also note, if the mex file is available (for which there is src
% code) it will be used instead.
%
% See: h_proj
