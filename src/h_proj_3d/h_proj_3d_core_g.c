/* Gateway function for h_proj_3d
 *
 * MATLAB sytax: A=h_proj_3d(r,z,h,L,N) */

#include <mex.h>

#include <stdio.h>

#include "h_proj.h"

/* There is a problem with mexErrMsgTxt and some glibc */
#define mexErrMsgTxt(string) printf("%s\n",string); exit(-1);

void mexFunction(
 int nlhs, mxArray *plhs[],
 int nrhs, const mxArray *prhs[]
) {

 double *AP;
 double *LP;
 double *NP;

/* Temporary local variables */
 double *r;
 double *z;
 double *h;
 size_t L,N;
 mwSize mrows,ncols;

 /* Check for the proper number of arguments */
 if( nlhs>1 || nrhs!=5 ) {
  mexErrMsgTxt("syntax: A=h_proj_3d(r,z,h,L,N)");
 }

 /* All arguments must be double */
 if( !mxIsDouble(prhs[0]) ) {
  mexErrMsgTxt("r must be a double.");
 }
 if( !mxIsDouble(prhs[1]) ) {
  mexErrMsgTxt("z must be a double.");
 }
 if( !mxIsDouble(prhs[2]) ) {
  mexErrMsgTxt("h must be a double.");
 }
 if( !mxIsDouble(prhs[3]) ) {
  mexErrMsgTxt("L must be a double.");
 }
 if( !mxIsDouble(prhs[4]) ) {
  mexErrMsgTxt("N must be a double.");
 }

 /* Assign pointers to the input */
 r  = mxGetPr(prhs[0]);
 z  = mxGetPr(prhs[1]);
 h  = mxGetPr(prhs[2]);
 LP = mxGetPr(prhs[3]);
 NP = mxGetPr(prhs[4]);

 /* Convert to integers */
 L = (size_t)(*LP);
 N = (size_t)(*NP);

 /* The L and N must be scalar */
 mrows = mxGetM(prhs[3]);
 ncols = mxGetN(prhs[3]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("L must be scalar");
 }
 mrows = mxGetM(prhs[4]);
 ncols = mxGetN(prhs[4]);
 if(mrows!=1 || ncols !=1) {
  mexErrMsgTxt("N must be scalar");
 }

 /* r must be 3 rows by N columns, N > 10 */
 mrows = mxGetM(prhs[0]);
 ncols = mxGetN(prhs[0]);
 if(mrows!=3 || ncols!=N) {
  mexErrMsgTxt("r must be a 3xN array with N==N");
 }

 /* z must be N columns */
 mrows = mxGetM(prhs[1]);
 ncols = mxGetN(prhs[1]);
 if( (mrows!=1 || ncols!=N) && (mrows!=N || ncols!=1) ) {
  mexErrMsgTxt("z must be a vector of length N");
 }

 /* h must be N columns */
 mrows = mxGetM(prhs[2]);
 ncols = mxGetN(prhs[2]);
 if( (mrows!=1 || ncols!=N) && (mrows!=N || ncols!=1) ) {
  mexErrMsgTxt("h must be a vector of length N");
 }

 /* Create a matrix for return argument.
  *  If unsuccessful in a MEX-file, the MEX-file terminates and returns
  *  control to the MATLAB prompt. */
 plhs[0] = mxCreateDoubleMatrix((mwSize)L*(mwSize)L*(mwSize)L, (mwSize)1, mxREAL);
 AP      = mxGetPr(plhs[0]);

 /* DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE */
 h_proj_3d_core(r,z,h,L,N,AP);

}
