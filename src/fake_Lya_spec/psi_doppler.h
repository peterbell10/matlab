#ifndef _PSI_DOPPLER_H_
#define _PSI_DOPPLER_H_

double psi_doppler(const double dv, const double nu_0, const double T,
                   const double mass);

#endif /* _PSI_DOPPLER_H_ */
