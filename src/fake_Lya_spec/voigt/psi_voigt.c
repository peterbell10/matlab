/* Voigt profile
 *
 * psi = psi_voigt(dv, nu_0, T);
 *
 * ARGUMENTS
 *  dv		Velocity relative to line centre. [km/s]
 *  nu_0	Frequency of the line centre. [Hz]
 *  T		Temperature. [K]
 *
 * RETURNS
 *  The line profile at dv. [Hz^-1]
 *
 * NOTES
 *  integral(psi_voigt, nu=0 to inf [dv = c(nu/nu_0 -1)] = 1
 *
 * AUTHOR
 *  Eric Tittley
 *
 * HISTORY
 *  05 10 21 First version.
 *  05 11 03 Fixed normalisation.
 *
 */

#include <stdio.h>
#include <math.h>

#include "voigt.h"

double psi_voigt(const double dv, const double nu_0, const double T,
                 const double mass) {

 const double c = 2.99792458e5;  	/* Speed of light (km/s) */
 /* Doppler velocity width (inverse)
  * b = sqrt(2.0 * k_B * T / mass * 1.0e-6); */
 const double b = sqrt(2.7613e-29 * T / mass ); /* Doppler width [km/s] */
 /* Doppler frequency width nu_D = nu_0*b/c; */
 const double nu_D = nu_0 * b / 2.99792458e5;
 /* Damping Constant (for Lorentz profile)
  * gamma_L = (8 pi^2 e^2)/( m_e c^3) * nu_0^2 * f_ul 
  * where f_ul = (g_l)/g_u)*f_lu where f_lu = 0.4162 for HI and HeII
  * with (g_l/g_u) = (2/6)
  * where I substitute e^2 with 10^9 e^2 when using MKS units */ 
 const double gamma_L = 1.02962317505849e-22 * pow(nu_0,2);
 
 double a,u,psi;

 /* M_1_PI is 1/pi (math.h) */
 a = gamma_L * 0.5 * M_1_PI / nu_D; /* Ratio of natural to Doppler line width */
 u = dv / b; /* Deviation from line centre in units of the Doppler width */
 psi = voigt_(&a,&u);
 /* Normalise. */
 psi *= M_2_SQRTPI /2 * c / (b * nu_0) ;
 return psi;
}
