      double precision function voigt(a,u)
* Function to compute Voigt function H(a,u). Uses series expansion
* in powers of a from Harris (1948) (ApJ 108:112).
*
* ARGUMENTS
*  a  Ratio of the effective natural line width to the Doppler width
*     (Delta nu_natural) / (Delta nu_Doppler)
*  u  Deviation from the centre of the line in units of the Doppler width.
      IMPLICIT NONE
      real*8 Dawsonf,coeff
      real*8 a,u,u2,y
      real*8 H0,H1,H2,H3,H4
      data coeff /1.12837917d0/
      u2 = u*u
      if(u.lt.0.d0) u = -u
      y = 2.d0*u*Dawsonf(u)
      if(u2.lt.60.d0) then
       H0 = 1./ dexp(u2)
      else
       H0 = 0.d0
      endif
      H1 = coeff*(y - 1.d0)
      H2 = (1.d0 - 2.d0*u2)*H0
      H3 = -coeff*(y*(2.d0*u2 - 3.d0) + 2.d0*(1.d0 - u2))/ 3.d0
      H4 = H0*(3.d0 - 4.d0*u2*(3.d0 - u2))/ 6.d0
      if(dabs(a*a*H3).lt.dabs(H1)) then
       voigt = H0 + a*(H1 + a*(H2 + a*(H3 + a*H4)))
      else
       voigt = 0.5d0*a*coeff/ (a*a + u2)
      endif
      return
      end
