#ifndef _SPECIES_H_
#define _SPECIES_H_

enum psi_Methods { Doppler, Voigt} ;
/* Lya, Lyb, Lyg = Lymann alpha, beta, gamma */
enum Species { HI_Lya, HeI, HeII_Lya, HI_Lyb, HI_Lyg, HeII_Lyb, HeII_Lyg,
               SpeciesTerminator };
/* SpeciesTerminator exists so we can test species is < SpeciesTerminator */

#endif
