% enzo_read_IC_particle_file: The contents of an enzo IC particle file (r or v)
%
% r=enzo_read_IC_particle_file(file)
%
% ARGUMENTS
%  file		The file to read.
%
% RETURNS

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab
%
% HISTORY
%  09 04 15 First version.

function r=enzo_read_IC_particle_file(file)

info=hdf5info(file);
r = hdf5read(file,info.GroupHierarchy.Datasets.Name);
