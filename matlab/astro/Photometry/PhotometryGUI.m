function varargout = PhotometryGUI(varargin)
% PHOTOMETRYGUI Application M-file for PhotometryGUI.fig
%    FIG = PHOTOMETRYGUI launch PhotometryGUI GUI.
%    PHOTOMETRYGUI('callback_name', ...) invoke the named callback.

% Last Modified by GUIDE v2.0 19-Nov-2001 09:42:42

if nargin == 0  % LAUNCH GUI

	fig = openfig(mfilename,'reuse');

	% Use system color scheme for figure:
	set(fig,'Color',get(0,'defaultUicontrolBackgroundColor'));

	% Generate a structure of handles to pass to callbacks, and store it. 
	handles = guihandles(fig);
	guidata(fig, handles);

	if nargout > 0
		varargout{1} = fig;
	end

elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK

	try
		if (nargout)
			[varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
		else
			feval(varargin{:}); % FEVAL switchyard
		end
	catch
		disp(lasterr);
	end

end


%| ABOUT CALLBACKS:
%| GUIDE automatically appends subfunction prototypes to this file, and 
%| sets objects' callback properties to call them through the FEVAL 
%| switchyard above. This comment describes that mechanism.
%|
%| Each callback subfunction declaration has the following form:
%| <SUBFUNCTION_NAME>(H, EVENTDATA, HANDLES, VARARGIN)
%|
%| The subfunction name is composed using the object's Tag and the 
%| callback type separated by '_', e.g. 'slider2_Callback',
%| 'figure1_CloseRequestFcn', 'axis1_ButtondownFcn'.
%|
%| H is the callback object's handle (obtained using GCBO).
%|
%| EVENTDATA is empty, but reserved for future use.
%|
%| HANDLES is a structure containing handles of components in GUI using
%| tags as fieldnames, e.g. handles.figure1, handles.slider2. This
%| structure is created at GUI startup using GUIHANDLES and stored in
%| the figure's application data using GUIDATA. A copy of the structure
%| is passed to each callback.  You can store additional information in
%| this structure at GUI startup, and you can change the structure
%| during callbacks.  Call guidata(h, handles) after changing your
%| copy to replace the stored original so that subsequent callbacks see
%| the updates. Type "help guihandles" and "help guidata" for more
%| information.
%|
%| VARARGIN contains any extra arguments you have passed to the
%| callback. Specify the extra arguments by editing the callback
%| property in the inspector. By default, GUIDE sets the property to:
%| <MFILENAME>('<SUBFUNCTION_NAME>', gcbo, [], guidata(gcbo))
%| Add any extra arguments after the last argument, before the final
%| closing parenthesis.

% --------------------------------------------------------------------
% ---- Select a Star to do photometry ----
function varargout = SelectStar_Callback(h, eventdata, handles, varargin)
%
% HISTORY
%  01-12-11 Bug fix: the routine to find the centre of the star would error
%	out with improperly scaled data.  I had it scaled by 1e5 arbitrarily
%	chosen.  Now I scale the image frame to max at 65536.

axes(handles.axes1);
[x_lo,y_lo,width,height]=getbox(1);
x_lo=floor(x_lo);
y_lo=floor(y_lo);
if(x_lo<1) x_lo=1; end
x_hi=ceil(x_lo+width); if(x_hi>handles.Img.width) x_hi=handles.Img.width; end
if(y_lo<1) y_lo=1; end
y_hi=ceil(y_lo+height); if(y_hi>handles.Img.height) y_hi=handles.Img.height; end

figure(1)
Star=handles.img(y_lo:y_hi,x_lo:x_hi);
imagesc(Star)
% --- Do the photometry ---
% -- By summing up the counts and subtracting the background contribution --
TotalCounts=sum(sum(Star));
BG=handles.BackGround;
[M,N]=size(Star);
NPixels=M*N;
Counts=TotalCounts-BG*NPixels;
set(handles.Counts,'String',num2str(Counts));

%% -- By Fitting a Gaussian --
factor=65536/max(max(handles.img(y_lo:y_hi,x_lo:x_hi)));
[bias,sigma,ampl,x,y]=fit_gaussian(handles.img(y_lo:y_hi,x_lo:x_hi)*factor);
%bias=bias/1e5;
%ampl=ampl/1e5;
%% Subtract the star
%fake=fake_star(size(handles.img(y_lo:y_hi,x_lo:x_hi)),0,sigma,ampl,x,y);
%handles.img(y_lo:y_hi,x_lo:x_hi)=handles.img(y_lo:y_hi,x_lo:x_hi)-fake;
%x=x+x_lo-1;
%y=y+y_lo-1;

handles.NStars=handles.NStars+1;
handles.StarX(handles.NStars)=x+x_lo-1;
handles.StarY(handles.NStars)=y+y_lo-1;
handles.StarCounts(handles.NStars)=Counts;
handles.StarNPix(handles.NStars)=(y_hi-y_lo+1)*(x_hi-x_lo+1);

PlotImage(handles)
guidata(h,handles);


% --------------------------------------------------------------------
% ---- Set the minimum of the CLim scale ----
function varargout = MinScale_Callback(h, eventdata, handles, varargin)
MinScale=get(handles.MinScale,'Value');
tol=(handles.Img.Max-handles.Img.Min)*1e-3;
if(get(handles.MaxScale,'Value')<MinScale+tol)
 set(handles.MaxScale,'Value',MinScale+tol);
end
set(handles.MaxScale,'Min',MinScale+tol);
PlotImage(handles)


% --------------------------------------------------------------------
% ---- Set the maximum of the CLim scale ----
function varargout = MaxScale_Callback(h, eventdata, handles, varargin)
MaxScale=get(handles.MaxScale,'Value');
tol=(handles.Img.Max-handles.Img.Min)*1e-3;
set(handles.MinScale,'Max',MaxScale);
PlotImage(handles)


% --------------------------------------------------------------------
% ---- Zoom in button ----
function varargout = ZoomIn_Callback(h, eventdata, handles, varargin)
centre=ginput(1);
XLim=get(handles.axes1,'XLim');
YLim=get(handles.axes1,'YLim');
width =diff(XLim);
height=diff(YLim);
XLim(1)=centre(1)-width/4;
XLim(2)=centre(1)+width/4;
YLim(1)=centre(2)-height/4;
YLim(2)=centre(2)+height/4;
set(handles.axes1,'XLim',XLim)
set(handles.axes1,'YLim',YLim)


% --------------------------------------------------------------------
% ---- Zoom out button ----
function varargout = ZoomOut_Callback(h, eventdata, handles, varargin)
centre=ginput(1);
XLim=get(handles.axes1,'XLim');
YLim=get(handles.axes1,'YLim');
width =diff(XLim);
height=diff(YLim);
XLim(1)=centre(1)-width;
XLim(2)=centre(1)+width;
YLim(1)=centre(2)-height;
YLim(2)=centre(2)+height;
set(handles.axes1,'XLim',XLim)
set(handles.axes1,'YLim',YLim)


% --------------------------------------------------------------------
% ---- Open button ----
function varargout = FileOpen_Callback(h, eventdata, handles, varargin)
[filename,pathname]=uigetfile('*.fits','File containing FITS image to load');
[handles.hdr, handles.img] = fitsload(fullfile(pathname,filename));

% The Z-axis scaling
handles.Img.Min=0;
handles.Img.Max=max(max(handles.img));
tol=(handles.Img.Max-handles.Img.Min)*1e-3;
set(handles.MinScale,'Value',handles.Img.Min, ...
                     'Min',handles.Img.Min, ...
                     'Max',handles.Img.Max-tol)
set(handles.MaxScale,'Value',handles.Img.Max, ...
                     'Min',handles.Img.Min+tol, ...
                     'Max',handles.Img.Max)
% The current view
handles.Img.width =size(handles.img,2);
handles.Img.height=size(handles.img,1);
XLim=[1 handles.Img.width];
YLim=[1 handles.Img.height];
set(handles.axes1,'XLim',XLim)
set(handles.axes1,'YLim',YLim)
handles.BackGround=str2double(get(handles.BG,'string'));
handles.NStars=0;
PlotImage(handles)
guidata(h,handles);

% ---------------------------------------------------------------------
% ---- Plot the Image ----
function PlotImage(handles)
axes(handles.axes1);
% The current axis scaling
XLim=get(handles.axes1,'XLim');
YLim=get(handles.axes1,'YLim');
% The Z-axis scaling
MinScale=get(handles.MinScale,'Value');
MaxScale=get(handles.MaxScale,'Value');
% Plot the image
imagesc(handles.img,[MinScale,MaxScale])
% Reset the current axis scaling
set(handles.axes1,'XLim',XLim)
set(handles.axes1,'YLim',YLim)
% Set the colour to gray
colormap(gray)
% Don't display the axes
set(handles.axes1,'Visible','Off')

% Plot the stars for which we already have photometry
if(handles.NStars>0)
 hold on
 plot(handles.StarX,handles.StarY,'go')
 hold off
end


% --------------------------------------------------------------------
% ---- Plot a histogram of the image intensities -----
function varargout = Histogram_Callback(h, eventdata, handles, varargin)
%
% HISTORY
%  01-12-11 Histogram was being calculated in Log space.  I'm sure I had a 
%	good reason, but it escapes me know.

figure(1)

%[n,x] = hist(reshape(log10(handles.img),handles.Img.width*handles.Img.height,1),1000);
%loglog(10.^x,n);
[n,x] = hist(reshape(handles.img,handles.Img.width*handles.Img.height,1),[1:65536]);
loglog(x,n);


% --------------------------------------------------------------------
% ---- Calculate the Back Ground ----
function varargout = CalcBG_Callback(h, eventdata, handles, varargin)
%
% HISTORY
%  01-12-11 Iterative approach to calculating the background, plus a fix
%	to the histogram calc which previously worked in log space.
%  01-12-13 Add a cut-off on the maximum number of iterations to perform
%	to calculate the background.

figure(1)
clf
%width=10;
%[n,x] = hist(reshape(log10(handles.img),handles.Img.width*handles.Img.height,1),1000);
%x=10.^x;
%loglog(x,n);
% Iterate the span over which the BG is fit until width=2*sigma
% WARNING: WE ARE ASSUMING THAT i=x(i) in this analysis, which is correct
% so long as bins == 1:65536
[n,x] = hist(reshape(handles.img,handles.Img.width*handles.Img.height,1),[1:65536]);
sigma=5;
width=0;
max_n_at=find(n==max(n));
span=max_n_at+[-2*sigma:2*sigma];
P(1)=sum(n(span).*diff(x([span,span(end)+1]))) /2;
P(2)=sigma;
P(3)=x(max_n_at);
MaxNIters=20;
NIters=0;
while(2*sigma~=width & NIters<MaxNIters)
 NIters=NIters+1;
 width=2*sigma;
 amp_o=P(1);
 sigma_o=P(2);
 wave_o=P(3);
 span=round(wave_o+[-width:width]);
 P=gauss_fit(x(span),n(span),[amp_o, sigma_o, wave_o]);
 sigma=P(2);
end
plot(x(span),n(span))
xlabel('Intensity')
ylabel('Relative frequency')
hold on
n_fit=gauss(P(1),P(2),x(span),P(3));
plot(x(span),n_fit,'r-')
hold off
legend('Data','Fit')
handles.BackGround=P(3);
handles.BackGround_sigma=P(2);
set(handles.BG,'String',num2str(P(3)))
guidata(h,handles);

% -----------------------------------------------------------------------
% ---- Report the present background level, and optionally allow the ----
% ---- user to enter their own level ------------------------------------
function varargout = BG_Callback(h, eventdata, handles, varargin)
BG = str2double(get(h,'string'));
if isnan(BG)
 errordlg('You must enter a numeric value','Bad Input','modal')
end
handles.BackGround=BG;
guidata(h,handles);


% --------------------------------------------------------------------
function varargout = Quit_Callback(h, eventdata, handles, varargin)
delete(handles.figure1)
%pos_size = get(handles.figure1,'Position');
%user_response = modaldlg([pos_size(1)+pos_size(3)/5 pos_size(2)+pos_size(4)/5]);
%switch user_response
%case {'no','cancel'}
%        % take no action
%case 'yes'
%        delete(handles.figure1)
%end


% --------------------------------------------------------------------
% ---- Plot a cross section taken horizontally from the image --------
function varargout = VCross_Callback(h, eventdata, handles, varargin)
axes(handles.axes1);
[y,x]=ginput(1);
x=round(x);
y=round(y);
figure(1)
crossection=handles.img(x+[-20:20],y);
plot(x+[-20:20],crossection);


% --------------------------------------------------------------------
% ---- Plot a cross section taken vertically from the image ----------
function varargout = HCross_Callback(h, eventdata, handles, varargin)
axes(handles.axes1);
[y,x]=ginput(1);
x=round(x);
y=round(y);
figure(1)
crossection=handles.img(x,y+[-20:20]);
plot(y+[-20:20],crossection);


% --------------------------------------------------------------------
% ---- Save the photometry data --------------------------------------
function varargout = Save_Callback(h, eventdata, handles, varargin)
%
% HISTORY
%  01-12-11 Save only the data we need, not handles.

data.BackGround=handles.BackGround;
data.BackGround_sigma=handles.BackGround_sigma;
data.NStars=handles.NStars;
data.StarX=handles.StarX;
data.StarY=handles.StarY;
data.StarCounts=handles.StarCounts;
data.StarNPix=handles.StarNPix;
data.img=handles.img;
data.hdr=handles.hdr;
[filename, pathname] = uiputfile('*.mat', 'Save photometry data as');
eval(['save ',fullfile(pathname,filename),' data'])
